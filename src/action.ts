import * as React from 'react';
import { IconProps, SvgIcon } from '@mui/material';

type SvgIconComponent = typeof SvgIcon;

export type GroupAction<DataType extends object = {}> = Omit<
  Action<DataType>,
  'isActionGroup' | 'actions' | 'tooltip'
> &
  Required<Pick<Action<DataType>, 'tooltip'>>;

export type Action<DataType extends object = {}> = {
  /**
   * Tooltip for button
   */
  tooltip?: string;
  /**
   * Props for icon as IconProps from https://mui.com/api/icon/
   */
  iconProps?: IconProps;
  /**
   * Button hidden flag
   * @default false
   */
  hidden?: boolean;
  /**
   * Button enabled/disabled
   * @default false
   */
  disabled?: boolean;
  /**
   * Explicit positioning for an action
   */
  position?: 'toolbar' | 'toolbarOnSelect' | 'row';
  /**
   * Independent actions that will not on row' actions section
   * @default false
   */
  isFreeAction?: boolean;
} & (
  | {
      /**
       * Type of an action.
       * action - is material-table like action.
       * @default action
       */
      type?: 'action';
      /**
       * Icon of button from material icons or custom component
       */
      icon: string | (() => React.ReactElement<any>) | SvgIconComponent;
      /**
       * This event will be fired when button clicked. Parameters are event, row and selected rows
       * This event is not available, if type of the action is action-group.
       * @param event
       * @param row
       * @param selectedRows
       */
      // eslint-disable-next-line
      onClick: (event: any, row: DataType, selectedRows: Array<DataType>) => void;
      actions?: never;
    }
  | {
      /**
       * group-action - is an action group that pop up as menu on click on this action
       */
      type: 'action-group';
      icon?: string | (() => React.ReactElement<any>) | SvgIconComponent;
      onClick?: never;
      /**
       * A list of actions that should be offered or promised on click on this action.
       * This option is available only for an action-group type action.
       */
      actions:
        | Array<GroupAction<DataType> | ((rowData: DataType) => GroupAction<DataType>)>
        | ((rowData: DataType) => Promise<Array<GroupAction<DataType>>>);
    }
);
