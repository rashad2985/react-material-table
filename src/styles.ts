import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/material/styles';

declare module '@mui/styles/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}

export const useStyles = makeStyles((theme) => ({
  dnd: {
    cursor: 'grabbing',
  },

  search: {
    minWidth: 256,
    [theme.breakpoints.up('md')]: {
      maxWidth: 256,
    },
    [theme.breakpoints.down('xl')]: {
      minWidth: 128,
      flexGrow: 1,
    },
  },
  toolbarOnSelection: {
    backgroundColor: theme.palette.action.selected,
  },

  select: {
    minWidth: 128,
  },

  padding2: {
    padding: theme.spacing(2),
  },
  noWrap: {
    whiteSpace: 'nowrap',
  },

  tablePaginationRoot: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
  },
  tablePaginationToolbar: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
    padding: theme.spacing(1),
  },
  flex: {
    display: 'flex',
  },

  tablePaginationActions: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },

  clickable: {
    cursor: 'pointer',
  },
  stickyLeft: {
    position: 'sticky',
    left: 0,
    borderRight: `1px solid ${theme.palette.divider}`,
    backgroundColor: theme.palette.background.paper,
  },
  borderRight: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
  stickyRight: {
    position: 'sticky',
    right: 0,
    borderLeft: `1px solid ${theme.palette.divider}`,
    backgroundColor: theme.palette.background.paper,
  },

  responsiveActionsContainer: {
    marginLeft: `${theme.spacing(-1.5)} !important`,
  },

  invisible: {
    opacity: 0,
  },

  bordered: {
    border: '1px solid',
  },
}));
