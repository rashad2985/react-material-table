import * as ReactTable from 'react-table';
import { DefaultFilterTypes } from 'react-table';
import { Column } from './column';

export type FilterType = DefaultFilterTypes | 'includesSome' | 'includesValue';

export interface Filter<T extends object = {}> extends Pick<Column<T>, 'field'> {
  type: FilterType;
  builtInFilterType: BuiltInFilterType;
}

export type BuiltInFilterType =
  | 'standard'
  | 'multiple-choice'
  | 'single-choice'
  | 'range'
  | 'greater-than'
  | 'less-than';

export type FilterValueType = string | number | boolean;

export interface FilterValuesPickerProps<T extends object = {}, V = FilterValueType | Array<FilterValueType>> {
  column: ReactTable.ColumnInstance<T> &
    Partial<
      Pick<
        Column<T>,
        | 'type'
        | 'align'
        | 'headerStyle'
        | 'cellStyle'
        | 'render'
        | 'lookup'
        | 'dateSetting'
        | 'currencySetting'
        | 'numberSettings'
        | 'defaultFilter'
        | 'filterPlaceholder'
        | 'filterComponent'
        | 'filterCellStyle'
        | 'field'
        | 'title'
      >
    > & {
      filterValue: V | undefined;
      setFilter: (filterValue: V | undefined) => void;
      preFilteredRows: Array<ReactTable.Row<T>>;
    };
}
