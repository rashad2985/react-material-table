import { Description, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import { Meta, Story } from '@storybook/react/types-6-0';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { ComplexObjectTableEntry, SimpleTableEntry } from './data';

export default {
  title: 'Features/Export',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table has built-in export feature. Currently, it supports only CSV and PDF format." />
          <Subheading>Usage</Subheading>
          <Description markdown="To make export available, `options.exportButton` should be enabled." />
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              
              <ReactMaterialTable
                // other props
                options={{
                  exportButton: true
                }}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Hints</Subheading>
          <Description
            markdown={
              '* You can change export file name by setting `options.exportFileName`.' +
              '\n* You can change CSV delimiter by setting `options.exportDelimiter`.'
            }
          />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const BasicExportExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Basic Export Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      exportButton: true,
    }}
  />
);
BasicExportExample.parameters = {
  docs: {
    description: {
      story: 'To make export available `options.exportButtons` is being set to `true`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ExportDeepObjectValueExample: Story<ReactMaterialTableProps<ComplexObjectTableEntry>> = () => (
  <ReactMaterialTable
    title="Export Deep/Child Object attribute value Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'values.calories', type: 'numeric', value: (rowData) => rowData.values?.calories },
      { title: 'Fat (g)', field: 'values.fat', type: 'numeric', value: (rowData) => rowData.values?.fat },
      { title: 'Carbs (g)', field: 'values.carbs', type: 'numeric', value: (rowData) => rowData.values?.carbs },
      { title: 'Protein (g)', field: 'values.protein', type: 'numeric', value: (rowData) => rowData.values?.protein },
    ]}
    data={[
      {
        name: 'Frozen yoghurt',
        values: {
          calories: 159,
          fat: 6.0,
          carbs: 24,
          protein: 4.0,
        },
      },
      {
        name: 'Ice cream sandwich',
        values: {
          calories: 237,
          fat: 9.0,
          carbs: 37,
          protein: 4.3,
        },
      },
      {
        name: 'Eclair',
        values: {
          calories: 262,
          fat: 16.0,
          carbs: 24,
          protein: 6.0,
        },
      },
      {
        name: 'Cupcake',
        values: {
          calories: 305,
          fat: 3.7,
          carbs: 67,
          protein: 4.3,
        },
      },
      {
        name: 'Gingerbread',
        values: {
          calories: 356,
          fat: 16.0,
          carbs: 49,
          protein: 3.9,
        },
      },
    ]}
    options={{
      exportButton: true,
    }}
  />
);
ExportDeepObjectValueExample.parameters = {
  docs: {
    description: {
      story:
        '`column.value` field is used to access values of object type entries. (It is recommended to pass also a unique `column.field` attribute.)',
    },
    source: {
      type: 'code',
    },
  },
};

export const NonExportFieldExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Non Export Field Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string', export: false },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      exportButton: true,
    }}
  />
);
NonExportFieldExample.parameters = {
  docs: {
    description: {
      story: '`name` field marked as non-export by setting `export` field of specified column as `false`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const OverridingExportFunctionExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Overriding Export Function Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      exportButton: true,
      exportCsv: (columns, data) => {
        alert(JSON.stringify({ columns, data }, null, 2));
      },
      exportPdf: (columns, data) => {
        alert(JSON.stringify({ columns, data }, null, 2));
      },
    }}
  />
);
OverridingExportFunctionExample.parameters = {
  docs: {
    description: {
      story: 'Export functions of table can be override by setting `options.exportCsv` and/or `options.exportPdf`.',
    },
    source: {
      type: 'code',
    },
  },
};
