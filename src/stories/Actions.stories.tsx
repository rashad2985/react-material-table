import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Props, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry } from './data';
import { ActionPropsSimulator } from './props-simulations/action-props-simulator';

export default {
  title: 'Features/Actions',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="You can add buttons to rows or toolbar by using actions prop. Actions prop takes array of actions." />
          <Subheading>Usage</Subheading>
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';

              <ReactMaterialTable
                // other props
                actions={[
                  {
                    icon: 'save',
                    tooltip: 'Save',
                    onClick: (event, rowData) => alert(JSON.stringify({...rowData}, null, 2))
                  }
                ]}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Action Fields</Subheading>
          <Description markdown="Every action can have these fields:" />
          <Props sort="requiredFirst" of={ActionPropsSimulator} />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const SimpleActionExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Simple Action Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    actions={[
      {
        icon: 'save',
        tooltip: 'Save',
        onClick: (event, rowData) => alert(JSON.stringify({ ...rowData }, null, 2)),
      },
    ]}
  />
);
SimpleActionExample.parameters = {
  docs: {
    description: {
      story: 'A simple action added to table. It fires an alert with name of current user when clicked.',
    },
    source: {
      type: 'code',
    },
  },
};

export const MultipleActionExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Multiple Actions Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    actions={[
      {
        icon: 'save',
        tooltip: 'Save',
        onClick: (event, rowData) => alert(JSON.stringify({ ...rowData }, null, 2)),
      },
      {
        icon: 'delete',
        tooltip: 'Delete',
        onClick: (event, rowData) => alert(JSON.stringify({ ...rowData }, null, 2)),
      },
    ]}
  />
);
MultipleActionExample.parameters = {
  docs: {
    description: {
      story: 'You can add more that one actions.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ConditionalsActionExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Conditional Actions Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    actions={[
      {
        icon: 'save',
        tooltip: 'Save',
        onClick: (event, rowData) => alert(JSON.stringify({ ...rowData }, null, 2)),
      },
      (rowData) => ({
        icon: 'delete',
        tooltip: 'Delete',
        onClick: (event, row) => alert(JSON.stringify({ ...row }, null, 2)),
        disabled: rowData.calories >= 200 && rowData.calories < 300,
        hidden: rowData.calories > 350,
      }),
    ]}
  />
);
ConditionalsActionExample.parameters = {
  docs: {
    description: {
      story:
        'You can set an action to disabled or change its color according to `rowData`. In example you can not delete entries with calories between 200 and 300.',
    },
    source: {
      type: 'code',
    },
  },
};

export const PositioningActionsColumnExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Positioning Actions Column Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    actions={[
      {
        icon: 'save',
        tooltip: 'Save',
        onClick: (event, rowData) => alert(JSON.stringify({ ...rowData }, null, 2)),
      },
      (rowData) => ({
        icon: 'delete',
        tooltip: 'Delete',
        onClick: (event, row) => alert(JSON.stringify({ ...row }, null, 2)),
        disabled: rowData.calories >= 200 && rowData.calories < 300,
      }),
    ]}
    options={{
      actionsColumnIndex: -1,
    }}
  />
);
PositioningActionsColumnExample.parameters = {
  docs: {
    description: {
      story: 'You can change position of columns by setting `options.actionsColumnIndex`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const FreeActionExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Free Action Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    actions={[
      {
        icon: 'save',
        tooltip: 'Save',
        onClick: (event, rowData) => alert(JSON.stringify({ ...rowData }, null, 2)),
        isFreeAction: true,
      },
    ]}
  />
);
FreeActionExample.parameters = {
  docs: {
    description: {
      story: 'You can add `rowData` independent actions into the toolbar by setting `isFreeAction` to `true`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const PositioningActionExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Positioning Action Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    actions={[
      {
        icon: 'edit',
        tooltip: 'Edit',
        onClick: (event, rowData) => alert(JSON.stringify({ ...rowData }, null, 2)),
        isFreeAction: true,
      },
      {
        icon: 'save',
        tooltip: 'Save',
        onClick: (event, rowData) => alert(JSON.stringify({ ...rowData }, null, 2)),
        position: 'toolbar',
      },
      {
        icon: 'delete',
        tooltip: 'Delete',
        onClick: (event, rowData, selectedRows) => alert(JSON.stringify({ rowData, selectedRows }, null, 2)),
        position: 'toolbarOnSelect',
      },
    ]}
    options={{
      selection: true,
    }}
  />
);
PositioningActionExample.parameters = {
  docs: {
    description: {
      story:
        'You can add `rowData` independent actions into the toolbar depending from the row selection by setting `position` to `toolbar | toolbarOnSelect`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const GroupedActionsExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Grouped Actions Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    actions={[
      {
        type: 'action-group',
        tooltip: 'Grouped actions',
        actions: [
          {
            icon: 'save',
            tooltip: 'Save',
            onClick: (event, rowData, selectedRows) => alert(JSON.stringify({ rowData, selectedRows }, null, 2)),
          },
          {
            icon: 'edit',
            tooltip: 'Edit',
            onClick: (event, rowData) => alert(JSON.stringify({ ...rowData }, null, 2)),
          },
        ],
      },
      {
        type: 'action-group',
        icon: 'menu',
        tooltip: 'Grouped actions with own icon in toolbar',
        actions: [
          {
            icon: 'save',
            tooltip: 'Save',
            onClick: (event, rowData, selectedRows) => alert(JSON.stringify({ rowData, selectedRows }, null, 2)),
          },
          {
            icon: 'edit',
            tooltip: 'Edit',
            onClick: (event, rowData) => alert(JSON.stringify({ ...rowData }, null, 2)),
          },
        ],
        isFreeAction: true,
      },
    ]}
  />
);
GroupedActionsExample.parameters = {
  docs: {
    description: {
      story: 'You can group multiple actions into one by setting `type` to `action-group`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const LazyGroupedActionsExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Lazy Grouped Actions Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    actions={[
      {
        type: 'action-group',
        tooltip: 'Lazy Grouped actions',
        actions: (rowData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              console.log('Lazy Grouped action', rowData);
              resolve([
                {
                  icon: 'save',
                  tooltip: 'Save',
                  onClick: (event, rowData, selectedRows) => alert(JSON.stringify({ rowData, selectedRows }, null, 2)),
                },
                {
                  icon: 'edit',
                  tooltip: 'Edit',
                  onClick: (event, rowData) => alert(JSON.stringify({ ...rowData }, null, 2)),
                },
              ]);
            }, 3000);
          }),
      },
    ]}
  />
);
LazyGroupedActionsExample.parameters = {
  docs: {
    description: {
      story: 'You can use promises to lazy load the actions of a action-group.',
    },
    source: {
      type: 'code',
    },
  },
};
