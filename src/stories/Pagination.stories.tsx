import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry } from './data';

export default {
  title: 'Features/Pagination',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table has a responsive design pagination feature that lets users to show data in the table fully or partially with pagination." />
          <Subheading>Usage</Subheading>
          <Description markdown="By default the pagination feature is enabled. It can be disable by setting `options.paging` to `false`." />
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              
              <ReactMaterialTable
                // other props
                options={{
                  paging: true
                }}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Hints</Subheading>
          <Description
            markdown={
              '* You can set the default page size via `options.pageSize`.' +
              '\n* You can deactivate the filling remaining space with empty rows behaviour by setting `options.emptyRowsWhenPaging` to `false`.' +
              '\n* You can add the so called "Show All" option into the page size options by setting `options.pageSizeAll` to `true`. And override its label text via Localization (`pagination.pageSizeAll`)' +
              '\n* You can define custom paging options via providing them in `options.pageSizeOptions`.' +
              '\n* You can place the pagination panel from bottom to the top of the container by setting `options.paginationPosition` to `top`.' +
              '\n* Or You can make it to be shown at top and at the bottom at the same time by setting `options.paginationPosition` to `both`.' +
              '\n* You can change the type of the pagination from default to stepping type pagination by setting `options.paginationType` to `stepped`.' +
              '\n* You can change the responsive breakpoint of the pagination by setting `options.responsiveBreakPoint` to one of the `mui` breakpoints.'
            }
          />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const BasicPaginationExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Basic Pagination Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Gingerbread Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      paging: true,
    }}
  />
);
BasicPaginationExample.parameters = {
  docs: {
    description: {
      story:
        'Pagination is active by default, the example just demonstrates how to activate it, in case it is already deactivated somewhere else.',
    },
    source: {
      type: 'code',
    },
  },
};

export const PaginationDisabledExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Pagination Disabled Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Gingerbread Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      paging: false,
    }}
  />
);
PaginationDisabledExample.parameters = {
  docs: {
    description: {
      story: 'Disabling pagination removes whole pagination panel from the table container and shows all entries.',
    },
    source: {
      type: 'code',
    },
  },
};

export const PaginationWithoutEmptyFillerRowsExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Pagination without Empty Filler Rows Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Gingerbread Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      paging: true,
      emptyRowsWhenPaging: false,
    }}
  />
);
PaginationWithoutEmptyFillerRowsExample.parameters = {
  docs: {
    description: {
      story:
        'By default, if number of rows less than page size, empty rows are added into the table to keep table height unchanged. This behaviour can be deactivated by setting `options.emptyRowsWhenPaging` to `false`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const PaginationWithShowAllOptionExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Pagination with Show All Option Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Gingerbread Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      paging: true,
      pageSizeAll: true,
    }}
  />
);
PaginationWithShowAllOptionExample.parameters = {
  docs: {
    description: {
      story:
        'Pagination is active by default, the example just demonstrates how to activate it, in case it is already deactivated somewhere else.',
    },
    source: {
      type: 'code',
    },
  },
};

export const CustomInitialPageSizeExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Custom Initial Page Size Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Gingerbread Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      paging: true,
      emptyRowsWhenPaging: false,
      pageSize: 10,
    }}
  />
);
CustomInitialPageSizeExample.parameters = {
  docs: {
    description: {
      story:
        'Initial page size is 5 by default and can be overridden via setting `options.pageSize` to another number, like 10 as in this example.',
    },
    source: {
      type: 'code',
    },
  },
};

export const CustomPageSizeOptionsExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Custom Page Size Options Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Gingerbread Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      paging: true,
      emptyRowsWhenPaging: false,
      pageSizeOptions: [{ value: 3, label: 'Three' }, 5, { value: 7, label: 'Seven' }, 10],
      pageSize: 3,
    }}
  />
);
CustomPageSizeOptionsExample.parameters = {
  docs: {
    description: {
      story:
        'Page size options can be provided as numbers or as number value pairs. In this example, four options are provided, where first and third option has custom text labels.',
    },
    source: {
      type: 'code',
    },
  },
};

export const PositioningPaginationPanelToTheTopExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Positioning Pagination Panel to the top Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Gingerbread Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      paging: true,
      paginationPosition: 'top',
    }}
  />
);
PositioningPaginationPanelToTheTopExample.parameters = {
  docs: {
    description: {
      story: 'In this example, the pagination panel is shown on the top of the table.',
    },
    source: {
      type: 'code',
    },
  },
};

export const PositioningPaginationPanelAtTheTopAndBottomAtTheSameTimeExample: Story<
  ReactMaterialTableProps<SimpleTableEntry>
> = () => (
  <ReactMaterialTable
    title="Positioning Pagination Panel at the Top and Bottom at the same Time Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Gingerbread Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      paging: true,
      paginationPosition: 'both',
    }}
  />
);
PositioningPaginationPanelAtTheTopAndBottomAtTheSameTimeExample.parameters = {
  docs: {
    description: {
      story: 'In this example, the pagination panel is shown both on the top and on the bottom of the table.',
    },
    source: {
      type: 'code',
    },
  },
};

export const SteppedTypePaginationExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Stepped Type Pagination Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Gingerbread Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      paging: true,
      paginationType: 'stepped',
    }}
  />
);
SteppedTypePaginationExample.parameters = {
  docs: {
    description: {
      story: 'This example demonstrates `stepped` type pagination.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ChangingPaginationResponsiveBreakpointExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Changing Pagination Responsive Breakpoint Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Gingerbread Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      paging: true,
      responsiveBreakPoint: 'md',
    }}
    localization={{
      pagination: {
        labelDisplayedRows: '{from}-{to} of {count} (this part of the text is for demonstration)',
      },
    }}
  />
);
ChangingPaginationResponsiveBreakpointExample.parameters = {
  docs: {
    description: {
      story:
        'In this example, responsive breakpoint of the pagination was set to `md`. Please resize the table till `md` breakpoint width is reached to see the result.',
    },
    source: {
      type: 'code',
    },
  },
};
