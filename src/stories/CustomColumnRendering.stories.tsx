import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry } from './data';
import { Avatar } from '@mui/material';
import frozenYogurtUrl from './images/frozen-yoghurt.jpg';
import iceCreamSandwichUrl from './images/ice-cream-sandwich.jpg';
import eclairUrl from './images/eclair.jpg';
import cupcakeUrl from './images/cupcake.jpg';
import gingerbreadUrl from './images/gingerbread.jpg';

export default {
  title: 'Features/Custom Column Rendering',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table lets you to override a render of any column. For example, you can render an image instead of image url." />
          <Subheading>Usage</Subheading>
          <Description markdown="By default the sorting feature is enabled. It can be disable by setting `options.sorting` to `false`." />
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              
              <ReactMaterialTable
                // other props
                columns={[
                  {
                    field: 'url',
                    title: 'Avatar',
                    render: rowData => <img src={rowData.url} style={{width: 50, borderRadius: '50%'}}/>
                  }
                ]}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const RenderImageExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Render Image Preview"
    columns={[
      {
        title: 'Preview',
        // field property can be omitted on using render() property
        // Attention: search and filtering also will be disabled for such fields!
        // In this case, we do not need filtering and search features for this column!
        // field: 'url',
        type: 'string',
        render: (rowData) => <Avatar alt={rowData.name} src={rowData.url} />,
      },
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0, url: frozenYogurtUrl },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3, url: iceCreamSandwichUrl },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0, url: eclairUrl },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3, url: cupcakeUrl },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9, url: gingerbreadUrl },
    ]}
  />
);
RenderImageExample.parameters = {
  docs: {
    description: {
      story: 'In this example, image is being rendered from provided `url` field.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ColumnWidthRecommendationExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Render Image Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric', width: 64 },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      columnsButton: true,
    }}
  />
);
ColumnWidthRecommendationExample.parameters = {
  docs: {
    description: {
      story:
        'In this example, Fat Column receives width recommendation. The width recommendation sets column with to the given width, as long as there is at least one another column without a width recommendation that can fill the table width.',
    },
    source: {
      type: 'code',
    },
  },
};
