import { Column } from '../column';

export interface SimpleTableEntry {
  name: string;
  calories: number;
  fat: number;
  carbs: number;
  protein: number;
}

export interface MultiStringTableEntry {
  name: Array<string>;
  calories: Array<string>;
  fat: Array<string>;
}

export const simpleTableEntries: Array<SimpleTableEntry> = [
  { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
  { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
  { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
  { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
  { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
  { name: 'Gingerbread', calories: 358, fat: 11.0, carbs: 52, protein: 6.9 },
];

export const multiStringTableEntries: Array<MultiStringTableEntry> = [
  { name: ['Eclair'], calories: ['262'], fat: ['16.0'] },
  { name: ['Eclair', 'Cupcake'], calories: ['262', '305'], fat: ['16.0', '3.7'] },
  { name: ['Eclair', 'Cupcake', 'Gingerbread'], calories: ['262', '305', '358'], fat: ['16.0', '3.7', '11.0'] },
];

export interface ComplexObjectTableEntry {
  name: string;
  values: {
    calories: number;
    fat: number;
    carbs: number;
    protein: number;
  };
}

export const complexObjectTableEntries: Array<ComplexObjectTableEntry> = [
  { name: 'Frozen yoghurt', values: { calories: 159, fat: 6.0, carbs: 24, protein: 4.0 } },
  { name: 'Ice cream sandwich', values: { calories: 237, fat: 9.0, carbs: 37, protein: 4.3 } },
  { name: 'Eclair', values: { calories: 262, fat: 16.0, carbs: 24, protein: 6.0 } },
  { name: 'Cupcake', values: { calories: 305, fat: 3.7, carbs: 67, protein: 4.3 } },
  { name: 'Gingerbread', values: { calories: 356, fat: 16.0, carbs: 49, protein: 3.9 } },
  { name: 'Gingerbread', values: { calories: 358, fat: 11.0, carbs: 52, protein: 6.9 } },
];

export const simpleColumns: Array<Column<SimpleTableEntry>> = [
  { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
  { title: 'Calories', field: 'calories', type: 'numeric' },
  { title: 'Fat (g)', field: 'fat', type: 'numeric' },
  { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
  { title: 'Protein (g)', field: 'protein', type: 'numeric' },
];

export interface TableEntry extends SimpleTableEntry {
  date: Date;
  time: Date;
  datetime: Date;
  recommended: boolean;
  price: number;
}

export const tableEntries: Array<TableEntry> = [
  {
    name: 'Frozen yoghurt',
    calories: 159,
    fat: 6.0,
    carbs: 24,
    protein: 4.0,
    date: new Date(2021, 3, 5, 15, 15, 5, 200),
    time: new Date(2021, 3, 5, 15, 15, 5, 200),
    datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
    recommended: true,
    price: 2.99,
  },
  {
    name: 'Ice cream sandwich',
    calories: 237,
    fat: 9.0,
    carbs: 37,
    protein: 4.3,
    date: new Date(2021, 3, 1, 0, 15, 15, 100),
    time: new Date(2021, 3, 1, 0, 15, 15, 100),
    datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
    recommended: true,
    price: 5.99,
  },
  {
    name: 'Eclair',
    calories: 262,
    fat: 16.0,
    carbs: 24,
    protein: 6.0,
    date: new Date(2021, 1, 15, 12, 45, 25, 500),
    time: new Date(2021, 1, 15, 12, 45, 25, 500),
    datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
    recommended: false,
    price: 0.99,
  },
  {
    name: 'Cupcake',
    calories: 305,
    fat: 3.7,
    carbs: 67,
    protein: 4.3,
    date: new Date(2021, 6, 5, 8, 30, 35, 800),
    time: new Date(2021, 6, 5, 8, 30, 35, 800),
    datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
    recommended: false,
    price: 1.4999,
  },
  {
    name: 'Gingerbread',
    calories: 356,
    fat: 16.0,
    carbs: 49,
    protein: 3.9,
    date: new Date(2020, 11, 23, 9, 58, 45, 999),
    time: new Date(2020, 11, 23, 9, 58, 45, 999),
    datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
    recommended: true,
    price: 7,
  },
  {
    name: 'Gingerbread',
    calories: 358,
    fat: 11.0,
    carbs: 52,
    protein: 6.9,
    date: new Date(2020, 9, 14, 9, 58, 55, 888),
    time: new Date(2020, 9, 14, 9, 58, 55, 888),
    datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
    recommended: true,
    price: 1000000,
  },
];

export const columns: Array<Column<TableEntry>> = [
  { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
  { title: 'Calories', field: 'calories', type: 'numeric', numberSettings: { locale: 'en-US' } },
  { title: 'Fat (g)', field: 'fat', type: 'numeric', numberSettings: { locale: 'en-US' } },
  { title: 'Carbs (g)', field: 'carbs', type: 'numeric', numberSettings: { locale: 'en-US' } },
  { title: 'Protein (g)', field: 'protein', type: 'numeric', numberSettings: { locale: 'en-US' } },
  { title: 'Date', field: 'date', type: 'date', dateSetting: { locale: 'en-US' } },
  { title: 'Time', field: 'time', type: 'time', dateSetting: { locale: 'en-US' } },
  { title: 'Date & Time', field: 'datetime', type: 'datetime', dateSetting: { locale: 'en-US' } },
  { title: 'Recommended', field: 'recommended', type: 'boolean' },
  {
    title: 'Price',
    field: 'price',
    type: 'currency',
    currencySetting: { locale: 'en-US', formatOptions: { currency: 'USD' } },
  },
];
