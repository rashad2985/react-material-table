import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Stories, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry, TableEntry } from './data';

export default {
  title: 'Features/Styling',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table has a few styling options to apply styles to some components." />
          <Description
            markdown={
              '* `react-material-table uses mui theme, so any changes to the mui theme object also affects react-material-table.' +
              '\n* `column.cellStyle` (`React.CSSProperties`) - style to apply specified cells' +
              '\n* `column.headerStyle` (`React.CSSProperties`) - style to apply specified column header' +
              '\n* `options.headerStyle` (`React.CSSProperties`) - style to apply all header cells' +
              '\n* `options.rowStyle` (`React.CSSProperties`) - style to apply all row cells' +
              '\n* `options.responsiveRowStyle` (`React.CSSProperties`) - style to apply all responsive row cells' +
              '\n* `options.inputVariant` (`TextFieldProps.variant`) - variant that applied globally to all input' +
              ' fields. By default the variant of all input fields is set to `standard`.'
            }
          />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const StickyHeaderExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Sticky Header Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      maxBodyHeight: 300,
    }}
  />
);
StickyHeaderExample.parameters = {
  docs: {
    description: {
      story:
        'In this example headers are sticky/fixed and only rows are scrolled. To make headers sticky, `options.maxBodyHeight` should be set to the desired table height.',
    },
    source: {
      type: 'code',
    },
  },
};

export const CellHeaderStylingExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Cell Header Styling Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        cellStyle: {
          backgroundColor: '#039be5',
          color: '#FFF',
        },
        headerStyle: {
          backgroundColor: '#039be5',
        },
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      headerStyle: {
        backgroundColor: '#01579b',
        color: '#FFF',
      },
    }}
  />
);
CellHeaderStylingExample.parameters = {
  docs: {
    description: {
      story: 'In this example styles applied to cells and headers.',
    },
    source: {
      type: 'code',
    },
  },
};

export const RowStylingExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Row Styling Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      rowStyle: {
        backgroundColor: '#EEE',
      },
    }}
  />
);
RowStylingExample.parameters = {
  docs: {
    description: {
      story: 'In this example style applied to all rows.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ConditionalRowStylingExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Conditional Row Styling Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      rowStyle: (rowData) => ({
        backgroundColor: rowData.calories >= 250 && rowData.calories < 350 ? '#EEE' : undefined,
      }),
    }}
  />
);
ConditionalRowStylingExample.parameters = {
  docs: {
    description: {
      story: 'In this example style applied to rows where Calories value is between 250 and 350.',
    },
    source: {
      type: 'code',
    },
  },
};

export const StandardInputVariantExample: Story<
  ReactMaterialTableProps<Omit<TableEntry, 'fat' | 'carbs' | 'protein'>>
> = () => (
  // Here we have date/time/datetime type fields
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      title="Standard Input Variant Preview"
      columns={[
        { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
        { title: 'Calories', field: 'calories', type: 'numeric' },
        { title: 'Date', field: 'date', type: 'date' },
        { title: 'Time', field: 'time', type: 'time' },
        { title: 'Date & Time', field: 'datetime', type: 'datetime' },
        { title: 'Recommended', field: 'recommended', type: 'boolean' },
        {
          title: 'Price',
          field: 'price',
          type: 'currency',
          currencySetting: { locale: 'en-US', formatOptions: { currency: 'USD' } },
        },
      ]}
      data={[
        {
          name: 'Frozen yoghurt',
          calories: 159,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          recommended: true,
          price: 2.99,
        },
        {
          name: 'Ice cream sandwich',
          calories: 237,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          recommended: true,
          price: 5.99,
        },
        {
          name: 'Eclair',
          calories: 262,
          date: new Date(2021, 1, 15, 12, 45, 25, 500),
          time: new Date(2021, 1, 15, 12, 45, 25, 500),
          datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
          recommended: false,
          price: 0.99,
        },
        {
          name: 'Cupcake',
          calories: 305,
          date: new Date(2021, 6, 5, 8, 30, 35, 800),
          time: new Date(2021, 6, 5, 8, 30, 35, 800),
          datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
          recommended: false,
          price: 1.4999,
        },
        {
          name: 'Gingerbread',
          calories: 356,
          date: new Date(2020, 11, 23, 9, 58, 45, 999),
          time: new Date(2020, 11, 23, 9, 58, 45, 999),
          datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
          recommended: true,
          price: 7,
        },
        {
          name: 'Gingerbread',
          calories: 358,
          date: new Date(2020, 9, 14, 9, 58, 55, 888),
          time: new Date(2020, 9, 14, 9, 58, 55, 888),
          datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
          recommended: true,
          price: 1000000,
        },
      ]}
      editable={{
        onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
        onRowUpdate: (newData) =>
          new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
        onRowDelete: (newData) =>
          new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      }}
      options={{
        search: true,
        filtering: true,
        // this option is optional, default is standard
        // inputVariant: 'standard'
      }}
    />
  </LocalizationProvider>
);
StandardInputVariantExample.parameters = {
  docs: {
    description: {
      story: 'By default the variant of all input fields is set to `standard`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const OutlinedInputVariantExample: Story<
  ReactMaterialTableProps<Omit<TableEntry, 'fat' | 'carbs' | 'protein'>>
> = () => (
  // Here we have date/time/datetime type fields
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      title="Standard Input Variant Preview"
      columns={[
        { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
        { title: 'Calories', field: 'calories', type: 'numeric' },
        { title: 'Date', field: 'date', type: 'date' },
        { title: 'Time', field: 'time', type: 'time' },
        { title: 'Date & Time', field: 'datetime', type: 'datetime' },
        { title: 'Recommended', field: 'recommended', type: 'boolean' },
        {
          title: 'Price',
          field: 'price',
          type: 'currency',
          currencySetting: { locale: 'en-US', formatOptions: { currency: 'USD' } },
        },
      ]}
      data={[
        {
          name: 'Frozen yoghurt',
          calories: 159,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          recommended: true,
          price: 2.99,
        },
        {
          name: 'Ice cream sandwich',
          calories: 237,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          recommended: true,
          price: 5.99,
        },
        {
          name: 'Eclair',
          calories: 262,
          date: new Date(2021, 1, 15, 12, 45, 25, 500),
          time: new Date(2021, 1, 15, 12, 45, 25, 500),
          datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
          recommended: false,
          price: 0.99,
        },
        {
          name: 'Cupcake',
          calories: 305,
          date: new Date(2021, 6, 5, 8, 30, 35, 800),
          time: new Date(2021, 6, 5, 8, 30, 35, 800),
          datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
          recommended: false,
          price: 1.4999,
        },
        {
          name: 'Gingerbread',
          calories: 356,
          date: new Date(2020, 11, 23, 9, 58, 45, 999),
          time: new Date(2020, 11, 23, 9, 58, 45, 999),
          datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
          recommended: true,
          price: 7,
        },
        {
          name: 'Gingerbread',
          calories: 358,
          date: new Date(2020, 9, 14, 9, 58, 55, 888),
          time: new Date(2020, 9, 14, 9, 58, 55, 888),
          datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
          recommended: true,
          price: 1000000,
        },
      ]}
      editable={{
        onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
        onRowUpdate: (newData) =>
          new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
        onRowDelete: (newData) =>
          new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      }}
      options={{
        search: true,
        filtering: true,
        inputVariant: 'outlined',
      }}
    />
  </LocalizationProvider>
);
OutlinedInputVariantExample.parameters = {
  docs: {
    description: {
      story: 'In this example, the variant of all input fields is set to `outlined` via `options.inputVariant`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const FilledInputVariantExample: Story<
  ReactMaterialTableProps<Omit<TableEntry, 'fat' | 'carbs' | 'protein'>>
> = () => (
  // Here we have date/time/datetime type fields
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      title="Standard Input Variant Preview"
      columns={[
        { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
        { title: 'Calories', field: 'calories', type: 'numeric' },
        { title: 'Date', field: 'date', type: 'date' },
        { title: 'Time', field: 'time', type: 'time' },
        { title: 'Date & Time', field: 'datetime', type: 'datetime' },
        { title: 'Recommended', field: 'recommended', type: 'boolean' },
        {
          title: 'Price',
          field: 'price',
          type: 'currency',
          currencySetting: { locale: 'en-US', formatOptions: { currency: 'USD' } },
        },
      ]}
      data={[
        {
          name: 'Frozen yoghurt',
          calories: 159,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          recommended: true,
          price: 2.99,
        },
        {
          name: 'Ice cream sandwich',
          calories: 237,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          recommended: true,
          price: 5.99,
        },
        {
          name: 'Eclair',
          calories: 262,
          date: new Date(2021, 1, 15, 12, 45, 25, 500),
          time: new Date(2021, 1, 15, 12, 45, 25, 500),
          datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
          recommended: false,
          price: 0.99,
        },
        {
          name: 'Cupcake',
          calories: 305,
          date: new Date(2021, 6, 5, 8, 30, 35, 800),
          time: new Date(2021, 6, 5, 8, 30, 35, 800),
          datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
          recommended: false,
          price: 1.4999,
        },
        {
          name: 'Gingerbread',
          calories: 356,
          date: new Date(2020, 11, 23, 9, 58, 45, 999),
          time: new Date(2020, 11, 23, 9, 58, 45, 999),
          datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
          recommended: true,
          price: 7,
        },
        {
          name: 'Gingerbread',
          calories: 358,
          date: new Date(2020, 9, 14, 9, 58, 55, 888),
          time: new Date(2020, 9, 14, 9, 58, 55, 888),
          datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
          recommended: true,
          price: 1000000,
        },
      ]}
      editable={{
        onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
        onRowUpdate: (newData) =>
          new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
        onRowDelete: (newData) =>
          new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      }}
      options={{
        search: true,
        filtering: true,
        inputVariant: 'filled',
      }}
    />
  </LocalizationProvider>
);
FilledInputVariantExample.parameters = {
  docs: {
    description: {
      story: 'In this example, the variant of all input fields is set to `filled` via `options.inputVariant`.',
    },
    source: {
      type: 'code',
    },
  },
};
