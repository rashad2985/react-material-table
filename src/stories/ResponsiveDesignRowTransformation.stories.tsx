import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry } from './data';
import { Avatar } from '@mui/material';
import frozenYogurtUrl from './images/frozen-yoghurt.jpg';
import iceCreamSandwichUrl from './images/ice-cream-sandwich.jpg';
import eclairUrl from './images/eclair.jpg';
import cupcakeUrl from './images/cupcake.jpg';
import gingerbreadUrl from './images/gingerbread.jpg';

export default {
  title: 'Features/Responsive Design Row Transformation',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table has built-in responsive design table row rendering mechanism that transforms rows into boxes containing cell header and value pairs as rows in it. This is against Google Material Design Guidelines, but is a nice to have feature." />
          <Subheading>Usage</Subheading>
          <Description markdown="By default the responsive design is disabled. It can be enabled by setting `options.responsive` to `true`." />
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              
              <ReactMaterialTable
                // other props
                options={{
                  responsive: true
                }}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Hints</Subheading>
          <Description
            markdown={
              '* You can exclude a column from responsive transformed row by setting `column.responsive` to `false`.' +
              '\n* You can define the initially number of cell to be showed in responsive transformed row by setting `options.responsiveMaxInitialCells` to another positive integer.' +
              '\n* You can set the default responsive view transformation breakpoint by setting `options.responsiveBreakPoint` to another `mui` breakpoint.' +
              '\n* You can change the default header and value rendering proportions in in responsive transformed row by setting `options.responsiveRowHeaderValueProportions` to another pair of `mui` GridSize.'
            }
          />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const BasicResponsiveDesignRowExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Basic Responsive Design Row Preview"
    columns={[
      {
        title: 'Preview',
        field: 'url',
        type: 'string',
        render: (rowData) => <Avatar alt={rowData.name} src={rowData.url} />,
      },
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      {
        name: 'Frozen yoghurt',
        calories: 159,
        fat: 6.0,
        carbs: 24,
        protein: 4.0,
        recommended: true,
        url: frozenYogurtUrl,
      },
      {
        name: 'Ice cream sandwich',
        calories: 237,
        fat: 9.0,
        carbs: 37,
        protein: 4.3,
        recommended: true,
        url: iceCreamSandwichUrl,
      },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0, recommended: true, url: eclairUrl },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3, recommended: false, url: cupcakeUrl },
      {
        name: 'Gingerbread',
        calories: 356,
        fat: 16.0,
        carbs: 49,
        protein: 3.9,
        recommended: false,
        url: gingerbreadUrl,
      },
    ]}
    options={{
      responsive: true,
    }}
  />
);
BasicResponsiveDesignRowExample.parameters = {
  docs: {
    description: {
      story:
        'In this example, responsive row view feature is activated and rows will transform into responsive boxes for the default breakpoint `xs` and below. Resize the screen to see the transformation result.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ExcludeColumnFromResponsiveDesignRowExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Exclude Column from Responsive Design Row Preview"
    columns={[
      {
        title: 'Preview',
        field: 'url',
        type: 'string',
        render: (rowData) => <Avatar alt={rowData.name} src={rowData.url} />,
        responsive: false,
      },
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      {
        name: 'Frozen yoghurt',
        calories: 159,
        fat: 6.0,
        carbs: 24,
        protein: 4.0,
        recommended: true,
        url: frozenYogurtUrl,
      },
      {
        name: 'Ice cream sandwich',
        calories: 237,
        fat: 9.0,
        carbs: 37,
        protein: 4.3,
        recommended: true,
        url: iceCreamSandwichUrl,
      },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0, recommended: true, url: eclairUrl },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3, recommended: false, url: cupcakeUrl },
      {
        name: 'Gingerbread',
        calories: 356,
        fat: 16.0,
        carbs: 49,
        protein: 3.9,
        recommended: false,
        url: gingerbreadUrl,
      },
    ]}
    options={{
      responsive: true,
    }}
  />
);
ExcludeColumnFromResponsiveDesignRowExample.parameters = {
  docs: {
    description: {
      story:
        'In this example, Preview column is excluded from responsive row. Resize the screen to see the transformation result.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ResponsiveDesignRowWithInitiallyReducedNumberOfCellsExample: Story<
  ReactMaterialTableProps<SimpleTableEntry>
> = () => (
  <ReactMaterialTable
    title="Responsive Design Row with Initially Reduced Number of Cells Preview"
    columns={[
      {
        title: 'Preview',
        field: 'url',
        type: 'string',
        render: (rowData) => <Avatar alt={rowData.name} src={rowData.url} />,
      },
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      {
        name: 'Frozen yoghurt',
        calories: 159,
        fat: 6.0,
        carbs: 24,
        protein: 4.0,
        recommended: true,
        url: frozenYogurtUrl,
      },
      {
        name: 'Ice cream sandwich',
        calories: 237,
        fat: 9.0,
        carbs: 37,
        protein: 4.3,
        recommended: true,
        url: iceCreamSandwichUrl,
      },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0, recommended: true, url: eclairUrl },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3, recommended: false, url: cupcakeUrl },
      {
        name: 'Gingerbread',
        calories: 356,
        fat: 16.0,
        carbs: 49,
        protein: 3.9,
        recommended: false,
        url: gingerbreadUrl,
      },
    ]}
    options={{
      responsive: true,
      responsiveMaxInitialCells: 3,
    }}
  />
);
ResponsiveDesignRowWithInitiallyReducedNumberOfCellsExample.parameters = {
  docs: {
    description: {
      story:
        'In this example, responsive row initially renders only three cell. The remaining cells are hidden and can be show via expanding the row. Resize the screen to see the transformation result.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ResponsiveDesignBreakpointChangeExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Responsive Design Breakpoint Change Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Gingerbread Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      responsive: true,
      responsiveBreakPoint: 'md',
    }}
  />
);
ResponsiveDesignBreakpointChangeExample.parameters = {
  docs: {
    description: {
      story:
        'In this example, the rows will transform into responsive boxes for the breakpoint `md` and below. Resize the screen to see the transformation result.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ResponsiveDesignRowHeaderAndValueProportionsExample: Story<
  ReactMaterialTableProps<SimpleTableEntry>
> = () => (
  <ReactMaterialTable
    title="Responsive Design Row Header and Value Proportions Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Gingerbread Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      responsive: true,
      responsiveRowHeaderValueProportions: [8, 4],
    }}
  />
);
ResponsiveDesignRowHeaderAndValueProportionsExample.parameters = {
  docs: {
    description: {
      story:
        'In this example, in responsive transformed rows header text will take 8/12 and value 4/12 grid proportions. Resize the screen to see the transformation result.',
    },
    source: {
      type: 'code',
    },
  },
};
