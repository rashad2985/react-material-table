import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Props, Source, Stories, Subheading, Subtitle, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { Column } from '../column';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { MultiStringTableEntry, SimpleTableEntry, TableEntry } from './data';
import { EditablePropsSimulator } from './props-simulations/editable-props-simulator';
import { ListItemIcon, ListItemText } from '@mui/material';
import { AlarmOn } from '@mui/icons-material';

export default {
  title: 'Features/Editable',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table has Google Material Design conform editing feature that lets users to add, remove, update new rows." />
          <Subheading>Usage</Subheading>
          <Subtitle>Row Editing</Subtitle>
          <Description markdown="To make table rows editable, you should set `editable` prop that has `onRowAdd`, `onRowUpdate`, `onRowDelete` functions to manipulate data. Every function must return a promise." />
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              
              <ReactMaterialTable
                // other props
                editable={{
                  isEditable: rowData => rowData.name === 'a', // only name(a) rows would be editable
                  isEditHidden: rowData => rowData.name === 'x',
                  isDeletable: rowData => rowData.name === 'b', // only name(b) rows would be deletable,
                  isDeleteHidden: rowData => rowData.name === 'y',
                  onRowAddCancelled: rowData => console.log('Row adding cancelled'),
                  onRowUpdateCancelled: rowData => console.log('Row editing cancelled'),
                  onRowAdd: newData =>
                    new Promise((resolve, reject) => {
                      setTimeout(() => {
                        /* setData([...data, newData]); */
                        resolve(newData);
                      }, 1000);
                    }),
                  onRowUpdate: (newData, oldData) =>
                    new Promise((resolve, reject) => {
                      setTimeout(() => {
                        /* setData([...data, newData]); */
                        resolve(newData);
                      }, 1000);
                    }),
                  onRowDelete: oldData =>
                    new Promise((resolve, reject) => {
                      setTimeout(() => {
                        /* setData([...data, newData]); */
                        resolve(oldData);
                      }, 1000);
                    })
                }}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Hints</Subheading>
          <Description
            markdown={
              '* You can use `column.editComponent` to override edit component for a column.' +
              '\n* You can use `column.initialEditValue` to set initial value on adding new row.' +
              '\n* You can call `reject()` to keep open row editable.' +
              '\n* You can use `column.editable` to make a column cells editable or non-editable.' +
              '\n* You can use `column.editComponentFieldRequired` to show required symbol (`*`) in label.' +
              '\n* You can use `column.addSetRowData` and `column.editSetRowData` to manipulate other inputs/row data during edit/add.' +
              '\n* You can use `column.lookup` that renders a form input field as a Combobox.' +
              '\n* You can use `column.lookupInputType` to make appear lookup selector as an autocomplete, radio' +
              ' button-button group or check-boxes group (`multi-string` field type only).' +
              '\n* You can use `column.lookupItemRender` to customize how lookup values should be shown. In this case, if autocomplete also activated, then value will be used as a search string to find and select matching values.' +
              '\n* You can use `column.lookupItemDisabled` to check and disable lookup items.' +
              '\n* You can use `column.lookupSort` to sort lookup items before they appear on the GUI.' +
              '\n* You can use `column.addLookup` to to override lookup items depending rom the row data for add from. Row data will be provided for the added row with initial edit values.' +
              '\n* You can use `column.editLookup` to to override lookup items depending rom the row data for edit from. Row data will be provided for the edited row.'
            }
          />
          <Subheading>Action Fields</Subheading>
          <Description markdown="Editable object can have these fields:" />
          <Props sort="requiredFirst" of={EditablePropsSimulator} />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const EditableExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Editable Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowDelete: (rowData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(rowData), 3000)),
    }}
  />
);
EditableExample.parameters = {
  docs: {
    description: {
      story: 'A simple editable implementation',
    },
    source: {
      type: 'code',
    },
  },
};

export const ManipulateOtherFieldsOnEditOrAddFormExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable<SimpleTableEntry>
    title="Manipulate Other Fields Preview"
    columns={
      [
        {
          title: 'Dessert (100g serving)',
          field: 'name',
          type: 'string',
          addSetRowData: (
            newValue: string,
            setRowData: React.Dispatch<React.SetStateAction<Partial<SimpleTableEntry>>>,
          ) => setRowData((prevState) => ({ ...prevState, name: newValue, calories: (newValue ?? '').length })),
          editSetRowData: (
            newValue: string,
            setRowData: React.Dispatch<React.SetStateAction<Partial<SimpleTableEntry>>>,
          ) => setRowData((prevState) => ({ ...prevState, name: newValue, fat: (newValue ?? '').length })),
        },
        { title: 'Calories', field: 'calories', type: 'numeric' },
        { title: 'Fat (g)', field: 'fat', type: 'numeric' },
        {
          title: 'Carbs (g)',
          field: 'carbs',
          type: 'numeric',
          addSetRowData: (
            newValue: number,
            setRowData: React.Dispatch<React.SetStateAction<Partial<SimpleTableEntry>>>,
          ) => setRowData((prevState) => ({ ...prevState, carbs: newValue, protein: newValue })),
        },
        {
          title: 'Protein (g)',
          field: 'protein',
          type: 'numeric',
          editSetRowData: (
            newValue: number,
            setRowData: React.Dispatch<React.SetStateAction<Partial<SimpleTableEntry>>>,
          ) => setRowData((prevState) => ({ ...prevState, protein: newValue, calories: 0 })),
        },
      ] as Array<Column<SimpleTableEntry>>
    }
    data={
      [
        { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
        { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
        { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
        { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
        { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
      ] as Array<SimpleTableEntry>
    }
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowDelete: (rowData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(rowData), 3000)),
    }}
  />
);
ManipulateOtherFieldsOnEditOrAddFormExample.parameters = {
  docs: {
    description: {
      story:
        'A simple editable implementation, where' +
        '<ul>' +
        '<li>editing `name` field sets its new length to `calories` field only on "add" form (not on edit form).</li>' +
        '<li>editing `name` field sets its new length to `fat` field only on "edit" form (not on add form).</li>' +
        '<li>editing `carbs` field sets its new value to `protein` field only on "add" form (not on edit form).</li>' +
        '<li>editing `protein` field resets `calories` field to 0 only on "edit" form (not on add form).</li>' +
        '</ul>',
    },
    source: {
      type: 'code',
    },
  },
};

export const ConditionalAddEditDeleteExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Conditional Add/Edit/Delete Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      canAdd: false,
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      isEditable: (rowData) => (rowData?.calories ?? 0) < 300,
      isEditHidden: (rowData) => (rowData?.calories ?? 0) > 350,
      onRowDelete: (rowData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(rowData), 3000)),
      isDeletable: (rowData) => (rowData?.calories ?? 0) > 250,
      isDeleteHidden: (rowData) => (rowData?.calories ?? 0) > 350,
    }}
  />
);
ConditionalAddEditDeleteExample.parameters = {
  docs: {
    description: {
      story:
        'A simple editable implementation, where disabled and hidden states of edit/delete/add buttons are set conditionally.',
    },
    source: {
      type: 'code',
    },
  },
};

export const EditableWithRequiredFieldsExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Editable With Required Fields Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string', editComponentFieldRequired: true },
      { title: 'Calories', field: 'calories', type: 'numeric', editComponentFieldRequired: true },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowDelete: (rowData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(rowData), 3000)),
    }}
  />
);
EditableWithRequiredFieldsExample.parameters = {
  docs: {
    description: {
      story:
        'A simple editable implementation with fields marked as required via setting `column.editComponentFieldRequired` to `true`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const LookupAutocompleteFieldExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => {
  return (
    <ReactMaterialTable
      title="Lookup Autocomplete Field Preview"
      columns={[
        {
          title: 'Dessert (100g serving)',
          field: 'name',
          type: 'string',
          lookup: new Map<string, string>([
            ['FR', 'Frozen yoghurt'],
            ['ICS', 'Ice cream sandwich'],
            ['E', 'Eclair'],
            ['C', 'Cupcake'],
            ['G', 'Gingerbread'],
          ]),
          lookupInputType: 'autocomplete',
          lookupItemRender: ({ value, text }) => (
            <React.Fragment>
              <ListItemIcon>
                <AlarmOn />
              </ListItemIcon>
              <ListItemText primary={text} secondary={value} />
            </React.Fragment>
          ),
          initialEditValue: 'ICS',
        },
        {
          title: 'Calories',
          field: 'calories',
          type: 'numeric',
          lookup: new Map<string, string>([
            ['159', 'min'],
            ['237', '237'],
            ['262', '262'],
            ['305', '305'],
            ['356', 'max'],
          ]),
          initialEditValue: '159',
          lookupInputType: 'autocomplete',
        },
        {
          title: 'Fat (g)',
          field: 'fat',
          type: 'numeric',
          lookup: new Map<string, string>([
            ['5', '5'],
            ['6', 'Six'],
          ]),
          lookupItemRender: ({ value, text }) => (
            <React.Fragment>
              <ListItemIcon>
                <AlarmOn />
              </ListItemIcon>
              <ListItemText primary={text} secondary={value} />
            </React.Fragment>
          ),
          initialEditValue: '6',
        },
        {
          title: 'Carbs (g)',
          field: 'carbs',
          type: 'numeric',
          lookup: new Map<string, string>([
            ['24', '24'],
            ['37', '37'],
            ['67', '67'],
            ['49', '49'],
          ]),
          // Let's allow on edit only values of interval [carbs - 2, carbs + 2], and if undefined then 0 is the initial value
          editLookup: ({ carbs = 0 }) =>
            new Promise((resolve) => {
              resolve(
                new Map<string, string>([
                  [`${carbs - 2}`, `${carbs - 2}`],
                  [`${carbs - 1}`, `${carbs - 1}`],
                  [`${carbs}`, `${carbs}`],
                  [`${carbs + 1}`, `${carbs + 1}`],
                  [`${carbs + 2}`, `${carbs + 2}`],
                ]),
              );
            }),
        },
        { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      ]}
      data={[
        { name: 'FR', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
        { name: 'ICS', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
        { name: 'E', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
        { name: 'C', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
        { name: 'G', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
      ]}
      editable={{
        onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
        onRowUpdate: (newData) =>
          new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      }}
      options={{ filtering: true }}
    />
  );
};
LookupAutocompleteFieldExample.parameters = {
  docs: {
    description: {
      story:
        'Besides boolean, string and numeric type fields also support lookup on add/edit form.' +
        '<ul>' +
        '<li>`name` string type field shows an autocomplete combobox with custom item rendering and initial value Ice cream sandwich (ICS) selected automatically on add.</li>' +
        '<li>`calories` numeric type field also shows an autocomplete combobox with just rendering lookup values from lookup object.</li>' +
        '<li>`fat` numeric type field shows a simple combobox but with custom item rendering.</li>' +
        '<li>`carbs` numeric type field also shows a simple combobox. But only on add from `column.lookup` elements are used. On edit, `column.editLookup` function is used to resolve the possible combobox values depending from the edited row data. In this example, only values of interval [carbs - 2, carbs + 2] are suggested in the combobox.</li>' +
        '</ul>',
    },
    source: {
      type: 'code',
    },
  },
};

export const LookupAutocompleteFieldWithLazyLoadedFieldExample: Story<
  ReactMaterialTableProps<SimpleTableEntry>
> = () => {
  return (
    <ReactMaterialTable
      title="Lookup Autocomplete Field with Lazy Loaded Field Preview"
      columns={[
        {
          title: 'Dessert (100g serving)',
          field: 'name',
          type: 'string',
          lookup: new Map<string, string>([
            ['FR', 'Frozen yoghurt'],
            ['ICS', 'Ice cream sandwich'],
            ['E', 'Eclair'],
            ['C', 'Cupcake'],
            ['G', 'Gingerbread'],
          ]),
          lookupInputType: 'autocomplete',
          lookupItemRender: ({ value, text }) => (
            <React.Fragment>
              <ListItemIcon>
                <AlarmOn />
              </ListItemIcon>
              <ListItemText primary={text} secondary={value} />
            </React.Fragment>
          ),
          initialEditValue: 'ICS',
        },
        {
          title: 'Calories',
          field: 'calories',
          type: 'numeric',
          lookup: new Map<string, string>([
            ['159', 'min'],
            ['237', '237'],
            ['262', '262'],
            ['305', '305'],
            ['356', 'max'],
          ]),
          initialEditValue: '159',
          lookupInputType: 'autocomplete',
        },
        {
          title: 'Fat (g)',
          field: 'fat',
          type: 'numeric',
          lookup: new Map<string, string>([
            ['5', '5'],
            ['6', 'Six'],
          ]),
          lookupItemRender: ({ value, text }) => (
            <React.Fragment>
              <ListItemIcon>
                <AlarmOn />
              </ListItemIcon>
              <ListItemText primary={text} secondary={value} />
            </React.Fragment>
          ),
          initialEditValue: '6',
        },
        {
          title: 'Carbs (g)',
          field: 'carbs',
          type: 'numeric',
          lookup: new Map<string, string>([
            ['24', '24'],
            ['37', '37'],
            ['67', '67'],
            ['49', '49'],
          ]),
          // Promise to return values after 3 seconds
          editLookup: ({ carbs = 0 }) =>
            new Promise((resolve) => {
              setTimeout(
                () =>
                  resolve(
                    new Map<string, string>([
                      [`${carbs - 2}`, `${carbs - 2}`],
                      [`${carbs - 1}`, `${carbs - 1}`],
                      [`${carbs}`, `${carbs}`],
                      [`${carbs + 1}`, `${carbs + 1}`],
                      [`${carbs + 2}`, `${carbs + 2}`],
                    ]),
                  ),
                3000,
              );
            }),
          addLookup: ({ carbs = 0 }) =>
            new Promise((resolve) => {
              setTimeout(
                () =>
                  resolve(
                    new Map<string, string>([
                      [`${carbs - 2}`, `${carbs - 2}`],
                      [`${carbs - 1}`, `${carbs - 1}`],
                      [`${carbs}`, `${carbs}`],
                      [`${carbs + 1}`, `${carbs + 1}`],
                      [`${carbs + 2}`, `${carbs + 2}`],
                    ]),
                  ),
                3000,
              );
            }),
          // Don't forget to validate the input, it is not set at least on create until not loaded
          validate: (rowData) => Number.isFinite(rowData.carbs),
        },
        { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      ]}
      data={[
        { name: 'FR', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
        { name: 'ICS', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
        { name: 'E', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
        { name: 'C', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
        { name: 'G', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
      ]}
      editable={{
        onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
        onRowUpdate: (newData) =>
          new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      }}
      options={{ filtering: true }}
    />
  );
};
LookupAutocompleteFieldWithLazyLoadedFieldExample.parameters = {
  docs: {
    description: {
      story: 'As example above but `carbs`-Field lookup values on are lazy loaded on edit:',
    },
    source: {
      type: 'code',
    },
  },
};

export const LookupRadioButtonFieldExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Lookup Radio Button Field Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        lookup: new Map<string, string>([
          ['FR', 'Frozen yoghurt'],
          ['ICS', 'Ice cream sandwich'],
          ['E', 'Eclair'],
          ['C', 'Cupcake'],
          ['G', 'Gingerbread'],
        ]),
        lookupInputType: 'radio-group',
        initialEditValue: 'ICS',
      },
      {
        title: 'Calories',
        field: 'calories',
        type: 'numeric',
        lookup: new Map<string, string>([
          ['159', 'min'],
          ['237', '237'],
          ['262', '262'],
          ['305', '305'],
          ['356', 'max'],
        ]),
        initialEditValue: '159',
        lookupInputType: 'radio-group',
        lookupRadioGroupProps: { row: true },
      },
      {
        title: 'Fat (g)',
        field: 'fat',
        type: 'numeric',
        lookup: new Map<string, string>([
          ['5', '5'],
          ['6', 'Six'],
        ]),
        lookupInputType: 'radio-group',
        lookupRadioGroupProps: { row: true },
        initialEditValue: '6',
      },
      {
        title: 'Carbs (g)',
        field: 'carbs',
        type: 'numeric',
        lookup: new Map<string, string>([
          ['24', '24'],
          ['37', '37'],
          ['67', '67'],
          ['49', '49'],
        ]),
        lookupInputType: 'radio-group',
        lookupRadioGroupProps: { row: true },
        // Let's allow on edit only values of interval [carbs - 2, carbs + 2], and if undefined then 0 is the initial value
        editLookup: ({ carbs = 0 }) =>
          Promise.resolve(
            new Map<string, string>([
              [`${carbs - 2}`, `${carbs - 2}`],
              [`${carbs - 1}`, `${carbs - 1}`],
              [`${carbs}`, `${carbs}`],
              [`${carbs + 1}`, `${carbs + 1}`],
              [`${carbs + 2}`, `${carbs + 2}`],
            ]),
          ),
        // let's allow only 1, 2, 3, 4, 5 on add and disable adding of 3
        addLookup: () =>
          Promise.resolve(
            new Map<string, string>([
              ['1', '1'],
              ['2', '2'],
              ['3', '3'],
              ['4', '4'],
              ['5', '5'],
            ]),
          ),
        lookupItemDisabled: (rowData, item) => item.value === '3',
      },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'FR', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'ICS', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'E', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'C', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'G', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
    }}
  />
);
LookupRadioButtonFieldExample.parameters = {
  docs: {
    description: {
      story:
        'Besides boolean, string and numeric type fields also support lookup on add/edit form.' +
        '<ul>' +
        '<li>`name` string type field shows a group of radio buttons in the same column' +
        ' cream sandwich (ICS) selected automatically on add.</li>' +
        '<li>`calories`, `fat` and `carbs` numeric type field also shows a group of radio buttons but in the same row' +
        ' rendering lookup values from lookup object.</li>' +
        '</ul>',
    },
    source: {
      type: 'code',
    },
  },
};

export const MultiStringFieldAndCheckboxLookupExample: Story<ReactMaterialTableProps<MultiStringTableEntry>> = () => (
  <ReactMaterialTable
    title="Multi String Field And Checkbox Lookup Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'multi-string',
        lookup: new Map<string, string>([
          ['E', 'Eclair'],
          ['C', 'Cupcake'],
          ['G', 'Gingerbread'],
        ]),
        lookupInputType: 'checkbox-group',
        initialEditValue: ['C'],
      },
      {
        title: 'Calories',
        field: 'calories',
        type: 'multi-string',
        lookup: new Map<string, string>([
          ['262', 'min'],
          ['305', '305'],
          ['358', 'max'],
        ]),
        initialEditValue: ['358'],
        lookupInputType: 'checkbox-group',
        lookupFormGroupProps: { row: true },
      },
      {
        title: 'Fat (g)',
        field: 'fat',
        type: 'multi-string',
        lookupInputType: 'checkbox-group',
        lookupFormGroupProps: { row: true },
      },
      {
        title: 'Name (autocomplete)',
        field: 'name-as-autocomplete',
        value: (rowData) => rowData.name,
        type: 'multi-string',
        lookup: new Map<string, string>([
          ['E', 'Eclair'],
          ['C', 'Cupcake'],
          ['G', 'Gingerbread'],
        ]),
        lookupInputType: 'autocomplete',
        // let's disable 3rd item on add/edit
        lookupItemDisabled: (rowData, item) => item.value === 'G',
      },
      {
        title: 'Name (text-field)',
        field: 'name-as-text-field',
        value: (rowData) => rowData.name,
        type: 'multi-string',
        lookup: new Map<string, string>([
          ['E', 'Eclair'],
          ['C', 'Cupcake'],
          ['G', 'Gingerbread'],
        ]),
        editLookup: (rowData) =>
          Promise.resolve(
            new Map<string, string>((rowData.name ?? []).filter((value) => !!value).map((value) => [value, value])),
          ),
        lookupInputType: 'text-field',
        // let's disable 2nd item on add/edit
        lookupItemDisabled: (rowData, item) => item.value === 'C',
      },
    ]}
    data={[
      { name: ['E'], calories: ['262'], fat: ['16.0'] },
      { name: ['E', 'C'], calories: ['262', '305'], fat: ['16.0', '3.7'] },
      { name: ['E', 'C', 'G'], calories: ['262', '305', '358'], fat: ['16.0', '3.7', '11.0'] },
    ]}
    editable={{
      onRowAdd: (newData) =>
        new Promise<Partial<MultiStringTableEntry>>((resolve) => {
          setTimeout(() => resolve(newData), 3000);
        }),
      onRowUpdate: (newData) =>
        new Promise<Partial<MultiStringTableEntry>>((resolve) => {
          setTimeout(() => resolve(newData), 3000);
        }),
    }}
  />
);
MultiStringFieldAndCheckboxLookupExample.parameters = {
  docs: {
    description: {
      story:
        '`multi-string` type fields support lookup on add/edit form. They are best suited for list/set type fields to allow choose multiple values.',
    },
    source: {
      type: 'code',
    },
  },
};

export const LookupSortingExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Lookup Sorting Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        lookup: new Map<string, string>([
          ['FR', 'Frozen yoghurt'],
          ['ICS', 'Ice cream sandwich'],
          ['E', 'Eclair'],
          ['C', 'Cupcake'],
          ['G', 'Gingerbread'],
        ]),
        lookupInputType: 'autocomplete',
        lookupItemRender: ({ value, text }) => (
          <React.Fragment>
            <ListItemIcon>
              <AlarmOn />
            </ListItemIcon>
            <ListItemText primary={text} secondary={value} />
          </React.Fragment>
        ),
        initialEditValue: 'ICS',
        // let's sort them by their text descending
        lookupSort: (item1, item2) =>
          (item2.text ?? '').localeCompare(item1.text ?? '', navigator?.language, { sensitivity: 'base' }),
      },
      {
        title: 'Calories',
        field: 'calories',
        type: 'numeric',
        lookup: new Map<string, string>([
          ['159', 'min'],
          ['237', '237'],
          ['262', '262'],
          ['305', '305'],
          ['356', 'max'],
        ]),
        // let's sort them by their values ascending so that min appears on top and max at the bottom
        lookupSort: (item1, item2) =>
          (item1.value ?? '').localeCompare(item2.value ?? '', navigator?.language, {
            sensitivity: 'base',
            numeric: true,
          }),
        initialEditValue: '159',
        lookupInputType: 'autocomplete',
      },
      {
        title: 'Fat (g)',
        field: 'fat',
        type: 'numeric',
        lookup: new Map<string, string>([
          ['5', '5'],
          ['6', 'Six'],
        ]),
        lookupItemRender: ({ value, text }) => (
          <React.Fragment>
            <ListItemIcon>
              <AlarmOn />
            </ListItemIcon>
            <ListItemText primary={text} secondary={value} />
          </React.Fragment>
        ),
        initialEditValue: '6',
      },
      {
        title: 'Carbs (g)',
        field: 'carbs',
        type: 'numeric',
        lookup: new Map<string, string>([
          ['24', '24'],
          ['37', '37'],
          ['67', '67'],
          ['49', '49'],
        ]),
        // Let's allow on edit only values of interval [carbs - 2, carbs + 2], and if undefined then 0 is the initial value
        editLookup: ({ carbs = 0 }) =>
          Promise.resolve(
            new Map([
              [`${carbs - 2}`, `${carbs - 2}`],
              [`${carbs - 1}`, `${carbs - 1}`],
              [`${carbs}`, `${carbs}`],
              [`${carbs + 1}`, `${carbs + 1}`],
              [`${carbs + 2}`, `${carbs + 2}`],
            ]),
          ),
      },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'FR', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'ICS', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'E', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'C', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'G', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
    }}
  />
);
LookupSortingExample.parameters = {
  docs: {
    description: {
      story:
        'In this example:' +
        '<ul>' +
        '<li>`name` string type field items are sorted by their lookup text descending.</li>' +
        '<li>`calories` numeric type field items are sorted by their lookup value ascending so that min appears on top and max at the bottom.</li>' +
        '</ul>',
    },
    source: {
      type: 'code',
    },
  },
};

export const DisableFieldEditableExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Disable Field Editable Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string', editable: 'onUpdate' },
      { title: 'Calories', field: 'calories', type: 'numeric', editable: 'never' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowDelete: (rowData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(rowData), 3000)),
    }}
  />
);
DisableFieldEditableExample.parameters = {
  docs: {
    description: {
      story:
        'A simple editable implementation where the Calories column is whether updatable nor addable, but the Name column is only updatable.',
    },
    source: {
      type: 'code',
    },
  },
};

export const DynamicallyDisableFieldEditableExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Dynamically Disable Field Editable Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      {
        title: 'Calories',
        field: 'calories',
        type: 'numeric',
        editable: (column, row, mode) => mode === 'add' || (row.name ?? '').length < 5,
      },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowDelete: (rowData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(rowData), 3000)),
    }}
  />
);
DynamicallyDisableFieldEditableExample.parameters = {
  docs: {
    description: {
      story:
        'A simple editable implementation where the Calories column input is visible if it is add mode or the length of string value in name column less than 5.',
    },
    source: {
      type: 'code',
    },
  },
};

export const CustomEditComponentExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Custom Edit Component Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        editComponent: (props) => (
          <input type="text" value={props.value} onChange={(e) => props.onChange(e.target.value)} />
        ),
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowDelete: (rowData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(rowData), 3000)),
    }}
  />
);
CustomEditComponentExample.parameters = {
  docs: {
    description: {
      story: 'A simple editable implementation with a custom edit component for name field.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ManageAddEditFormResponsivenessExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Manage Add/Edit Form Responsiveness Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowDelete: (rowData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(rowData), 3000)),
    }}
    options={{
      editable: {
        add: {
          grid: {
            xs: 12,
            sm: 12,
            md: 12,
            lg: 12,
            xl: 12,
          },
          dialog: {
            maxWidth: 'xs',
            fullScreenBreakpoint: 'xs',
          },
        },
        edit: {
          dialog: {
            fullScreenBreakpoint: 'md',
          },
        },
        delete: {
          dialog: {
            maxWidth: 'md',
          },
        },
      },
    }}
  />
);
ManageAddEditFormResponsivenessExample.parameters = {
  docs: {
    description: {
      story:
        'Responsive design settings for Add/Edit/Delete Dialog can be configured via `options.editable` object. Here' +
        '<ul>' +
        '<li>Add dialog will fill the screen not more than to `xs`, will show one per row and go fullscreen on `xs`.</li>' +
        '<li>Edit dialog will switch to the full screen for `md` and below, remaining behaviour unchanged and follow the default configuration.</li>' +
        '<li>Delete dialog just will fill the screen up to `md`, remaining behaviour unchanged and follow the default configuration.</li>' +
        '</ul>',
    },
    source: {
      type: 'code',
    },
  },
};
