import * as React from 'react';

type FlatLocalization = {
  /**
   * Key value pair to localize body component
   */
  body: object;
  /**
   * @default No records to display
   */
  'body.emptyDataSourceMessage': string;
  /**
   * @default Add
   */
  'body.addTooltip': string;
  /**
   * @default Delete
   */
  'body.deleteTooltip': string;
  /**
   * @default Edit
   */
  'body.editTooltip': string;
  /**
   * Key value pair to localize filter row component
   */
  'body.filterRow': object;
  /**
   * @default
   */
  'body.filterRow.filterPlaceHolder': string;
  /**
   * @default Filter
   */
  'body.filterRow.filterTooltip': string;
  /**
   * Key value pair to localize edit row component
   */
  'body.editRow': object;
  /**
   * @default Are you sure to delete this row?
   */
  'body.editRow.deleteText': string;
  /**
   * @default Cancel
   */
  'body.editRow.cancelTooltip': string;
  /**
   * @default Save
   */
  'body.editRow.saveTooltip': string;
  /**
   * Key value pair to localize grouping component
   */
  grouping: object;
  /**
   * @default Drag headers ...
   */
  'grouping.placeholder': string;
  /**
   * @default Grouped By:
   */
  'grouping.groupedBy': string;
  /**
   * Key value pair to localize header component
   */
  header: object;
  /**
   * @default Actions
   */
  'header.actions': string;
  /**
   * Key value pair to localize row values
   */
  row: object;
  /**
   * @default Select
   */
  'row.selectTooltip': string;
  /**
   * @default Details
   */
  'row.detailPanelTooltip': string;
  /**
   * Key value pair to localize boolean type cell values
   */
  'row.boolean': object;
  /**
   * @default Yes
   */
  'row.boolean.true': string;
  /**
   * @default No
   */
  'row.boolean.false': string;
  /**
   * @default
   */
  'row.boolean.undefined': string;
  /**
   * Key value pair to localize pagination component
   */
  pagination: object;
  /**
   * @default {from}-{to} of {count}
   */
  'pagination.labelDisplayedRows': string;
  /**
   * @default rows
   */
  'pagination.labelRowsSelect': string;
  /**
   * @default Rows per page:
   */
  'pagination.labelRowsPerPage': string;
  /**
   * @default First Page
   */
  'pagination.firstAriaLabel': string;
  /**
   * @default First Page
   */
  'pagination.firstTooltip': string;
  /**
   * @default Previous Page
   */
  'pagination.previousAriaLabel': string;
  /**
   * @default Previous Page
   */
  'pagination.previousTooltip': string;
  /**
   * @default Next Page
   */
  'pagination.nextAriaLabel': string;
  /**
   * @default Next Page
   */
  'pagination.nextTooltip': string;
  /**
   * @default Last Page
   */
  'pagination.lastAriaLabel': string;
  /**
   * @default Last Page
   */
  'pagination.lastTooltip': string;
  /**
   * @default All
   */
  'pagination.pageSizeAll': string;
  /**
   * Key value pair to localize toolbar component
   */
  toolbar: object;
  /**
   * @default Add or remove columns
   */
  'toolbar.addRemoveColumns': string;
  /**
   * @default {n} row(s) selected
   */
  'toolbar.nRowsSelected': string;
  /**
   * @default Add or remove columns
   */
  'toolbar.showColumnsTitle': string;
  /**
   * @default Add or remove columns
   */
  'toolbar.showColumnsAriaLabel': string;
  /**
   * @default Export
   */
  'toolbar.exportTitle': string;
  /**
   * @default Export
   */
  'toolbar.exportAriaLabel': string;
  /**
   * @default Export as CSV
   */
  'toolbar.exportAsCsv': string;
  /**
   * @default Export as PDF
   */
  'toolbar.exportAsPdf': string;
  /**
   * @default
   */
  'toolbar.searchLabel': string;
  /**
   * @default
   */
  'toolbar.searchTooltip': string;
  /**
   * @default Search
   */
  'toolbar.searchPlaceholder': string;
  /**
   * Key value pair to localize built in filters
   */
  filters: object;
  /**
   * @default Add filter
   */
  'filters.addFilter': string;
  /**
   * @default Filter type
   */
  'filters.filterType': string;
  /**
   * @default from
   */
  'filters.from': string;
  /**
   * @default to
   */
  'filters.to': string;
  /**
   * @default Apply
   */
  'filters.apply': string;
  /**
   * @default Standard filter
   */
  'filters.standard': string;
  /**
   * @default Multiple choice filter
   */
  'filters.multiple-choice': string;
  /**
   * @default Single choice filter
   */
  'filters.single-choice': string;
  /**
   * @default Range filter
   */
  'filters.range': string;
  /**
   * @default Greater than filter
   */
  'filters.greater-than': string;
  /**
   * @default Less than filter
   */
  'filters.less-than': string;
};

export function LocalizationPropsSimulator(props: Partial<FlatLocalization>) {
  console.table(props);
  return React.useMemo(() => <React.Fragment />, []);
}
