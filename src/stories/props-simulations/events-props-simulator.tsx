import * as React from 'react';
import { ReactMaterialTableProps } from '../../react-material-table';

export function EventsPropsSimulator<RowData extends object = {}>(
  props: Partial<
    Pick<
      ReactMaterialTableProps<RowData>,
      | 'onQueryChangeEvent'
      | 'onRowsPerPageChange'
      | 'onPageChange'
      | 'onSelectionChange'
      | 'onSearchChange'
      | 'onChangeColumnHidden'
      | 'onColumnDragged'
      | 'onOrderChange'
      | 'onRowClick'
    >
  >,
) {
  console.table(props);
  return React.useMemo(() => <React.Fragment />, []);
}
