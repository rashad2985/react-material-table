import * as React from 'react';
import { Components } from '../../internal';

export function ComponentsPropsSimulator<RowData extends object = {}>(props: Partial<Components<RowData>>) {
  console.table(props);
  return React.useMemo(() => <React.Fragment />, []);
}
