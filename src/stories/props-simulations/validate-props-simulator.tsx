import * as React from 'react';

type ValidationPropsSimulatorProps<RowData extends object = {}> = {
  /**
   * Return a boolean to block/allow save operation and show red bar below that field.
   * @param rowData
   * @default true
   */
  boolean?: (rowData: Partial<RowData>) => boolean;
  /**
   * Return a non-empty string to block save operation and show red bar below that field and the string as error message.
   * @param rowData
   */
  string?: (rowData: Partial<RowData>) => string;
  /**
   * Return an object to block/allow save operation and in case of error show red bar below that field and provided helperText as error message.
   * @param rowData
   */
  object?: (rowData: Partial<RowData>) => { isValid: boolean; helperText?: string };
};

export function ValidationPropsSimulator<RowData extends object = {}>(props: ValidationPropsSimulatorProps<RowData>) {
  console.table(props);
  return React.useMemo(() => <React.Fragment />, []);
}
