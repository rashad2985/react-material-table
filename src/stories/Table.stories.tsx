import { Fab } from '@mui/material';
import { Meta, Story } from '@storybook/react/types-6-0';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { Add, MoreVert } from '@mui/icons-material';
import { columns, tableEntries, TableEntry } from './data';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { Description, Stories, Title } from '@storybook/addon-docs/blocks';

export default {
  title: 'Demo/Demo React Material Table',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <React.Fragment>
          <Title />
          <Description markdown="You can add buttons to rows or toolbar by using actions prop. Actions prop takes array of actions." />
          <Stories title="Examples" includePrimary={true} />
        </React.Fragment>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'canvas',
  },
} as Meta;

export const ReactMaterialTableAllInOneExample: Story<ReactMaterialTableProps<TableEntry>> = (props) => (
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable<TableEntry> {...props} />
  </LocalizationProvider>
);
ReactMaterialTableAllInOneExample.args = {
  title: 'Table',
  isLoading: false,
  columns,
  data: [
    ...tableEntries,
    ...tableEntries,
    ...tableEntries,
    ...tableEntries,
    ...tableEntries,
    ...tableEntries,
    ...tableEntries,
    ...tableEntries,
    ...tableEntries,
    ...tableEntries,
    ...tableEntries,
    ...tableEntries,
    ...tableEntries.slice(0, 3),
  ],
  onRowClick: (event, rowData, togglePanel) => togglePanel?.(),
  options: {
    searchText: '',
    draggable: true,
    initialPage: 0,
    showTitle: false,
    searchFieldAlignment: 'left',
    toolbarButtonAlignment: 'right',
    selection: true,
    padding: 'default',
    paging: true,
    pageSizeAll: true,
    pageSizeOptions: [5, 10, 20],
    pageSize: 5,
    paginationType: 'normal',
    emptyRowsWhenPaging: true,
    filtering: true,
    columnsButton: true,
    exportButton: true,
    loadingType: 'skeleton',
    detailPanelType: 'multiple',
    detailPanelColumnAlignment: 'left',
    actionsColumnIndex: columns.length,
    editable: {
      add: {
        grid: {
          xs: 12,
          sm: 6,
          md: 4,
        },
        dialog: {
          maxWidth: 'lg',
          fullScreenBreakpoint: 'xs',
        },
      },
      edit: {
        grid: {
          xs: 12,
          sm: 6,
          md: 4,
        },
        dialog: {
          maxWidth: 'lg',
          fullScreenBreakpoint: 'xs',
        },
      },
      delete: {
        grid: {},
        dialog: {
          maxWidth: 'sm',
          fullScreenBreakpoint: 'xs',
        },
      },
    },
    responsive: true,
  },
  localization: {
    row: {
      boolean: {
        true: 'Yes',
        false: 'No',
        undefined: 'Unknown',
      },
    },
    filters: {
      standard: 'Direct input',
    },
  },
  components: {
    Add: (showForm) => (
      <Fab onClick={showForm} color="primary" style={{ position: 'fixed', bottom: 64, right: 16 }}>
        <Add />
      </Fab>
    ),
  },
  actions: [
    {
      type: 'action-group',
      icon: MoreVert,
      tooltip: 'Group',
      actions: [
        {
          icon: 'save',
          tooltip: 'Save',
          onClick: (event, row, selectedRows) => alert(JSON.stringify({ row, selectedRows }, null, 2)),
        },
        {
          icon: 'delete',
          tooltip: 'Custom delete',
          onClick: (event, row, selectedRows) => alert(JSON.stringify({ row, selectedRows }, null, 2)),
        },
      ],
    },
    {
      type: 'action-group',
      icon: 'more_vert',
      tooltip: 'Group',
      actions: [
        {
          icon: 'save',
          tooltip: 'Save',
          onClick: (event, row, selectedRows) => alert(JSON.stringify({ row, selectedRows }, null, 2)),
        },
      ],
      isFreeAction: true,
    },
    {
      icon: 'save',
      tooltip: 'Simple',
      onClick: (event, row, selectedRows) => alert(JSON.stringify({ row, selectedRows }, null, 2)),
      position: 'toolbar',
    },
    {
      icon: 'edit',
      tooltip: 'Simple',
      onClick: (event, row, selectedRows) => alert(JSON.stringify({ row, selectedRows }, null, 2)),
      position: 'toolbarOnSelect',
    },
    {
      icon: 'delete',
      tooltip: 'Simple',
      onClick: (event, row, selectedRows) => alert(JSON.stringify({ row, selectedRows }, null, 2)),
      isFreeAction: true,
    },
  ],
  detailPanel: {
    tooltip: 'Show Name',
    render: (rowData) => {
      return (
        <div
          style={{
            fontSize: 100,
            textAlign: 'center',
            color: 'white',
            backgroundColor: '#43A047',
          }}
        >
          {rowData.name}
        </div>
      );
    },
  },
  editable: {
    onRowAddCancelled: () => {
      return;
    },
    onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
    onRowUpdateCancelled: () => {
      return;
    },
    onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
    onRowDelete: (rowData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(rowData), 3000)),
  },
};
