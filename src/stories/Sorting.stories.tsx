import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry } from './data';

export default {
  title: 'Features/Sorting',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table has sorting feature that lets users sort data by any column." />
          <Subheading>Usage</Subheading>
          <Description markdown="By default the sorting feature is enabled. It can be disable by setting `options.sorting` to `false`." />
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              
              <ReactMaterialTable
                // other props
                options={{
                  sorting: true
                }}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Hints</Subheading>
          <Description
            markdown={
              '* You can add a default sort value to a column by setting `column.defaultSort` with `asc` or `desc`.' +
              '\n* You can disable sorting for any column by setting `column.sorting` to `false`.' +
              '\n* You can change sorting algorithm by setting a function to `column.customSort`.'
            }
          />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const BasicSortingExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Basic Sorting Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      sorting: true,
    }}
  />
);
BasicSortingExample.parameters = {
  docs: {
    description: {
      story: 'To make sorting available `options.sorting` was set to `true`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const MultipleSortingExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Multiple Sorting Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 1, fat: 1, carbs: 1, protein: 1.0 },
      { name: 'Ice cream sandwich', calories: 2, fat: 2, carbs: 2, protein: 2.3 },
      { name: 'Eclair', calories: 3, fat: 3, carbs: 3, protein: 3.0 },
      { name: 'Cupcake', calories: 4, fat: 4, carbs: 4, protein: 4.3 },
      { name: 'Gingerbread', calories: 5, fat: 5, carbs: 5, protein: 5.9 },
      { name: 'Frozen yoghurt', calories: 6, fat: 1, carbs: 6, protein: 6.0 },
      { name: 'Ice cream sandwich', calories: 7, fat: 7, carbs: 2, protein: 7.3 },
      { name: 'Eclair', calories: 3, fat: 8, carbs: 8, protein: 8.0 },
      { name: 'Cupcake', calories: 9, fat: 9, carbs: 9, protein: 4.3 },
      { name: 'Gingerbread', calories: 10, fat: 10, carbs: 10, protein: 10.9 },
      { name: 'Frozen yoghurt', calories: 11, fat: 11, carbs: 11, protein: 11.0 },
      { name: 'Ice cream sandwich', calories: 12, fat: 12, carbs: 12, protein: 12.3 },
      { name: 'Eclair', calories: 13, fat: 13, carbs: 13, protein: 13.0 },
      { name: 'Cupcake', calories: 14, fat: 14, carbs: 14, protein: 14.3 },
      { name: 'Gingerbread', calories: 15, fat: 15, carbs: 15, protein: 15.9 },
    ]}
    options={{
      sorting: true,
    }}
  />
);
MultipleSortingExample.parameters = {
  docs: {
    description: {
      story:
        'Multiple sorting is always active and one can sort by multiple columns just holding `Shift` key an then pressing on wished columns to sort. Sorting order is the order of clicks.',
    },
    source: {
      type: 'code',
    },
  },
};

export const NonSortingFieldExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Non Sorting Field Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string', sorting: false },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      sorting: true,
    }}
  />
);
NonSortingFieldExample.parameters = {
  docs: {
    description: {
      story: 'Here, sorting is disabled for the field `name` by setting `column.sorting` to `false`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const InitiallySortedColumnExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Initially Sorted Column Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric', defaultSort: 'desc' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      sorting: true,
    }}
  />
);
InitiallySortedColumnExample.parameters = {
  docs: {
    description: {
      story: 'Here, the table is initially sorted by field `protein` descending.',
    },
    source: {
      type: 'code',
    },
  },
};

export const CustomSortingAlgorithmExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Custom Sorting Algorithm Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        customSort: (a, b) => (a?.name?.length ?? 0) - (b?.name?.length ?? 0),
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      sorting: true,
    }}
  />
);
CustomSortingAlgorithmExample.parameters = {
  docs: {
    description: {
      story:
        'Here, the algorithm of sorting for the `name` field was changed. New algorithm sorts the values by length of string.',
    },
    source: {
      type: 'code',
    },
  },
};
