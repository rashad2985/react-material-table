import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Props, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry } from './data';
import { Button, Chip, LinearProgress } from '@mui/material';
import { Toolbar } from '../components';
import { ComponentsPropsSimulator } from './props-simulations/components-props-simulator';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';

export default {
  title: 'Features/Component Overriding',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table has many components inside itself. You can change some of them according to your needs." />
          <Subheading>Usage</Subheading>
          <Description markdown="In this usage developer changes `backgroundColor` of Toolbar component." />
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              import { Toolbar } from '@trautmann/react-material-table/components';
              
              <ReactMaterialTable
                // other props
                components={{
                  Toolbar: props => (
                    <div style={{ backgroundColor: '#e8eaf5' }}>
                      <Toolbar {...props} />
                    </div>
                  )
                }}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>List of Components</Subheading>
          <Description markdown="You can override these components:" />
          <Props sort="requiredFirst" of={ComponentsPropsSimulator} />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const ToolbarOverridingExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Toolbar Overriding Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    components={{
      Toolbar: (props) => (
        <div>
          <Toolbar {...props} />
          <div style={{ padding: '0px 10px' }}>
            <Chip label="Chip 1" color="secondary" style={{ marginRight: 5 }} />
            <Chip label="Chip 2" color="secondary" style={{ marginRight: 5 }} />
            <Chip label="Chip 3" color="secondary" style={{ marginRight: 5 }} />
            <Chip label="Chip 4" color="secondary" style={{ marginRight: 5 }} />
            <Chip label="Chip 5" color="secondary" style={{ marginRight: 5 }} />
          </div>
        </div>
      ),
    }}
  />
);
ToolbarOverridingExample.parameters = {
  docs: {
    description: {
      story: 'In this example, a new line added to toolbar.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ActionOverridingExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Action Overriding Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    actions={[
      {
        icon: 'save',
        tooltip: 'Save User',
        onClick: (event, rowData) => alert('You saved ' + rowData.name),
      },
    ]}
    components={{
      Action: (props) => (
        <Button
          onClick={(event) => props.onClick(event, props.data)}
          color="primary"
          variant="contained"
          style={{ textTransform: 'none' }}
          size="small"
        >
          My Button
        </Button>
      ),
    }}
  />
);
ActionOverridingExample.parameters = {
  docs: {
    description: {
      story: 'In this example, table renders a normal button instead of icon button for an action.',
    },
    source: {
      type: 'code',
    },
  },
};

export const EmptyRowOverridingExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Empty Row Overriding Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[{ name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 }]}
    components={{
      EmptyRow: ({ colSpan }) => (
        <TableRow>
          <TableCell variant="body" colSpan={colSpan} align="center">
            Empty row
          </TableCell>
        </TableRow>
      ),
    }}
  />
);
EmptyRowOverridingExample.parameters = {
  docs: {
    description: {
      story:
        'In this example, table renders a custom defined empty rows to fill the entries. This is very useful to fix the changing table height, if rows with data are rendered differently than empty rows, e.g. height of each row is more than it should be because of the image in the row etc.',
    },
    source: {
      type: 'code',
    },
  },
};

export const LoadingRowOverridingExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Loading Row Overriding Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[]}
    isLoading={true}
    components={{
      LoadingRow: ({ colSpan }) => (
        <TableRow>
          {new Array(colSpan).fill(0).map((_, cellIndex) => (
            <TableCell key={`loading-cell-${cellIndex}`} variant="body">
              <LinearProgress />
            </TableCell>
          ))}
        </TableRow>
      ),
    }}
  />
);
LoadingRowOverridingExample.parameters = {
  docs: {
    description: {
      story:
        'In this example, table renders a custom defined rows on loading state to fill the entries. This is very useful to fix the changing table height, if rows with data are rendered differently than empty rows, e.g. height of each row is more than it should be because of the image in the row etc., or showing skeleton without setting loading type to skeleton or something else. In this example, Linear Progress components were shown to demonstrate this.',
    },
    source: {
      type: 'code',
    },
  },
};
