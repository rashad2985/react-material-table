import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Props, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { TableEntry } from './data';
import { LocalizationPropsSimulator } from './props-simulations/localization-props-simulator';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';

export default {
  title: 'Features/Localization',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="You can use your own texts to localize react-material-table." />
          <Subheading>Usage</Subheading>
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              
              <ReactMaterialTable
                // other props
                localization={{
                  pagination: {
                    labelDisplayedRows: '{from}-{to} of {count}'
                  },
                  toolbar: {
                    nRowsSelected: '{n} row(s) selected'
                  },
                  header: {
                    actions: 'Actions'
                  },
                  body: {
                    emptyDataSourceMessage: 'No records to display',
                    filterRow: {
                      filterTooltip: 'Filter'
                    }
                  }
                }}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Localization Fields</Subheading>
          <Description markdown="Localization settings could be used to change/translate default texts of datatable." />
          <Props sort="requiredFirst" of={LocalizationPropsSimulator} />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const GermanTranslatedTableExample: Story<ReactMaterialTableProps<TableEntry>> = () => (
  <ReactMaterialTable
    title="Beispiel für eine deutsch übersetzte Tabelle"
    columns={[
      { title: 'Dessert (100g Portion)', field: 'name', type: 'string' },
      { title: 'Kalorien', field: 'calories', type: 'numeric' },
      { title: 'Fett (g)', field: 'fat', type: 'numeric' },
      { title: 'Eiweiß (g)', field: 'protein', type: 'numeric' },
      { title: 'Empfohlen', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Gefrorener Joghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Eiscreme-Sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Lebkuchen', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: 'Lebkuchen Lite', calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      pageSizeAll: true,
    }}
    localization={{
      body: {
        emptyDataSourceMessage: 'Keine Einträge',
        addTooltip: 'Hinzufügen',
        deleteTooltip: 'Löschen',
        editTooltip: 'Bearbeiten',
        filterRow: {
          filterTooltip: 'Filter',
        },
        editRow: {
          deleteText: 'Diese Zeile wirklich löschen?',
          cancelTooltip: 'Abbrechen',
          saveTooltip: 'Speichern',
        },
      },
      grouping: {
        placeholder: 'Spalten ziehen ...',
        groupedBy: 'Gruppiert nach:',
      },
      header: {
        actions: 'Aktionen',
      },
      pagination: {
        labelDisplayedRows: '{from}-{to} von {count}',
        labelRowsSelect: 'Zeilen',
        labelRowsPerPage: 'Zeilen pro Seite:',
        firstAriaLabel: 'Erste Seite',
        firstTooltip: 'Erste Seite',
        previousAriaLabel: 'Vorherige Seite',
        previousTooltip: 'Vorherige Seite',
        nextAriaLabel: 'Nächste Seite',
        nextTooltip: 'Nächste Seite',
        lastAriaLabel: 'Letzte Seite',
        lastTooltip: 'Letzte Seite',
        pageSizeAll: 'Alle',
      },
      toolbar: {
        addRemoveColumns: 'Spalten hinzufügen oder löschen',
        nRowsSelected: '{0} Zeile(n) ausgewählt',
        showColumnsTitle: 'Zeige Spalten',
        showColumnsAriaLabel: 'Zeige Spalten',
        exportTitle: 'Export',
        exportAriaLabel: 'Export',
        exportAsCsv: 'Export als CSV',
        exportAsPdf: 'Export als PDF',
        searchLabel: 'Suche',
        searchTooltip: 'Suche',
        searchPlaceholder: 'Suchen',
      },
      row: {
        boolean: {
          false: 'Nein',
          true: 'Ja',
        },
      },
    }}
  />
);
GermanTranslatedTableExample.parameters = {
  docs: {
    description: {
      story: 'A table with some text translated to German language.',
    },
    source: {
      type: 'code',
    },
  },
};

export const FrenchTranslatedTableExample: Story<ReactMaterialTableProps<TableEntry>> = () => (
  <ReactMaterialTable
    title="Visualisation du tableau en Français"
    columns={[
      { title: 'Dessert (portion de 100g)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Graisse (g)', field: 'fat', type: 'numeric' },
      { title: 'Protéine (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommandé', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Yaourt glacé', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Sandwich à la crème glacée', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Éclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Petit gâteau', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: "Pain d'épices", calories: 356, fat: 16.0, protein: 3.9, recommended: false },
      { name: "Pain d'épices Lite", calories: 300, fat: 12.0, protein: 5, recommended: true },
    ]}
    options={{
      pageSizeAll: true,
    }}
    localization={{
      body: {
        emptyDataSourceMessage: "Pas d'enregistreent à afficher",
        addTooltip: 'Ajouter',
        deleteTooltip: 'Supprimer',
        editTooltip: 'Editer',
        filterRow: {
          filterTooltip: 'Filtrer',
        },
        editRow: {
          deleteText: 'Voulez-vous supprimer cette ligne?',
          cancelTooltip: 'Annuler',
          saveTooltip: 'Enregistrer',
        },
      },
      grouping: {
        placeholder: "Tirer l'entête ...",
        groupedBy: 'Grouper par:',
      },
      header: {
        actions: 'Actions',
      },
      pagination: {
        labelDisplayedRows: '{from}-{to} de {count}',
        labelRowsSelect: 'lignes',
        labelRowsPerPage: 'lignes par page:',
        firstAriaLabel: 'Première page',
        firstTooltip: 'Première page',
        previousAriaLabel: 'Page précédente',
        previousTooltip: 'Page précédente',
        nextAriaLabel: 'Page suivante',
        nextTooltip: 'Page suivante',
        lastAriaLabel: 'Dernière page',
        lastTooltip: 'Dernière page',
        pageSizeAll: 'Tous',
      },
      toolbar: {
        addRemoveColumns: 'Ajouter ou supprimer des colonnes',
        nRowsSelected: '{0} ligne(s) sélectionée(s)',
        showColumnsTitle: 'Voir les colonnes',
        showColumnsAriaLabel: 'Voir les colonnes',
        exportTitle: 'Exporter',
        exportAriaLabel: 'Exporter',
        exportAsCsv: 'Exporter en CSV',
        exportAsPdf: 'Exporter en PDF',
        searchLabel: 'Chercher',
        searchTooltip: 'Chercher',
        searchPlaceholder: 'Chercher',
      },
      row: {
        boolean: {
          false: 'Non',
          true: 'Oui',
        },
      },
    }}
  />
);
FrenchTranslatedTableExample.parameters = {
  docs: {
    description: {
      story: 'A table with some text translated to French language.',
    },
    source: {
      type: 'code',
    },
  },
};

export const GermanFormattedNumbersCurrenciesAndTimestampsExample: Story<
  ReactMaterialTableProps<Omit<TableEntry, 'fat' | 'protein' | 'carbs'>>
> = () => (
  // Here we have date/time/datetime type fields
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      title="German Formatted Numbers Currencies and Timestamps Preview"
      columns={[
        { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
        {
          title: 'Calories',
          field: 'calories',
          type: 'numeric',
          numberSettings: {
            locale: 'de',
            formatOptions: {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            },
          },
        },
        {
          title: 'Date',
          field: 'date',
          type: 'date',
          dateSetting: {
            locale: 'de',
            formatOptions: {
              day: 'numeric',
              month: 'numeric',
              year: 'numeric',
            },
          },
        },
        {
          title: 'Time',
          field: 'time',
          type: 'time',
          dateSetting: {
            locale: 'de',
            formatOptions: {
              hour: '2-digit',
              minute: '2-digit',
            },
          },
        },
        {
          title: 'Date & Time',
          field: 'datetime',
          type: 'datetime',
          dateSetting: {
            locale: 'de',
            formatOptions: {
              day: 'numeric',
              month: 'numeric',
              year: 'numeric',
              hour: '2-digit',
              minute: '2-digit',
            },
          },
        },
        {
          title: 'Price',
          field: 'price',
          type: 'currency',
          currencySetting: {
            locale: 'de',
            formatOptions: {
              currency: 'EUR',
              maximumFractionDigits: 2,
              minimumFractionDigits: 2,
            },
          },
        },
      ]}
      data={[
        {
          name: 'Frozen yoghurt',
          calories: 159,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          price: 3,
        },
        {
          name: 'Ice cream sandwich',
          calories: 237,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          price: 5.8,
        },
        {
          name: 'Eclair',
          calories: 262,
          date: new Date(2021, 1, 15, 12, 45, 25, 500),
          time: new Date(2021, 1, 15, 12, 45, 25, 500),
          datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
          recommended: false,
          price: 0.999,
        },
        {
          name: 'Cupcake',
          calories: 305,
          date: new Date(2021, 6, 5, 8, 30, 35, 800),
          time: new Date(2021, 6, 5, 8, 30, 35, 800),
          datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
          price: 1.4999,
        },
        {
          name: 'Gingerbread',
          calories: 356,
          date: new Date(2020, 11, 23, 9, 58, 45, 999),
          time: new Date(2020, 11, 23, 9, 58, 45, 999),
          datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
          price: 7,
        },
        {
          name: 'Gingerbread',
          calories: 358,
          date: new Date(2020, 9, 14, 9, 58, 55, 888),
          time: new Date(2020, 9, 14, 9, 58, 55, 888),
          datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
          price: 100,
        },
      ]}
    />
  </LocalizationProvider>
);
GermanFormattedNumbersCurrenciesAndTimestampsExample.parameters = {
  docs: {
    description: {
      story:
        "In this example, numeric/date/time/date-time/currency type cell values are formatted with German Locale Options. Only providing the `locale: 'de'` is enough for the standard locale formatting. In example, also customizing of the format values are demonstrated via additional `formatOptions` object.",
    },
    source: {
      type: 'code',
    },
  },
};

export const USFormattedNumbersCurrenciesAndTimestampsExample: Story<
  ReactMaterialTableProps<Omit<TableEntry, 'fat' | 'protein' | 'carbs'>>
> = () => (
  // Here we have date/time/datetime type fields
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      title="US Formatted Numbers Currencies and Timestamps Preview"
      columns={[
        { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
        {
          title: 'Calories',
          field: 'calories',
          type: 'numeric',
          numberSettings: {
            locale: 'en-US',
            formatOptions: {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            },
          },
        },
        {
          title: 'Date',
          field: 'date',
          type: 'date',
          dateSetting: {
            locale: 'en-US',
            formatOptions: {
              day: 'numeric',
              month: 'numeric',
              year: 'numeric',
            },
          },
        },
        {
          title: 'Time',
          field: 'time',
          type: 'time',
          dateSetting: {
            locale: 'en-US',
            formatOptions: {
              hour: 'numeric',
              minute: 'numeric',
            },
          },
        },
        {
          title: 'Date & Time',
          field: 'datetime',
          type: 'datetime',
          dateSetting: {
            locale: 'en-US',
            formatOptions: {
              day: 'numeric',
              month: 'numeric',
              year: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
            },
          },
        },
        {
          title: 'Price',
          field: 'price',
          type: 'currency',
          currencySetting: {
            locale: 'en-US',
            formatOptions: {
              currency: 'USD',
              maximumFractionDigits: 2,
              minimumFractionDigits: 2,
            },
          },
        },
      ]}
      data={[
        {
          name: 'Frozen yoghurt',
          calories: 159,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          price: 3,
        },
        {
          name: 'Ice cream sandwich',
          calories: 237,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          price: 5.8,
        },
        {
          name: 'Eclair',
          calories: 262,
          date: new Date(2021, 1, 15, 12, 45, 25, 500),
          time: new Date(2021, 1, 15, 12, 45, 25, 500),
          datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
          price: 0.999,
        },
        {
          name: 'Cupcake',
          calories: 305,
          date: new Date(2021, 6, 5, 8, 30, 35, 800),
          time: new Date(2021, 6, 5, 8, 30, 35, 800),
          datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
          price: 1.4999,
        },
        {
          name: 'Gingerbread',
          calories: 356,
          date: new Date(2020, 11, 23, 9, 58, 45, 999),
          time: new Date(2020, 11, 23, 9, 58, 45, 999),
          datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
          price: 7,
        },
        {
          name: 'Gingerbread',
          calories: 358,
          date: new Date(2020, 9, 14, 9, 58, 55, 888),
          time: new Date(2020, 9, 14, 9, 58, 55, 888),
          datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
          price: 100,
        },
      ]}
    />
  </LocalizationProvider>
);
USFormattedNumbersCurrenciesAndTimestampsExample.parameters = {
  docs: {
    description: {
      story:
        "In this example, numeric/date/time/date-time/currency type cell values are formatted with US Locale Options. Only providing the `locale: 'de'` is enough for the standard locale formatting. In example, also customizing of the format values are demonstrated via additional `formatOptions` object.",
    },
    source: {
      type: 'code',
    },
  },
};
