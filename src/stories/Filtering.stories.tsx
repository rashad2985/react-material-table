import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { TableEntry } from './data';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';

export default {
  title: 'Features/Filtering',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table offers a rich range of built-in filters for different type of fields." />
          <Subheading>Usage</Subheading>
          <Description markdown="To make filtering available, `options.filtering` should be enabled." />
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              
              <ReactMaterialTable
                // other props
                options={{
                  filtering: true
                }}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Hints</Subheading>
          <Description
            markdown={
              '* You can add a default filter value to a column by setting `column.defaultFilter`.' +
              '\n* You can disable filtering for any column by setting `column.filtering` to `false`.' +
              '\n* You can change filter icon with overriding `icons.Filter`' +
              '\n* You can apply style to all filter inputs with `options.filterCellStyle`' +
              '\n* You can apply style for a specified column with `column.filterCellStyle`'
            }
          />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const BasicFilteringExample: Story<
  ReactMaterialTableProps<Omit<TableEntry & { multiName: Array<string> }, 'fat' | 'protein' | 'carbs'>>
> = () => (
  // Here we have date/time/datetime type fields
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      title="Basic Filtering Preview"
      columns={[
        { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
        {
          title: 'Multi-Dessert (each 100g serving)',
          field: 'multiName',
          type: 'multi-string',
          lookup: new Map<string, string>([
            ['E', 'Eclair'],
            ['C', 'Cupcake'],
            ['G', 'Gingerbread'],
          ]),
        },
        { title: 'Calories', field: 'calories', type: 'numeric' },
        { title: 'Date', field: 'date', type: 'date' },
        { title: 'Time', field: 'time', type: 'time' },
        { title: 'Date & Time', field: 'datetime', type: 'datetime' },
        { title: 'Recommended', field: 'recommended', type: 'boolean' },
        {
          title: 'Price',
          field: 'price',
          type: 'currency',
          currencySetting: { locale: 'en', formatOptions: { currency: 'USD' } },
        },
      ]}
      data={[
        {
          name: 'Frozen yoghurt',
          multiName: ['E', 'G'],
          calories: 159,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          recommended: true,
          price: 2.99,
        },
        {
          name: 'Ice cream sandwich',
          multiName: ['E', 'G'],
          calories: 237,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          recommended: true,
          price: 5.99,
        },
        {
          name: 'Eclair',
          multiName: ['E'],
          calories: 262,
          date: new Date(2021, 1, 15, 12, 45, 25, 500),
          time: new Date(2021, 1, 15, 12, 45, 25, 500),
          datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
          recommended: false,
          price: 0.99,
        },
        {
          name: 'Cupcake',
          multiName: ['E', 'C'],
          calories: 305,
          date: new Date(2021, 6, 5, 8, 30, 35, 800),
          time: new Date(2021, 6, 5, 8, 30, 35, 800),
          datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
          recommended: false,
          price: 1.4999,
        },
        {
          name: 'Gingerbread',
          multiName: ['C', 'G'],
          calories: 356,
          date: new Date(2020, 11, 23, 9, 58, 45, 999),
          time: new Date(2020, 11, 23, 9, 58, 45, 999),
          datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
          recommended: true,
          price: 7,
        },
        {
          name: 'Gingerbread',
          multiName: ['C', 'G'],
          calories: 358,
          date: new Date(2020, 9, 14, 9, 58, 55, 888),
          time: new Date(2020, 9, 14, 9, 58, 55, 888),
          datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
          recommended: true,
          price: 100,
        },
      ]}
      options={{
        filtering: true,
      }}
    />
  </LocalizationProvider>
);
BasicFilteringExample.parameters = {
  docs: {
    description: {
      story: 'To make filtering available `options.filtering` is being set to `true`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const NonFilteringFieldExample: Story<
  ReactMaterialTableProps<Omit<TableEntry, 'fat' | 'protein' | 'carbs'>>
> = () => (
  // Here we have date/time/datetime type fields
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      title="Non Filtering Field Preview"
      columns={[
        { title: 'Dessert (100g serving)', field: 'name', type: 'string', filtering: false },
        { title: 'Calories', field: 'calories', type: 'numeric' },
        { title: 'Date', field: 'date', type: 'date' },
        { title: 'Time', field: 'time', type: 'time' },
        { title: 'Date & Time', field: 'datetime', type: 'datetime' },
        { title: 'Recommended', field: 'recommended', type: 'boolean' },
        {
          title: 'Price',
          field: 'price',
          type: 'currency',
          currencySetting: { locale: 'en', formatOptions: { currency: 'USD' } },
        },
      ]}
      data={[
        {
          name: 'Frozen yoghurt',
          calories: 159,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          recommended: true,
          price: 2.99,
        },
        {
          name: 'Ice cream sandwich',
          calories: 237,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          recommended: true,
          price: 5.99,
        },
        {
          name: 'Eclair',
          calories: 262,
          date: new Date(2021, 1, 15, 12, 45, 25, 500),
          time: new Date(2021, 1, 15, 12, 45, 25, 500),
          datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
          recommended: false,
          price: 0.99,
        },
        {
          name: 'Cupcake',
          calories: 305,
          date: new Date(2021, 6, 5, 8, 30, 35, 800),
          time: new Date(2021, 6, 5, 8, 30, 35, 800),
          datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
          recommended: false,
          price: 1.4999,
        },
        {
          name: 'Gingerbread',
          calories: 356,
          date: new Date(2020, 11, 23, 9, 58, 45, 999),
          time: new Date(2020, 11, 23, 9, 58, 45, 999),
          datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
          recommended: true,
          price: 7,
        },
        {
          name: 'Gingerbread',
          calories: 358,
          date: new Date(2020, 9, 14, 9, 58, 55, 888),
          time: new Date(2020, 9, 14, 9, 58, 55, 888),
          datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
          recommended: true,
          price: 100,
        },
      ]}
      options={{
        filtering: true,
      }}
    />
  </LocalizationProvider>
);
NonFilteringFieldExample.parameters = {
  docs: {
    description: {
      story: '`name` field marked as non-filter by setting `filtering` field of specified column to `false`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const StandardFilteringWithLookupExample: Story<
  ReactMaterialTableProps<Omit<TableEntry, 'fat' | 'protein' | 'carbs'>>
> = () => (
  // Here we have date/time/datetime type fields
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      title="Standard Filtering with Lookup Preview"
      columns={[
        {
          title: 'Dessert (100g serving)',
          field: 'name',
          type: 'string',
          lookup: new Map<string, string>([
            ['Frozen yoghurt', 'Frozen yoghurt'],
            ['Ice cream sandwich', 'Ice cream sandwich'],
          ]),
        },
        {
          title: 'Calories',
          field: 'calories',
          type: 'numeric',
          lookup: new Map<string, string>([
            ['159', '159'],
            ['237', '237'],
            ['262', 'two hundred sixty two'],
          ]),
        },
        { title: 'Date', field: 'date', type: 'date' },
        { title: 'Time', field: 'time', type: 'time' },
        { title: 'Date & Time', field: 'datetime', type: 'datetime' },
        { title: 'Recommended', field: 'recommended', type: 'boolean' },
        {
          title: 'Price',
          field: 'price',
          type: 'currency',
          currencySetting: { locale: 'en', formatOptions: { currency: 'USD' } },
        },
      ]}
      data={[
        {
          name: 'Frozen yoghurt',
          calories: 159,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          recommended: true,
          price: 2.99,
        },
        {
          name: 'Ice cream sandwich',
          calories: 237,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          recommended: true,
          price: 5.99,
        },
        {
          name: 'Eclair',
          calories: 262,
          date: new Date(2021, 1, 15, 12, 45, 25, 500),
          time: new Date(2021, 1, 15, 12, 45, 25, 500),
          datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
          recommended: false,
          price: 0.99,
        },
        {
          name: 'Cupcake',
          calories: 305,
          date: new Date(2021, 6, 5, 8, 30, 35, 800),
          time: new Date(2021, 6, 5, 8, 30, 35, 800),
          datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
          recommended: false,
          price: 1.4999,
        },
        {
          name: 'Gingerbread',
          calories: 356,
          date: new Date(2020, 11, 23, 9, 58, 45, 999),
          time: new Date(2020, 11, 23, 9, 58, 45, 999),
          datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
          recommended: true,
          price: 7,
        },
        {
          name: 'Gingerbread',
          calories: 358,
          date: new Date(2020, 9, 14, 9, 58, 55, 888),
          time: new Date(2020, 9, 14, 9, 58, 55, 888),
          datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
          recommended: true,
          price: 100,
        },
      ]}
      options={{
        filtering: true,
      }}
    />
  </LocalizationProvider>
);
StandardFilteringWithLookupExample.parameters = {
  docs: {
    description: {
      story:
        'To make standard input field for string and numeric types to show up like a combobox/selection, `column.lookup` property can be used.',
    },
    source: {
      type: 'code',
    },
  },
};

export const CustomFilteringAlgorithmExample: Story<
  ReactMaterialTableProps<Omit<TableEntry, 'fat' | 'protein' | 'carbs'>>
> = () => (
  // Here we have date/time/datetime type fields
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      title="Custom Filtering Algorithm Preview"
      columns={[
        {
          title: 'Dessert (100g serving)',
          field: 'name',
          type: 'string',
          customFilterAndSearch: (term, rowData) => Number(term) === (rowData?.name ?? '').length,
        },
        { title: 'Calories', field: 'calories', type: 'numeric' },
        { title: 'Date', field: 'date', type: 'date' },
        { title: 'Time', field: 'time', type: 'time' },
        { title: 'Date & Time', field: 'datetime', type: 'datetime' },
        { title: 'Recommended', field: 'recommended', type: 'boolean' },
        {
          title: 'Price',
          field: 'price',
          type: 'currency',
          currencySetting: { locale: 'en', formatOptions: { currency: 'USD' } },
        },
      ]}
      data={[
        {
          name: 'Frozen yoghurt',
          calories: 159,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          recommended: true,
          price: 2.99,
        },
        {
          name: 'Ice cream sandwich',
          calories: 237,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          recommended: true,
          price: 5.99,
        },
        {
          name: 'Eclair',
          calories: 262,
          date: new Date(2021, 1, 15, 12, 45, 25, 500),
          time: new Date(2021, 1, 15, 12, 45, 25, 500),
          datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
          recommended: false,
          price: 0.99,
        },
        {
          name: 'Cupcake',
          calories: 305,
          date: new Date(2021, 6, 5, 8, 30, 35, 800),
          time: new Date(2021, 6, 5, 8, 30, 35, 800),
          datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
          recommended: false,
          price: 1.4999,
        },
        {
          name: 'Gingerbread',
          calories: 356,
          date: new Date(2020, 11, 23, 9, 58, 45, 999),
          time: new Date(2020, 11, 23, 9, 58, 45, 999),
          datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
          recommended: true,
          price: 7,
        },
        {
          name: 'Gingerbread',
          calories: 358,
          date: new Date(2020, 9, 14, 9, 58, 55, 888),
          time: new Date(2020, 9, 14, 9, 58, 55, 888),
          datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
          recommended: true,
          price: 100,
        },
      ]}
      options={{
        filtering: true,
      }}
    />
  </LocalizationProvider>
);
CustomFilteringAlgorithmExample.parameters = {
  docs: {
    description: {
      story:
        'Changing algorithm of standard filtering for `name` field. It expects length of cell data. Type `6` in Standard filter type input field to find the third row with name Eclair.',
    },
    source: {
      type: 'code',
    },
  },
};

export const DisableStandardFilteringExample: Story<
  ReactMaterialTableProps<Omit<TableEntry, 'fat' | 'protein' | 'carbs'>>
> = () => (
  // Here we have date/time/datetime type fields
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      title="Disable Standard Filtering Preview"
      columns={[
        { title: 'Dessert (100g serving)', field: 'name', type: 'string', useStandardFilter: false },
        { title: 'Calories', field: 'calories', type: 'numeric' },
        { title: 'Date', field: 'date', type: 'date' },
        { title: 'Time', field: 'time', type: 'time' },
        { title: 'Date & Time', field: 'datetime', type: 'datetime' },
        { title: 'Recommended', field: 'recommended', type: 'boolean' },
        {
          title: 'Price',
          field: 'price',
          type: 'currency',
          currencySetting: { locale: 'en', formatOptions: { currency: 'USD' } },
        },
      ]}
      data={[
        {
          name: 'Frozen yoghurt',
          calories: 159,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          recommended: true,
          price: 2.99,
        },
        {
          name: 'Ice cream sandwich',
          calories: 237,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          recommended: true,
          price: 5.99,
        },
        {
          name: 'Eclair',
          calories: 262,
          date: new Date(2021, 1, 15, 12, 45, 25, 500),
          time: new Date(2021, 1, 15, 12, 45, 25, 500),
          datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
          recommended: false,
          price: 0.99,
        },
        {
          name: 'Cupcake',
          calories: 305,
          date: new Date(2021, 6, 5, 8, 30, 35, 800),
          time: new Date(2021, 6, 5, 8, 30, 35, 800),
          datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
          recommended: false,
          price: 1.4999,
        },
        {
          name: 'Gingerbread',
          calories: 356,
          date: new Date(2020, 11, 23, 9, 58, 45, 999),
          time: new Date(2020, 11, 23, 9, 58, 45, 999),
          datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
          recommended: true,
          price: 7,
        },
        {
          name: 'Gingerbread',
          calories: 358,
          date: new Date(2020, 9, 14, 9, 58, 55, 888),
          time: new Date(2020, 9, 14, 9, 58, 55, 888),
          datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
          recommended: true,
          price: 100,
        },
      ]}
      options={{
        filtering: true,
      }}
    />
  </LocalizationProvider>
);
DisableStandardFilteringExample.parameters = {
  docs: {
    description: {
      story:
        'Standard filter can also be disabled for a column by setting `column.useStandardFilter` to `false`. Here, standard filter was disabled for `name` field of the column.',
    },
    source: {
      type: 'code',
    },
  },
};

export const DisableBuiltInFilteringExample: Story<
  ReactMaterialTableProps<Omit<TableEntry, 'fat' | 'protein' | 'carbs'>>
> = () => (
  // Here we have date/time/datetime type fields
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      title="Disable Built-In Filtering Preview"
      columns={[
        { title: 'Dessert (100g serving)', field: 'name', type: 'string', useBuiltInFilters: false },
        { title: 'Calories', field: 'calories', type: 'numeric' },
        { title: 'Date', field: 'date', type: 'date' },
        { title: 'Time', field: 'time', type: 'time' },
        { title: 'Date & Time', field: 'datetime', type: 'datetime' },
        { title: 'Recommended', field: 'recommended', type: 'boolean' },
        {
          title: 'Price',
          field: 'price',
          type: 'currency',
          currencySetting: { locale: 'en', formatOptions: { currency: 'USD' } },
        },
      ]}
      data={[
        {
          name: 'Frozen yoghurt',
          calories: 159,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          recommended: true,
          price: 2.99,
        },
        {
          name: 'Ice cream sandwich',
          calories: 237,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          recommended: true,
          price: 5.99,
        },
        {
          name: 'Eclair',
          calories: 262,
          date: new Date(2021, 1, 15, 12, 45, 25, 500),
          time: new Date(2021, 1, 15, 12, 45, 25, 500),
          datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
          recommended: false,
          price: 0.99,
        },
        {
          name: 'Cupcake',
          calories: 305,
          date: new Date(2021, 6, 5, 8, 30, 35, 800),
          time: new Date(2021, 6, 5, 8, 30, 35, 800),
          datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
          recommended: false,
          price: 1.4999,
        },
        {
          name: 'Gingerbread',
          calories: 356,
          date: new Date(2020, 11, 23, 9, 58, 45, 999),
          time: new Date(2020, 11, 23, 9, 58, 45, 999),
          datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
          recommended: true,
          price: 7,
        },
        {
          name: 'Gingerbread',
          calories: 358,
          date: new Date(2020, 9, 14, 9, 58, 55, 888),
          time: new Date(2020, 9, 14, 9, 58, 55, 888),
          datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
          recommended: true,
          price: 100,
        },
      ]}
      options={{
        filtering: true,
      }}
    />
  </LocalizationProvider>
);
DisableBuiltInFilteringExample.parameters = {
  docs: {
    description: {
      story:
        'Built-in filters can be disabled for a column by setting `column.useBuiltInFilters` to `false`. Here, built-in filters were disabled for `name` field of the column.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ManagingBuiltInFilteringExample: Story<
  ReactMaterialTableProps<Omit<TableEntry, 'fat' | 'protein' | 'carbs'>>
> = () => (
  // Here we have date/time/datetime type fields
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      title="Managing Built-In Filtering Preview"
      columns={[
        { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
        { title: 'Calories', field: 'calories', type: 'numeric' },
        { title: 'Date', field: 'date', type: 'date' },
        { title: 'Time', field: 'time', type: 'time' },
        { title: 'Date & Time', field: 'datetime', type: 'datetime' },
        { title: 'Recommended', field: 'recommended', type: 'boolean' },
        {
          title: 'Price',
          field: 'price',
          type: 'currency',
          currencySetting: { locale: 'en', formatOptions: { currency: 'USD' } },
        },
      ]}
      data={[
        {
          name: 'Frozen yoghurt',
          calories: 159,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          recommended: true,
          price: 2.99,
        },
        {
          name: 'Ice cream sandwich',
          calories: 237,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          recommended: true,
          price: 5.99,
        },
        {
          name: 'Eclair',
          calories: 262,
          date: new Date(2021, 1, 15, 12, 45, 25, 500),
          time: new Date(2021, 1, 15, 12, 45, 25, 500),
          datetime: new Date(2021, 1, 15, 12, 45, 25, 500),
          recommended: false,
          price: 0.99,
        },
        {
          name: 'Cupcake',
          calories: 305,
          date: new Date(2021, 6, 5, 8, 30, 35, 800),
          time: new Date(2021, 6, 5, 8, 30, 35, 800),
          datetime: new Date(2021, 6, 5, 8, 30, 35, 800),
          recommended: false,
          price: 1.4999,
        },
        {
          name: 'Gingerbread',
          calories: 356,
          date: new Date(2020, 11, 23, 9, 58, 45, 999),
          time: new Date(2020, 11, 23, 9, 58, 45, 999),
          datetime: new Date(2020, 11, 23, 9, 58, 45, 999),
          recommended: true,
          price: 7,
        },
        {
          name: 'Gingerbread',
          calories: 358,
          date: new Date(2020, 9, 14, 9, 58, 55, 888),
          time: new Date(2020, 9, 14, 9, 58, 55, 888),
          datetime: new Date(2020, 9, 14, 9, 58, 55, 888),
          recommended: true,
          price: 100,
        },
      ]}
      options={{
        filtering: true,
        builtInFilters: {
          string: ['single-choice'],
          'multi-string': ['multiple-choice'],
          numeric: ['range', 'less-than'],
          currency: ['greater-than'],
          boolean: ['standard'],
          date: ['range'],
          time: ['standard'],
          datetime: ['range'],
        },
      }}
    />
  </LocalizationProvider>
);
ManagingBuiltInFilteringExample.parameters = {
  docs: {
    description: {
      story:
        'Filtering can be configured in column type level by setting/changing mapping for `options.builtInFilters` object. Here, for all' +
        '<br/>`string` type columns always `single-choice` built-in filtering;' +
        '<br/>`numeric` type columns always `range` and `less-than` built-in filtering;' +
        '<br/>`currency` type columns always `greater-than` built-in filtering;' +
        '<br/>`boolean` type columns always `standard` built-in filtering;' +
        '<br/>`date` type columns always `range` built-in filtering;' +
        '<br/>`time` type columns always `standard` built-in filtering;' +
        '<br/>`datetime` type columns always `range` built-in filtering;' +
        '<br/>was configured.',
    },
    source: {
      type: 'code',
    },
  },
};
