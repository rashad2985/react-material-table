import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Props, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry } from './data';
import { EventsPropsSimulator } from './props-simulations/events-props-simulator';

export default {
  title: 'Features/Events',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table allows the user to implement callbacks for available events. Using this functionality, user can trigger additional method calls on table events." />
          <Subheading>Usage</Subheading>
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';

              <ReactMaterialTable
                // other props
                onSelectionChange={(selectedRows, changedRow) => {
                  /* do something with rows or changed row */
                  return;
                }}
                onRowsPerPageChange={(pageSize: number) => {
                  /* do something with pageSize */
                  return;
                }}
                // other events
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Available Events</Subheading>
          <Description markdown="Following events are available:" />
          <Props sort="requiredFirst" of={EventsPropsSimulator} />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const OnPageChangeExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="On Page Change Event Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
    ]}
    onPageChange={(page, pageSize) => alert(`page=${page}, pageSize=${pageSize}`)}
  />
);
OnPageChangeExample.parameters = {
  docs: {
    description: {
      story: 'Implementing a callback for the page change event for pagination enabled table.',
    },
    source: {
      type: 'code',
    },
  },
};

export const OnRowsPerPageChangeExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="On Rows Per Page Change Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
    ]}
    onRowsPerPageChange={(pageSize) => alert(`pageSize=${pageSize}`)}
  />
);
OnRowsPerPageChangeExample.parameters = {
  docs: {
    description: {
      story: 'Implementing a callback for the page size change event for pagination enabled table.',
    },
    source: {
      type: 'code',
    },
  },
};

export const OnChangeHiddenColumnExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="On Change Hidden Column Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    onChangeColumnHidden={(column, hidden) => alert(`column=${column.field}, hidden=${hidden}`)}
    options={{
      columnsButton: true,
    }}
  />
);
OnChangeHiddenColumnExample.parameters = {
  docs: {
    description: {
      story: 'Implementing a callback for the column hidden state change.',
    },
    source: {
      type: 'code',
    },
  },
};

export const OnColumnDraggedAndDroppedExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="On Column Dragged & Dropped Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    onColumnDragged={(sourceIndex, destinationIndex) =>
      alert(`sourceIndex=${sourceIndex}, destinationIndex=${destinationIndex}`)
    }
  />
);
OnColumnDraggedAndDroppedExample.parameters = {
  docs: {
    description: {
      story: 'Implementing a callback for the column drag and drop.',
    },
    source: {
      type: 'code',
    },
  },
};

export const OnSortingOrderChangeExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="On Sorting Order Change Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    onOrderChange={(orderBy, orderDirection) => alert(`orderBy=${orderBy}, orderDirection=${orderDirection}`)}
  />
);
OnSortingOrderChangeExample.parameters = {
  docs: {
    description: {
      story: 'Implementing a callback for the sorting change event.',
    },
    source: {
      type: 'code',
    },
  },
};

export const OnRowClickExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="On Row Click Event Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    onRowClick={(event, rowData, togglePanel) =>
      alert(
        `event=${event}\nrowData=${JSON.stringify(rowData, null, 2)}\ntogglePanel=${JSON.stringify(
          togglePanel,
          null,
          2,
        )}`,
      )
    }
  />
);
OnRowClickExample.parameters = {
  docs: {
    description: {
      story: 'Implementing a callback for the on row clicked event.',
    },
    source: {
      type: 'code',
    },
  },
};

export const OnSelectionChangeExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="On Selection Change Event Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    onSelectionChange={(selectedRows, rowData) =>
      alert(`selectedRows=${JSON.stringify(selectedRows, null, 2)}\nrowData=${JSON.stringify(rowData, null, 2)}`)
    }
    options={{ selection: true }}
  />
);
OnSelectionChangeExample.parameters = {
  docs: {
    description: {
      story:
        'Implementing a callback for the on row selection change event. Event delivers always the selected rows. On triggering all select/deselect checkbox, either all or empty list will be returned and changed row will be undefined as expected.',
    },
    source: {
      type: 'code',
    },
  },
};

export const OnSearchChangeExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="On Search Input Change Event Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    onSearchChange={(searchText) => alert(`searchText=${searchText}`)}
  />
);
OnSearchChangeExample.parameters = {
  docs: {
    description: {
      story: 'This event is triggered on change of search input field value.',
    },
    source: {
      type: 'code',
    },
  },
};
