import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry } from './data';

export default {
  title: 'Features/Fixed Columns',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table has fixed columns feature both left and right." />
          <Subheading>Usage</Subheading>
          <Description markdown="To use fixed columns feature you should add `fixedColumns` object to `options`." />
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              
              <ReactMaterialTable
                // other props
                options={{
                  fixedColumns: {
                    left: true, 
                    right: true
                  }
                }}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Hints</Subheading>
          <Description
            markdown={'* You can not use fixed columns feature with detail panels, treedata or grouping for now.'}
          />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const BasicFixedColumnsExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Basic Fixed Columns Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
      {
        title: 'Price',
        field: 'price',
        type: 'currency',
        currencySetting: { locale: 'en', formatOptions: { currency: 'USD' } },
      },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0, recommended: true, price: 2.99 },
      {
        name: 'Ice cream sandwich',
        calories: 237,
        fat: 9.0,
        carbs: 37,
        protein: 4.3,
        recommended: true,
        price: 2.99,
      },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0, recommended: true, price: 2.99 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3, recommended: true, price: 2.99 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9, recommended: true, price: 2.99 },
    ]}
    options={{
      fixedColumns: {
        left: true,
        right: true,
      },
    }}
  />
);
BasicFixedColumnsExample.parameters = {
  docs: {
    description: {
      story:
        'To use fixed columns feature you should add `fixedColumns` object to `options`. In the example, it was activated for both, left and right side columns. Please resize the browser view port till horizontal scroll is displayed to see the result.',
    },
    source: {
      type: 'code',
    },
  },
};
