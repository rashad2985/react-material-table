import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Props, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry, TableEntry } from './data';
import { ValidationPropsSimulator } from './props-simulations/validate-props-simulator';

export default {
  title: 'Features/Validation',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table supports cell validation during add and update operations." />
          <Subheading>Usage</Subheading>
          <Description markdown="To activate validation for a column, `column.validate` function should be provided." />
          <Subheading>Return types</Subheading>
          <Description markdown="Following returns are supported:" />
          <Props sort="requiredFirst" of={ValidationPropsSimulator} />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const BasicBooleanValidationExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Basic Boolean Validation Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => !!rowData?.name,
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
    }}
  />
);
BasicBooleanValidationExample.parameters = {
  docs: {
    description: {
      story: 'In this example a boolean value is returned as a result of validation.',
    },
    source: {
      type: 'code',
    },
  },
};

export const StringValidationExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="String Validation Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => (!rowData?.name ? 'Name of a dessert cannot be empty' : ''),
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
    }}
  />
);
StringValidationExample.parameters = {
  docs: {
    description: {
      story: 'In this example a string value (error message) is returned as a result of validation.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ObjectValidationExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Object Validation Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => ({
          isValid: !!rowData?.name,
          helperText: !rowData?.name ? 'Name of a dessert cannot be empty' : '',
        }),
      },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
    }}
  />
);
ObjectValidationExample.parameters = {
  docs: {
    description: {
      story:
        'In this example an object that contains validation result and error text is returned as a result of validation.',
    },
    source: {
      type: 'code',
    },
  },
};

export const CustomInputPropsValidationExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Custom Input Props Validation Preview"
    columns={[
      {
        title: 'Dessert (100g serving)',
        field: 'name',
        type: 'string',
        validate: (rowData) => ({
          isValid: !!rowData?.name,
          helperText: !rowData?.name ? 'Name of a dessert cannot be empty' : '',
        }),
        editComponentInputProps: { maxLength: 10 },
      },
      {
        title: 'Calories',
        field: 'calories',
        type: 'numeric',
        editComponentInputProps: { min: 0, max: 500, step: 10 },
      },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
      { title: 'Recommended', field: 'recommended', type: 'boolean' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, protein: 4.0, recommended: true },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, protein: 4.3, recommended: true },
      { name: 'Eclair', calories: 262, fat: 16.0, protein: 6.0, recommended: true },
      { name: 'Cupcake', calories: 305, fat: 3.7, protein: 4.3, recommended: false },
      { name: 'Gingerbread', calories: 356, fat: 16.0, protein: 3.9, recommended: false },
    ]}
    editable={{
      onRowAdd: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
      onRowUpdate: (newData) => new Promise<Partial<TableEntry>>((resolve) => setTimeout(() => resolve(newData), 3000)),
    }}
  />
);
CustomInputPropsValidationExample.parameters = {
  docs: {
    description: {
      story:
        'Standard `inputProps` of Material UI input components can be passed via `column.editComponentInputProps`,' +
        ' e.g.: `maxLength` for an input field. Also in this example an object that contains validation result and' +
        ' error text is returned as a result of validation.',
    },
    source: {
      type: 'code',
    },
  },
};
