import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { ComplexObjectTableEntry } from './data';

export default {
  title: 'Features/Deep Data Access',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table lets you access not only direct attributes of an object but also data from deep/child objects." />
          <Subheading>Usage</Subheading>
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              
              <ReactMaterialTable
                // other props
                columns={[
                  {
                    field: 'child1.child2.value',
                    title: 'Child2 Value',
                    type: 'numeric'
                    value: (rowData) => rowData.child1.child2.value
                  }
                ]}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const DeepObjectValueExample: Story<ReactMaterialTableProps<ComplexObjectTableEntry>> = () => (
  <ReactMaterialTable
    title="Export Deep/Child Object attribute value Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string', export: false },
      { title: 'Calories', field: 'values.calories', type: 'numeric', value: (rowData) => rowData.values?.calories },
      { title: 'Fat (g)', field: 'values.fat', type: 'numeric', value: (rowData) => rowData.values?.fat },
      { title: 'Carbs (g)', field: 'values.carbs', type: 'numeric', value: (rowData) => rowData.values?.carbs },
      { title: 'Protein (g)', field: 'values.protein', type: 'numeric', value: (rowData) => rowData.values?.protein },
    ]}
    data={[
      {
        name: 'Frozen yoghurt',
        values: {
          calories: 159,
          fat: 6.0,
          carbs: 24,
          protein: 4.0,
        },
      },
      {
        name: 'Ice cream sandwich',
        values: {
          calories: 237,
          fat: 9.0,
          carbs: 37,
          protein: 4.3,
        },
      },
      {
        name: 'Eclair',
        values: {
          calories: 262,
          fat: 16.0,
          carbs: 24,
          protein: 6.0,
        },
      },
      {
        name: 'Cupcake',
        values: {
          calories: 305,
          fat: 3.7,
          carbs: 67,
          protein: 4.3,
        },
      },
      {
        name: 'Gingerbread',
        values: {
          calories: 356,
          fat: 16.0,
          carbs: 49,
          protein: 3.9,
        },
      },
    ]}
    options={{
      exportButton: true,
      filtering: true,
    }}
  />
);
DeepObjectValueExample.parameters = {
  docs: {
    description: {
      story:
        '`column.value` field is used to access values of object type entries. This way, such entries can be' +
        ' rendered automatically without implementing render function. Search, filtering, export will also work out' +
        ' of the box. Please be aware, `column.field` is necessary for internal computations and a custom `column.field`' +
        ' is generated using the array index, if it is missed. The latter one can lead to an unexpected behaviour.' +
        ' It is recommended to pass a unique `column.field` attribute.',
    },
    source: {
      type: 'code',
    },
  },
};
