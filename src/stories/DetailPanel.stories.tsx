import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Props, Source, Stories, Subheading, Subtitle, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry } from './data';
import { DetailsPanelPropsSimulator } from './props-simulations/details-panel-props-simulator';

export default {
  title: 'Features/Detail Panel',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="You can add a detail panel to every row by using `detailPanel` feature." />
          <Subheading>Usage</Subheading>
          <Subtitle>One detail panel as a functions</Subtitle>
          <Description markdown="If you neeed just one type of detail panel, you can just set a function that returns a React element." />
          <Source
            dark={true}
            code={`
              <ReactMaterialTable
                // other props
                detailPanel={{
                  render: rowData => (
                    <iframe
                      width="100%"
                      height="315"
                      src="https://www.youtube.com/embed/C0DPdy98e4c"
                      frameBorder="0"
                      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                      allowFullScreen
                    />
                  )
                }}
              />
              `.replace(/              /gi, '')}
            language="tsx"
          />
          <Subtitle>Multiple detail panel as array of detail panels</Subtitle>
          <Description markdown="If you neeed more than one type of detail panel, you can set array of `detailPanel` objects." />
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';

              <ReactMaterialTable
                // other props
                detailPanel={[
                  {
                    tooltip: 'Show Name',
                    render: (rowData) => (
                      <div
                        style={{
                          fontSize: 100,
                          textAlign: 'center',
                          color: 'white',
                          backgroundColor: '#43A047',
                        }}
                      >
                        {rowData.name}
                      </div>
                    )
                  },
                  {
                    icon: 'account_circle',
                    tooltip: 'Show Price',
                    render: (rowData) => (
                      <div
                        style={{
                          fontSize: 100,
                          textAlign: 'center',
                          color: 'white',
                          backgroundColor: '#E53935',
                        }}
                      >
                        {rowData.price}
                      </div>
                    )
                  },
                  {
                    icon: 'favorite_border',
                    openIcon: 'favorite',
                    render: (rowData) => (
                      <div
                        style={{
                          fontSize: 100,
                          textAlign: 'center',
                          color: 'white',
                          backgroundColor: '#FDD835',
                        }}
                      >
                        {rowData.name} {rowData.price}
                      </div>
                    )
                  }
                ]}
              />
              `.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Hints</Subheading>
          <Description
            markdown={
              '* You can use `options.detailPanelColumnAlignment` to set the detail panel actions column position.' +
              '\n* You can use `options.detailPanelType` to close old detail panel on showing the new one by setting it to `single`.'
            }
          />
          <Subheading>DetailPanel Fields</Subheading>
          <Description markdown="Every DetailPanel can have these fields:" />
          <Props sort="requiredFirst" of={DetailsPanelPropsSimulator} />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const OneDetailPanelExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="One Detail Panel Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    detailPanel={{
      render: () => (
        <iframe
          width="100%"
          height="315"
          src="https://www.youtube.com/embed/C0DPdy98e4c"
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      ),
    }}
  />
);
OneDetailPanelExample.parameters = {
  docs: {
    description: {
      story: 'A simple detail panel example that renders a youtube video in detail panel.',
    },
    source: {
      type: 'code',
    },
  },
};

export const MultipleDetailPanelsExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Multiple Detail Panels Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    detailPanel={[
      {
        tooltip: 'Show Name',
        render: (rowData) => {
          return (
            <div
              style={{
                fontSize: 100,
                textAlign: 'center',
                color: 'white',
                backgroundColor: '#43A047',
              }}
            >
              {rowData.name}
            </div>
          );
        },
      },
      {
        icon: 'account_circle',
        tooltip: 'Show Calories',
        render: (rowData) => {
          return (
            <div
              style={{
                fontSize: 100,
                textAlign: 'center',
                color: 'white',
                backgroundColor: '#E53935',
              }}
            >
              {rowData.calories}
            </div>
          );
        },
      },
      {
        icon: 'favorite_border',
        openIcon: 'favorite',
        render: (rowData) => {
          return (
            <div
              style={{
                fontSize: 100,
                textAlign: 'center',
                color: 'white',
                backgroundColor: '#FDD835',
              }}
            >
              {rowData.name} {rowData.fat}
            </div>
          );
        },
      },
    ]}
  />
);
MultipleDetailPanelsExample.parameters = {
  docs: {
    description: {
      story: 'A table example that has multiple detail panels. Every panel renders different data about row.',
    },
    source: {
      type: 'code',
    },
  },
};

export const DetailPanelWithRowClickExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Detail Panel With RowClick Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    detailPanel={{
      render: () => (
        <iframe
          width="100%"
          height="315"
          src="https://www.youtube.com/embed/C0DPdy98e4c"
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      ),
    }}
    onRowClick={(event, rowData, togglePanel) => togglePanel?.()}
  />
);
DetailPanelWithRowClickExample.parameters = {
  docs: {
    description: {
      story: 'In this example, `togglePanel` function is being called in `onRowClick` event.',
    },
    source: {
      type: 'code',
    },
  },
};

export const SinglePanelAtTheSameTimeExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Single Panel at the same Time Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    detailPanel={{
      render: () => (
        <iframe
          width="100%"
          height="315"
          src="https://www.youtube.com/embed/C0DPdy98e4c"
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      ),
    }}
    options={{
      detailPanelType: 'single',
    }}
  />
);
SinglePanelAtTheSameTimeExample.parameters = {
  docs: {
    description: {
      story: 'In this example, `options.detailPanelType` was set to `single` to show single panel at the same time.',
    },
    source: {
      type: 'code',
    },
  },
};
