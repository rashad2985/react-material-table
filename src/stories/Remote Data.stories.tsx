import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry } from './data';
import { Avatar } from '@mui/material';
import { onQueryChangeEvent } from '../events';

export default {
  title: 'Features/Remote Data',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table remote data feature allows the user to implement a custom data fetching function. Using this functionality, searching, filtering, sorting and paging are ignored by react-material-table and have to be manually implemented." />
          <Subheading>Usage</Subheading>
          <Description markdown="Instead of providing the `data` as array of elements, you have to provide a function, which returns a Promise holding the data, the current page and the total count." />
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              import { Toolbar } from '@trautmann/react-material-table/components';
              
              <ReactMaterialTable
                // other props
                data={query =>
                  new Promise((resolve) => {
                    // prepare your data and then call resolve like this:
                    resolve({
                      data: // your data array
                      page: // current page number
                      totalCount: // total row number
                    });
                  })
                }
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Hints</Subheading>
          <Description markdown="* It calls your function with initial options on first render. Then on every change it calls function with new query object." />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const RemoteDataExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Remote Data Preview"
    columns={[
      {
        title: 'Avatar',
        field: 'avatar',
        render: (rowData) => <Avatar alt={rowData.avatar} src={rowData.avatar} />,
      },
      { title: 'Id', field: 'id' },
      { title: 'First Name', field: 'first_name' },
      { title: 'Last Name', field: 'last_name' },
    ]}
    data={(query) =>
      new Promise((resolve) => {
        let url = 'https://reqres.in/api/users?';
        url += 'per_page=' + query.pageSize;
        url += '&page=' + (query.page + 1);
        fetch(url)
          .then((response) => response.json())
          .then((result) => {
            resolve({
              data: result.data,
              page: result.page - 1,
              totalCount: result.total,
            });
          });
      })
    }
  />
);
RemoteDataExample.parameters = {
  docs: {
    description: {
      story: 'Data fetched from a remote url with fetch function.',
    },
    source: {
      type: 'code',
    },
  },
};

export const RefreshDataExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Refresh Data Preview"
    columns={[
      {
        title: 'Avatar',
        field: 'avatar',
        render: (rowData) => <img style={{ height: 36, borderRadius: '50%' }} src={rowData.avatar} />,
      },
      { title: 'Id', field: 'id' },
      { title: 'First Name', field: 'first_name' },
      { title: 'Last Name', field: 'last_name' },
    ]}
    data={(query) =>
      new Promise((resolve) => {
        let url = 'https://reqres.in/api/users?';
        url += 'per_page=' + query.pageSize;
        url += '&page=' + (query.page + 1);
        fetch(url)
          .then((response) => response.json())
          .then((result) => {
            resolve({
              data: result.data,
              page: result.page - 1,
              totalCount: result.total,
            });
          });
      })
    }
    actions={[
      {
        icon: 'refresh',
        tooltip: 'Refresh Data',
        isFreeAction: true,
        // onQueryChangeEvent can be imported from the library
        // or you can pass its value '@trautmann/react-material-table/on-query-change' as a string
        onClick: (event) => event.target?.dispatchEvent?.(new Event(onQueryChangeEvent)),
      },
    ]}
  />
);
RefreshDataExample.parameters = {
  docs: {
    description: {
      story: 'Table is being forced to refresh data.',
    },
    source: {
      type: 'code',
    },
  },
};
