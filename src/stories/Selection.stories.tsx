import { Meta, Story } from '@storybook/react/types-6-0';
import { Description, Source, Stories, Subheading, Title } from '@storybook/addon-docs/blocks';
import * as React from 'react';
import { ReactMaterialTable, ReactMaterialTableProps } from '../react-material-table';
import { SimpleTableEntry } from './data';

export default {
  title: 'Features/Selection',
  component: ReactMaterialTable,
  parameters: {
    docs: {
      page: () => (
        <>
          <Title />
          <Description markdown="react-material-table has selection feature that lets users to select multiple rows." />
          <Subheading>Usage</Subheading>
          <Description markdown="To make selection available, it has to be activated via setting `options.selection` to `true`." />
          <Source
            dark={true}
            code={`
              import { ReactMaterialTable } from '@trautmann/react-material-table';
              
              <ReactMaterialTable
                // other props
                options={{
                  selection: true
                }}
              />`.replace(/              /gi, '')}
            language="tsx"
          />
          <Subheading>Hints</Subheading>
          <Description
            markdown={
              '* You can use `onSelectionChange` to handle selection changes.' +
              '\n* You can also use actions to handle selection changes.'
            }
          />
          <Stories title="Examples" includePrimary={true} />
        </>
      ),
    },
    controls: { sort: 'requiredFirst' },
    viewMode: 'docs',
  },
  argTypes: {
    localization: { table: { disable: true } },
    options: { table: { disable: true } },
    components: { table: { disable: true } },
    isLoading: { table: { disable: true } },
    editable: { table: { disable: true } },
    style: { table: { disable: true } },
    tableRef: { table: { disable: true } },
    detailPanel: { table: { disable: true } },
    onPageChange: { table: { disable: true } },
    onRowsPerPageChange: { table: { disable: true } },
    onChangeColumnHidden: { table: { disable: true } },
    onColumnDragged: { table: { disable: true } },
    onOrderChange: { table: { disable: true } },
    onRowClick: { table: { disable: true } },
    onSelectionChange: { table: { disable: true } },
    onSearchChange: { table: { disable: true } },

    columns: { table: { disable: true } },
    data: { table: { disable: true } },
    actions: { table: { disable: true } },
    icons: { table: { disable: true } },
    title: { table: { disable: true } },
  },
} as Meta;

export const BasicSelectionExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Basic Selection Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      selection: true,
    }}
  />
);
BasicSelectionExample.parameters = {
  docs: {
    description: {
      story: 'To make selection available `options.selection` was set to `true`.',
    },
    source: {
      type: 'code',
    },
  },
};

export const HandlingSelectionChangesExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Handling Selection Changes Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      selection: true,
    }}
    onSelectionChange={(selectedRows, changedRow) => {
      /* do something with rows or changed row */
      console.table(selectedRows);
      console.table(changedRow);
      return;
    }}
  />
);
HandlingSelectionChangesExample.parameters = {
  docs: {
    description: {
      story: 'In this example, `onSelectionChange` event is used to handle selection changes.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ActionsOnSelectedRowsExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Actions On Selected Rows Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      selection: true,
    }}
    actions={[
      {
        tooltip: 'Remove All Selected Users',
        icon: 'delete',
        onClick: (evt, currentRow, selectedRows) => alert('You want to delete ' + selectedRows.length + ' rows?'),
        position: 'toolbarOnSelect',
      },
    ]}
  />
);
ActionsOnSelectedRowsExample.parameters = {
  docs: {
    description: {
      story: 'In this example, action behaves like a toolbar action to process selection of multiple rows.',
    },
    source: {
      type: 'code',
    },
  },
};

export const ConditionalSelectionBoxesExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Conditional Selection Boxes Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      selection: true,
      selectionProps: (rowData) => ({
        disabled: rowData.calories >= 200 && rowData.calories < 300,
      }),
    }}
  />
);
ConditionalSelectionBoxesExample.parameters = {
  docs: {
    description: {
      story:
        'Selection boxes can receive Material-UI `CheckboxProps` according to `rowData`. In this example, the checkboxes with calories between 200 and 300 are disabled.',
    },
    source: {
      type: 'code',
    },
  },
};

export const InitialSelectionExample: Story<ReactMaterialTableProps<SimpleTableEntry>> = () => (
  <ReactMaterialTable
    title="Initial Selection Preview"
    columns={[
      { title: 'Dessert (100g serving)', field: 'name', type: 'string' },
      { title: 'Calories', field: 'calories', type: 'numeric' },
      { title: 'Fat (g)', field: 'fat', type: 'numeric' },
      { title: 'Carbs (g)', field: 'carbs', type: 'numeric' },
      { title: 'Protein (g)', field: 'protein', type: 'numeric' },
    ]}
    data={[
      { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
      { name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3 },
      { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
      { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
      { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
    ]}
    options={{
      selection: true,
    }}
    isRowInitiallySelected={(row) => row.calories <= 250}
  />
);
InitialSelectionExample.parameters = {
  docs: {
    description: {
      story:
        'To initially select rows `isRowInitiallySelected` function can be provided. in this example, the rows with calories less than 250 are initially selected.',
    },
    source: {
      type: 'code',
    },
  },
};
