import {
  FormControlLabelProps,
  FormGroupProps,
  FormLabelProps,
  InputProps as StandardInputProps,
  RadioGroupProps,
  TableCellProps,
  TableSortLabelProps,
  TextFieldProps,
  AutocompleteProps,
} from '@mui/material';
import * as React from 'react';
import { EditComponentProps, FilterComponentProps } from './internal';
import DateTimeFormatOptions = Intl.DateTimeFormatOptions;
import NumberFormatOptions = Intl.NumberFormatOptions;

export type LookupItemProps = {
  /**
   * Value used on selecting items. Row data field is used with this value.
   */
  value: string;
  /**
   * A text / label that is used to display instead of the value. If this field is missing, value will be used instead.
   */
  text: string;
};

export type ColumnType = 'string' | 'numeric' | 'currency' | 'boolean' | 'date' | 'datetime' | 'time' | 'multi-string';
export type Locale = { locale: string };
export type DateTimeSettings = Locale & { formatOptions?: DateTimeFormatOptions };
export type CurrencySettings = Locale & { formatOptions?: Omit<NumberFormatOptions, 'style'> };
export type NumberSettings = Locale & { formatOptions?: NumberFormatOptions };
export type EditableMode = 'add' | 'edit' | 'delete';
export type Editable<RowData extends object = {}> =
  | 'always'
  | 'onUpdate'
  | 'onAdd'
  | 'never'
  | ((columnDef: Column<RowData>, rowData: Partial<RowData>, mode: EditableMode) => boolean);

// eslint-disable-next-line
type LookupInput<RowData extends object = {}> =
  | {
      /**
       * type of the lookup input field. 'text-field' exposes TextFieldProps is also default, 'autocomplete' exposes
       * autocomplete props and 'form-group' exposes FormControlLabelProps that is used for each element, and
       * FormLabelProps that is used for overall label.
       */
      lookupInputType?: 'text-field';
      /**
       * Props of the main input. Depending from the lookupInputType, this type is required dynamically:
       * lookupInputType = 'text-field' then TextFieldProps,
       * lookupInputType = 'autocomplete' then AutocompleteProps,
       * lookupInputType = 'radio-group' then FormControlLabelProps,
       * lookupInputType = 'checkbox-group' then also FormControlLabelProps,
       */
      lookupInputProps?: Partial<TextFieldProps>;
      /**
       * FormLabelProps of each rendered radio button or checkbox label.
       * Can be passed only for lookupInputType 'radio-group' or 'checkbox-group'.
       */
      lookupLabelProps?: never;
      /**
       * Can be passed only for lookupInputType 'radio-group'.
       */
      lookupRadioGroupProps?: never;
      /**
       * Can be passed only for lookupInputType 'checkbox-group'.
       */
      lookupFormGroupProps?: never;
    }
  | {
      lookupInputType?: 'autocomplete';
      // eslint-disable-next-line
      lookupInputProps?: Partial<AutocompleteProps<LookupItemProps, any, any, any>>;
      lookupLabelProps?: never;
      lookupRadioGroupProps?: never;
      lookupFormGroupProps?: never;
    }
  | {
      lookupInputType?: 'radio-group';
      lookupInputProps?: Partial<FormControlLabelProps>;
      lookupLabelProps?: Partial<FormLabelProps>;
      lookupRadioGroupProps?: Partial<RadioGroupProps>;
      lookupFormGroupProps?: never;
    }
  | {
      lookupInputType?: 'checkbox-group';
      lookupInputProps?: Partial<FormControlLabelProps>;
      lookupLabelProps?: Partial<FormLabelProps>;
      lookupFormGroupProps?: Partial<FormGroupProps>;
      lookupRadioGroupProps?: never;
    };

type FieldValue<RowData extends object = {}> = {
  /**
   * A map of key value pairs that used to suggest in filters, add/edit inputs and also in cells (matching cell value is replaced with map value).
   */
  lookup?: Map<string, string>;

  /**
   * Overrides lookup on add from, resolves Key value pair for lookup render data from current row data.
   * If rejected, then rejected value will be shown as an error message.
   */
  addLookup?: (rowData: Partial<RowData>) => Promise<Map<string, string>>;

  /**
   * Overrides lookup on edit from, resolves Key value pair for lookup render data from current row data.
   */
  editLookup?: (rowData: Partial<RowData>) => Promise<Map<string, string>>;

  /**
   * Render function for lookup items. Overrides lookup value show. If provided, then lookup value will be used only for autocomplete search.
   */
  lookupItemRender?: (props: LookupItemProps) => React.ReactNode;

  /**
   * Function to check and disable lookup items depending on row data and item props.
   */
  lookupItemDisabled?: (rowData: Partial<RowData>, item: LookupItemProps) => boolean;

  /**
   * Function to sort lookup items before rendering options.
   */
  lookupSort?: (item1: LookupItemProps, item2: LookupItemProps) => number;
} & (
  | {
      type?: ColumnType & 'string';
      /**
       * Can be used to define complex way of access to the cell value from row data. Works well together with default
       * sorting, filtering, export, without needing to override them to access value from deep child objects.
       * Overrides data access via 'column.field' attribute.
       */
      value?: (rowData: Partial<RowData>) => string | undefined;

      /**
       * Can be used to reset or alter other input fields during editing on "edit" from.
       * @param newValue is the new value on input into the current column input field.
       * @param setRowData is the exposed set state dispatcher to manipulate other fields values.
       */
      editSetRowData?: (newValue: string, setRowData: React.Dispatch<React.SetStateAction<Partial<RowData>>>) => void;

      /**
       * Can be used to reset or alter other input fields during editing on "add" from.
       * @param newValue is the new value on input into the current column input field.
       * @param setRowData is the exposed set state dispatcher to manipulate other fields values.
       */
      addSetRowData?: (newValue: string, setRowData: React.Dispatch<React.SetStateAction<Partial<RowData>>>) => void;
    }
  | {
      type: ColumnType & 'multi-string';
      value?: (rowData: Partial<RowData>) => Array<string> | undefined;
      editSetRowData?: (
        newValue: Array<string>,
        setRowData: React.Dispatch<React.SetStateAction<Partial<RowData>>>,
      ) => void;
      addSetRowData?: (
        newValue: Array<string>,
        setRowData: React.Dispatch<React.SetStateAction<Partial<RowData>>>,
      ) => void;
    }
  | {
      type: ColumnType & 'boolean';
      value?: (rowData: Partial<RowData>) => boolean | undefined;
      editSetRowData?: (newValue: boolean, setRowData: React.Dispatch<React.SetStateAction<Partial<RowData>>>) => void;
      addSetRowData?: (newValue: boolean, setRowData: React.Dispatch<React.SetStateAction<Partial<RowData>>>) => void;
    }
  | {
      type: (ColumnType & 'numeric') | 'currency';
      value?: (rowData: Partial<RowData>) => number | undefined;
      editSetRowData?: (newValue: number, setRowData: React.Dispatch<React.SetStateAction<Partial<RowData>>>) => void;
      addSetRowData?: (newValue: number, setRowData: React.Dispatch<React.SetStateAction<Partial<RowData>>>) => void;
    }
  | {
      type: (ColumnType & 'date') | 'datetime' | 'time';
      value?: (rowData: Partial<RowData>) => Date | undefined;
      editSetRowData?: (newValue: Date, setRowData: React.Dispatch<React.SetStateAction<Partial<RowData>>>) => void;
      addSetRowData?: (newValue: Date, setRowData: React.Dispatch<React.SetStateAction<Partial<RowData>>>) => void;
    }
);

type ColumnProps<RowData extends object = {}> = Pick<TableCellProps, 'align'> &
  LookupInput<RowData> &
  FieldValue<RowData> & {
    /**
     * Header text
     */
    title?: string;

    /**
     * Field name of data row
     */
    field?: keyof RowData | string;

    /**
     * Data type
     * @default string
     */
    type?: ColumnType;

    /**
     * Header cell style
     */
    headerStyle?: TableCellProps['style'];

    /**
     * Cell cellStyle
     */
    cellStyle?: TableCellProps['style'];

    /**
     * Render a custom node for cell. Parameter is rowData and return value must be ReactElement
     * @param rowData
     */
    render?: (rowData: RowData) => React.ReactNode;

    /**
     * The locale settings used to format the displayed date/time
     * This field can be used when column type is date, time or date-time.
     */
    dateSetting?: DateTimeSettings;

    /**
     * The locale settings used to format the displayed number as a currency.
     * This field can be used when column type is currency.
     */
    currencySetting?: CurrencySettings;

    /**
     * The locale settings used to format the displayed number.
     * This field can be used when column type is numeric.
     */
    numberSettings?: NumberSettings;

    /**
     * Flag to hide column
     * @default false
     */
    hidden?: boolean;

    /**
     * Flag for showing columns marked as 'hidden' equal to true, in the selector under columnsButton
     * @default false
     */
    hiddenByColumnsButton?: boolean;

    /**
     * When data is empty or undefined, string value, ReactElement or function result can be set as default value
     */
    emptyValue?: React.ReactNode | (() => React.ReactNode);

    /**
     * Flag for column that could be removed with columnsButton or not
     * @default true
     */
    removable?: boolean;

    /**
     * Disable the 'onRowClick' event for this cell
     * @default false
     */
    disableClick?: boolean;

    /**
     * Flag to make column exportable or not
     * @default true
     */
    export?: boolean;

    /**
     * Defines, whether this column value should be inputted on add/update
     * @default always
     */
    editable?: Editable<RowData>;

    /**
     *
     * @param rowData
     * @return boolean to block/allow save operation and show red bar below that field;
     *         non-empty string to block save operation and show red bar below that field and the string as error message;
     *         object `{isValid: boolean, helperText: string}` to block/allow save operation and in case of error show red bar below that field and helperText as error message.
     */
    validate?: (rowData: Partial<RowData>) => { isValid: boolean; helperText?: string } | string | boolean;

    /**
     * Custom component for this column value on add/update form
     * @param props - Component props
     */
    editComponent?: (props: EditComponentProps<RowData>) => React.ReactElement<any>;

    /**
     * Standard input props for edit component (standard or custom edit component) for this column value on add/update form
     */
    editComponentInputProps?: StandardInputProps['inputProps'];

    /**
     * required flag for edit component (standard or custom edit component) for this column value on add/update form
     */
    editComponentFieldRequired?: boolean;

    /**
     * Initial value on adding new row
     */
    initialEditValue?: any;

    /**
     * Flag to make column readonly when editing, the column will still be editable when adding a row
     * @default false
     */
    readonly?: boolean;

    /**
     * Width recommendation for the column. As long as there is at least one another column without width recommendation, this column will have the given width.
     * @default undefined
     */
    width?: string | number;

    /**
     * If false, then column will be excluded from responsive transformed row.
     * @default true
     */
    responsive?: boolean;
  };

export type SortProps<RowData extends object = {}> = {
  /**
   * Flag to activate or disable sorting feature of column
   * @default true
   */
  sorting?: boolean;

  /**
   * Sorting direction: 'asc', 'desc'
   */
  defaultSort?: TableSortLabelProps['direction'];

  /**
   * This field can be used for overriding sort algorithm
   * @param a
   * @param b
   */
  customSort?: (a: RowData, b: RowData) => number;
};

export type FilterableProps<RowData extends object = {}> = {
  /**
   * Flag to activate or disable filtering feature of column
   * @default true
   */
  filtering?: boolean;

  /**
   * Filter text field placeholder
   */
  filterPlaceholder?: string;

  /**
   * Default Filter value for filtering column
   */
  defaultFilter?: any;

  /**
   * This field can be used for overriding filter and search algorithm
   * @param term
   * @param rowData
   */
  // eslint-disable-next-line
  customFilterAndSearch?: (term: any, rowData: RowData) => boolean;

  /**
   * Custom component for filtering
   * @param props
   */
  filterComponent?: (props: FilterComponentProps<RowData>) => React.ReactElement;

  /**
   * Flag to activate or disable standard/custom filter for the column
   * @default true
   */
  useStandardFilter?: boolean;

  /**
   * Flag to activate or disable built-in filters for the column
   * @default true
   */
  useBuiltInFilters?: boolean;

  /**
   * Filter component style
   */
  filterCellStyle?: React.CSSProperties;

  /**
   * If true, includes the column when performing a search
   * @default undefined
   */
  searchable?: boolean;
};

export type Column<RowData extends object = {}> = ColumnProps<RowData> & SortProps<RowData> & FilterableProps<RowData>;
