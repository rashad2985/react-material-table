// eslint-disable-next-line
export interface Query<RowData extends object = {}> {
  page: number;
  pageSize: number;
  totalCount: number;
}

export interface QueryResult<RowData extends object = {}> {
  data: Array<RowData>;
  page: number;
  totalCount: number;
}
