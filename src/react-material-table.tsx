import * as React from 'react';
import { Localization, Options, TableContainer, TableContainerProps } from './components';
import {
  Components,
  ComponentsContext,
  EditableContext,
  Events,
  EventsContext,
  FiltersContext,
  Icons,
  IconsContext,
  LocalizationContext,
  OptionsContext,
  TableStateContext,
  useFilters,
} from './internal';
import { Editable } from './editable';
import { Column } from './column';

function isValidColumn<RowData extends object = {}>(column: Column<RowData>): column is Column<RowData> {
  return !!column && typeof column === 'object';
}

export interface ReactMaterialTableProps<RowData extends object = {}>
  extends TableContainerProps<RowData>,
    Partial<Events<RowData>> {
  /**
   * All texts for localization
   */
  localization?: Localization;
  /**
   * All options of table
   */
  options?: Options<RowData>;
  /**
   * Component customization
   */
  components?: Partial<Components<RowData>>;
  /**
   * Icons customization
   */
  icons?: Partial<Icons>;
  /**
   * Flag for loading animation
   * @default false
   */
  isLoading?: boolean;
  /**
   * Object for add, update and delete functions
   */
  editable?: Editable<RowData>;
}

export function ReactMaterialTable<RowData extends object = {}>(props: ReactMaterialTableProps<RowData>) {
  const {
    data = [],
    columns: initialColumns = [],
    title,
    localization = {},
    options = {},
    actions = [],
    style,
    tableRef,
    isLoading,
    components = {},
    icons = {},
    detailPanel,
    editable,
    isRowInitiallySelected,
    // Events
    onPageChange,
    onRowsPerPageChange,
    onChangeColumnHidden,
    onColumnDragged,
    onOrderChange,
    onRowClick,
    onSelectionChange,
    onSearchChange,
  } = props;
  const columns = React.useMemo<Array<Column<RowData>>>(
    () =>
      initialColumns.filter(isValidColumn).map((column, index) => ({
        ...column,
        field: column.field ?? `field-${index}`,
      })),
    [initialColumns],
  );
  const filtersContext = useFilters<RowData>();

  return React.useMemo(
    () => (
      <LocalizationContext.Provider value={localization}>
        <IconsContext.Provider value={icons}>
          <EventsContext.Provider
            value={{
              onPageChange,
              onRowsPerPageChange,
              onChangeColumnHidden: onChangeColumnHidden as Events<any>['onChangeColumnHidden'],
              onColumnDragged,
              onOrderChange,
              onRowClick,
              onSelectionChange,
              onSearchChange,
            }}
          >
            <OptionsContext.Provider value={{ ...options }}>
              <EditableContext.Provider value={{ ...(editable ?? {}) }}>
                <ComponentsContext.Provider value={components}>
                  <TableStateContext.Provider value={{ isLoading }}>
                    <FiltersContext.Provider value={filtersContext}>
                      <TableContainer<RowData>
                        data={data}
                        columns={columns}
                        title={title}
                        actions={actions}
                        style={style}
                        tableRef={tableRef}
                        detailPanel={detailPanel}
                        isRowInitiallySelected={isRowInitiallySelected}
                      />
                    </FiltersContext.Provider>
                  </TableStateContext.Provider>
                </ComponentsContext.Provider>
              </EditableContext.Provider>
            </OptionsContext.Provider>
          </EventsContext.Provider>
        </IconsContext.Provider>
      </LocalizationContext.Provider>
    ),
    [
      actions,
      columns,
      components,
      data,
      detailPanel,
      editable,
      filtersContext,
      icons,
      isLoading,
      isRowInitiallySelected,
      localization,
      onChangeColumnHidden,
      onColumnDragged,
      onOrderChange,
      onPageChange,
      onRowClick,
      onRowsPerPageChange,
      onSearchChange,
      onSelectionChange,
      options,
      style,
      tableRef,
      title,
    ],
  );
}
