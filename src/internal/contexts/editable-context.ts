import * as React from 'react';
import { Editable } from '../../editable';

export const EditableContext = React.createContext<Editable<any>>({});
