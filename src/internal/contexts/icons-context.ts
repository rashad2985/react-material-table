import * as React from 'react';
import { Icons } from '../hooks';

export const IconsContext = React.createContext<Partial<Icons>>({});
