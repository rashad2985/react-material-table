import * as React from 'react';
import { Components } from '../hooks';

export const ComponentsContext = React.createContext<Partial<Components<any>>>({});
