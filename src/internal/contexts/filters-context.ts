import * as React from 'react';
import { Filter } from '../../filter';

export interface FiltersContextProps<T extends object = {}> {
  filters: Array<Filter<T>>;
  setFilters: (filters: Array<Filter<T>>) => void;
}

export const FiltersContext = React.createContext<FiltersContextProps<any>>({
  filters: [],
  // eslint-disable-next-line
  setFilters: (_) => {},
});
