import * as React from 'react';
import { Events } from '../hooks';

export const EventsContext = React.createContext<Partial<Events<any>>>({
  onQueryChangeEvent: '@trautmann/react-material-table/on-query-change',
});
