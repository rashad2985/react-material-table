import * as React from 'react';
import { InternalTableState } from '../hooks';

export const TableStateContext = React.createContext<Partial<InternalTableState>>({});
