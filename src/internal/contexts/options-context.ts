import * as React from 'react';
import { Options } from '../../components';

export const OptionsContext = React.createContext<Options<any>>({});
