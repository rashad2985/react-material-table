import * as React from 'react';
import { Localization } from '../../components';

export const LocalizationContext = React.createContext<Localization>({});
