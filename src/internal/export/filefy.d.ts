declare module 'filefy' {
  export class CsvBuilder {
    constructor(filename: string);
    setColumns(columns: Array<string>): CsvBuilder;
    setDelimeter(setDelimeter: string): CsvBuilder;
    addRow(row: Array<string>): CsvBuilder;
    addRows(rows: Array<Array<string>>): CsvBuilder;
    exportFile(): void;
  }
}
