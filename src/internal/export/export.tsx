import * as React from 'react';
import { IconButton, ListItemText, Menu, MenuItem, Tooltip } from '@mui/material';
import { useIcons, useLocalization, useOptions } from '../hooks';
import { Column } from '../../column';
import { CsvBuilder } from 'filefy';
import jsPDF from 'jspdf';
import autoTable, { ColumnInput, RowInput } from 'jspdf-autotable';
import { formatCell } from '../commons';

export interface ExportButtonProps<DataType extends object = {}> {
  tableTitle?: string;
  columns: Array<
    Pick<
      Column<DataType>,
      | 'title'
      | 'align'
      | 'field'
      | 'type'
      | 'lookup'
      | 'dateSetting'
      | 'currencySetting'
      | 'numberSettings'
      | 'emptyValue'
    >
  >;
  rows: Array<DataType>;
}

export function ExportButton<DataType extends object = {}>(props: ExportButtonProps<DataType>) {
  const { tableTitle, columns, rows } = props;
  const { Export: ExportIcon } = useIcons();
  const localization = useLocalization();
  const options = useOptions<DataType>();
  const { exportDelimiter, exportFileName = tableTitle ?? '', exportCsv, exportPdf } = options;

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = React.useCallback((event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  }, []);
  const handleClose = React.useCallback(() => setAnchorEl(null), []);

  const exportButton = React.useMemo(
    () => (
      <IconButton
        onClick={handleClick}
        aria-haspopup="true"
        aria-label={localization?.toolbar?.exportAriaLabel}
        size="large"
      >
        <ExportIcon />
      </IconButton>
    ),
    [handleClick, localization, ExportIcon],
  );

  const exportAsCsv = React.useCallback(() => {
    handleClose();
    const columnTitles = columns.map((column) => column.title ?? '');
    const rowsToWrite = rows.map((row) => columns.map((column) => formatCell(column, row, localization)));
    new CsvBuilder(`${exportFileName}.csv`)
      .setDelimeter(exportDelimiter)
      .setColumns(columnTitles)
      .addRows(rowsToWrite)
      .exportFile();
  }, [handleClose, exportFileName, exportDelimiter, columns, rows, localization]);

  const handleExportAsCsv = React.useCallback<React.MouseEventHandler<HTMLLIElement>>(
    () => (!!exportCsv ? exportCsv(columns, rows) : exportAsCsv()),
    [exportCsv, exportAsCsv, columns, rows],
  );

  const exportAsPdf = React.useCallback(() => {
    handleClose();
    const columnDefinitions: Array<ColumnInput> = columns.map((column) => ({
      header: column.title ?? '',
      dataKey: String(column.field),
      key: String(column.field),
    }));
    /*const columnStyles: UserOptions['columnStyles'] = {};
      columns.forEach(column => {
        const hAlign = resolveCellAlign(column);
        columnStyles[String(column.field)] = { halign: hAlign === 'inherit' ? 'left' : hAlign };
      });*/
    const writableRows: Array<RowInput> = rows.map((row) => {
      const rowInput: RowInput = {};
      columns.forEach((column) => {
        rowInput[String(column.field)] = formatCell(column, row, localization);
      });
      return rowInput;
    });
    const pdf = new jsPDF('landscape', 'pt', 'A4');
    pdf.setFontSize(15);
    pdf.text(exportFileName, 40, 40);
    autoTable(pdf, {
      startY: 50,
      columns: columnDefinitions,
      // columnStyles: columnStyles,
      body: writableRows,
    });
    pdf.save(`${exportFileName}.pdf`);
  }, [handleClose, columns, rows, exportFileName, localization]);

  const handleExportAsPdf = React.useCallback<React.MouseEventHandler<HTMLLIElement>>(
    () => (!!exportPdf ? exportPdf(columns, rows) : exportAsPdf()),
    [exportPdf, exportAsPdf, columns, rows],
  );

  return React.useMemo(
    () => (
      <React.Fragment>
        {!!localization?.toolbar?.exportTitle ? (
          <Tooltip title={localization.toolbar.exportTitle}>{exportButton}</Tooltip>
        ) : (
          exportButton
        )}
        <Menu anchorEl={anchorEl} keepMounted={false} open={Boolean(anchorEl)} onClose={handleClose}>
          <MenuItem onClick={handleExportAsCsv}>
            <ListItemText primary={localization?.toolbar?.exportAsCsv} />
          </MenuItem>
          <MenuItem onClick={handleExportAsPdf}>
            <ListItemText primary={localization?.toolbar?.exportAsPdf} />
          </MenuItem>
        </Menu>
      </React.Fragment>
    ),
    [localization, exportButton, anchorEl, handleClose, handleExportAsCsv, handleExportAsPdf],
  );
}
