import * as React from 'react';
import { TableStateContext } from '../contexts';
import { filterFieldsWithDefinedValues } from '../commons';

export interface InternalTableState {
  isLoading: boolean;
}

const defaultTableState: InternalTableState = {
  isLoading: false,
};

export function useTableState() {
  const tableStateContext = React.useContext<Partial<InternalTableState>>(TableStateContext);

  return React.useMemo<InternalTableState>(
    () => ({
      ...defaultTableState,
      ...filterFieldsWithDefinedValues(tableStateContext ?? {}),
    }),
    [tableStateContext],
  );
}
