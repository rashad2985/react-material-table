import * as React from 'react';
import * as ReactTable from 'react-table';
import { Column } from '../../column';
import { Filter } from '../../filter';
import { toFullTime } from '../commons';
import { useFiltersGenerator } from './use-filters-generator';
import { useLocalization } from './use-localization';
import { useOptions } from './use-options';

type SortingOptions<T extends object = {}> = Partial<ReactTable.Column<T>> & ReactTable.UseSortByColumnOptions<T>;

type RenderOptions<T extends object = {}> = Partial<ReactTable.Column<T>> &
  Pick<ReactTable.ColumnInstance<T>, 'Cell'> &
  Pick<ReactTable.UseGlobalFiltersColumnOptions<T>, 'disableGlobalFilter'> &
  Partial<
    Pick<
      Column<T>,
      | 'type'
      | 'align'
      | 'headerStyle'
      | 'cellStyle'
      | 'width'
      | 'render'
      | 'lookup'
      | 'lookupInputType'
      | 'lookupInputProps'
      | 'lookupLabelProps'
      | 'lookupRadioGroupProps'
      | 'lookupFormGroupProps'
      | 'addLookup'
      | 'editLookup'
      | 'lookupItemRender'
      | 'lookupItemDisabled'
      | 'lookupSort'
      | 'dateSetting'
      | 'currencySetting'
      | 'numberSettings'
      | 'defaultFilter'
      | 'filterComponent'
      | 'filterPlaceholder'
      | 'filterCellStyle'
      | 'useBuiltInFilters'
      | 'useStandardFilter'
      | 'searchable'
      | 'emptyValue'
      | 'removable'
      | 'disableClick'
      | 'export'
      | 'editable'
      | 'initialEditValue'
      | 'readonly'
      | 'editComponent'
      | 'editComponentInputProps'
      | 'editComponentFieldRequired'
      | 'editSetRowData'
      | 'addSetRowData'
      | 'validate'
      | 'responsive'
    >
  >;

type CellArguments<T extends object = {}> = {
  row: ReactTable.Row<T>;
  value: ReactTable.CellValue<keyof T>;
};

export function useColumnTransformer<T extends object = {}>() {
  const localization = useLocalization();
  const { filtering: generallyFiltering } = useOptions<T>();
  const { resolveFilterRenderer } = useFiltersGenerator<T>();

  const transform = React.useCallback<(column: Column<T>, filters: Array<Filter<T>>) => ReactTable.Column<T>>(
    (column: Column<T>, filters: Array<Filter<T>>) => {
      const { filtering } = column;
      // eslint-disable-next-line
      const filterOptions: ReactTable.UseFiltersColumnOptions<T> & any =
        !!generallyFiltering && filtering !== false
          ? resolveFilterRenderer(column, filters)
          : { disableFilters: !filtering };

      const {
        align,
        headerStyle,
        cellStyle,
        width,
        render,
        lookup: initialLookup,
        lookupInputType,
        lookupInputProps,
        lookupLabelProps,
        lookupRadioGroupProps,
        lookupFormGroupProps,
        addLookup,
        editLookup,
        lookupItemRender,
        lookupItemDisabled,
        lookupSort,
        dateSetting,
        currencySetting,
        numberSettings,
        filterComponent,
        defaultFilter,
        filterPlaceholder,
        filterCellStyle,
        useBuiltInFilters,
        useStandardFilter,
        searchable,
        emptyValue = '',
        removable = true,
        disableClick = false,
        export: exportable = true,
        editable,
        initialEditValue,
        readonly,
        editComponent,
        editComponentInputProps,
        editComponentFieldRequired,
        editSetRowData,
        addSetRowData,
        validate,
        responsive,
      } = column;
      const lookup: Column<T>['lookup'] =
        initialLookup ??
        (column.type === 'boolean'
          ? new Map<string, string>([
              ['true', localization.row?.boolean?.true],
              ['false', localization.row?.boolean?.false],
              ['undefined', localization.row?.boolean?.undefined],
            ])
          : undefined);
      const { locale: dateLocale, formatOptions: dateFormatOptions } = dateSetting || {};
      const { locale: currencyLocale, formatOptions: currencyFormatOptions } = currencySetting || {};
      const { locale: numberLocale, formatOptions: numberFormatOptions } = numberSettings || {};
      const renderOptions: RenderOptions<T> = {
        align,
        headerStyle,
        cellStyle,
        width,
        dateSetting,
        currencySetting,
        numberSettings,
        filterComponent,
        defaultFilter,
        filterPlaceholder,
        filterCellStyle,
        useBuiltInFilters,
        useStandardFilter,
        removable,
        disableClick,
        editable,
        initialEditValue,
        readonly,
        editComponent,
        editComponentInputProps,
        editComponentFieldRequired,
        validate,
        responsive,
        type: column.type || 'string',
        disableGlobalFilter: searchable === false,
        export: exportable,
        lookup,
        lookupInputType,
        lookupInputProps,
        lookupLabelProps,
        lookupRadioGroupProps,
        lookupFormGroupProps,
        addLookup,
        editLookup,
        lookupItemRender,
        lookupItemDisabled,
        lookupSort,
        editSetRowData,
        addSetRowData,
        accessor: (data) => {
          if (!!column.value) {
            return column.value(data);
          }
          return !column.value && !!column.field && data.hasOwnProperty(column.field)
            ? ((column.type === 'date' || column.type === 'datetime') &&
                (data[column.field as keyof T] as unknown as Date)?.getTime()) ||
                (column.type === 'time' && toFullTime(data[column.field as keyof T] as unknown as Date)?.getTime()) ||
                data[column.field as keyof T]
            : undefined;
        },
        Cell: (props: CellArguments<T>) =>
          !!render
            ? render(props.row.original)
            : (!!lookup &&
                (column.type === 'multi-string'
                  ? ((props.value ?? []) as unknown as Array<string>)
                      .map((value) => lookup.get(`${value}`))
                      .filter((value) => !!value)
                      .join(', ')
                  : lookup.get(`${props.value}`))) ||
              (column.type === 'multi-string' &&
                !!props.value &&
                (props.value as unknown as Array<string>).join(', ')) ||
              (column.type === 'currency' &&
                !!props.value &&
                !!currencyFormatOptions?.currency &&
                new Intl.NumberFormat(currencyLocale, { ...(currencyFormatOptions || {}), style: 'currency' }).format(
                  Number(props.value),
                )) ||
              (column.type === 'numeric' &&
                !!props.value &&
                new Intl.NumberFormat(numberLocale, { ...(numberFormatOptions || {}) }).format(Number(props.value))) ||
              (column.type === 'date' &&
                !!props.value &&
                new Date(props.value as string | number).toLocaleDateString(dateLocale, dateFormatOptions)) ||
              (column.type === 'time' &&
                !!props.value &&
                new Date(props.value as string | number).toLocaleTimeString(dateLocale, dateFormatOptions)) ||
              (column.type === 'datetime' &&
                !!props.value &&
                new Date(props.value as string | number).toLocaleString(dateLocale, dateFormatOptions)) ||
              (props.value ?? (typeof emptyValue === 'function' ? emptyValue() : emptyValue)),
      };

      const { sorting = true, customSort } = column;
      const sortingOptions: SortingOptions<T> = {
        defaultCanSort: sorting,
        disableSortBy: !sorting,
        sortType: !!customSort
          ? (rowA, rowB) => customSort(rowA.original, rowB.original)
          : ((column.type === 'date' || column.type === 'time' || column.type === 'datetime') && 'basic') ||
            ((column.type === 'boolean' || column.type === 'numeric' || column.type === 'currency') && 'basic') ||
            'alphanumeric',
        sortDescFirst: true,
        sortInverted: true,
      };

      return {
        Header: column.title ?? '',
        id: column.field,
        field: column.field,
        value: column.value,
        title: column.title ?? '',
        ...renderOptions,
        ...filterOptions,
        ...sortingOptions,
      };
    },
    [localization, resolveFilterRenderer, generallyFiltering],
  );

  return React.useMemo(() => ({ transform }), [transform]);
}
