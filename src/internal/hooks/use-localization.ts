import * as React from 'react';
import { LocalizationContext } from '../contexts';
import { Localization as PartialLocalization } from '../../components';
import { BuiltInFilterType } from '../../filter';
import { filterFieldsWithDefinedValues } from '../commons';

export interface InternalLocalization {
  /**
   * Key value pair to localize body component
   */
  body: {
    emptyDataSourceMessage: string;
    addTooltip: string;
    deleteTooltip: string;
    editTooltip: string;
    /**
     * Key value pair to localize filter row component
     */
    filterRow: {
      filterPlaceHolder: string;
      filterTooltip: string;
    };
    /**
     * Key value pair to localize edit row component
     */
    editRow: {
      deleteText: string;
      cancelTooltip: string;
      saveTooltip: string;
    };
  };
  /**
   * Key value pair to localize grouping component
   */
  grouping: {
    placeholder: string;
    groupedBy: string;
  };
  /**
   * Key value pair to localize header component
   */
  header: {
    actions: string;
  };
  /**
   * Key value pair to localize row values
   */
  row: {
    selectTooltip: string;
    detailPanelTooltip: string;
    boolean: {
      true: string;
      false: string;
      undefined: string;
    };
  };
  /**
   * Key value pair to localize pagination component
   */
  pagination: {
    labelDisplayedRows: string;
    labelRowsSelect: string;
    labelRowsPerPage: string;
    firstAriaLabel: string;
    firstTooltip: string;
    previousAriaLabel: string;
    previousTooltip: string;
    nextAriaLabel: string;
    nextTooltip: string;
    lastAriaLabel: string;
    lastTooltip: string;
    pageSizeAll: string;
  };
  /**
   * Key value pair to localize toolbar component
   */
  toolbar: {
    addRemoveColumns: string;
    nRowsSelected: string;
    showColumnsTitle: string;
    showColumnsAriaLabel: string;
    exportTitle: string;
    exportAriaLabel: string;
    exportAsCsv: string;
    exportAsPdf: string;
    searchLabel: string;
    searchTooltip: string;
    searchPlaceholder: string;
  };
  /**
   * Key value pair to localize built in filters
   */
  filters: Record<BuiltInFilterType, string> & {
    addFilter: string;
    filterType: string;
    from: string;
    to: string;
    apply: string;
  };
}

const defaultLocalization: InternalLocalization = {
  body: {
    emptyDataSourceMessage: 'No records to display',
    addTooltip: 'Add',
    deleteTooltip: 'Delete',
    editTooltip: 'Edit',
    filterRow: {
      filterPlaceHolder: '',
      filterTooltip: 'Filter',
    },
    editRow: {
      deleteText: 'Are you sure to delete this row?',
      cancelTooltip: 'Cancel',
      saveTooltip: 'Save',
    },
  },
  grouping: {
    placeholder: 'Drag headers ...',
    groupedBy: 'Grouped By:',
  },
  header: {
    actions: 'Actions',
  },
  row: {
    selectTooltip: 'Select',
    detailPanelTooltip: 'Details',
    boolean: {
      true: 'Yes',
      false: 'No',
      undefined: '',
    },
  },
  pagination: {
    labelDisplayedRows: '{from}-{to} of {count}',
    labelRowsSelect: 'rows',
    labelRowsPerPage: 'Rows per page:',
    firstAriaLabel: 'First Page',
    firstTooltip: 'First Page',
    previousAriaLabel: 'Previous Page',
    previousTooltip: 'Previous Page',
    nextAriaLabel: 'Next Page',
    nextTooltip: 'Next Page',
    lastAriaLabel: 'Last Page',
    lastTooltip: 'Last Page',
    pageSizeAll: 'All',
  },
  toolbar: {
    addRemoveColumns: 'Add or remove columns',
    nRowsSelected: '{n} row(s) selected',
    showColumnsTitle: 'Add or remove columns',
    showColumnsAriaLabel: 'Add or remove columns',
    exportTitle: 'Export',
    exportAriaLabel: 'Export',
    exportAsCsv: 'Export as CSV',
    exportAsPdf: 'Export as PDF',
    searchLabel: '',
    searchTooltip: '',
    searchPlaceholder: 'Search',
  },
  filters: {
    standard: 'Standard filter',
    'multiple-choice': 'Multiple choice filter',
    'single-choice': 'Single choice filter',
    range: 'Range filter',
    'greater-than': 'Greater than filter',
    'less-than': 'Less than filter',
    addFilter: 'Add filter',
    filterType: 'Filter type',
    from: 'from',
    to: 'to',
    apply: 'Apply',
  },
};

export function useLocalization() {
  const localizationContext = React.useContext<PartialLocalization>(LocalizationContext);
  return React.useMemo<InternalLocalization>(() => {
    const { body, grouping, header, row, pagination, toolbar, filters } = localizationContext || {};
    const { filterRow, editRow } = body || {};
    return {
      body: {
        ...defaultLocalization.body,
        ...filterFieldsWithDefinedValues(body ?? {}),
        filterRow: {
          ...defaultLocalization.body.filterRow,
          ...filterFieldsWithDefinedValues(filterRow ?? {}),
        },
        editRow: {
          ...defaultLocalization.body.editRow,
          ...filterFieldsWithDefinedValues(editRow ?? {}),
        },
      },
      grouping: {
        ...defaultLocalization.grouping,
        ...filterFieldsWithDefinedValues(grouping ?? {}),
      },
      header: {
        ...defaultLocalization.header,
        ...filterFieldsWithDefinedValues(header ?? {}),
      },
      row: {
        ...defaultLocalization.row,
        ...filterFieldsWithDefinedValues(row ?? {}),
        boolean: {
          ...defaultLocalization.row.boolean,
          ...filterFieldsWithDefinedValues(row?.boolean ?? {}),
        },
      },
      pagination: {
        ...defaultLocalization.pagination,
        ...filterFieldsWithDefinedValues(pagination ?? {}),
      },
      toolbar: {
        ...defaultLocalization.toolbar,
        ...filterFieldsWithDefinedValues(toolbar ?? {}),
      },
      filters: {
        ...defaultLocalization.filters,
        ...filterFieldsWithDefinedValues(filters ?? {}),
      },
    };
  }, [localizationContext]);
}
