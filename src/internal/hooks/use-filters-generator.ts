import * as React from 'react';
import * as ReactTable from 'react-table';
import {
  DateFilters,
  DateTimeFilters,
  FilterValuesBooleanOptionPicker,
  FilterValuesDateInput,
  FilterValuesDateRangePicker,
  FilterValuesDateTimeInput,
  FilterValuesDateTimeRangePicker,
  FilterValuesMultipleOptionPicker,
  FilterValuesMultiStringMultipleOptionPicker,
  FilterValuesNumberGreaterThan,
  FilterValuesNumberInput,
  FilterValuesNumberLessThan,
  FilterValuesNumberRangePicker,
  FilterValuesSingleOptionPicker,
  FilterValuesTextInput,
  FilterValuesTimeInput,
  FilterValuesTimeRangePicker,
  NumericFilters,
  RenderableFilter,
  StringFilters,
  TimeFilters,
} from '../';
import { Column, ColumnType } from '../../column';
import { BuiltInFilterType, Filter, FilterType } from '../../filter';
import { MultiStringFilters } from '../components';
import { useOptions } from './use-options';

export interface UseFiltersGenerator<RowData extends object = {}> {
  resolveFilterType: (columnType: ColumnType, builtInFilterType: BuiltInFilterType) => FilterType;
  generate: (column: Column<RowData>) => Array<Filter<RowData>>;
  resolveFilterRenderer: (column: Column<RowData>, filters: Array<Filter<RowData>>) => RenderableFilter<RowData>;
  resolveFilterInputForm: (column: Column<RowData>, filters: Array<Filter<RowData>>) => RenderableFilter<RowData>;
}

export function useFiltersGenerator<RowData extends object = {}>() {
  const { filtering, builtInFilters } = useOptions();

  const resolveFilterType = React.useCallback<UseFiltersGenerator<RowData>['resolveFilterType']>(
    (columnType, builtInFilterType) => {
      return (
        ((builtInFilterType === 'standard' || builtInFilterType === 'single-choice') &&
          (((columnType === 'date' || columnType === 'time' || columnType === 'datetime') && 'between') ||
            (columnType === 'string' && 'text') ||
            'exact')) ||
        ((builtInFilterType === 'range' || builtInFilterType === 'less-than' || builtInFilterType === 'greater-than') &&
          'between') ||
        (builtInFilterType === 'multiple-choice' && 'includesSome') ||
        'text'
      );
    },
    [],
  );

  const generate = React.useCallback<UseFiltersGenerator<RowData>['generate']>(
    (column) => {
      const { field, type: columnType, useStandardFilter, useBuiltInFilters } = column;
      if (!filtering) {
        return [];
      }

      return (
        builtInFilters?.[columnType ?? 'string']
          ?.filter((builtInFilterType) => useBuiltInFilters !== false || builtInFilterType === 'standard')
          ?.filter((builtInFilterType) => useStandardFilter !== false || builtInFilterType !== 'standard')
          ?.map((builtInFilterType) => ({
            field,
            builtInFilterType,
            type: resolveFilterType(columnType ?? 'string', builtInFilterType),
          })) || []
      );
    },
    [filtering, builtInFilters, resolveFilterType],
  );

  const resolveFilterRenderer = React.useCallback<UseFiltersGenerator<RowData>['resolveFilterRenderer']>(
    (column, filters) => {
      const columnType = column.type || 'string';

      const filterOptions: RenderableFilter<RowData> = {};
      filterOptions.type = columnType;

      const filter = filters.find((activeFilter) => activeFilter.field === column.field);
      if (!!filter) {
        filterOptions.filter =
          (!!column.customFilterAndSearch &&
            filter.builtInFilterType === 'standard' &&
            ((rows: Array<ReactTable.Row<RowData>>, _: any, filterValue: ReactTable.FilterValue) =>
              rows.filter(
                (row) =>
                  !!row.original &&
                  !!column.customFilterAndSearch &&
                  column.customFilterAndSearch(filterValue, row.original),
              ))) ||
          (!column.customFilterAndSearch &&
            columnType === 'multi-string' &&
            ((rows: Array<ReactTable.Row<RowData>>, _: any, filterValue: ReactTable.FilterValue) =>
              rows.filter((row) => {
                const filterValues = (filterValue ?? []) as Array<Array<string>>;
                const rowValues = (column?.value?.(row.original) ??
                  row.original[column.field as keyof RowData] ??
                  []) as Array<string>;
                return filterValues.some((fv) =>
                  fv.every((v) => rowValues.includes(v) && rowValues.every((v) => fv.includes(v))),
                );
              }))) ||
          filter.type;
        filterOptions.Filter =
          (columnType === 'boolean' && filter.builtInFilterType === 'standard' && FilterValuesBooleanOptionPicker) ||
          ((columnType === 'numeric' || columnType === 'currency') && NumericFilters) ||
          (columnType === 'string' && StringFilters) ||
          (columnType === 'multi-string' && MultiStringFilters) ||
          (columnType === 'date' && DateFilters) ||
          (columnType === 'time' && TimeFilters) ||
          (columnType === 'datetime' && DateTimeFilters) ||
          undefined;
      } else {
        filterOptions.disableFilters = true;
      }

      return filterOptions;
    },
    [],
  );

  const resolveFilterInputForm = React.useCallback<UseFiltersGenerator<RowData>['resolveFilterInputForm']>(
    (column, filters) => {
      const columnType = column.type || 'string';

      const filterOptions: RenderableFilter<RowData> = {};
      filterOptions.type = columnType;

      const filter = filters.find((activeFilter) => activeFilter.field === column.field);

      if (!!filter) {
        filterOptions.filter =
          (!!column.customFilterAndSearch &&
            filter.builtInFilterType === 'standard' &&
            ((rows: Array<ReactTable.Row<RowData>>, _: any, filterValue: ReactTable.FilterValue) =>
              rows.filter(
                (row) =>
                  !!row.original &&
                  !!column.customFilterAndSearch &&
                  column.customFilterAndSearch(filterValue, row.original),
              ))) ||
          filter.type;
        filterOptions.Filter =
          (columnType === 'boolean' && filter.builtInFilterType === 'standard' && FilterValuesBooleanOptionPicker) ||
          ((columnType === 'numeric' || columnType === 'currency') &&
            ((filter.builtInFilterType === 'standard' && FilterValuesNumberInput) ||
              (filter.builtInFilterType === 'range' && FilterValuesNumberRangePicker) ||
              (filter.builtInFilterType === 'greater-than' && FilterValuesNumberGreaterThan) ||
              (filter.builtInFilterType === 'less-than' && FilterValuesNumberLessThan) ||
              (filter.builtInFilterType === 'single-choice' && FilterValuesSingleOptionPicker) ||
              (filter.builtInFilterType === 'multiple-choice' && FilterValuesMultipleOptionPicker))) ||
          (columnType === 'string' &&
            ((filter.builtInFilterType === 'standard' && FilterValuesTextInput) ||
              (filter.builtInFilterType === 'single-choice' && FilterValuesSingleOptionPicker) ||
              (filter.builtInFilterType === 'multiple-choice' && FilterValuesMultipleOptionPicker))) ||
          (columnType === 'multi-string' &&
            filter.builtInFilterType === 'multiple-choice' &&
            FilterValuesMultiStringMultipleOptionPicker) ||
          (columnType === 'date' &&
            ((filter.builtInFilterType === 'standard' && FilterValuesDateInput) ||
              (filter.builtInFilterType === 'range' && FilterValuesDateRangePicker))) ||
          (columnType === 'time' &&
            ((filter.builtInFilterType === 'standard' && FilterValuesTimeInput) ||
              (filter.builtInFilterType === 'range' && FilterValuesTimeRangePicker))) ||
          (columnType === 'datetime' &&
            ((filter.builtInFilterType === 'standard' && FilterValuesDateTimeInput) ||
              (filter.builtInFilterType === 'range' && FilterValuesDateTimeRangePicker))) ||
          undefined;
      } else {
        filterOptions.disableFilters = true;
      }

      return filterOptions;
    },
    [],
  );

  return React.useMemo<UseFiltersGenerator<RowData>>(
    () => ({ resolveFilterType, generate, resolveFilterRenderer, resolveFilterInputForm }),
    [resolveFilterType, generate, resolveFilterRenderer, resolveFilterInputForm],
  );
}
