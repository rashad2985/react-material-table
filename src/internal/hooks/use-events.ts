import * as React from 'react';
import { EventsContext } from '../contexts';
import { Column } from '../../column';
import { filterFieldsWithDefinedValues } from '../commons';

export interface Events<DataType extends object = {}> {
  /**
   * To handle page changes
   * @param page - next chosen page number (page numeration starts with 0)
   * @param pageSize - current page size
   */
  onPageChange?: (page: number, pageSize: number) => void;

  /**
   * To handle rows per page / page size changes
   * @param pageSize - next chosen page size
   */
  onRowsPerPageChange?: (pageSize: number) => void;

  /**
   * To handle column show/hidden changes
   * @param column - that will be shown/hidden
   * @param hidden - next state of the column.hidden attribute to be set
   */
  onChangeColumnHidden?: (column: Column<DataType>, hidden: boolean) => void;

  /**
   * To handle column drag and drops
   * @param sourceIndex - old index of the column
   * @param destinationIndex - next index of the column to be set
   */
  onColumnDragged?: (sourceIndex: number, destinationIndex: number) => void;

  /**
   * To handle row order / sorting changes
   * @param orderBy - ordered column index
   * @param orderDirection - order direction of the next sorting, e.g. current 'asc', next 'desc'
   */
  onOrderChange?: (orderBy: number, orderDirection: 'asc' | 'desc' | undefined) => void;

  /**
   * To handle row click (event, rowData)
   * @param event
   * @param rowData - current row data
   * @param togglePanel - if details panel present, then this function can be called to toggle details panel state.
   */
  onRowClick?: (
    event: React.MouseEvent<HTMLTableHeaderCellElement, MouseEvent>,
    rowData: DataType,
    togglePanel?: () => void,
  ) => void;

  /**
   * To handle row selection changes
   * @param data - is the array holding all currently selected rows
   * @param rowData - is the row data of the row that currently triggered selection
   */
  onSelectionChange?: (data: Array<DataType>, rowData?: DataType) => void;

  /**
   * To handle search input changes
   * @param searchText - is the current content of the search input
   */
  onSearchChange?: (searchText: string) => void;

  /**
   * Dispatched, on query of the data is changed
   * @default '@trautmann/react-material-table/on-query-change'
   */
  readonly onQueryChangeEvent: '@trautmann/react-material-table/on-query-change';
}

export function useEvents<RowData extends object = {}>() {
  const eventsContext = React.useContext<Partial<Events<RowData>>>(EventsContext);
  return React.useMemo<Events<RowData>>(
    () => ({
      ...filterFieldsWithDefinedValues(eventsContext ?? {}),
      onQueryChangeEvent: '@trautmann/react-material-table/on-query-change',
    }),
    [eventsContext],
  );
}
