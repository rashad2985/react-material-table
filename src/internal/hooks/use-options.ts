import * as React from 'react';
import { OptionsContext } from '../contexts';
import {
  CheckboxProps,
  DialogProps,
  GridProps,
  GridSize,
  TextFieldProps,
  Breakpoint,
  TablePaginationProps,
} from '@mui/material';
import { Options } from '../../components';
import { Column, EditableMode } from '../../column';
import { BuiltInFilterType } from '../../filter';
import { filterFieldsWithDefinedValues } from '../commons';

type Alignment = 'right' | 'left';

export interface InternalOptions<RowData extends object = {}> {
  /**
   * Flag for columns button that controls which column could be rendered
   * @default false
   */
  columnsButton: boolean;

  /**
   * Debounce interval for search and filter
   * @default 200
   */
  debounceInterval: number;

  /**
   * Detail panel column alignment
   * @default left
   */
  detailPanelColumnAlignment: Alignment;

  /**
   * Detail panel visibility type
   * @default multiple
   */
  detailPanelType: 'single' | 'multiple';

  /**
   * Flag for rendering rows to complete page size
   * @default true
   */
  emptyRowsWhenPaging: boolean;

  /**
   * Flag for export all data instead of rendered data
   * @default false
   */
  exportAllData: boolean;

  /**
   * Flag for export button that render export buttons
   * @default false
   */
  exportButton: boolean;

  /**
   * Delimiter to use in exported CSV file
   * @default ,
   */
  exportDelimiter: string;

  /**
   * Exported file name.
   * @default Table title will be used as file name
   */
  exportFileName: string | undefined;

  /**
   * Function to create a custom CSV file
   * @default undefined
   */
  exportCsv: ((columns: Array<any>, renderData: Array<any>) => void) | undefined;

  /**
   * Function to create a custom PDF file
   * @default undefined
   */
  exportPdf: ((columns: Array<any>, renderData: Array<any>) => void) | undefined;

  /**
   * Fixes given number of columns from left/right so that they are always visible and are not scrolled.
   */
  fixedColumns: { left?: boolean; right?: boolean };

  /**
   * Flag for filtering feature
   * @default false
   */
  filtering: boolean;

  /**
   * Choose/Activate/Disable built-in filters for column types
   * @default {
   *  string: ['standard', 'single-choice', 'multiple-choice'],
   *  multi-string: ['multiple-choice'],
   *  numeric: ['standard', 'range', 'greater-than', 'less-than'],
   *  currency: ['standard', 'range', 'greater-than', 'less-than'],
   *  boolean: ['standard'],
   *  date: ['standard', 'range'],
   *  time: ['standard', 'range'],
   *  datetime: ['standard', 'range'],
   * }
   */
  builtInFilters: Record<Required<Column<RowData>>['type'], Array<BuiltInFilterType>>;

  /**
   * Variant for all input fields
   * @default outlined
   */
  inputVariant: TextFieldProps['variant'];

  /**
   * Flag for header visibility
   * @default true
   */
  header: boolean;

  /**
   * Style to apply all header cells
   * @default undefined
   */
  headerStyle?: React.CSSProperties;

  /**
   * Max body height
   * @default undefined
   */
  maxBodyHeight?: number | string;

  /**
   * Min body height
   * @default undefined
   */
  minBodyHeight?: number | string;

  /**
   * Zero based Initial Page Number
   * @default 0
   */
  initialPage: number;

  /**
   * Padding of cells and rows
   * @default default
   */
  padding: 'default' | 'dense';

  /**
   * Flag for paging feature
   * @default true
   */
  paging: boolean;

  /**
   * Number of rows that would be rendered on every page.
   * @default 5
   */
  pageSize: number;

  /**
   * Option to render all rows on the page
   * @default false
   */
  pageSizeAll: boolean;

  /**
   * Page size options that could be selected by user
   * @default [5, 10, 20]
   */
  pageSizeOptions: TablePaginationProps['rowsPerPageOptions'];

  /**
   * Flag for pagination type
   * @default normal
   */
  paginationType: 'normal' | 'stepped';

  /**
   * Defines pagination position in container.
   * @default bottom
   */
  paginationPosition: 'bottom' | 'top' | 'both';

  /**
   * Css style to be applied rows
   * @default undefined
   */
  rowStyle?: React.CSSProperties | ((rowData: RowData) => React.CSSProperties);

  /**
   * Flag for showing message if there is no data in table
   * @default true
   */
  showEmptyDataSourceMessage: boolean;

  /**
   * Flag for showing first and last page buttons on pagination component
   * @default true
   */
  showFirstLastPageButtons: boolean;

  /**
   * Flag for showing select all checkbox
   * @default true
   */
  showSelectAllCheckbox: boolean;

  /**
   * Flag for showing selected rows text on toolbar
   * @default true
   */
  showTextRowsSelected: boolean;

  /**
   * Flag for search feature
   * @default true
   */
  search: boolean;

  /**
   * Initialize search field focused
   * @default false
   */
  searchAutoFocus: boolean;

  /**
   * Alignment for search text field in toolbar
   * @default right
   */
  searchFieldAlignment: Alignment;

  /**
   * Search field css style
   * @default undefined
   */
  searchFieldStyle?: React.CSSProperties;

  /**
   * Initialize search field before table was render
   * @default undefined
   */
  searchText?: string;

  /**
   * Flag for selection feature
   * @default false
   */
  selection: boolean;

  /**
   * Selection checkbox props
   * @default undefined
   */
  selectionProps?: CheckboxProps | ((rowData: RowData) => CheckboxProps);

  /**
   * Flag to activate or disable sorting feature of table
   * @default true
   */
  sorting: boolean;

  /**
   * Flag for toolbar
   * @default true
   */
  toolbar: boolean;

  /**
   * Flag for title
   * @default true
   */
  showTitle: boolean;

  /**
   * Alignment for buttons in toolbar
   * @default right
   */
  toolbarButtonAlignment: Alignment;

  /**
   * Flag for drag and drop headers
   * @default true
   */
  draggable: boolean;

  /**
   * Flag to allow unsorted state on third header click
   * @default true
   */
  thirdSortClick: boolean;

  /**
   * Loading animation type
   * @default linear
   */
  loadingType: 'overlay' | 'linear' | 'skeleton';

  /**
   * Order of actions column
   * @default 0
   */
  actionsColumnIndex: number;

  /**
   * Add/Update/Delete row Dialog maximum with breakpoint, dialog full screen breakpoint and breakpoint values for inputs in the form.
   * @default {
   *  add: {
   *   grid: {
   *    xs: 12,
   *    sm: 6,
   *    md: 4
   *   },
   *   dialog: {
   *    maxWidth: 'lg',
   *    fullScreenBreakpoint: 'sm'
   *   }
   *  },
   *  edit: {
   *   grid: {
   *    xs: 12,
   *    sm: 6,
   *    md: 4
   *   },
   *   dialog: {
   *    maxWidth: 'lg',
   *    fullScreenBreakpoint: 'sm'
   *   }
   *  },
   *  delete: {
   *   dialog: {
   *    maxWidth: 'sm',
   *    fullScreenBreakpoint: 'sm'
   *   }
   *  }
   * }
   */
  editable: Record<
    EditableMode,
    {
      grid?: Pick<GridProps, 'xs' | 'sm' | 'md' | 'lg' | 'xl'>;
      dialog?: Pick<DialogProps, 'maxWidth'> & { fullScreenBreakpoint?: Breakpoint };
    }
  >;

  /**
   * Setting responsive to true affects the rendering of header and table rows when responsiveBreakPoint is reached:
   * <ul>
   *   <li>Table Headers are hidden from the top of the table.</li>
   *   <li>Rows are transformed into boxes per row.</li>
   *   <li>Each Row contains the header/cell information for each cell as row in a row, where each row in a row box shows header at the left and cell value at the right side by side.</li>
   * </ul>
   */
  responsive: boolean;
  /**
   * Maximum number of cells to be rendered initially in a responsive row. The remaining fields are hidden and row can be expanded to show them.
   * @default 0 (disabled)
   */
  responsiveMaxInitialCells: number;
  /**
   * Breakpoint to transform table entries into boxes below this breakpoint.
   * @default sm
   */
  responsiveBreakPoint: Breakpoint;
  /**
   * Header and value grid proportions on showing header value pair in responsive box. first value is for header and the second one is for value.
   * @default [4, 8]
   */
  responsiveRowHeaderValueProportions: [GridSize, GridSize];
  /**
   * Css style to be applied responsive rows
   * @default undefined
   */
  responsiveRowStyle?: React.CSSProperties | ((rowData: RowData) => React.CSSProperties);
}

const defaultOptions: InternalOptions = {
  columnsButton: false,
  debounceInterval: 200,
  detailPanelColumnAlignment: 'left',
  detailPanelType: 'multiple',
  emptyRowsWhenPaging: true,
  exportAllData: false,
  exportButton: false,
  exportDelimiter: ',',
  exportFileName: undefined,
  exportCsv: undefined,
  exportPdf: undefined,
  fixedColumns: { left: false, right: false },
  filtering: false,
  builtInFilters: {
    string: ['standard', 'single-choice', 'multiple-choice'],
    'multi-string': ['multiple-choice'],
    numeric: ['standard', 'range', 'greater-than', 'less-than'],
    currency: ['standard', 'range', 'greater-than', 'less-than'],
    boolean: ['standard'],
    date: ['standard', 'range'],
    time: ['standard', 'range'],
    datetime: ['standard', 'range'],
  },
  inputVariant: 'outlined',
  header: true,
  initialPage: 0,
  padding: 'default',
  paging: true,
  pageSize: 5,
  pageSizeAll: false,
  pageSizeOptions: [5, 10, 20],
  paginationType: 'normal',
  paginationPosition: 'bottom',
  showEmptyDataSourceMessage: true,
  showFirstLastPageButtons: true,
  showSelectAllCheckbox: true,
  showTextRowsSelected: true,
  search: true,
  searchAutoFocus: false,
  searchFieldAlignment: 'right',
  selection: false,
  sorting: true,
  toolbar: true,
  showTitle: true,
  toolbarButtonAlignment: 'right',
  draggable: true,
  thirdSortClick: true,
  loadingType: 'linear',
  actionsColumnIndex: 0,
  editable: {
    add: {
      grid: {
        xs: 12,
        sm: 6,
        md: 4,
      },
      dialog: {
        maxWidth: 'lg',
        fullScreenBreakpoint: 'sm',
      },
    },
    edit: {
      grid: {
        xs: 12,
        sm: 6,
        md: 4,
      },
      dialog: {
        maxWidth: 'lg',
        fullScreenBreakpoint: 'sm',
      },
    },
    delete: {
      grid: {},
      dialog: {
        maxWidth: 'sm',
        fullScreenBreakpoint: 'sm',
      },
    },
  },
  responsive: false,
  responsiveMaxInitialCells: 0,
  responsiveBreakPoint: 'sm',
  responsiveRowHeaderValueProportions: [4, 8],
};

export function useOptions<T extends object = {}>() {
  const optionsContext = React.useContext<Partial<Options<T>>>(OptionsContext);
  const { builtInFilters, editable, fixedColumns = {}, ...others } = optionsContext;
  const {
    builtInFilters: defaultBuiltInFilters,
    editable: defaultEditable,
    fixedColumns: defaultFixedColumns,
    ...defaultOthers
  } = defaultOptions;

  return React.useMemo<InternalOptions<T>>(
    () => ({
      ...defaultOthers,
      ...filterFieldsWithDefinedValues(others ?? {}),
      fixedColumns: {
        ...defaultFixedColumns,
        ...filterFieldsWithDefinedValues(fixedColumns),
      },
      builtInFilters: {
        ...defaultBuiltInFilters,
        ...filterFieldsWithDefinedValues(builtInFilters ?? {}),
      },
      editable: {
        add: {
          grid: {
            ...(defaultEditable?.add?.grid ?? {}),
            ...filterFieldsWithDefinedValues(editable?.add?.grid ?? {}),
          },
          dialog: {
            ...(defaultEditable?.add?.dialog ?? {}),
            ...filterFieldsWithDefinedValues(editable?.add?.dialog ?? {}),
          },
        },
        edit: {
          grid: {
            ...(defaultEditable?.edit?.grid ?? {}),
            ...filterFieldsWithDefinedValues(editable?.edit?.grid ?? {}),
          },
          dialog: {
            ...(defaultEditable?.edit?.dialog ?? {}),
            ...filterFieldsWithDefinedValues(editable?.edit?.dialog ?? {}),
          },
        },
        delete: {
          grid: {
            ...(defaultEditable?.delete?.grid ?? {}),
            ...filterFieldsWithDefinedValues(editable?.delete?.grid ?? {}),
          },
          dialog: {
            ...(defaultEditable?.delete?.dialog ?? {}),
            ...filterFieldsWithDefinedValues(editable?.delete?.dialog ?? {}),
          },
        },
      },
    }),
    [
      builtInFilters,
      defaultBuiltInFilters,
      defaultEditable,
      defaultFixedColumns,
      defaultOthers,
      editable,
      fixedColumns,
      others,
    ],
  );
}
