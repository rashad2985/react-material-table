import {
  CircularProgress,
  IconButton,
  Paper,
  SvgIconTypeMap,
  TableBody,
  TableBodyProps,
  TableCell,
} from '@mui/material';
import { OverridableComponent } from '@mui/material/OverridableComponent';
import { TableCellProps } from '@mui/material/TableCell';
import * as React from 'react';
import { ResponsiveRow, Row, RowProps, Toolbar, ToolbarProps } from '../../components';
import { TablePagination, TablePaginationProps } from '../../components/table-pagination';
import { ComponentsContext } from '../contexts';
import { EmptyTableRow, EmptyTableRowProps } from '../../components/empty-table-row';
import { LoadingTableRow, LoadingTableRowProps } from '../../components/loading-table-row';
import { filterFieldsWithDefinedValues } from '../commons';

export interface Components<RowData extends object = {}> {
  /**
   * A completely independent component to call/show add row dialog
   * @param showForm, calls the form to input data. E.g. could be used for onClick event of the custom component.
   * @param Icon is either the default Add button icon or an icon that provided via icons override.
   * @param disabled disabled state to set.
   * @default IconButton
   */
  Add: (showForm: () => void, Icon: OverridableComponent<SvgIconTypeMap>, disabled: boolean) => React.ReactElement;

  /**
   * A completely independent component to call/show edit row dialog
   * @param showForm, calls the form to input data. E.g. could be used for onClick event of the custom component.
   * @param Icon is either the default Edit button icon or an icon that provided via icons override.
   * @param disabled disabled state to set.
   * @default IconButton
   */
  Edit: (showForm: () => void, Icon: OverridableComponent<SvgIconTypeMap>, disabled: boolean) => React.ReactElement;

  /**
   * A completely independent component to call/show delete row dialog
   * @param showForm, calls the dialog to confirm the operation. E.g. could be used for onClick event of the custom component.
   * @param Icon is either the default Delete button icon or an icon that provided via icons override.
   * @param disabled disabled state to set.
   * @default IconButton
   */
  Delete: (showForm: () => void, Icon: OverridableComponent<SvgIconTypeMap>, disabled: boolean) => React.ReactElement;

  /**
   * Action component
   * @default IconButton
   */
  Action: React.ComponentType<any>;

  /**
   * Actions component
   */
  Actions?: React.ComponentType<any>;

  /**
   * Body component
   * @default TableBody
   */
  Body: React.ComponentType<TableBodyProps<any>>;

  /**
   * Cell component
   * @default TableCell
   */
  Cell: React.ComponentType<TableCellProps>;

  /**
   * Container that everything renders in
   * @default Paper
   */
  Container: React.ComponentType<any>;

  /**
   * EditField component
   */
  EditField?: React.ElementType;

  /**
   * EditRow component
   */
  EditRow?: React.ElementType;

  /**
   * FilterRow component
   */
  FilterRow?: React.ElementType;

  /**
   * FilterRow component
   */
  Groupbar?: React.ElementType;

  /**
   * Header component
   */
  Header?: React.ComponentType<TableCellProps>;

  /**
   * Overlay loading component
   * @default CircularProgress
   */
  OverlayLoading: React.ComponentType<any>;

  /**
   * Pagination component
   * @default TablePagination
   */
  Pagination: React.ComponentType<TablePaginationProps<RowData>>;

  /**
   * Row component
   * @default Row (TableRow)
   */
  Row: React.ComponentType<RowProps<RowData>>;

  /**
   * Empty Row component
   * @default EmptyTableRow (TableRow)
   */
  EmptyRow: React.ComponentType<EmptyTableRowProps<RowData>>;

  /**
   * Loading Row component
   * @default LoadingTableRow (TableRow), that shows skeleton on loading type skeleton else an empty row.
   */
  LoadingRow: React.ComponentType<LoadingTableRowProps<RowData>>;

  /**
   * Responsive Row component
   * @default ResponsiveRow (TableRow)
   */
  ResponsiveRow: React.ComponentType<RowProps<RowData>>;
  /**
   * Toolbar component
   * @default Grid
   */
  Toolbar: React.ComponentType<ToolbarProps<RowData>>;
}

function useDefaultComponents<RowData extends object = {}>(): Components<RowData> {
  // const {padding} = useOptions<RowData>();
  return React.useMemo(
    () => ({
      Add: (showForm, Icon, disabled) => (
        <IconButton onClick={showForm} disabled={disabled} size="large">
          <Icon fontSize="small" />
        </IconButton>
      ),
      Edit: (showForm, Icon, disabled) => (
        <IconButton onClick={showForm} disabled={disabled} size="large">
          <Icon fontSize="small" />
        </IconButton>
      ),
      Delete: (showForm, Icon, disabled) => (
        <IconButton onClick={showForm} disabled={disabled} size="large">
          <Icon fontSize="small" />
        </IconButton>
      ),
      Action: (props) => <IconButton {...props} size="large" />,
      Body: (props) => <TableBody {...props} />,
      Cell: (props) => <TableCell {...props} />,
      Header: (props) => <TableCell {...props} />,
      Container: React.forwardRef((props, ref) => <Paper ref={ref} {...props} />),
      OverlayLoading: (props) => <CircularProgress {...props} />,
      Pagination: (props) => <TablePagination {...props} />,
      Row: (props) => <Row {...props} />,
      EmptyRow: (props) => <EmptyTableRow {...props} />,
      LoadingRow: (props) => <LoadingTableRow {...props} />,
      ResponsiveRow: (props) => <ResponsiveRow {...props} />,
      Toolbar: (props) => <Toolbar {...props} />,
    }),
    [],
  );
}

export function useComponents<RowData extends object = {}>() {
  const componentsContext = React.useContext<Partial<Components<RowData>>>(ComponentsContext);
  const defaultComponents = useDefaultComponents<RowData>();
  return React.useMemo<Components<RowData>>(
    () => ({
      ...defaultComponents,
      ...filterFieldsWithDefinedValues(componentsContext ?? {}),
    }),
    [componentsContext, defaultComponents],
  );
}
