import * as React from 'react';
import { RowType, TableProps } from '../../components';
import { useOptions } from './use-options';
import { DetailPanel } from '../../components/detail-panel';

const init = <RowData extends object = {}>(rows: Array<RowType<RowData>>, detailsPanelsLength: number): Panels =>
  (rows ?? []).reduce(
    (panels, row) => ({
      ...panels,
      [row.id]: Array<boolean>(detailsPanelsLength).fill(false),
    }),
    {} as Panels,
  );

export type Panels = Record<string, Array<boolean>>;

export type UseDetailPanels = {
  openedPanels: Panels;
  togglePanel: (rowId: string, detailsPanelIndex: number) => void;
};

export function useDetailPanels<RowData extends object = {}>(
  rows: Array<RowType<RowData>>,
  detailPanel: TableProps<RowData>['detailPanel'],
) {
  const { detailPanelType } = useOptions<RowData>();

  const detailsPanelLength = React.useMemo<number>(
    () =>
      !!detailPanel && rows.length > 0
        ? Math.max(
            ...rows.map(
              (row) =>
                (
                  (Array.isArray(detailPanel) &&
                    detailPanel.map<DetailPanel<RowData>>((dp) =>
                      typeof dp === 'function' ? dp(row.original) : dp,
                    )) ||
                  (typeof detailPanel === 'function' && [detailPanel(row.original)]) ||
                  (!Array.isArray(detailPanel) && typeof detailPanel !== 'function' && [detailPanel]) ||
                  []
                ).length,
            ),
          )
        : 0,
    [rows, detailPanel],
  );

  const [openedPanels, setOpenedPanels] = React.useState<Panels>(init(rows, detailsPanelLength));

  React.useEffect(() => setOpenedPanels(init(rows, detailsPanelLength)), [rows, detailsPanelLength]);

  const togglePanel = React.useCallback<UseDetailPanels['togglePanel']>(
    (rowId, detailsPanelIndex) =>
      setOpenedPanels((prevState) => {
        const prevPanelState = prevState?.[rowId]?.[detailsPanelIndex];
        const newState: Panels = detailPanelType === 'single' ? init(rows, detailsPanelLength) : { ...prevState };

        if (newState?.[rowId]?.[detailsPanelIndex] !== undefined) {
          newState[rowId] = newState[rowId].map((_, index) => (index === detailsPanelIndex ? !prevPanelState : false));
          return newState;
        } else {
          return prevState;
        }
      }),
    [detailPanelType, rows, detailsPanelLength],
  );

  return React.useMemo<UseDetailPanels>(() => ({ openedPanels, togglePanel }), [openedPanels, togglePanel]);
}
