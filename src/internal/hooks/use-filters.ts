import * as React from 'react';
import { Filter } from '../../filter';
import { FiltersContextProps } from '../contexts';

export function useFilters<T extends object = {}>() {
  const [filters, set] = React.useState<Array<Filter<T>>>([]);

  const setFilters = React.useCallback((values: Array<Filter<T>>) => set(values), []);

  return React.useMemo<FiltersContextProps<T>>(() => ({ filters, setFilters }), [filters, setFilters]);
}
