import * as React from 'react';
import { SvgIconTypeMap } from '@mui/material';
import { IconsContext } from '../contexts';
import ViewColumnIcon from '@mui/icons-material/ViewColumn';
import FilterListIcon from '@mui/icons-material/FilterList';
import { OverridableComponent } from '@mui/material/OverridableComponent';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ClearIcon from '@mui/icons-material/Clear';
import SearchIcon from '@mui/icons-material/Search';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutline';
import { filterFieldsWithDefinedValues } from '../commons';

export interface Icons {
  /**
   * Add icon
   * @default mui icons 'add'
   */
  Add: OverridableComponent<SvgIconTypeMap>;

  /**
   * Edit icon
   * @default mui icons 'edit'
   */
  Edit: OverridableComponent<SvgIconTypeMap>;

  /**
   * Delete icon
   * @default mui icons 'delete_outline'
   */
  Delete: OverridableComponent<SvgIconTypeMap>;

  /**
   * DetailPanel icon
   * @default mui icons 'chevron_right'
   */
  DetailPanel: OverridableComponent<SvgIconTypeMap>;

  /**
   * Export icon
   * @default mui icons 'save_alt'
   */
  Export: OverridableComponent<SvgIconTypeMap>;

  /**
   * Filter icon
   * @default mui icons 'filter_list'
   */
  Filter: OverridableComponent<SvgIconTypeMap>;

  /**
   * FirstPage icon
   * @default mui icons 'first_page'
   */
  FirstPage: OverridableComponent<SvgIconTypeMap>;

  /**
   * LastPage icon
   * @default mui icons 'last_page'
   */
  LastPage: OverridableComponent<SvgIconTypeMap>;

  /**
   * NextPage icon
   * @default mui icons 'chevron_right'
   */
  NextPage: OverridableComponent<SvgIconTypeMap>;

  /**
   * PreviousPage icon
   * @default mui icons 'chevron_left'
   */
  PreviousPage: OverridableComponent<SvgIconTypeMap>;

  /**
   * ResetSearch icon
   * @default mui icons 'clear'
   */
  ResetSearch: OverridableComponent<SvgIconTypeMap>;

  /**
   * Search icon
   * @default mui icons 'search'
   */
  Search: OverridableComponent<SvgIconTypeMap>;

  /**
   * SortArrow icon
   * @default mui icons 'arrow_upward'
   */
  SortArrow: OverridableComponent<SvgIconTypeMap>;

  /**
   * ViewColumn icon
   * @default mui icons 'view_column'
   */
  ViewColumn: OverridableComponent<SvgIconTypeMap>;

  /**
   * Action group icon
   * @default mui icons 'more_vert'
   */
  ActionGroup: OverridableComponent<SvgIconTypeMap>;
}

const defaultIcons: Icons = {
  Add: AddIcon,
  Edit: EditIcon,
  Delete: DeleteIcon,
  DetailPanel: ChevronRightIcon,
  Export: SaveAltIcon,
  Filter: FilterListIcon,
  FirstPage: FirstPageIcon,
  LastPage: LastPageIcon,
  NextPage: ChevronRightIcon,
  PreviousPage: ChevronLeftIcon,
  ResetSearch: ClearIcon,
  Search: SearchIcon,
  SortArrow: ArrowUpwardIcon,
  ViewColumn: ViewColumnIcon,
  ActionGroup: MoreVertIcon,
};

export function useIcons() {
  const iconsContext = React.useContext<Partial<Icons>>(IconsContext);
  return React.useMemo<Icons>(
    () => ({
      ...defaultIcons,
      ...filterFieldsWithDefinedValues(iconsContext ?? {}),
    }),
    [iconsContext],
  );
}
