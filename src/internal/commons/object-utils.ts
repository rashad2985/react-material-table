export function filterFieldsWithDefinedValues<T extends object>(obj: T): T {
  return Object.entries(obj)
    .filter(([, value]) => value !== undefined || typeof value !== 'undefined')
    .reduce((newObj, [key, value]) => ({ ...newObj, [key]: value }), {}) as T;
}
