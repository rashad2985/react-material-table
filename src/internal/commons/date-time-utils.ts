export function toDate(dateTime: Date) {
  return new Date(dateTime.getFullYear(), dateTime.getMonth(), dateTime.getDate(), 0, 0, 0, 0);
}

export function toDateEnd(dateTime: Date) {
  return new Date(dateTime.getFullYear(), dateTime.getMonth(), dateTime.getDate(), 23, 59, 59, 999);
}

export function toFullTime(dateTime: Date) {
  return new Date(
    0,
    0,
    0,
    dateTime.getHours(),
    dateTime.getMinutes(),
    dateTime.getSeconds(),
    dateTime.getMilliseconds(),
  );
}

export function toTime(dateTime: Date) {
  return new Date(0, 0, 0, dateTime.getHours(), dateTime.getMinutes(), 0, 0);
}

export function toTimeEnd(dateTime: Date) {
  return new Date(0, 0, 0, dateTime.getHours(), dateTime.getMinutes(), 59, 999);
}

export function toDateTime(dateTime: Date) {
  return new Date(
    dateTime.getFullYear(),
    dateTime.getMonth(),
    dateTime.getDate(),
    dateTime.getHours(),
    dateTime.getMinutes(),
    0,
    0,
  );
}

export function toDateTimeEnd(dateTime: Date) {
  return new Date(
    dateTime.getFullYear(),
    dateTime.getMonth(),
    dateTime.getDate(),
    dateTime.getHours(),
    dateTime.getMinutes(),
    59,
    999,
  );
}

export const hoursAndMinutes: Intl.DateTimeFormatOptions = { hour: '2-digit', minute: '2-digit' };
export const dateTimeFormatOptions: Intl.DateTimeFormatOptions = {
  ...hoursAndMinutes,
  year: 'numeric',
  month: '2-digit',
  day: '2-digit',
};
