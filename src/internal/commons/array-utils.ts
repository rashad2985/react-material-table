export function insert<T>(array: Array<T>, index: number, element: T | undefined) {
  if (element !== undefined) {
    array.splice(index < 0 ? array.length - Math.abs(index) + 1 : index, 0, element);
  }
  return array;
}
