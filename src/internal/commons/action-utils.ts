import { Action } from '../../action';

export function isRowAction<DataType extends object = {}>(action: Action<DataType>): boolean {
  const { isFreeAction = false, position = 'row' } = action || {};
  return !!action && !isFreeAction && position === 'row';
}

export function isToolbarAction<DataType extends object = {}>(action: Action<DataType>): boolean {
  return !isRowAction(action);
}
