import { MenuProps, PaperProps } from '@mui/material';

export * from './noop';
export * from './array-utils';
export * from './defined';
export * from './format-localization';
export * from './table-cell-utils';
export * from './date-time-utils';
export * from './currency-utils';
export * from './number-utils';
export * from './object-utils';
export * from './action-utils';

export const commonMenuProps: Partial<MenuProps> = {
  anchorOrigin: {
    vertical: 'bottom',
    horizontal: 'left',
  },
  transformOrigin: {
    vertical: 'top',
    horizontal: 'left',
  },
  style: {
    maxHeight: '30vh',
  },
};

export const autocompleteListStyle: Required<PaperProps>['style'] = {
  maxHeight: '30vh',
  overflowY: 'auto',
};
