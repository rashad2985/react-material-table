export function formatLocalization(text: string, replacements: Record<string, string>) {
  return text.replace(/{(\w+)}/g, (placeholderWithDelimiters, placeholderWithoutDelimiters) =>
    replacements.hasOwnProperty(placeholderWithoutDelimiters)
      ? replacements[placeholderWithoutDelimiters]
      : placeholderWithDelimiters,
  );
}
