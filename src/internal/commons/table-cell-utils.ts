import { TableCellProps } from '@mui/material/TableCell';
import { Column } from '../../column';
import { Localization } from '../../components';

export function resolveCellAlign<DataType extends object = {}>(column: Column<DataType>): TableCellProps['align'] {
  const { type = 'string', align } = column;
  return (
    align ?? (((type === 'string' || type === 'multi-string') && 'left') || (type === 'boolean' && 'center') || 'right')
  );
}

// eslint-disable-next-line
export function formatCell<DataType extends object = {}, DataKey extends keyof DataType = never>(
  column: Column<DataType>,
  data: DataType,
  localization: Localization,
): string {
  const { field, lookup: initialLookup, dateSetting, currencySetting, numberSettings, emptyValue = '' } = column;
  const lookup: Column<DataType>['lookup'] =
    initialLookup ??
    ((column.type === 'boolean' &&
      new Map<string, string>([
        ['true', localization.row?.boolean?.true ?? 'Yes'],
        ['false', localization.row?.boolean?.false ?? 'No'],
        ['undefined', localization.row?.boolean?.undefined ?? ''],
      ])) ||
      undefined);
  const { locale: dateLocale, formatOptions: dateFormatOptions } = dateSetting || {};
  const { locale: currencyLocale, formatOptions: currencyFormatOptions } = currencySetting || {};
  const { locale: numberLocale, formatOptions: numberFormatOptions } = numberSettings || {};
  const value =
    (!!column.value && column.value(data)) ||
    (!column.value && !!field && data.hasOwnProperty(field) ? data[field as keyof DataType] : undefined);

  return (
    (!!lookup &&
      (column.type === 'multi-string'
        ? ((value ?? []) as unknown as Array<string>)
            .map((str) => (lookup ?? new Map<string, string>()).get(`${str}`))
            .filter((str) => !!str)
            .join(', ')
        : (lookup ?? new Map<string, string>()).get(`${value}`))) ||
    (column.type === 'multi-string' && !!value && (value as unknown as Array<string>).join(', ')) ||
    (column.type === 'currency' &&
      !!value &&
      !!currencyFormatOptions?.currency &&
      new Intl.NumberFormat(currencyLocale, { ...(currencyFormatOptions || {}), style: 'currency' }).format(
        Number(value),
      )) ||
    (column.type === 'numeric' &&
      !!value &&
      new Intl.NumberFormat(numberLocale, { ...(numberFormatOptions || {}) }).format(Number(value))) ||
    (column.type === 'date' &&
      !!value &&
      new Date(value as unknown as string | number).toLocaleDateString(dateLocale, dateFormatOptions)) ||
    (column.type === 'time' &&
      !!value &&
      new Date(value as unknown as string | number).toLocaleTimeString(dateLocale, dateFormatOptions)) ||
    (column.type === 'datetime' &&
      !!value &&
      new Date(value as unknown as string | number).toLocaleString(dateLocale, dateFormatOptions)) ||
    (value ?? (typeof emptyValue === 'function' ? emptyValue() : emptyValue))
  );
}
