import { ColumnType, NumberSettings } from '../../column';

export function formatNumber(value: number | undefined, columnType: ColumnType, numberSettings?: NumberSettings) {
  return (
    (columnType === 'numeric' &&
      value !== undefined &&
      new Intl.NumberFormat(numberSettings?.locale, { ...(numberSettings?.formatOptions ?? {}) }).format(
        Number(value),
      )) ||
    undefined
  );
}
