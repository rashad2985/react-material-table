import { ColumnType, CurrencySettings } from '../../column';

export function formatCurrency(value: number | undefined, columnType: ColumnType, currencySetting?: CurrencySettings) {
  return (
    (columnType === 'currency' &&
      value !== undefined &&
      !!currencySetting?.formatOptions?.currency &&
      new Intl.NumberFormat(currencySetting.locale, { ...currencySetting.formatOptions, style: 'currency' }).format(
        Number(value),
      )) ||
    undefined
  );
}
