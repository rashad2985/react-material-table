import { Typography } from '@mui/material';
import * as React from 'react';
import { FilterValuesPickerProps } from '../../../filter';
import { FilterValuesContainer } from './filter-values-container';
import { hoursAndMinutes } from '../../commons';
import { Column } from '../../../column';
import { useFiltersGenerator } from '../../hooks';
import { FiltersContext, FiltersContextProps } from '../../contexts';

export function TimeFilters<DataType extends object = {}>({
  column,
}: FilterValuesPickerProps<DataType, [number | undefined, number | undefined]>) {
  const { filterValue, setFilter, render, dateSetting, defaultFilter } = column;
  const { locale } = dateSetting || {};
  const { filters } = React.useContext<FiltersContextProps<DataType>>(FiltersContext);
  const currentFilter = React.useMemo(
    () => filters.find((filter) => filter.field === column.field),
    [filters, column.field],
  );
  const { resolveFilterInputForm } = useFiltersGenerator<DataType>();

  const filterValueContainerLabel = React.useMemo(
    () =>
      (currentFilter?.builtInFilterType === 'standard' && Array.isArray(filterValue) && (
        <React.Fragment>
          {render('Header')}
          {filterValue?.[0] !== undefined
            ? `: "${new Date(filterValue?.[0]).toLocaleTimeString(locale, hoursAndMinutes)}"`
            : ''}
        </React.Fragment>
      )) ||
      (currentFilter?.builtInFilterType === 'range' && Array.isArray(filterValue) && (
        <React.Fragment>
          {filterValue?.[0] === undefined
            ? ''
            : `${new Date(filterValue?.[0]).toLocaleTimeString(locale, hoursAndMinutes)}${String.fromCharCode(
                160,
                8804,
                160,
              )}`}
          {render('Header')}
          {filterValue?.[1] === undefined
            ? ''
            : `${String.fromCharCode(160, 8804, 160)}${new Date(filterValue?.[1]).toLocaleTimeString(
                locale,
                hoursAndMinutes,
              )}`}
        </React.Fragment>
      )) ||
      render('Header'),
    [currentFilter, filterValue, render, locale],
  );

  const FilterInputForm = React.useMemo(
    () => resolveFilterInputForm(column as unknown as Column<DataType>, filters).Filter,
    [resolveFilterInputForm, column, filters],
  );

  const [currentValue, setCurrentValue] =
    React.useState<
      FilterValuesPickerProps<DataType, [number | undefined, number | undefined]>['column']['filterValue']
    >(filterValue);
  const changeCurrentValue = React.useCallback(
    (newInputValue: typeof currentValue) => setCurrentValue(newInputValue),
    [],
  );
  const resetCurrentFilterValue = React.useCallback(() => {
    setFilter(undefined);
    setCurrentValue(undefined);
  }, [setFilter]);
  const applyFilter = React.useCallback(() => setFilter(currentValue || undefined), [currentValue, setFilter]);

  React.useEffect(() => {
    if (!!defaultFilter) {
      setFilter(defaultFilter);
    }
  }, [defaultFilter, setFilter]);

  return React.useMemo(
    () => (
      <FilterValuesContainer<DataType>
        column={column}
        label={
          <Typography component="div" variant="inherit" noWrap>
            {filterValueContainerLabel}
          </Typography>
        }
        clearFilterValues={setFilter}
        apply={applyFilter}
        resetCurrentFilterValue={resetCurrentFilterValue}
      >
        {!!FilterInputForm && (
          <FilterInputForm column={{ ...column, filterValue: currentValue, setFilter: changeCurrentValue }} />
        )}
      </FilterValuesContainer>
    ),
    [
      applyFilter,
      changeCurrentValue,
      currentValue,
      resetCurrentFilterValue,
      column,
      filterValueContainerLabel,
      setFilter,
      FilterInputForm,
    ],
  );
}
