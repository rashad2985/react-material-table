import { Box, Checkbox, ListItemText, MenuItem, TextField, Typography } from '@mui/material';
import * as React from 'react';
import { FilterValuesPickerProps } from '../../../filter';
import { useStyles } from '../../../styles';
import { useOptions } from '../../hooks';
import { commonMenuProps } from '../../commons';

export function FilterValuesMultiStringMultipleOptionPicker<T extends object = {}>({
  column,
}: FilterValuesPickerProps<T, Array<Array<string>>>) {
  const { id = '', preFilteredRows = [], filterValue = [], setFilter, render, filterPlaceholder } = column;
  const styles = useStyles();
  const { inputVariant } = useOptions<T>();
  const options = React.useMemo<Array<Array<string>>>(() => {
    const optionsSet: Array<Array<string>> = [];
    preFilteredRows.forEach((row) => {
      const newOption: Array<string> = [...(row.values[id] ?? [])];
      const exists: boolean =
        newOption.length > 0 &&
        optionsSet.some(
          (existingOption) =>
            existingOption.every((item) => newOption.includes(item)) &&
            newOption.every((item) => existingOption.includes(item)),
        );
      if (!exists) {
        optionsSet.push(newOption);
      }
    });
    return [...optionsSet];
  }, [id, preFilteredRows]);

  const selectedValues = React.useMemo<Array<Array<string>>>(() => filterValue ?? [], [filterValue]);

  const handleChange = React.useCallback(
    (event: React.ChangeEvent<{ value: unknown }>) => {
      const newSelectedValues = (event?.target?.value ?? []) as Array<Array<string>>;
      const newFilterValue = options.filter((option) =>
        newSelectedValues.some(
          (newSelectedValue) =>
            option.every((item) => newSelectedValue.includes(item)) &&
            newSelectedValue.every((item) => option.includes(item)),
        ),
      );
      setFilter(newFilterValue.length > 0 ? newFilterValue : undefined);
    },
    [setFilter, options],
  );

  const items = React.useMemo(
    () =>
      options.map((option) => (
        <MenuItem
          key={String(option)}
          value={option}
          selected={selectedValues.some((selectedValue) =>
            selectedValue.every(
              (item) => option.includes(item) && option.every((item) => selectedValue.includes(item)),
            ),
          )}
        >
          <Checkbox
            checked={selectedValues.some((selectedValue) =>
              selectedValue.every(
                (item) => option.includes(item) && option.every((item) => selectedValue.includes(item)),
              ),
            )}
          />
          <ListItemText primary={option.map((key) => column?.lookup?.get?.(key) ?? key).join(', ')} />
        </MenuItem>
      )),
    [options, selectedValues, column?.lookup],
  );

  return React.useMemo(
    () => (
      <Box minWidth={128} maxWidth={384}>
        <TextField
          select
          margin="dense"
          size="small"
          variant={inputVariant}
          fullWidth
          label={
            <Typography component="div" variant="inherit" noWrap>
              {render('Header')}
            </Typography>
          }
          value={selectedValues}
          onChange={handleChange}
          className={styles.select}
          placeholder={filterPlaceholder}
          SelectProps={{
            multiple: true,
            renderValue: (selected: Array<Array<string>>) =>
              (selected ?? [])
                .map((option) => `"${option.map((key) => column?.lookup?.get?.(key) ?? key).join(', ')}"`)
                .join(', '),
            MenuProps: commonMenuProps,
          }}
        >
          {items}
        </TextField>
      </Box>
    ),
    [inputVariant, render, selectedValues, items, handleChange, filterPlaceholder, styles.select, column?.lookup],
  );
}
