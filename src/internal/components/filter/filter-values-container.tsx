import * as React from 'react';
import * as ReactTable from 'react-table';
import { Button, Chip, ChipProps, Grid, ListItemText, MenuItem, Paper, Popover, TextField } from '@mui/material';
import { useStyles } from '../../../styles';
import { FiltersContext, FiltersContextProps } from '../../contexts';
import { BuiltInFilterType, Filter } from '../../../filter';
import { useFiltersGenerator, useLocalization, useOptions } from '../../hooks';
import { Column } from '../../../column';
import { commonMenuProps } from '../../commons';

const ID = 'trautmann-material-table-filter-values-container';

export interface FilterValuesContainerProps<RowData extends object = {}> extends Pick<ChipProps, 'label'> {
  column: ReactTable.ColumnInstance<RowData> & Pick<Column<RowData>, 'type'>;
  children: React.ReactNode | Array<React.ReactNode>;
  clearFilterValues: (value: unknown | undefined) => void;
  resetCurrentFilterValue: () => void;
  apply: () => void;
}

export function FilterValuesContainer<RowData extends object = {}>(props: FilterValuesContainerProps<RowData>) {
  const { column, children, label, clearFilterValues, resetCurrentFilterValue, apply: applyFilter } = props;

  const localization = useLocalization();
  const { resolveFilterType, generate } = useFiltersGenerator<RowData>();
  const styles = useStyles();
  const { padding, inputVariant } = useOptions<RowData>();

  const [anchorEl, setAnchorEl] = React.useState<HTMLDivElement | null>(null);
  const handleClick = React.useCallback(
    (event: React.MouseEvent<HTMLDivElement>) => setAnchorEl(event.currentTarget),
    [],
  );

  const handleClose = React.useCallback(() => setAnchorEl(null), []);
  const apply = React.useCallback(() => {
    applyFilter();
    handleClose();
  }, [applyFilter, handleClose]);

  const { filters: activeFilters, setFilters: setActiveFilters } =
    React.useContext<FiltersContextProps<RowData>>(FiltersContext);

  const removeFilter = React.useCallback(() => {
    clearFilterValues(undefined);
    setActiveFilters([...activeFilters.filter((filter) => filter.field !== column.id)]);
    handleClose();
  }, [activeFilters, clearFilterValues, setActiveFilters, handleClose, column.id]);

  const changeFilterType = React.useCallback(
    (event: React.ChangeEvent<{ value: unknown }>) => {
      resetCurrentFilterValue();
      const builtInFilterType = event.target.value as BuiltInFilterType;
      const newActiveFilters = activeFilters.map<Filter<RowData>>((activeFilter) =>
        activeFilter.field !== column.id
          ? activeFilter
          : {
              ...activeFilter,
              builtInFilterType,
              type: resolveFilterType(column.type || 'string', builtInFilterType),
            },
      );
      setActiveFilters(newActiveFilters);
    },
    [activeFilters, setActiveFilters, column, resolveFilterType, resetCurrentFilterValue],
  );

  const options = React.useMemo(() => {
    const columnFilters = generate(column as unknown as Column<RowData>);
    return columnFilters.map((columnFilter) => (
      <MenuItem key={columnFilter.builtInFilterType} value={columnFilter.builtInFilterType}>
        <ListItemText primary={localization.filters[columnFilter.builtInFilterType]} />
      </MenuItem>
    ));
  }, [generate, column, localization]);

  const activeFilterType = React.useMemo<BuiltInFilterType | undefined>(
    () => activeFilters.find((activeFilter) => activeFilter.field === column.id)?.builtInFilterType,
    [activeFilters, column],
  );

  const open = React.useMemo<boolean>(() => Boolean(anchorEl), [anchorEl]);
  const id = React.useMemo<string | undefined>(() => (open ? `${ID}-${column.id}` : undefined), [open, column.id]);

  return React.useMemo(
    () => (
      <React.Fragment>
        <Chip
          id={id}
          label={label}
          onClick={handleClick}
          onDelete={removeFilter}
          sx={{ maxWidth: { xs: 256, sm: 384 } }}
          size={padding === 'dense' ? 'small' : undefined}
        />
        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <Paper elevation={0} sx={{ p: 1 }}>
            <Grid container direction="column" justifyContent="flex-start" alignItems="stretch" spacing={2}>
              {options.length > 1 && (
                <Grid item>
                  <TextField
                    select
                    margin="dense"
                    size="small"
                    variant={inputVariant}
                    fullWidth
                    label={localization.filters.filterType}
                    value={activeFilterType ?? ''}
                    onChange={changeFilterType}
                    className={styles.select}
                    SelectProps={{
                      renderValue: (value) => localization.filters[value as BuiltInFilterType],
                      MenuProps: commonMenuProps,
                    }}
                  >
                    {options}
                  </TextField>
                </Grid>
              )}
              <Grid item xs={12}>
                {children}
              </Grid>
              <Grid item xs={12}>
                <Grid container direction={'row'} justifyContent={'flex-end'} alignItems={'center'}>
                  <Grid item>
                    <Button color="primary" onClick={apply}>
                      {localization.filters.apply}
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </Popover>
      </React.Fragment>
    ),
    [
      id,
      padding,
      label,
      handleClick,
      removeFilter,
      inputVariant,
      styles,
      open,
      anchorEl,
      handleClose,
      activeFilterType,
      changeFilterType,
      localization,
      options,
      children,
      apply,
    ],
  );
}
