import { Box, Slider, Typography } from '@mui/material';
import * as React from 'react';
import { FilterValuesPickerProps } from '../../../filter';
import * as ReactTable from 'react-table';

export function FilterValuesNumberRangePicker<T extends object = {}>({
  column,
}: FilterValuesPickerProps<T, [number | undefined, number | undefined]>) {
  const { id = '', preFilteredRows = [], filterValue = [], setFilter, render, filterPlaceholder } = column;
  const [min, max] = React.useMemo(() => {
    let currentMin = preFilteredRows.length ? preFilteredRows[0].values[id] : 0;
    let currentMax = preFilteredRows.length ? preFilteredRows[0].values[id] : 0;
    preFilteredRows.forEach((row) => {
      currentMin = Math.min(row.values[id], currentMin);
      currentMax = Math.max(row.values[id], currentMax);
    });
    return [currentMin, currentMax];
  }, [id, preFilteredRows]);

  const handleChange = ReactTable.useAsyncDebounce(
    (event: Event, value: number | Array<number>) => setFilter(value as [number | undefined, number | undefined]),
    200,
  );

  React.useEffect(() => {
    if (!filterValue?.length) {
      setFilter([min, max]);
    }
  }, [filterValue, min, max, setFilter]);

  const currentMin = filterValue?.[0];
  const currentMax = filterValue?.[1];
  return React.useMemo(
    () => (
      <Box minWidth={256} maxWidth={300}>
        <Typography component="div" id={`${id}-filter-label`} gutterBottom>
          {render('Header')}
        </Typography>
        <Box ml={1.25} mr={1.25}>
          <Slider
            aria-labelledby={`${id}-filter-label`}
            defaultValue={[currentMin ?? min, currentMax ?? max]}
            min={min}
            max={max}
            valueLabelDisplay="auto"
            onChange={handleChange}
            placeholder={filterPlaceholder}
          />
        </Box>
      </Box>
    ),
    [id, render, min, max, handleChange, filterPlaceholder, currentMin, currentMax],
  );
}
