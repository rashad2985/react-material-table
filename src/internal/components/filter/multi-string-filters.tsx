import { Typography } from '@mui/material';
import * as React from 'react';
import { Column } from '../../../column';
import { FilterValuesPickerProps } from '../../../filter';
import { FiltersContext, FiltersContextProps } from '../../contexts';
import { useFiltersGenerator } from '../../hooks';
import { FilterValuesContainer } from './filter-values-container';

export function MultiStringFilters<DataType extends object = {}>({
  column,
}: FilterValuesPickerProps<DataType, Array<Array<string>>>) {
  const { filterValue, setFilter, render, defaultFilter } = column;

  const { filters } = React.useContext<FiltersContextProps<DataType>>(FiltersContext);
  const currentFilter = React.useMemo(
    () => filters.find((filter) => filter.field === column.field),
    [filters, column.field],
  );
  const { resolveFilterInputForm } = useFiltersGenerator<DataType>();

  const filterValueContainerLabel = React.useMemo(
    () =>
      (currentFilter?.builtInFilterType === 'multiple-choice' && Array.isArray(filterValue) && (
        <React.Fragment>
          {render('Header')}
          {filterValue.length > 0
            ? `: ${filterValue
                .map((option) => `"${option.map((key) => column?.lookup?.get?.(key) ?? key).join(', ')}"`)
                .join(', ')}`
            : ''}
        </React.Fragment>
      )) ||
      render('Header'),
    [currentFilter, filterValue, render, column?.lookup],
  );

  const FilterInputForm = React.useMemo(
    () => resolveFilterInputForm(column as unknown as Column<DataType>, filters).Filter,
    [resolveFilterInputForm, column, filters],
  );

  const [currentValue, setCurrentValue] =
    React.useState<FilterValuesPickerProps<DataType, Array<Array<string>>>['column']['filterValue']>(filterValue);
  const changeCurrentValue = React.useCallback(
    (newInputValue: typeof currentValue) => setCurrentValue(newInputValue),
    [],
  );
  const resetCurrentFilterValue = React.useCallback(() => {
    setFilter(undefined);
    setCurrentValue(undefined);
  }, [setFilter]);
  const applyFilter = React.useCallback(() => setFilter(currentValue || undefined), [currentValue, setFilter]);

  React.useEffect(() => {
    if (!!defaultFilter) {
      setFilter(defaultFilter);
    }
  }, [defaultFilter, setFilter]);

  return React.useMemo(
    () => (
      <FilterValuesContainer<DataType>
        column={column}
        label={
          <Typography component="div" variant="inherit" noWrap>
            {filterValueContainerLabel}
          </Typography>
        }
        clearFilterValues={setFilter}
        apply={applyFilter}
        resetCurrentFilterValue={resetCurrentFilterValue}
      >
        {!!FilterInputForm && (
          <FilterInputForm column={{ ...column, filterValue: currentValue, setFilter: changeCurrentValue }} />
        )}
      </FilterValuesContainer>
    ),
    [
      applyFilter,
      changeCurrentValue,
      resetCurrentFilterValue,
      currentValue,
      column,
      filterValueContainerLabel,
      setFilter,
      FilterInputForm,
    ],
  );
}
