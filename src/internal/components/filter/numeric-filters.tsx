import { Typography } from '@mui/material';
import * as React from 'react';
import { FilterValuesPickerProps } from '../../../filter';
import { FilterValuesContainer } from './filter-values-container';
import { formatCurrency, formatNumber } from '../../commons';
import { Column } from '../../../column';
import { useFiltersGenerator } from '../../hooks';
import { FiltersContext, FiltersContextProps } from '../../contexts';

export function NumericFilters<DataType extends object = {}>({
  column,
}: FilterValuesPickerProps<DataType, number | [number | undefined, number | undefined]>) {
  const {
    type: columnType = 'string',
    filterValue,
    setFilter,
    render,
    currencySetting,
    numberSettings,
    defaultFilter,
  } = column;

  const { filters } = React.useContext<FiltersContextProps<DataType>>(FiltersContext);
  const currentFilter = React.useMemo(
    () => filters.find((filter) => filter.field === column.field),
    [filters, column.field],
  );
  const { resolveFilterInputForm } = useFiltersGenerator<DataType>();

  const renderableFilterValue = React.useMemo<string | [string | undefined, string | undefined] | undefined>(
    () =>
      (typeof filterValue === 'number' &&
        (formatCurrency(filterValue, columnType, currencySetting) ??
          formatNumber(filterValue, columnType, numberSettings))) ||
      (Array.isArray(filterValue) && [
        formatCurrency(filterValue?.[0], columnType, currencySetting) ??
          formatNumber(filterValue?.[0], columnType, numberSettings),
        formatCurrency(filterValue?.[1], columnType, currencySetting) ??
          formatNumber(filterValue?.[1], columnType, numberSettings),
      ]) ||
      undefined,
    [columnType, filterValue, currencySetting, numberSettings],
  );

  const filterValueContainerLabel = React.useMemo(
    () =>
      (currentFilter?.builtInFilterType === 'standard' && typeof renderableFilterValue === 'string' && (
        <React.Fragment>
          {render('Header')}
          {renderableFilterValue !== undefined ? `: "${renderableFilterValue}"` : ''}
        </React.Fragment>
      )) ||
      (currentFilter?.builtInFilterType === 'range' && Array.isArray(renderableFilterValue) && (
        <React.Fragment>
          {renderableFilterValue[0] === undefined ? '' : `${renderableFilterValue[0]} ${String.fromCharCode(8804)} `}
          {render('Header')}
          {renderableFilterValue[1] === undefined ? '' : ` ${String.fromCharCode(8804)} ${renderableFilterValue[1]}`}
        </React.Fragment>
      )) ||
      (currentFilter?.builtInFilterType === 'greater-than' && Array.isArray(renderableFilterValue) && (
        <React.Fragment>
          {render('Header')}
          {renderableFilterValue[0] !== undefined ? ` ${String.fromCharCode(8805)} "${renderableFilterValue[0]}"` : ''}
        </React.Fragment>
      )) ||
      (currentFilter?.builtInFilterType === 'less-than' && Array.isArray(renderableFilterValue) && (
        <React.Fragment>
          {render('Header')}
          {renderableFilterValue[1] !== undefined ? ` ${String.fromCharCode(8804)} "${renderableFilterValue[1]}"` : ''}
        </React.Fragment>
      )) ||
      render('Header'),
    [currentFilter, renderableFilterValue, render],
  );

  const FilterInputForm = React.useMemo(
    () => resolveFilterInputForm(column as unknown as Column<DataType>, filters).Filter,
    [resolveFilterInputForm, column, filters],
  );

  const [currentValue, setCurrentValue] =
    React.useState<
      FilterValuesPickerProps<DataType, number | [number | undefined, number | undefined]>['column']['filterValue']
    >(filterValue);
  const changeCurrentValue = React.useCallback(
    (newInputValue: typeof currentValue) => setCurrentValue(newInputValue),
    [],
  );
  const resetCurrentFilterValue = React.useCallback(() => {
    setFilter(undefined);
    setCurrentValue(undefined);
  }, [setFilter]);
  const applyFilter = React.useCallback(() => setFilter(currentValue || undefined), [currentValue, setFilter]);

  React.useEffect(() => {
    if (!!defaultFilter) {
      setFilter(defaultFilter);
    }
  }, [defaultFilter, setFilter]);

  return React.useMemo(
    () => (
      <FilterValuesContainer<DataType>
        column={column}
        label={
          <Typography component="div" variant="inherit" noWrap>
            {filterValueContainerLabel}
          </Typography>
        }
        clearFilterValues={setFilter}
        apply={applyFilter}
        resetCurrentFilterValue={resetCurrentFilterValue}
      >
        {!!FilterInputForm && (
          <FilterInputForm column={{ ...column, filterValue: currentValue, setFilter: changeCurrentValue }} />
        )}
      </FilterValuesContainer>
    ),
    [
      column,
      filterValueContainerLabel,
      resetCurrentFilterValue,
      setFilter,
      FilterInputForm,
      applyFilter,
      changeCurrentValue,
      currentValue,
    ],
  );
}
