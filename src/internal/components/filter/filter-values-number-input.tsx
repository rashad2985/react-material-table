import * as React from 'react';
import { FilterValuesPickerProps } from '../../../filter';
import { FilterComponentProps } from './index';
import { Column } from '../../../column';
import { NumericInput } from '../input';

export function FilterValuesNumberInput<DataType extends object = {}>({
  column,
}: FilterValuesPickerProps<DataType, number>) {
  const {
    filterValue,
    title,
    lookup,
    setFilter,
    filterComponent: FilterComponent,
    filterPlaceholder,
    filterCellStyle,
  } = column;
  const applyFilter = React.useCallback((value: number) => setFilter(value ?? undefined), [setFilter]);

  const onFilterChangedInCustomFilterComponent = React.useCallback<FilterComponentProps<DataType>['onFilterChanged']>(
    (_, value) => setFilter(value as number),
    [setFilter],
  );

  return React.useMemo(
    () =>
      !!FilterComponent ? (
        <FilterComponent
          columnDefinition={column as unknown as Column<DataType>}
          onFilterChanged={onFilterChangedInCustomFilterComponent}
        />
      ) : (
        <NumericInput
          title={title ?? ''}
          value={filterValue}
          onChange={applyFilter}
          placeholder={filterPlaceholder}
          style={filterCellStyle}
          lookup={lookup}
        />
      ),
    [
      FilterComponent,
      column,
      lookup,
      onFilterChangedInCustomFilterComponent,
      title,
      filterValue,
      applyFilter,
      filterPlaceholder,
      filterCellStyle,
    ],
  );
}
