import * as React from 'react';
import { FilterValuesPickerProps } from '../../../filter';
import { toDate, toDateEnd } from '../../commons';
import { FilterComponentProps } from './index';
import { Column } from '../../../column';
import { DateInput } from '../input';

export function FilterValuesDateInput<T extends object = {}>({
  column,
}: FilterValuesPickerProps<T, [number | undefined, number | undefined]>) {
  const {
    filterValue,
    setFilter,
    title,
    dateSetting,
    filterComponent: FilterComponent,
    defaultFilter,
    filterPlaceholder,
    filterCellStyle,
  } = column;
  const [from] = filterValue || [];

  const applyFilter = React.useCallback(
    (date: Date | null) => setFilter(!!date ? [toDate(date).getTime(), toDateEnd(date).getTime()] : undefined),
    [setFilter],
  );

  const onFilterChangedInCustomFilterComponent = React.useCallback<FilterComponentProps<T>['onFilterChanged']>(
    (_, value) => setFilter(!!value ? [toDate(value).getTime(), toDateEnd(value).getTime()] : undefined),
    [setFilter],
  );

  React.useEffect(() => {
    if (!!defaultFilter) {
      setFilter(defaultFilter);
    }
  }, [defaultFilter, setFilter]);

  return React.useMemo(
    () =>
      !!FilterComponent ? (
        <FilterComponent
          columnDefinition={column as unknown as Column<T>}
          onFilterChanged={onFilterChangedInCustomFilterComponent}
        />
      ) : (
        <DateInput
          title={title ?? ''}
          value={from !== undefined ? new Date(from) : undefined}
          onChange={applyFilter}
          placeholder={filterPlaceholder}
          style={filterCellStyle}
          dateSetting={dateSetting}
        />
      ),
    [
      FilterComponent,
      column,
      onFilterChangedInCustomFilterComponent,
      title,
      from,
      applyFilter,
      filterPlaceholder,
      filterCellStyle,
      dateSetting,
    ],
  );
}
