import { ClickAwayListener, IconButton, MenuItem, MenuList, Popover, Tooltip } from '@mui/material';
import * as React from 'react';
import { Column } from '../../../column';
import { Filter } from '../../../filter';
import { ReactMaterialTableProps } from '../../../react-material-table';
import { defined } from '../../commons';
import { FiltersContext, FiltersContextProps } from '../../contexts';
import { useFiltersGenerator, useIcons, useLocalization } from '../../hooks';

const ID = 'trautmann-material-table-filter-chooser';

export interface FilterGroup<RowData extends object = {}> extends Pick<Column<RowData>, 'title' | 'field'> {
  filters: Array<Filter<RowData>>;
}

export interface FilterChooserProps<RowData extends object = {}> {
  initialColumns: ReactMaterialTableProps<RowData>['columns'];
}

export function FilterChooser<RowData extends object = {}>(props: FilterChooserProps<RowData>) {
  const { initialColumns } = props;
  const localization = useLocalization();
  const { Filter: FilterIcon } = useIcons();

  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

  const handleClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => setAnchorEl(event.currentTarget),
    [],
  );

  const handleClose = React.useCallback(() => setAnchorEl(null), []);

  const open = Boolean(anchorEl);
  const id = open ? ID : undefined;

  const { filters, setFilters } = React.useContext<FiltersContextProps<RowData>>(FiltersContext);

  const addFilter = React.useCallback(
    (newFilter: Filter<RowData>) => {
      setFilters([...filters, newFilter]);
      handleClose();
    },
    [filters, setFilters, handleClose],
  );

  const { generate } = useFiltersGenerator<RowData>();
  const filterMenuItems = React.useMemo(
    () =>
      initialColumns
        .filter(
          (column) =>
            !!column.field &&
            !!column.title &&
            column.filtering !== false &&
            !filters.some((filter) => filter.field === column.field),
        )
        .map((column, index) => {
          const possibleColumnFilters = generate(column);
          return possibleColumnFilters.length > 0 ? (
            <MenuItem key={index} onClick={() => addFilter(possibleColumnFilters[0])}>
              {column.title ?? ''}
            </MenuItem>
          ) : undefined;
        })
        .filter(defined),
    [initialColumns, filters, addFilter, generate],
  );

  return React.useMemo<React.ReactElement<FilterChooserProps, React.JSXElementConstructor<FilterChooserProps>>>(
    () => (
      <React.Fragment>
        <Tooltip title={localization.filters.addFilter}>
          <IconButton aria-describedby={id} onClick={handleClick} disabled={filterMenuItems.length <= 0} size="large">
            <FilterIcon />
          </IconButton>
        </Tooltip>
        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <ClickAwayListener onClickAway={handleClose}>
            <MenuList disablePadding>{filterMenuItems}</MenuList>
          </ClickAwayListener>
        </Popover>
      </React.Fragment>
    ),
    [id, handleClick, open, anchorEl, handleClose, filterMenuItems, localization, FilterIcon],
  );
}
