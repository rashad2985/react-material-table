import { Box, ListItemText, MenuItem, TextField, Typography } from '@mui/material';
import * as React from 'react';
import { FilterValuesPickerProps, FilterValueType } from '../../../filter';
import { useStyles } from '../../../styles';
import { useOptions } from '../../hooks';
import { commonMenuProps } from '../../commons';

export function FilterValuesSingleOptionPicker<T extends object = {}>({
  column,
}: FilterValuesPickerProps<T, FilterValueType>) {
  const styles = useStyles();
  const { inputVariant } = useOptions<T>();

  const { id = '', preFilteredRows = [], filterValue, setFilter, render, filterPlaceholder, lookup } = column;

  const options = React.useMemo(() => {
    const optionsSet = new Set<string>();
    preFilteredRows.forEach((row) => {
      optionsSet.add(String(row.values[id]));
    });
    return Array.from(optionsSet.values());
  }, [id, preFilteredRows]);

  const selectedValue = React.useMemo(() => (!!filterValue ? String(filterValue) : ''), [filterValue]);

  const handleChange = React.useCallback(
    (event: React.ChangeEvent<{ value: unknown }>) => setFilter(event.target.value as string),
    [setFilter],
  );

  const items = React.useMemo(
    () =>
      options.map((option, index) => (
        <MenuItem key={index} value={String(option)}>
          <ListItemText primary={lookup?.get(option) ?? String(option)} />
        </MenuItem>
      )),
    [options, lookup],
  );

  return React.useMemo(
    () => (
      <Box minWidth={128} maxWidth={384}>
        <TextField
          select
          margin="dense"
          size="small"
          variant={inputVariant}
          fullWidth
          label={
            <Typography component="div" variant="inherit" noWrap>
              {render('Header')}
            </Typography>
          }
          value={selectedValue ?? ''}
          onChange={handleChange}
          className={styles.select}
          placeholder={filterPlaceholder}
          SelectProps={{
            renderValue: (value: string) => lookup?.get(value) ?? value,
            MenuProps: commonMenuProps,
          }}
        >
          {items}
        </TextField>
      </Box>
    ),
    [inputVariant, render, selectedValue, items, handleChange, filterPlaceholder, styles.select, lookup],
  );
}
