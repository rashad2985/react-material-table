import * as React from 'react';
import { FilterValuesPickerProps } from '../../../filter';
import { FilterComponentProps } from './index';
import { Column } from '../../../column';
import { StringInput } from '../input';

export function FilterValuesTextInput<T extends object = {}>({ column }: FilterValuesPickerProps<T, string>) {
  const {
    filterValue,
    setFilter,
    title,
    lookup,
    filterComponent: FilterComponent,
    defaultFilter,
    filterPlaceholder,
    filterCellStyle,
  } = column;
  const applyFilter = React.useCallback((value: string) => setFilter(value || undefined), [setFilter]);
  const onFilterChangedInCustomFilterComponent = React.useCallback<FilterComponentProps<T>['onFilterChanged']>(
    (_, value) => setFilter(value as string),
    [setFilter],
  );

  return React.useMemo(
    () =>
      !!FilterComponent ? (
        <FilterComponent
          columnDefinition={column as unknown as Column<T>}
          onFilterChanged={onFilterChangedInCustomFilterComponent}
        />
      ) : (
        <StringInput
          title={title ?? ''}
          value={filterValue ?? defaultFilter ?? ''}
          onChange={applyFilter}
          style={filterCellStyle}
          placeholder={filterPlaceholder}
          lookup={lookup}
        />
      ),
    [
      FilterComponent,
      defaultFilter,
      title,
      column,
      lookup,
      onFilterChangedInCustomFilterComponent,
      filterValue,
      applyFilter,
      filterCellStyle,
      filterPlaceholder,
    ],
  );
}
