import { TextField, Typography } from '@mui/material';
import * as React from 'react';
import { FilterValuesPickerProps } from '../../../filter';
import { defined } from '../../commons';
import { useOptions } from '../../hooks';

export function FilterValuesNumberGreaterThan<T extends object = {}>({
  column,
}: FilterValuesPickerProps<T, [number | undefined, number | undefined]>) {
  const { filterValue, setFilter, render, filterPlaceholder } = column;
  const { inputVariant, padding } = useOptions<T>();
  const applyFilter = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) =>
      setFilter([Number(event.target.value), Number.POSITIVE_INFINITY] || undefined),
    [setFilter],
  );

  const value = React.useMemo(
    () => (filterValue !== undefined ? Math.min(...filterValue?.filter(defined)) : undefined),
    [filterValue],
  );

  return React.useMemo(
    () => (
      <TextField
        margin={(padding ?? 'dense') === 'dense' ? 'dense' : 'normal'}
        size={(padding ?? 'dense') === 'dense' ? 'small' : undefined}
        fullWidth
        variant={inputVariant}
        type="number"
        label={
          <Typography component="div" variant="inherit" noWrap>
            {render('Header')}
            {String.fromCharCode(160, 8805)}
          </Typography>
        }
        value={value ?? ''}
        onChange={applyFilter}
        placeholder={filterPlaceholder}
      />
    ),
    [inputVariant, render, value, applyFilter, filterPlaceholder, padding],
  );
}
