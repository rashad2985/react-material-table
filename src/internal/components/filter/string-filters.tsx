import { Typography } from '@mui/material';
import * as React from 'react';
import { FilterValuesPickerProps } from '../../../filter';
import { FilterValuesContainer } from './filter-values-container';
import { Column } from '../../../column';
import { useFiltersGenerator } from '../../hooks';
import { FiltersContext, FiltersContextProps } from '../../contexts';

export function StringFilters<DataType extends object = {}>({
  column,
}: FilterValuesPickerProps<DataType, string | Array<string>>) {
  const { filterValue, setFilter, render, defaultFilter, lookup } = column;

  const { filters } = React.useContext<FiltersContextProps<DataType>>(FiltersContext);
  const currentFilter = React.useMemo(
    () => filters.find((filter) => filter.field === column.field),
    [filters, column.field],
  );

  const filterValueContainerLabel = React.useMemo(
    () =>
      (currentFilter?.builtInFilterType === 'standard' && typeof filterValue === 'string' && (
        <React.Fragment>
          {render('Header')}
          {!!filterValue ? `: "${lookup?.get(filterValue) ?? filterValue}"` : ''}
        </React.Fragment>
      )) ||
      (currentFilter?.builtInFilterType === 'single-choice' && typeof filterValue === 'string' && (
        <React.Fragment>
          {render('Header')}
          {!!filterValue ? `: "${lookup?.get(filterValue) ?? filterValue}"` : ''}
        </React.Fragment>
      )) ||
      (currentFilter?.builtInFilterType === 'multiple-choice' && Array.isArray(filterValue) && (
        <React.Fragment>
          {render('Header')}
          {filterValue.length > 0 ? `: "${filterValue.map((value) => lookup?.get(value) ?? value).join(', ')}"` : ''}
        </React.Fragment>
      )) ||
      render('Header'),
    [currentFilter, filterValue, render, lookup],
  );

  const { resolveFilterInputForm } = useFiltersGenerator<DataType>();
  const FilterInputForm = React.useMemo(
    () => resolveFilterInputForm(column as unknown as Column<DataType>, filters).Filter,
    [resolveFilterInputForm, column, filters],
  );

  const [currentValue, setCurrentValue] =
    React.useState<FilterValuesPickerProps<DataType, string | Array<string>>['column']['filterValue']>(filterValue);
  const changeCurrentValue = React.useCallback(
    (newInputValue: typeof currentValue) => setCurrentValue(newInputValue),
    [],
  );
  const resetCurrentFilterValue = React.useCallback(() => {
    setFilter(undefined);
    setCurrentValue(undefined);
  }, [setFilter]);
  const applyFilter = React.useCallback(() => setFilter(currentValue || undefined), [currentValue, setFilter]);

  React.useEffect(() => {
    if (!!defaultFilter) {
      setFilter(defaultFilter);
    }
  }, [defaultFilter, setFilter]);

  return React.useMemo(
    () => (
      <FilterValuesContainer<DataType>
        column={column}
        label={
          <Typography component="div" variant="inherit" noWrap>
            {filterValueContainerLabel}
          </Typography>
        }
        clearFilterValues={setFilter}
        apply={applyFilter}
        resetCurrentFilterValue={resetCurrentFilterValue}
      >
        {!!FilterInputForm && (
          <FilterInputForm column={{ ...column, filterValue: currentValue, setFilter: changeCurrentValue }} />
        )}
      </FilterValuesContainer>
    ),
    [
      column,
      filterValueContainerLabel,
      setFilter,
      FilterInputForm,
      applyFilter,
      currentValue,
      changeCurrentValue,
      resetCurrentFilterValue,
    ],
  );
}
