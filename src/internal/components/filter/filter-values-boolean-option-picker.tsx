import { Box, Typography } from '@mui/material';
import * as React from 'react';
import { FilterValuesPickerProps } from '../../../filter';
import { useStyles } from '../../../styles';
import { FilterValuesContainer } from './filter-values-container';
import { useLocalization } from '../../hooks';
import { Column } from '../../../column';
import { FilterComponentProps } from './index';
import { BooleanInput, StringBoolean } from '../input';

export function FilterValuesBooleanOptionPicker<RowData extends object = {}>({
  column,
}: FilterValuesPickerProps<RowData, boolean>) {
  const styles = useStyles();
  const localization = useLocalization();

  const {
    filterValue,
    setFilter,
    render,
    lookup,
    filterComponent: FilterComponent,
    defaultFilter,
    filterCellStyle,
    title,
  } = column;
  const onFilterChangedInCustomFilterComponent = React.useCallback<FilterComponentProps<RowData>['onFilterChanged']>(
    (_, value) => setFilter(value as boolean),
    [setFilter],
  );

  const [currentValue, setCurrentValue] = React.useState<
    FilterValuesPickerProps<RowData, boolean>['column']['filterValue']
  >(column?.filterValue);
  const changeCurrentValue = React.useCallback(
    (newInputValue: typeof currentValue) => setCurrentValue(newInputValue),
    [],
  );
  const resetCurrentFilterValue = React.useCallback(() => {
    setFilter(undefined);
    setCurrentValue(undefined);
  }, [setFilter]);
  const applyFilter = React.useCallback(() => setFilter(currentValue), [currentValue, setFilter]);

  React.useEffect(() => {
    if (!!defaultFilter) {
      setFilter(defaultFilter);
    }
  }, [defaultFilter, setFilter]);

  return React.useMemo(
    () => (
      <FilterValuesContainer<RowData>
        column={column}
        label={
          <Typography component="div" variant="inherit" noWrap>
            {render('Header')}
            {filterValue !== undefined
              ? `: "${localization.row.boolean[String(filterValue ?? 'undefined') as StringBoolean]}"`
              : ''}
          </Typography>
        }
        clearFilterValues={setFilter}
        apply={applyFilter}
        resetCurrentFilterValue={resetCurrentFilterValue}
      >
        <Box minWidth={128}>
          {!!FilterComponent ? (
            <FilterComponent
              columnDefinition={
                { ...column, filterValue: currentValue, setFilter: changeCurrentValue } as unknown as Column<RowData>
              }
              onFilterChanged={onFilterChangedInCustomFilterComponent}
            />
          ) : (
            <BooleanInput
              title={title ?? ''}
              lookup={lookup}
              onChange={changeCurrentValue}
              value={currentValue}
              style={filterCellStyle}
              className={styles.select}
            />
          )}
        </Box>
      </FilterValuesContainer>
    ),
    [
      currentValue,
      changeCurrentValue,
      resetCurrentFilterValue,
      applyFilter,
      column,
      render,
      filterValue,
      localization,
      setFilter,
      FilterComponent,
      onFilterChangedInCustomFilterComponent,
      title,
      lookup,
      filterCellStyle,
      styles.select,
    ],
  );
}
