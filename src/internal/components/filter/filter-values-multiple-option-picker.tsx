import { Box, Checkbox, ListItemText, MenuItem, TextField, Typography } from '@mui/material';
import * as React from 'react';
import { FilterValuesPickerProps, FilterValueType } from '../../../filter';
import { useStyles } from '../../../styles';
import { useOptions } from '../../hooks';
import { commonMenuProps } from '../../commons';

export function FilterValuesMultipleOptionPicker<T extends object = {}>({
  column,
}: FilterValuesPickerProps<T, Array<FilterValueType>>) {
  const { id = '', preFilteredRows = [], filterValue = [], setFilter, render, filterPlaceholder, lookup } = column;
  const styles = useStyles();
  const { inputVariant } = useOptions<T>();
  const options = React.useMemo(() => {
    const optionsSet = new Set<string>();
    preFilteredRows.forEach((row) => {
      optionsSet.add(String(row.values[id]));
    });
    return Array.from(optionsSet.values());
  }, [id, preFilteredRows]);

  const selectedValues = React.useMemo(() => filterValue.map((value) => String(value)), [filterValue]);

  const handleChange = React.useCallback(
    (event: React.ChangeEvent<{ value: unknown }>) => setFilter(event.target.value as Array<string>),
    [setFilter],
  );

  const items = React.useMemo(
    () =>
      options.map((option, index) => (
        <MenuItem key={index} value={String(option)}>
          <Checkbox checked={selectedValues.indexOf(option) > -1} />
          <ListItemText primary={lookup?.get(option) ?? String(option)} />
        </MenuItem>
      )),
    [options, selectedValues, lookup],
  );

  return React.useMemo(
    () => (
      <Box minWidth={128} maxWidth={384}>
        <TextField
          select
          margin="dense"
          size="small"
          variant={inputVariant}
          fullWidth
          label={
            <Typography component="div" variant="inherit" noWrap>
              {render('Header')}
            </Typography>
          }
          value={selectedValues}
          onChange={handleChange}
          className={styles.select}
          placeholder={filterPlaceholder}
          SelectProps={{
            multiple: true,
            renderValue: (selected) =>
              (selected as Array<string>).map((option) => lookup?.get(option) ?? option).join(', '),
            MenuProps: commonMenuProps,
          }}
        >
          {items}
        </TextField>
      </Box>
    ),
    [inputVariant, render, selectedValues, items, handleChange, filterPlaceholder, styles.select, lookup],
  );
}
