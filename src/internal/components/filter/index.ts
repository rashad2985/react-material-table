import * as React from 'react';
import * as ReactTable from 'react-table';
import { Column } from '../../../column';
import { Filter, FilterValuesPickerProps } from '../../../filter';

export * from './filter-chooser';
export * from './filter-values-boolean-option-picker';
export * from './filter-values-date-range-picker';
export * from './filter-values-date-input';
export * from './filter-values-date-time-range-picker';
export * from './filter-values-date-time-input';
export * from './filter-values-time-input';
export * from './filter-values-time-range-picker';
export * from './filter-values-multi-string-multiple-option-picker';
export * from './filter-values-multiple-option-picker';
export * from './filter-values-number-input';
export * from './filter-values-number-greater-than';
export * from './filter-values-number-less-than';
export * from './filter-values-number-range-picker';
export * from './filter-values-single-option-picker';
export * from './filter-values-text-input';
export * from './date-filters';
export * from './date-time-filters';
export * from './time-filters';
export * from './numeric-filters';
export * from './string-filters';
export * from './multi-string-filters';

export type FilterComponentProps<DataType extends object = {}> = {
  columnDefinition: Column<DataType>;
  onFilterChanged: (rowId: keyof DataType, value: any) => void;
};

export interface RenderableFilter<DataType extends object = {}> {
  Filter?: ({ column }: FilterValuesPickerProps<DataType, unknown>) => React.ReactElement;
  filter?: Filter<DataType>['type'] | ReactTable.FilterType<DataType>;
  disableFilters?: boolean;
  type?: Column<DataType>['type'];
}
