import { Grid, Typography } from '@mui/material';
import * as React from 'react';
import { FilterValuesPickerProps } from '../../../filter';
import { useLocalization } from '../../hooks';
import { toDate, toDateEnd } from '../../commons';
import { DateInput } from '../input';

export function FilterValuesDateRangePicker<T extends object = {}>({
  column,
}: FilterValuesPickerProps<T, [number | undefined, number | undefined]>) {
  const { id = '', filterValue = [], setFilter, render, dateSetting, filterPlaceholder, filterCellStyle } = column;
  const localization = useLocalization();

  const [from, to] = filterValue || [];

  const applyFrom = React.useCallback(
    (date: Date | null) => setFilter([!!date ? toDate(date).getTime() : undefined, to]),
    [setFilter, to],
  );

  const applyTo = React.useCallback(
    (date: Date | null) => setFilter([from, !!date ? toDateEnd(date).getTime() : undefined]),
    [setFilter, from],
  );

  return React.useMemo(
    () => (
      <Grid container direction="column" justifyContent="flex-start" alignItems="flex-start">
        <Typography component="div" id={`${id}-filter-label`} gutterBottom>
          {render('Header')}
        </Typography>
        <Grid container direction="row" justifyContent="space-between" alignItems="flex-start" spacing={1}>
          <Grid item xs={12} sm={6}>
            <DateInput
              title={localization.filters.from}
              value={!!from ? new Date(from) : undefined}
              onChange={applyFrom}
              placeholder={filterPlaceholder}
              style={filterCellStyle}
              dateSetting={dateSetting}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <DateInput
              title={localization.filters.to}
              value={!!to ? new Date(to) : undefined}
              onChange={applyTo}
              placeholder={filterPlaceholder}
              style={filterCellStyle}
              dateSetting={dateSetting}
            />
          </Grid>
        </Grid>
      </Grid>
    ),
    [
      id,
      render,
      from,
      applyFrom,
      to,
      applyTo,
      localization.filters.from,
      localization.filters.to,
      filterPlaceholder,
      filterCellStyle,
      dateSetting,
    ],
  );
}
