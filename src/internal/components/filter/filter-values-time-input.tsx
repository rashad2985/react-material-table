import * as React from 'react';
import { FilterValuesPickerProps } from '../../../filter';
import { toTime, toTimeEnd } from '../../commons';
import { FilterComponentProps } from './index';
import { Column } from '../../../column';
import { TimeInput } from '../input';

export function FilterValuesTimeInput<T extends object = {}>({ column }: FilterValuesPickerProps<T, Array<number>>) {
  const {
    filterValue,
    setFilter,
    title,
    dateSetting,
    filterComponent: FilterComponent,
    defaultFilter,
    filterPlaceholder,
    filterCellStyle,
  } = column;
  const [from] = filterValue || [];

  const applyFilter = React.useCallback(
    (date: Date | null) => setFilter(!!date ? [toTime(date).getTime(), toTimeEnd(date).getTime()] : undefined),
    [setFilter],
  );

  const onFilterChangedInCustomFilterComponent = React.useCallback<FilterComponentProps<T>['onFilterChanged']>(
    (_, value) => setFilter(!!value ? [toTime(value).getTime(), toTimeEnd(value).getTime()] : undefined),
    [setFilter],
  );

  React.useEffect(() => {
    if (!!defaultFilter) {
      setFilter(defaultFilter);
    }
  }, [defaultFilter, setFilter]);

  return React.useMemo(
    () =>
      !!FilterComponent ? (
        <FilterComponent
          columnDefinition={column as unknown as Column<T>}
          onFilterChanged={onFilterChangedInCustomFilterComponent}
        />
      ) : (
        <TimeInput
          title={title ?? ''}
          value={from !== undefined ? new Date(from) : undefined}
          onChange={applyFilter}
          placeholder={filterPlaceholder}
          style={filterCellStyle}
          dateSetting={dateSetting}
        />
      ),
    [
      FilterComponent,
      column,
      onFilterChangedInCustomFilterComponent,
      title,
      from,
      applyFilter,
      filterPlaceholder,
      filterCellStyle,
      dateSetting,
    ],
  );
}
