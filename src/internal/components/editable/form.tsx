import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
  LinearProgress,
  useMediaQuery,
  useTheme,
} from '@mui/material';
import { Breakpoint } from '@mui/material/styles';
import * as React from 'react';
import { Column } from '../../../column';
import { defined } from '../../commons';
import { InternalOptions, useLocalization, useOptions } from '../../hooks';
import { DateInput, DateTimeInput, Input, TimeInput } from '../input';

type ValidatorReturnType<RowData extends object = {}> = ReturnType<Required<Column<RowData>>['validate']>;

const isValidResult = <RowData extends object = {}>(validationResult: ValidatorReturnType<RowData>): boolean =>
  (typeof validationResult === 'boolean' && validationResult) ||
  (typeof validationResult === 'string' && !validationResult) ||
  (typeof validationResult === 'object' && validationResult.isValid);

const getErrorMessage = <RowData extends object = {}>(
  validationResult: ValidatorReturnType<RowData>,
): string | undefined => {
  if (typeof validationResult === 'boolean') {
    return undefined;
  } else if (typeof validationResult === 'string' && !!validationResult) {
    return validationResult;
  } else if (typeof validationResult === 'object' && !!validationResult.helperText) {
    return validationResult.helperText;
  } else {
    return undefined;
  }
};

export type FormProps<RowData extends object = {}> = {
  title: string;
  columns: Array<Column<RowData>>;
  visible: boolean;
  hide: () => void;
  initialData?: Partial<RowData>;
  onSave?: (newData: Partial<RowData>, oldData?: Partial<RowData>) => Promise<any>;
  onCancel?: (newData: Partial<RowData>, oldData?: Partial<RowData>) => void;
  text?: string;
  layoutOptions: keyof InternalOptions<RowData>['editable'];
};

export function Form<RowData extends object = {}>(props: FormProps<RowData>) {
  const { title = '', columns = [], initialData = {}, onCancel, onSave, visible, hide, text, layoutOptions } = props;
  const { editable, padding } = useOptions<RowData>();
  const { grid = {}, dialog = {} } = editable?.[layoutOptions] ?? {};
  const { maxWidth, fullScreenBreakpoint = 'sm' } = dialog ?? {};
  const localization = useLocalization();
  const [rowData, setRowData] = React.useState<Partial<RowData>>({ ...initialData });
  const [inProgress, setInProgress] = React.useState<boolean>(false);

  const startProgress = React.useCallback(() => setInProgress(true), []);
  const stopProgress = React.useCallback(() => setInProgress(false), []);
  const theme = useTheme();

  const fullScreen = useMediaQuery(theme.breakpoints.down(fullScreenBreakpoint));

  const hasError = React.useMemo(
    () =>
      !columns.length
        ? false
        : columns
            .map((column) => column.validate)
            .filter(defined)
            .reduce(
              (previousValidationResult, nextColumnValidate) =>
                previousValidationResult || !isValidResult(nextColumnValidate(rowData)),
              false,
            ),
    [columns, rowData],
  );

  const save = React.useCallback(() => {
    startProgress();
    onSave?.(rowData, initialData)
      ?.then(stopProgress)
      ?.then(() => hide?.())
      ?.then(() => setRowData(() => ({ ...initialData })))
      ?.catch(stopProgress);
  }, [hide, onSave, rowData, initialData, startProgress, stopProgress]);

  const cancel = React.useCallback(() => {
    onCancel?.(rowData, initialData);
    hide?.();
    setRowData(() => ({ ...initialData }));
  }, [hide, onCancel, rowData, initialData]);

  const formFields = React.useMemo(
    () =>
      columns
        .filter((column) => typeof column.editable !== 'function' || column.editable(column, rowData, layoutOptions))
        .map(
          ({ type: columnType = 'string', ...column }, index) =>
            (!!column.editComponent && (
              <Grid key={index} item {...grid}>
                {column.editComponent?.({
                  rowData,
                  value:
                    !!column.field && rowData.hasOwnProperty(column.field)
                      ? rowData?.[column.field as keyof RowData]
                      : column?.initialEditValue,
                  onChange: (value) =>
                    setRowData((previousData) =>
                      !!column.field
                        ? {
                            ...previousData,
                            [column.field]: value,
                          }
                        : previousData,
                    ),
                  onRowDataChange: setRowData,
                  error:
                    column.validate !== undefined &&
                    column.validate !== null &&
                    !isValidResult(column.validate(rowData)),
                  inputProps: column.editComponentInputProps,
                  required: column.editComponentFieldRequired,
                })}
              </Grid>
            )) ||
            (columnType === 'string' && (
              <Grid key={index} item {...grid}>
                <Input
                  inputType="string"
                  inputProps={{
                    title: column.title ?? '',
                    value: String(
                      !!column.field && rowData.hasOwnProperty(column.field)
                        ? rowData?.[column.field as keyof RowData]
                        : !!column.value
                        ? column.value(rowData)
                        : column?.initialEditValue ?? '',
                    ),
                    onChange: (value) =>
                      (layoutOptions === 'edit' &&
                        !!column.editSetRowData &&
                        column.editSetRowData(value as never, setRowData) &&
                        true) ||
                      (layoutOptions === 'add' &&
                        !!column.addSetRowData &&
                        column.addSetRowData(value as never, setRowData) &&
                        true) ||
                      setRowData((previousData) =>
                        !!column.field
                          ? {
                              ...previousData,
                              [column.field]: value,
                            }
                          : previousData,
                      ),
                    disabled: inProgress,
                    error:
                      column.validate !== undefined &&
                      column.validate !== null &&
                      !isValidResult(column.validate(rowData)),
                    helperText:
                      column.validate !== undefined &&
                      column.validate !== null &&
                      getErrorMessage(column.validate(rowData)),
                    size: padding === 'dense' ? 'small' : undefined,
                    lookupInputType: column?.lookupInputType,
                    lookupInputProps: column?.lookupInputProps,
                    lookupLabelProps: column?.lookupLabelProps,
                    lookupRadioGroupProps: column?.lookupRadioGroupProps,
                    lookupItemRender: column?.lookupItemRender,
                    lookupSort: column?.lookupSort,
                    lookupItemDisabled: !!column?.lookupItemDisabled
                      ? (item) => !!column?.lookupItemDisabled && column.lookupItemDisabled(rowData, item)
                      : undefined,
                    inputProps: column.editComponentInputProps,
                    required: column.editComponentFieldRequired,
                  }}
                  lookupPromise={
                    (layoutOptions === 'edit' && !!column.editLookup && column.editLookup(rowData)) ||
                    (layoutOptions === 'add' && !!column.addLookup && column.addLookup(rowData)) ||
                    (column.lookup && Promise.resolve(column?.lookup)) ||
                    undefined
                  }
                />
              </Grid>
            )) ||
            (columnType === 'multi-string' && (
              <Grid key={index} item {...grid}>
                <Input
                  inputType="multi-string"
                  inputProps={{
                    title: column.title ?? '',
                    value:
                      !!column.field && rowData.hasOwnProperty(column.field)
                        ? rowData?.[column.field as keyof RowData]
                        : !!column.value
                        ? column.value(rowData)
                        : column?.initialEditValue ?? [],
                    onChange: (value) =>
                      (layoutOptions === 'edit' &&
                        !!column.editSetRowData &&
                        column.editSetRowData(value as never, setRowData) &&
                        true) ||
                      (layoutOptions === 'add' &&
                        !!column.addSetRowData &&
                        column.addSetRowData(value as never, setRowData) &&
                        true) ||
                      setRowData((previousData) =>
                        !!column.field
                          ? {
                              ...previousData,
                              [column.field]: value,
                            }
                          : previousData,
                      ),
                    disabled: inProgress,
                    error:
                      column.validate !== undefined &&
                      column.validate !== null &&
                      !isValidResult(column.validate(rowData)),
                    helperText:
                      column.validate !== undefined &&
                      column.validate !== null &&
                      getErrorMessage(column.validate(rowData)),
                    size: padding === 'dense' ? 'small' : undefined,
                    lookupInputType: column?.lookupInputType,
                    lookupInputProps: column?.lookupInputProps,
                    lookupLabelProps: column?.lookupLabelProps,
                    lookupFormGroupProps: column?.lookupFormGroupProps,
                    lookupItemRender: column?.lookupItemRender,
                    lookupSort: column?.lookupSort,
                    lookupItemDisabled: !!column?.lookupItemDisabled
                      ? (item) => !!column?.lookupItemDisabled && column.lookupItemDisabled(rowData, item)
                      : undefined,
                    inputProps: column.editComponentInputProps,
                    required: column.editComponentFieldRequired,
                  }}
                  lookupPromise={
                    (layoutOptions === 'edit' && !!column.editLookup && column.editLookup(rowData)) ||
                    (layoutOptions === 'add' && !!column.addLookup && column.addLookup(rowData)) ||
                    (column.lookup && Promise.resolve(column?.lookup)) ||
                    undefined
                  }
                />
              </Grid>
            )) ||
            ((columnType === 'numeric' || columnType === 'currency') && (
              <Grid key={index} item {...grid}>
                <Input
                  inputType={columnType}
                  inputProps={{
                    title: column.title ?? '',
                    value:
                      !!column.field && rowData.hasOwnProperty(column.field)
                        ? rowData?.[column.field as keyof RowData]
                        : !!column.value
                        ? column.value(rowData)
                        : column?.initialEditValue,
                    onChange: (value) =>
                      (layoutOptions === 'edit' &&
                        !!column.editSetRowData &&
                        column.editSetRowData(value as never, setRowData) &&
                        true) ||
                      (layoutOptions === 'add' &&
                        !!column.addSetRowData &&
                        column.addSetRowData(value as never, setRowData) &&
                        true) ||
                      setRowData((previousData) =>
                        !!column.field
                          ? {
                              ...previousData,
                              [column.field]: value,
                            }
                          : previousData,
                      ),
                    disabled: inProgress,
                    error:
                      column.validate !== undefined &&
                      column.validate !== null &&
                      !isValidResult(column.validate(rowData)),
                    helperText:
                      column.validate !== undefined &&
                      column.validate !== null &&
                      getErrorMessage(column.validate(rowData)),
                    size: padding === 'dense' ? 'small' : undefined,
                    lookupInputType: column?.lookupInputType,
                    lookupInputProps: column?.lookupInputProps,
                    lookupLabelProps: column?.lookupLabelProps,
                    lookupRadioGroupProps: column?.lookupRadioGroupProps,
                    lookupItemRender: column?.lookupItemRender,
                    lookupSort: column?.lookupSort,
                    lookupItemDisabled: !!column?.lookupItemDisabled
                      ? (item) => !!column?.lookupItemDisabled && column.lookupItemDisabled(rowData, item)
                      : undefined,
                    inputProps: column.editComponentInputProps,
                    required: column.editComponentFieldRequired,
                  }}
                  lookupPromise={
                    (layoutOptions === 'edit' && !!column.editLookup && column.editLookup(rowData)) ||
                    (layoutOptions === 'add' && !!column.addLookup && column.addLookup(rowData)) ||
                    (column.lookup && Promise.resolve(column?.lookup)) ||
                    undefined
                  }
                />
              </Grid>
            )) ||
            (columnType === 'boolean' && (
              <Grid key={index} item {...grid}>
                <Input
                  inputType="boolean"
                  inputProps={{
                    title: column.title ?? '',
                    onChange: (newValue) =>
                      (layoutOptions === 'edit' &&
                        !!column.editSetRowData &&
                        column.editSetRowData(newValue as never, setRowData) &&
                        true) ||
                      (layoutOptions === 'add' &&
                        !!column.addSetRowData &&
                        column.addSetRowData(newValue as never, setRowData) &&
                        true) ||
                      setRowData((previousData) =>
                        !!column.field
                          ? {
                              ...previousData,
                              [column.field]: newValue,
                            }
                          : previousData,
                      ),
                    value:
                      !!column.field && rowData.hasOwnProperty(column.field)
                        ? rowData?.[column.field as keyof RowData]
                        : !!column.value
                        ? column.value(rowData)
                        : column?.initialEditValue,
                    disabled: inProgress,
                    error:
                      column.validate !== undefined &&
                      column.validate !== null &&
                      !isValidResult(column.validate(rowData)),
                    helperText:
                      column.validate !== undefined &&
                      column.validate !== null &&
                      getErrorMessage(column.validate(rowData)),
                    size: padding === 'dense' ? 'small' : undefined,
                    required: column.editComponentFieldRequired,
                    lookupInputType: column?.lookupInputType,
                    lookupInputProps: column?.lookupInputProps,
                    lookupLabelProps: column?.lookupLabelProps,
                    lookupRadioGroupProps: column?.lookupRadioGroupProps,
                    lookupItemRender: column?.lookupItemRender,
                    lookupSort: column?.lookupSort,
                    lookupItemDisabled: !!column?.lookupItemDisabled
                      ? (item) => !!column?.lookupItemDisabled && column.lookupItemDisabled(rowData, item)
                      : undefined,
                  }}
                  lookupPromise={
                    (layoutOptions === 'edit' && !!column.editLookup && column.editLookup(rowData)) ||
                    (layoutOptions === 'add' && !!column.addLookup && column.addLookup(rowData)) ||
                    (column.lookup && Promise.resolve(column?.lookup)) ||
                    undefined
                  }
                />
              </Grid>
            )) ||
            (columnType === 'date' && (
              <Grid key={index} item {...grid}>
                <DateInput
                  title={column.title ?? ''}
                  onChange={(newValue) =>
                    (layoutOptions === 'edit' &&
                      !!column.editSetRowData &&
                      column.editSetRowData(newValue as never, setRowData) &&
                      true) ||
                    (layoutOptions === 'add' &&
                      !!column.addSetRowData &&
                      column.addSetRowData(newValue as never, setRowData) &&
                      true) ||
                    setRowData((previousData) =>
                      !!column.field
                        ? {
                            ...previousData,
                            [column.field]: newValue,
                          }
                        : previousData,
                    )
                  }
                  value={
                    !!column.field && rowData.hasOwnProperty(column.field)
                      ? rowData?.[column.field as keyof RowData]
                      : !!column.value
                      ? column.value(rowData)
                      : column?.initialEditValue
                  }
                  dateSetting={column?.dateSetting}
                  disabled={inProgress}
                  error={
                    column.validate !== undefined &&
                    column.validate !== null &&
                    !isValidResult(column.validate(rowData))
                  }
                  helperText={
                    column.validate !== undefined &&
                    column.validate !== null &&
                    getErrorMessage(column.validate(rowData))
                  }
                  size={padding === 'dense' ? 'small' : undefined}
                  required={column.editComponentFieldRequired}
                />
              </Grid>
            )) ||
            (columnType === 'datetime' && (
              <Grid key={index} item {...grid}>
                <DateTimeInput
                  title={column.title ?? ''}
                  onChange={(newValue) =>
                    (layoutOptions === 'edit' &&
                      !!column.editSetRowData &&
                      column.editSetRowData(newValue as never, setRowData) &&
                      true) ||
                    (layoutOptions === 'add' &&
                      !!column.addSetRowData &&
                      column.addSetRowData(newValue as never, setRowData) &&
                      true) ||
                    setRowData((previousData) =>
                      !!column.field
                        ? {
                            ...previousData,
                            [column.field]: newValue,
                          }
                        : previousData,
                    )
                  }
                  value={
                    !!column.field && rowData.hasOwnProperty(column.field)
                      ? rowData?.[column.field as keyof RowData]
                      : !!column.value
                      ? column.value(rowData)
                      : column?.initialEditValue
                  }
                  dateSetting={column?.dateSetting}
                  disabled={inProgress}
                  error={
                    column.validate !== undefined &&
                    column.validate !== null &&
                    !isValidResult(column.validate(rowData))
                  }
                  helperText={
                    column.validate !== undefined &&
                    column.validate !== null &&
                    getErrorMessage(column.validate(rowData))
                  }
                  size={padding === 'dense' ? 'small' : undefined}
                  required={column.editComponentFieldRequired}
                />
              </Grid>
            )) ||
            (columnType === 'time' && (
              <Grid key={index} item {...grid}>
                <TimeInput
                  title={column.title ?? ''}
                  onChange={(newValue) =>
                    (layoutOptions === 'edit' &&
                      !!column.editSetRowData &&
                      column.editSetRowData(newValue as never, setRowData) &&
                      true) ||
                    (layoutOptions === 'add' &&
                      !!column.addSetRowData &&
                      column.addSetRowData(newValue as never, setRowData) &&
                      true) ||
                    setRowData((previousData) =>
                      !!column.field
                        ? {
                            ...previousData,
                            [column.field]: newValue,
                          }
                        : previousData,
                    )
                  }
                  value={
                    !!column.field && rowData.hasOwnProperty(column.field)
                      ? rowData?.[column.field as keyof RowData]
                      : !!column.value
                      ? column.value(rowData)
                      : column?.initialEditValue
                  }
                  dateSetting={column?.dateSetting}
                  disabled={inProgress}
                  error={
                    column.validate !== undefined &&
                    column.validate !== null &&
                    !isValidResult(column.validate(rowData))
                  }
                  helperText={
                    column.validate !== undefined &&
                    column.validate !== null &&
                    getErrorMessage(column.validate(rowData))
                  }
                  size={padding === 'dense' ? 'small' : undefined}
                  required={column.editComponentFieldRequired}
                />
              </Grid>
            )) ||
            undefined,
        )
        .filter(defined),
    [columns, rowData, inProgress, grid, padding, layoutOptions],
  );

  return React.useMemo(
    () => (
      <Dialog
        fullScreen={fullScreen}
        open={visible}
        onClose={hide}
        fullWidth
        keepMounted={false}
        maxWidth={maxWidth as Breakpoint}
      >
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          {!!formFields?.length && (
            <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start" spacing={2}>
              {formFields}
            </Grid>
          )}
          {!!text && <DialogContentText>{text}</DialogContentText>}
        </DialogContent>
        {inProgress && <LinearProgress />}
        <DialogActions>
          <Button autoFocus onClick={cancel} color="primary" disabled={inProgress}>
            {localization.body.editRow.cancelTooltip}
          </Button>
          <Button onClick={save} color="primary" autoFocus disabled={inProgress || hasError}>
            {localization.body.editRow.saveTooltip}
          </Button>
        </DialogActions>
      </Dialog>
    ),
    [fullScreen, visible, hide, maxWidth, title, formFields, text, inProgress, cancel, localization, save, hasError],
  );
}
