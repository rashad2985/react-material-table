import { InputProps as StandardInputProps } from '@mui/material/Input/Input';

export * from './add-row-button';
export * from './edit-row-button';

export interface EditComponentProps<RowData extends object> {
  rowData: Partial<RowData>;
  value: any;
  onChange: (newValue: any) => void;
  onRowDataChange: (newValue: Partial<RowData>) => void;
  error: boolean;
  inputProps?: StandardInputProps['inputProps'];
  required?: boolean;
}
