import * as React from 'react';
import { useComponents, useIcons, useLocalization } from '../../hooks';
import { Tooltip } from '@mui/material';
import { Column } from '../../../column';
import { defined } from '../../commons';
import { Editable } from '../../../editable';
import { EditableContext } from '../../contexts';
import { Form } from './form';

export type EditRowButtonProps<RowData extends object = {}> = {
  columns: Array<Column<RowData>>;
  rowData: Partial<RowData>;
};

export function EditRowButton<RowData extends object = {}>(props: EditRowButtonProps<RowData>) {
  const { columns, rowData } = props;
  const { onRowUpdate, onRowUpdateCancelled, editTooltip, isEditable, isEditHidden } =
    React.useContext<Editable<RowData>>(EditableContext);
  const { Edit: Icon } = useIcons();
  const { Edit: InteractionElement } = useComponents();
  const localization = useLocalization();

  const [open, setOpen] = React.useState(false);

  const formFields = React.useMemo(
    () =>
      columns
        .filter(defined)
        .filter(
          (column) =>
            column.editable !== 'never' && column.editable !== 'onAdd' && (column.readonly ?? false) === false,
        ),
    [columns],
  );

  const show = React.useCallback(() => setOpen(true), []);
  const hide = React.useCallback(() => setOpen(false), []);

  return React.useMemo(
    () =>
      !!isEditHidden && isEditHidden?.(rowData) ? null : (
        <React.Fragment>
          <Tooltip title={editTooltip?.(rowData) ?? localization.body.editTooltip}>
            <span>{InteractionElement(show, Icon, !!isEditable ? !isEditable?.(rowData) : false)}</span>
          </Tooltip>
          <Form
            title={localization.body.editTooltip}
            columns={formFields}
            visible={open}
            hide={hide}
            initialData={rowData}
            onSave={onRowUpdate}
            onCancel={onRowUpdateCancelled}
            layoutOptions="edit"
          />
        </React.Fragment>
      ),
    [
      InteractionElement,
      show,
      editTooltip,
      localization,
      formFields,
      open,
      hide,
      rowData,
      onRowUpdate,
      onRowUpdateCancelled,
      isEditHidden,
      isEditable,
      Icon,
    ],
  );
}
