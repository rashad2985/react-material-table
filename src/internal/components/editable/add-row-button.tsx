import * as React from 'react';
import { useComponents, useIcons, useLocalization } from '../../hooks';
import { Tooltip } from '@mui/material';
import { Column } from '../../../column';
import { defined } from '../../commons';
import { Editable } from '../../../editable';
import { EditableContext } from '../../contexts';
import { Form } from './form';

export type AddRowButtonProps<RowData extends object = {}> = {
  columns: Array<Column<RowData>>;
};

export function AddRowButton<RowData extends object = {}>(props: AddRowButtonProps<RowData>) {
  const { columns } = props;
  const {
    onRowAdd,
    onRowAddCancelled,
    canAdd = true,
    isAddHidden = false,
  } = React.useContext<Editable<RowData>>(EditableContext);
  const { Add: Icon } = useIcons();
  const { Add: InteractionElement } = useComponents();
  const localization = useLocalization();
  const [open, setOpen] = React.useState(false);

  const formFields = React.useMemo(
    () => columns.filter(defined).filter((column) => column.editable !== 'never' && column.editable !== 'onUpdate'),
    [columns],
  );

  const initialData = React.useMemo<Partial<RowData>>(
    () =>
      formFields
        .filter((column) => !!column.initialEditValue)
        .reduce(
          (previousValue, currentValue) =>
            !!currentValue.field
              ? {
                  ...previousValue,
                  [currentValue.field]: currentValue.initialEditValue,
                }
              : previousValue,
          {} as Partial<RowData>,
        ),
    [formFields],
  );

  const show = React.useCallback(() => setOpen(true), []);
  const hide = React.useCallback(() => setOpen(false), []);

  return React.useMemo(
    () =>
      isAddHidden ? null : (
        <React.Fragment>
          <Tooltip title={localization.body.addTooltip}>
            <span>{InteractionElement(show, Icon, !canAdd)}</span>
          </Tooltip>
          <Form
            title={localization.body.addTooltip}
            columns={formFields}
            visible={open}
            hide={hide}
            initialData={initialData}
            onSave={onRowAdd}
            onCancel={onRowAddCancelled}
            layoutOptions="add"
          />
        </React.Fragment>
      ),
    [
      InteractionElement,
      show,
      localization,
      formFields,
      open,
      hide,
      initialData,
      onRowAdd,
      onRowAddCancelled,
      canAdd,
      isAddHidden,
      Icon,
    ],
  );
}
