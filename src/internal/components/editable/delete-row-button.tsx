import * as React from 'react';
import { useComponents, useIcons, useLocalization } from '../../hooks';
import { Tooltip } from '@mui/material';
import { Editable } from '../../../editable';
import { EditableContext } from '../../contexts';
import { Form } from './form';

export type DeleteRowButtonProps<RowData extends object = {}> = {
  rowData: Partial<RowData>;
};

export function DeleteRowButton<RowData extends object = {}>(props: DeleteRowButtonProps<RowData>) {
  const { rowData } = props;
  const { onRowDelete, deleteTooltip, isDeletable, isDeleteHidden } =
    React.useContext<Editable<RowData>>(EditableContext);
  const { Delete: Icon } = useIcons();
  const { Delete: InteractionElement } = useComponents();
  const localization = useLocalization();

  const [open, setOpen] = React.useState(false);

  const show = React.useCallback(() => setOpen(true), []);
  const hide = React.useCallback(() => setOpen(false), []);

  return React.useMemo(
    () =>
      !!isDeleteHidden && isDeleteHidden?.(rowData) ? null : (
        <React.Fragment>
          <Tooltip title={deleteTooltip?.(rowData) ?? localization.body.deleteTooltip}>
            <span>{InteractionElement(show, Icon, !!isDeletable ? !isDeletable?.(rowData) : false)}</span>
          </Tooltip>
          <Form
            title={localization.body.deleteTooltip}
            columns={[]}
            visible={open}
            hide={hide}
            onSave={onRowDelete}
            text={localization.body.editRow.deleteText}
            layoutOptions="delete"
          />
        </React.Fragment>
      ),
    [
      InteractionElement,
      show,
      deleteTooltip,
      localization,
      open,
      hide,
      onRowDelete,
      isDeleteHidden,
      isDeletable,
      Icon,
      rowData,
    ],
  );
}
