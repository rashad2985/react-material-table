import { DetailPanel } from '../../../components/detail-panel';
import * as React from 'react';
import { Icon, IconButton, IconButtonProps, Tooltip } from '@mui/material';
import { useIcons } from '../../hooks';

export interface DetailPanelButtonProps<DataType extends object = {}>
  extends DetailPanel<DataType>,
    Pick<IconButtonProps, 'onClick'> {
  open: boolean;
}

export function DetailPanelButton<DataType extends object = {}>(props: DetailPanelButtonProps<DataType>) {
  const { open, disabled = false, icon: InitialIcon, openIcon: OpenIcon, iconProps = {}, tooltip, onClick } = props;
  const { DetailPanel: DetailPanelStandardIcon } = useIcons();

  const style = React.useMemo<React.CSSProperties | undefined>(
    () =>
      open && !OpenIcon
        ? {
            transition: 'all 200ms ease 0s',
            transform: 'rotate(90deg)',
          }
        : undefined,
    [open, OpenIcon],
  );

  const resolvedIcon = React.useMemo<React.ReactElement>(
    () =>
      open && !!OpenIcon ? (
        typeof OpenIcon === 'string' ? (
          <Icon {...iconProps} style={style}>
            {OpenIcon}
          </Icon>
        ) : (
          <OpenIcon style={style} />
        )
      ) : !!InitialIcon ? (
        typeof InitialIcon === 'string' ? (
          <Icon {...iconProps} style={style}>
            {InitialIcon}
          </Icon>
        ) : (
          <InitialIcon style={style} />
        )
      ) : (
        <DetailPanelStandardIcon style={style} />
      ),
    [open, OpenIcon, InitialIcon, DetailPanelStandardIcon, iconProps, style],
  );

  return React.useMemo<React.ReactElement>(
    () =>
      !!tooltip ? (
        <Tooltip title={tooltip}>
          <span>
            <IconButton disabled={disabled} onClick={onClick} size="large">
              {resolvedIcon}
            </IconButton>
          </span>
        </Tooltip>
      ) : (
        <IconButton disabled={disabled} onClick={onClick} size="large">
          {resolvedIcon}
        </IconButton>
      ),
    [tooltip, disabled, onClick, resolvedIcon],
  );
}
