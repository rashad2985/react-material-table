import { Action as ActionAttributes, GroupAction } from '../../../action';
import * as React from 'react';
import {
  Icon as DynamicIcon,
  IconButtonProps,
  LinearProgress,
  ListItemIcon,
  ListItemText,
  MenuItem,
  MenuList,
  Popover,
  PopoverOrigin,
  Tooltip,
} from '@mui/material';
import { noop } from '../../commons';
import { useComponents, useIcons } from '../../hooks';

const anchorOrigin: PopoverOrigin = {
  vertical: 'bottom',
  horizontal: 'center',
};
const transformOrigin: PopoverOrigin = {
  vertical: 'top',
  horizontal: 'center',
};

export type ActionProps<RowData extends object = {}> = ActionAttributes<RowData> & {
  row: RowData;
  selectedRows: Array<RowData>;
};

export function Action<RowData extends object = {}>(props: ActionProps<RowData>) {
  const { type = 'action', actions = [], onClick = noop, icon, hidden = false, ...remainingProps } = props;
  return React.useMemo(
    () =>
      (!hidden && type === 'action-group' && (
        <ActionGroup type="action-group" icon={icon} actions={actions} {...remainingProps} />
      )) ||
      (!hidden && type === 'action' && !!icon && (
        <StandardAction type="action" icon={icon} onClick={onClick} {...remainingProps} />
      )) ||
      null,
    [type, actions, onClick, remainingProps, hidden, icon],
  );
}

type OnClick = Required<IconButtonProps>['onClick'];

function StandardAction<RowData extends object = {}>(props: ActionProps<RowData>) {
  const { tooltip, icon: Icon, onClick = noop, row, selectedRows, iconProps = {}, disabled = false } = props;
  const { Action: ActionButton } = useComponents<RowData>();

  const handleClick = React.useCallback<OnClick>(
    (event) => onClick(event, row, selectedRows),
    [onClick, row, selectedRows],
  );

  const icon = React.useMemo(
    () =>
      (!!Icon &&
        (typeof Icon === 'string' ? (
          <DynamicIcon {...iconProps} fontSize={iconProps.fontSize ?? 'small'}>
            {Icon}
          </DynamicIcon>
        ) : (
          <Icon fontSize={iconProps.fontSize ?? 'small'} />
        ))) ||
      null,
    [Icon, iconProps],
  );
  const button = React.useMemo(
    () => (
      <ActionButton onClick={handleClick} disabled={disabled}>
        {icon}
      </ActionButton>
    ),
    [icon, handleClick, ActionButton, disabled],
  );

  return React.useMemo(
    () =>
      !!tooltip ? (
        <Tooltip title={tooltip}>
          <span>{button}</span>
        </Tooltip>
      ) : (
        button
      ),
    [tooltip, button],
  );
}

export type PromiseState<RowData extends object = {}> = {
  state: 'initial' | 'loading' | 'done';
  data?: Array<GroupAction<RowData>>;
  error?: unknown;
};

function ActionGroup<RowData extends object = {}>(props: ActionProps<RowData>) {
  const {
    tooltip,
    icon: ReceivedIcon,
    onClick = noop,
    row,
    selectedRows,
    iconProps = {},
    disabled = false,
    actions = [],
  } = props;

  const [promisedActions, setPromisedActions] = React.useState<PromiseState>({ state: 'initial' });
  const loadActions = React.useCallback(() => {
    if (actions && !Array.isArray(actions)) {
      setPromisedActions({ state: 'loading' });
      actions(row)
        .then((result) => setPromisedActions({ state: 'done', data: result }))
        .catch((error: any) => setPromisedActions({ state: 'done', error: error }));
    }
  }, [actions, row]);

  const { ActionGroup: ActionGroupIcon } = useIcons();
  const Icon = ReceivedIcon ?? ActionGroupIcon;
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const { Action: ActionButton } = useComponents<RowData>();
  const handleClick = React.useCallback(
    (event: React.MouseEvent<HTMLElement>) => {
      setAnchorEl(event.currentTarget);
      if (!Array.isArray(actions) && promisedActions.state === 'initial') {
        loadActions();
      } else {
        onClick(event, row, selectedRows);
      }
    },
    [onClick, row, selectedRows, actions, promisedActions.state, loadActions],
  );
  const handleClose = React.useCallback(() => setAnchorEl(null), []);

  const icon = React.useMemo(
    () =>
      typeof Icon === 'string' ? (
        <DynamicIcon {...iconProps} fontSize={iconProps.fontSize ?? 'small'}>
          {Icon}
        </DynamicIcon>
      ) : (
        <Icon fontSize={iconProps.fontSize ?? 'small'} />
      ),
    [Icon, iconProps],
  );

  const menuItems = React.useMemo(
    () =>
      promisedActions.state === 'loading'
        ? [
            <MenuItem key="loading" disabled>
              <ListItemText primary={<LinearProgress />} primaryTypographyProps={{ width: '7ch' }} />
            </MenuItem>,
          ]
        : (Array.isArray(actions) ? actions : promisedActions.data ?? []).map((action, index) => {
            const {
              tooltip: MenuItemTooltip,
              icon: MenuItemIcon,
              onClick: menuItemOnClick = noop,
              iconProps: menuItemIconProps = {},
              disabled: menuItemDisabled = false,
            } = typeof action === 'function' ? action(row) : action;
            const menuItemIcon =
              !!MenuItemIcon &&
              (typeof MenuItemIcon === 'string' ? (
                <DynamicIcon {...menuItemIconProps} fontSize="small">
                  {MenuItemIcon}
                </DynamicIcon>
              ) : (
                <MenuItemIcon fontSize="small" />
              ));
            return (
              <MenuItem
                key={index}
                onClick={(event) => menuItemOnClick(event, row, selectedRows)}
                disabled={menuItemDisabled}
              >
                {menuItemIcon && <ListItemIcon>{menuItemIcon}</ListItemIcon>}
                <ListItemText primary={MenuItemTooltip} />
              </MenuItem>
            );
          }),
    [actions, row, selectedRows, promisedActions],
  );

  const button = React.useMemo(
    () => (
      <ActionButton
        onClick={handleClick}
        disabled={(Array.isArray(actions) && menuItems.length < 1) || disabled}
        aria-haspopup="true"
      >
        {icon}
      </ActionButton>
    ),
    [handleClick, disabled, icon, ActionButton, menuItems.length, actions],
  );

  return React.useMemo(
    () => (
      <React.Fragment>
        {!!tooltip ? (
          <Tooltip title={tooltip}>
            <span>{button}</span>
          </Tooltip>
        ) : (
          button
        )}
        <Popover
          anchorEl={anchorEl}
          keepMounted={true}
          open={Boolean(anchorEl)}
          onClose={handleClose}
          anchorOrigin={anchorOrigin}
          transformOrigin={transformOrigin}
        >
          <MenuList>{menuItems}</MenuList>
        </Popover>
      </React.Fragment>
    ),
    [tooltip, button, anchorEl, handleClose, menuItems],
  );
}
