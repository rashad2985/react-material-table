import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormControlLabelProps,
  FormGroup,
  FormHelperText,
  FormLabel,
  List,
  MenuItem,
  Popper,
  TextField,
  TextFieldProps,
  Typography,
} from '@mui/material';
import { Autocomplete, AutocompleteProps } from '@mui/material';
import * as React from 'react';
import { Column, LookupItemProps } from '../../../column';
import { useOptions } from '../../hooks';
import { autocompleteListStyle, commonMenuProps } from '../../commons';

const autocompleteGetOptionLabel = (option: LookupItemProps) => option?.text ?? option?.value ?? '';

export type MultiStringInputProps<RowData extends object = {}> = Pick<
  Column<RowData>,
  | 'title'
  | 'lookup'
  | 'lookupInputType'
  | 'lookupInputProps'
  | 'lookupLabelProps'
  | 'lookupFormGroupProps'
  | 'lookupItemRender'
  | 'lookupSort'
> &
  Pick<
    TextFieldProps,
    | 'margin'
    | 'className'
    | 'style'
    | 'placeholder'
    | 'disabled'
    | 'error'
    | 'helperText'
    | 'size'
    | 'inputProps'
    | 'required'
  > & {
    lookupItemDisabled?: (item: LookupItemProps) => boolean;
    value?: Array<string>;
    onChange: (value: Array<string>) => void;
  };

export function MultiStringInput<RowData extends object = {}>(props: MultiStringInputProps) {
  const {
    title,
    lookup,
    lookupInputType = 'text-field',
    lookupLabelProps,
    lookupInputProps,
    lookupFormGroupProps,
    lookupItemRender,
    lookupItemDisabled,
    lookupSort,
    value,
    onChange,
    margin,
    className,
    style,
    placeholder,
    disabled,
    error,
    helperText,
    inputProps,
    required,
    size,
  } = props;

  const lookupItemProps = React.useMemo<Array<LookupItemProps>>(() => {
    const unsortedLookupItemProps = Array.from(lookup ?? new Map<string, string>()).map<LookupItemProps>(
      ([key, text]) => ({
        value: key,
        text: text ?? key,
      }),
    );
    return !lookupSort ? unsortedLookupItemProps.sort(lookupSort) : unsortedLookupItemProps;
  }, [lookup, lookupSort]);

  const { inputVariant } = useOptions<RowData>();

  const optionRenderFn = React.useMemo(
    () =>
      !!lookupItemRender
        ? (props: any, option: LookupItemProps) => <li {...props}>{lookupItemRender(option)}</li>
        : undefined,
    [lookupItemRender],
  );

  const handleCheckboxClick = React.useCallback(
    (key: string) => (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
      const newValues = !!checked ? [...(value ?? []), key] : (value ?? []).filter((valueKey) => valueKey !== key);
      onChange?.(newValues);
    },
    [value, onChange],
  );

  const lookupOptions = React.useMemo(
    () =>
      lookupItemProps.map(({ value: key, text }) =>
        lookupInputType === 'radio-group' || lookupInputType === 'checkbox-group' ? (
          <FormControlLabel
            key={key}
            value={key}
            {...((lookupInputProps ?? {}) as FormControlLabelProps)}
            label={!!optionRenderFn ? optionRenderFn({}, { value: key, text: text ?? key }) : text ?? key}
            control={
              ((lookupInputProps ?? {}) as FormControlLabelProps).control ?? (
                <Checkbox
                  checked={(value ?? []).some((valueKey) => valueKey === key)}
                  onChange={handleCheckboxClick(key)}
                />
              )
            }
            disabled={
              ((lookupInputProps ?? {}) as FormControlLabelProps).disabled ??
              (!!lookupItemDisabled
                ? lookupItemDisabled({
                    value: key,
                    text: text ?? key,
                  })
                : undefined)
            }
          />
        ) : (
          <MenuItem
            key={key}
            value={key}
            disabled={
              !!lookupItemDisabled
                ? lookupItemDisabled({
                    value: key,
                    text: text ?? key,
                  })
                : undefined
            }
          >
            {!!optionRenderFn ? optionRenderFn({}, { value: key, text: text ?? key }) : text ?? key}
          </MenuItem>
        ),
      ),
    [
      lookupItemProps,
      lookupInputType,
      lookupInputProps,
      optionRenderFn,
      value,
      lookupItemDisabled,
      handleCheckboxClick,
    ],
  );

  const handleChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      onChange?.((event.target.value ?? []) as unknown as Array<string>);
    },
    [onChange],
  );

  const handleAutocompleteChange = React.useCallback(
    (_: unknown, changedOptions: Array<LookupItemProps> | null) => {
      const newValues = [...(changedOptions ?? []).map((co) => co.value)];
      onChange?.(newValues);
    },
    [onChange],
  );

  const resolvedInputProps = React.useMemo<TextFieldProps['inputProps']>(
    () => ({ ...(inputProps ?? {}) }),
    [inputProps],
  );

  const autocompleteProps = React.useMemo<
    AutocompleteProps<LookupItemProps, true, undefined, undefined> | undefined
  >(() => {
    const passedProps = (lookupInputProps ?? {}) as AutocompleteProps<LookupItemProps, true, undefined, undefined>;
    return lookupInputType === 'autocomplete'
      ? ({
          ...passedProps,
          PopperComponent:
            // eslint-disable-next-line
            // @ts-ignore
            passedProps.PopperComponent ?? ((popperProps) => <Popper {...popperProps} placement="bottom-start" />),
          ListboxComponent:
            passedProps.ListboxComponent ??
            ((listboxProps) => (
              <List {...listboxProps} style={{ ...(listboxProps?.style ?? {}), ...autocompleteListStyle }} />
            )),
          multiple: true,
          autoComplete: passedProps.autoComplete ?? true,
          fullWidth: passedProps.fullWidth ?? true,
          disabled: passedProps.disabled ?? disabled,
          size: passedProps.size ?? size ?? 'small',
          options: passedProps.options ?? lookupItemProps,
          value: passedProps.value ?? lookupItemProps.filter((option) => (value ?? []).includes(option.value)) ?? [],
          onChange: passedProps.onChange ?? handleAutocompleteChange,
          getOptionLabel: passedProps.getOptionLabel ?? autocompleteGetOptionLabel,
          renderOption: passedProps.renderOption ?? optionRenderFn,
          renderTags:
            passedProps.renderTags ??
            ((values = []) => (
              <Typography component="div" variant="inherit" noWrap>
                {values.map((_) => _.text).join(', ')}
              </Typography>
            )),
          renderInput:
            passedProps.renderInput ??
            ((params) => (
              <TextField
                {...params}
                fullWidth
                variant={inputVariant}
                margin={margin ?? 'dense'}
                label={title}
                style={style}
                className={className}
                placeholder={placeholder}
                error={error}
                helperText={helperText}
                inputProps={{ ...(params?.inputProps ?? {}), ...resolvedInputProps }}
                required={required}
              />
            )),
          getOptionDisabled:
            passedProps.getOptionDisabled ?? (!!lookupItemDisabled ? (item) => lookupItemDisabled(item) : undefined),
        } as AutocompleteProps<LookupItemProps, true, undefined, undefined>)
      : undefined;
  }, [
    lookupInputProps,
    lookupInputType,
    lookupItemDisabled,
    lookupItemProps,
    disabled,
    size,
    value,
    handleAutocompleteChange,
    optionRenderFn,
    inputVariant,
    margin,
    title,
    style,
    className,
    placeholder,
    error,
    helperText,
    resolvedInputProps,
    required,
  ]);

  return React.useMemo(
    () =>
      (!!autocompleteProps && lookupInputType === 'autocomplete' && <Autocomplete {...autocompleteProps} />) ||
      (lookupInputType === 'checkbox-group' && (
        <FormControl fullWidth margin={margin} error={error}>
          <FormLabel {...(lookupLabelProps ?? {})}>{(lookupLabelProps ?? {})?.children ?? title}</FormLabel>
          <FormGroup {...(lookupFormGroupProps ?? {})}>{lookupOptions}</FormGroup>
          <FormHelperText>{helperText}</FormHelperText>
        </FormControl>
      )) || (
        <TextField
          select={true}
          SelectProps={{
            multiple: true,
            MenuProps: commonMenuProps,
          }}
          margin={margin ?? 'dense'}
          variant={inputVariant}
          fullWidth
          label={title}
          value={value}
          onChange={handleChange}
          style={style}
          className={className}
          placeholder={placeholder}
          disabled={disabled}
          error={error}
          helperText={helperText}
          size={size ?? 'small'}
          inputProps={resolvedInputProps}
          required={required}
        >
          {lookupOptions}
        </TextField>
      ),
    [
      autocompleteProps,
      lookupInputType,
      lookupLabelProps,
      lookupFormGroupProps,
      lookupOptions,
      margin,
      title,
      inputVariant,
      value,
      handleChange,
      style,
      className,
      placeholder,
      disabled,
      error,
      helperText,
      size,
      resolvedInputProps,
      required,
    ],
  );
}
