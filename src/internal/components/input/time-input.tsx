import * as React from 'react';
import { Column } from '../../../column';
import { TimePicker, TimePickerProps } from '@mui/x-date-pickers';
import { toTime } from '../../commons';
import { useOptions } from '../../hooks';
import { TextField, TextFieldProps } from '@mui/material';

export type TimeInputProps<RowData extends object = {}> = Pick<Column<RowData>, 'title' | 'dateSetting'> &
  Pick<TimePickerProps<Date, Date>, 'onChange'> &
  Pick<
    TextFieldProps,
    'margin' | 'className' | 'style' | 'placeholder' | 'disabled' | 'error' | 'helperText' | 'size' | 'required'
  > & {
    value?: number | Date;
  };

export function TimeInput<RowData extends object = {}>(props: TimeInputProps<RowData>) {
  const {
    title,
    value,
    // dateSetting,
    onChange,
    margin,
    className,
    style,
    placeholder,
    disabled,
    error,
    helperText,
    size,
    required,
  } = props;
  // const { locale } = dateSetting || {};
  const { inputVariant } = useOptions<RowData>();
  const handleChange = React.useCallback(
    (date: Date | null) => {
      if (date !== null) {
        onChange?.(toTime(date));
      } else {
        onChange?.(null);
      }
    },
    [onChange],
  );

  return React.useMemo(
    () => (
      <TimePicker
        componentsProps={{
          actionBar: {
            actions: ['today', 'clear', 'cancel', 'accept'],
          },
        }}
        label={title}
        //labelFunc={() => (value !== undefined ? new Date(value).toLocaleTimeString(locale, hoursAndMinutes) : '')}
        value={value !== undefined ? new Date(value) : undefined}
        onChange={handleChange}
        disabled={disabled}
        renderInput={(props) => (
          <TextField
            {...props}
            fullWidth
            margin={margin ?? props.margin ?? 'dense'}
            size={size ?? props.size ?? 'small'}
            style={style ?? props.style}
            className={className ?? props.className}
            placeholder={placeholder ?? props.placeholder}
            variant={inputVariant ?? props.variant}
            label={title ?? props.label}
            required={required ?? props.required}
            helperText={helperText ?? props.helperText}
            error={error ?? props.error}
          />
        )}
      />
    ),
    [
      inputVariant,
      margin,
      title,
      value,
      handleChange,
      style,
      className,
      placeholder,
      error,
      helperText,
      disabled,
      size,
      required,
    ],
  );
}
