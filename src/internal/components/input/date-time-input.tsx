import * as React from 'react';
import { Column } from '../../../column';
import { DateTimePicker, DateTimePickerProps } from '@mui/x-date-pickers';
import { useOptions } from '../../hooks';
import { TextField, TextFieldProps } from '@mui/material';

export type DateTimeInputProps<RowData extends object = {}> = Pick<Column<RowData>, 'title' | 'dateSetting'> &
  Pick<DateTimePickerProps<Date, Date>, 'onChange'> &
  Pick<
    TextFieldProps,
    'margin' | 'className' | 'style' | 'placeholder' | 'disabled' | 'error' | 'helperText' | 'size' | 'required'
  > & {
    value?: number | Date;
  };

export function DateTimeInput<RowData extends object = {}>(props: DateTimeInputProps<RowData>) {
  const {
    title,
    value,
    // dateSetting,
    onChange,
    margin,
    className,
    style,
    placeholder,
    disabled,
    error,
    helperText,
    size,
    required,
  } = props;
  // const { locale, formatOptions } = dateSetting || {};
  const { inputVariant } = useOptions<RowData>();
  /*
  const finalFormatOptions = React.useMemo<Intl.DateTimeFormatOptions>(
    () => ({ ...dateTimeFormatOptions, ...formatOptions, second: undefined }),
    [formatOptions],
  );
  */

  const handleChange = React.useCallback(
    (date: Date | null) => {
      onChange?.(date);
    },
    [onChange],
  );

  return React.useMemo(
    () => (
      <DateTimePicker
        componentsProps={{
          actionBar: {
            actions: ['today', 'clear', 'cancel', 'accept'],
          },
        }}
        //labelFunc={() => (value !== undefined ? new Date(value).toLocaleString(locale, finalFormatOptions) : '')}
        value={value !== undefined ? new Date(value) : undefined}
        onChange={handleChange}
        disabled={disabled}
        renderInput={(props) => (
          <TextField
            {...props}
            fullWidth
            margin={margin ?? props.margin ?? 'dense'}
            size={size ?? props.size ?? 'small'}
            style={style ?? props.style}
            className={className ?? props.className}
            placeholder={placeholder ?? props.placeholder}
            variant={inputVariant ?? props.variant}
            label={title ?? props.label}
            required={required ?? props.required}
            helperText={helperText ?? props.helperText}
            error={error ?? props.error}
          />
        )}
      />
    ),
    [
      inputVariant,
      margin,
      title,
      value,
      handleChange,
      style,
      className,
      placeholder,
      error,
      helperText,
      disabled,
      size,
      required,
    ],
  );
}
