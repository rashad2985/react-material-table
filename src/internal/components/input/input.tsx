import { FormControl, FormHelperText, FormLabel, Skeleton, Typography } from '@mui/material';
import * as React from 'react';
import { useOptions } from '../../hooks';
import {
  BooleanInput,
  BooleanInputProps,
  MultiStringInput,
  MultiStringInputProps,
  NumericInput,
  NumericInputProps,
  StringInput,
  StringInputProps,
} from './index';

export type LookupPromise = Promise<Map<string, string>>;
export type LookupPromiseState = {
  inProgress: boolean;
  data?: Map<string, string>;
  error?: unknown;
};

export type InputProps<RowData extends object = {}> = {
  lookupPromise?: LookupPromise;
} & (
  | { readonly inputType: 'string'; inputProps: StringInputProps<RowData> }
  | { readonly inputType: 'numeric' | 'currency'; inputProps: NumericInputProps<RowData> }
  | { readonly inputType: 'boolean'; inputProps: BooleanInputProps<RowData> }
  | { readonly inputType: 'multi-string'; inputProps: MultiStringInputProps<RowData> }
);

export function Input<RowData extends object = {}>(props: InputProps<RowData>) {
  const { lookupPromise, inputType, inputProps } = props;
  const { title, lookupLabelProps, margin, size, error, helperText, required } = inputProps;

  const { inputVariant } = useOptions<RowData>();

  const [lookupPromiseState, setLookupPromiseState] = React.useState<LookupPromiseState>({ inProgress: true });
  React.useEffect(() => {
    if (lookupPromise) {
      lookupPromise
        .then((result) => setLookupPromiseState({ inProgress: false, data: result }))
        .catch((lookupError: any) => setLookupPromiseState({ inProgress: false, error: lookupError }));
    }
  }, [lookupPromise]);

  return React.useMemo(
    () =>
      lookupPromise && lookupPromiseState.inProgress ? (
        <FormControl
          fullWidth
          margin={margin}
          variant={inputVariant}
          size={size}
          required={required}
          error={error || !!lookupPromiseState.error}
        >
          <Skeleton height={size === 'medium' ? 72 : 56} />
          <Typography position="absolute" left="8px" top="50%" sx={{ transform: 'translateY(-50%)' }}>
            {(lookupLabelProps ?? {})?.children ?? title}
          </Typography>
        </FormControl>
      ) : (
        (inputType === 'string' && (
          <StringInput
            {...(inputProps as StringInputProps<RowData>)}
            lookup={lookupPromiseState.data}
            error={error || !!lookupPromiseState.error}
            helperText={
              helperText ||
              (typeof lookupPromiseState.error === 'string' ? lookupPromiseState.error : undefined) ||
              String.fromCodePoint(160)
            }
          />
        )) ||
        ((inputType === 'numeric' || inputType === 'currency') && (
          <NumericInput
            {...(inputProps as NumericInputProps<RowData>)}
            lookup={lookupPromiseState.data}
            error={error || !!lookupPromiseState.error}
            helperText={
              helperText ||
              (typeof lookupPromiseState.error === 'string' ? lookupPromiseState.error : undefined) ||
              String.fromCodePoint(160)
            }
          />
        )) ||
        (inputType === 'boolean' && (
          <BooleanInput
            {...(inputProps as BooleanInputProps<RowData>)}
            lookup={lookupPromiseState.data}
            error={error || !!lookupPromiseState.error}
            helperText={
              helperText ||
              (typeof lookupPromiseState.error === 'string' ? lookupPromiseState.error : undefined) ||
              String.fromCodePoint(160)
            }
          />
        )) ||
        (inputType === 'multi-string' && (
          <MultiStringInput
            {...(inputProps as MultiStringInputProps<RowData>)}
            lookup={lookupPromiseState.data}
            error={error || !!lookupPromiseState.error}
            helperText={
              helperText ||
              (typeof lookupPromiseState.error === 'string' ? lookupPromiseState.error : undefined) ||
              String.fromCodePoint(160)
            }
          />
        )) || (
          <FormControl
            fullWidth
            margin={margin}
            variant={inputVariant}
            size={size}
            required={required}
            error={error || !!lookupPromiseState.error}
          >
            <FormLabel {...(lookupLabelProps ?? {})}>{(lookupLabelProps ?? {})?.children ?? title}</FormLabel>
            {String.fromCodePoint(160)}
            <FormHelperText>
              {(typeof lookupPromiseState.error === 'string' ? lookupPromiseState.error : undefined) ||
                String.fromCodePoint(160)}
            </FormHelperText>
          </FormControl>
        )
      ),
    [
      inputType,
      inputProps,
      lookupPromise,
      lookupPromiseState,
      lookupLabelProps,
      margin,
      title,
      inputVariant,
      error,
      helperText,
      size,
      required,
    ],
  );
}
