import {
  FormControl,
  FormControlLabel,
  FormControlLabelProps,
  FormHelperText,
  FormLabel,
  List,
  MenuItem,
  Popper,
  Radio,
  RadioGroup,
  TextField,
  TextFieldProps,
} from '@mui/material';
import { Autocomplete, AutocompleteProps } from '@mui/material';
import * as React from 'react';
import { Column, LookupItemProps } from '../../../column';
import { useOptions } from '../../hooks';
import { autocompleteListStyle, commonMenuProps } from '../../commons';

const autocompleteGetOptionLabel = (option: LookupItemProps) => option?.text ?? option?.value ?? '';

export type NumericInputProps<RowData extends object = {}> = Pick<
  Column<RowData>,
  | 'field'
  | 'title'
  | 'lookup'
  | 'lookupInputType'
  | 'lookupInputProps'
  | 'lookupLabelProps'
  | 'lookupItemRender'
  | 'lookupRadioGroupProps'
  | 'lookupSort'
> &
  Pick<
    TextFieldProps,
    | 'margin'
    | 'className'
    | 'style'
    | 'placeholder'
    | 'disabled'
    | 'error'
    | 'helperText'
    | 'size'
    | 'inputProps'
    | 'required'
  > & {
    lookupItemDisabled?: (item: LookupItemProps) => boolean;
    value?: number;
    onChange: (value: number) => void;
  };

export function NumericInput<RowData extends object = {}>(props: NumericInputProps<RowData>) {
  const {
    field,
    title,
    lookup,
    lookupInputType = 'text-field',
    lookupLabelProps,
    lookupInputProps,
    lookupRadioGroupProps,
    lookupItemRender,
    lookupItemDisabled,
    lookupSort,
    value,
    onChange,
    margin,
    className,
    style,
    placeholder,
    disabled,
    error,
    helperText,
    inputProps,
    required,
    size,
  } = props;

  const { inputVariant } = useOptions<RowData>();

  const optionRenderFn = React.useMemo(
    () =>
      !!lookupItemRender
        ? (props: any, option: LookupItemProps) => <li {...props}>{lookupItemRender(option)}</li>
        : undefined,
    [lookupItemRender],
  );

  const lookupItemProps = React.useMemo<Array<LookupItemProps>>(() => {
    const unsortedLookupItemProps = Array.from(lookup ?? new Map<string, string>()).map<LookupItemProps>(
      ([key, text]) => ({ value: key, text: text ?? key }),
    );
    return !lookupSort ? unsortedLookupItemProps.sort(lookupSort) : unsortedLookupItemProps;
  }, [lookup, lookupSort]);

  const lookupOptions = React.useMemo(() => {
    return lookupItemProps.map(({ value: key, text }) =>
      lookupInputType === 'radio-group' || lookupInputType === 'checkbox-group' ? (
        <FormControlLabel
          key={key}
          value={key}
          {...((lookupInputProps ?? {}) as FormControlLabelProps)}
          label={!!optionRenderFn ? optionRenderFn({}, { value: key, text: text ?? key }) : text ?? key}
          control={((lookupInputProps ?? {}) as FormControlLabelProps).control ?? <Radio />}
          disabled={
            ((lookupInputProps ?? {}) as FormControlLabelProps).disabled ??
            (!!lookupItemDisabled
              ? lookupItemDisabled({
                  value: key,
                  text: text ?? key,
                })
              : undefined)
          }
        />
      ) : (
        <MenuItem
          key={key}
          value={key}
          disabled={
            !!lookupItemDisabled
              ? lookupItemDisabled({
                  value: key,
                  text: text ?? key,
                })
              : undefined
          }
        >
          {!!optionRenderFn ? optionRenderFn({}, { value: key, text: text ?? key }) : text ?? key}
        </MenuItem>
      ),
    );
  }, [lookupItemProps, lookupInputType, lookupInputProps, optionRenderFn, lookupItemDisabled]);

  const handleChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      onChange?.(Number(event.target.value));
    },
    [onChange],
  );

  const handleAutocompleteChange = React.useCallback(
    (_: unknown, newValue: LookupItemProps | null) => {
      onChange?.(Number(newValue?.value));
    },
    [onChange],
  );

  const resolvedInputProps = React.useMemo<TextFieldProps['inputProps']>(
    () => ({ ...(inputProps ?? {}) }),
    [inputProps],
  );

  const autocompleteProps = React.useMemo<
    AutocompleteProps<LookupItemProps, undefined, undefined, undefined> | undefined
  >(() => {
    const passedProps = (lookupInputProps ?? {}) as AutocompleteProps<LookupItemProps, undefined, undefined, undefined>;
    return lookupInputType === 'autocomplete'
      ? ({
          ...passedProps,
          PopperComponent:
            // eslint-disable-next-line
            // @ts-ignore
            passedProps.PopperComponent ?? ((popperProps) => <Popper {...popperProps} placement="bottom-start" />),
          ListboxComponent:
            passedProps.ListboxComponent ??
            ((listboxProps) => (
              <List {...listboxProps} style={{ ...(listboxProps?.style ?? {}), ...autocompleteListStyle }} />
            )),
          autoComplete: passedProps.autoComplete ?? true,
          fullWidth: passedProps.fullWidth ?? true,
          disabled: passedProps.disabled ?? disabled,
          size: passedProps.size ?? size ?? 'small',
          options: passedProps.options ?? lookupItemProps,
          value: passedProps.value ?? lookupItemProps.find((option) => option.value === String(value)) ?? '',
          onChange: passedProps.onChange ?? handleAutocompleteChange,
          getOptionLabel: passedProps.getOptionLabel ?? autocompleteGetOptionLabel,
          renderOption: passedProps.renderOption ?? optionRenderFn,
          renderInput:
            passedProps.renderInput ??
            ((params) => (
              <TextField
                {...params}
                fullWidth
                variant={inputVariant}
                margin={margin ?? 'dense'}
                label={title}
                style={style}
                className={className}
                placeholder={placeholder}
                error={error}
                helperText={helperText}
                inputProps={{ ...(params?.inputProps ?? {}), ...resolvedInputProps }}
                required={required}
              />
            )),
          getOptionDisabled:
            passedProps.getOptionDisabled ?? (!!lookupItemDisabled ? (item) => lookupItemDisabled(item) : undefined),
        } as AutocompleteProps<LookupItemProps, undefined, undefined, undefined>)
      : undefined;
  }, [
    lookupInputProps,
    lookupInputType,
    lookupItemDisabled,
    lookupItemProps,
    disabled,
    size,
    optionRenderFn,
    value,
    handleAutocompleteChange,
    inputVariant,
    margin,
    title,
    style,
    className,
    placeholder,
    error,
    helperText,
    resolvedInputProps,
    required,
  ]);

  return React.useMemo(
    () =>
      (!!autocompleteProps && lookupInputType === 'autocomplete' && <Autocomplete {...autocompleteProps} />) ||
      (lookupInputType === 'radio-group' && (
        <FormControl fullWidth margin={margin} error={error}>
          <FormLabel {...(lookupLabelProps ?? {})}>{(lookupLabelProps ?? {})?.children ?? title}</FormLabel>
          <RadioGroup
            {...(lookupRadioGroupProps ?? {})}
            name={field ? `${field}` : undefined}
            value={(lookupRadioGroupProps ?? {})?.value ?? String(value) ?? ''}
            onChange={!!(lookupRadioGroupProps ?? {}).onChange ? (lookupRadioGroupProps ?? {})?.onChange : handleChange}
          >
            {lookupOptions}
          </RadioGroup>
          <FormHelperText>{helperText}</FormHelperText>
        </FormControl>
      )) || (
        <TextField
          type={lookupOptions.length > 0 ? undefined : 'number'}
          select={lookupOptions.length > 0}
          margin={margin ?? 'dense'}
          variant={inputVariant}
          fullWidth
          label={title}
          value={value ?? ''}
          onChange={handleChange}
          style={style}
          className={className}
          placeholder={placeholder}
          disabled={disabled}
          error={error}
          helperText={helperText}
          size={size ?? 'small'}
          inputProps={resolvedInputProps}
          required={required}
          SelectProps={{
            renderValue: (selectedValue) =>
              (lookup ?? new Map<string, string>()).get(String(selectedValue)) ?? String(selectedValue),
            MenuProps: commonMenuProps,
          }}
        >
          {lookupOptions}
        </TextField>
      ),
    [
      autocompleteProps,
      lookup,
      lookupInputType,
      lookupLabelProps,
      lookupRadioGroupProps,
      lookupOptions,
      margin,
      field,
      title,
      inputVariant,
      value,
      handleChange,
      style,
      className,
      placeholder,
      disabled,
      error,
      helperText,
      size,
      resolvedInputProps,
      required,
    ],
  );
}
