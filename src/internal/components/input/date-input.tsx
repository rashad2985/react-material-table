import * as React from 'react';
import { Column } from '../../../column';
import { DatePicker, DatePickerProps } from '@mui/x-date-pickers';
import { toDate } from '../../commons';
import { useOptions } from '../../hooks';
import { TextField, TextFieldProps } from '@mui/material';

export type DateInputProps<RowData extends object = {}> = Pick<Column<RowData>, 'title' | 'dateSetting'> &
  Pick<DatePickerProps<Date, Date>, 'onChange'> &
  Pick<
    TextFieldProps,
    'margin' | 'className' | 'style' | 'placeholder' | 'disabled' | 'error' | 'helperText' | 'size' | 'required'
  > & {
    value?: number | Date;
  };

export function DateInput<RowData extends object = {}>(props: DateInputProps<RowData>) {
  const {
    title,
    value,
    // dateSetting,
    onChange,
    margin,
    className,
    style,
    placeholder,
    disabled,
    error,
    helperText,
    required,
    size,
  } = props;
  // const { locale, formatOptions } = dateSetting || {};
  const { inputVariant } = useOptions<RowData>();
  const handleChange = React.useCallback(
    (date: Date | null) => {
      if (date !== null) {
        onChange?.(toDate(date));
      } else {
        onChange?.(null);
      }
    },
    [onChange],
  );

  return React.useMemo(
    () => (
      <DatePicker
        componentsProps={{
          actionBar: {
            actions: ['today', 'clear', 'cancel', 'accept'],
          },
        }}
        // labelFunc={() => (value !== undefined ? new Date(value).toLocaleDateString(locale, formatOptions) : '')}
        value={value !== undefined ? new Date(value) : ''}
        onChange={handleChange}
        disabled={disabled}
        renderInput={(props) => (
          <TextField
            {...props}
            fullWidth
            margin={margin ?? props.margin ?? 'dense'}
            size={size ?? props.size ?? 'small'}
            style={style ?? props.style}
            className={className ?? props.className}
            placeholder={placeholder ?? props.placeholder}
            variant={inputVariant ?? props.variant}
            label={title ?? props.label}
            required={required ?? props.required}
            helperText={helperText ?? props.helperText}
            error={error ?? props.error}
          />
        )}
      />
    ),
    [
      inputVariant,
      margin,
      title,
      value,
      handleChange,
      style,
      className,
      placeholder,
      error,
      helperText,
      disabled,
      size,
      required,
    ],
  );
}
