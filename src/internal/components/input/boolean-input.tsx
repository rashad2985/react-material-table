import {
  FormControl,
  FormControlLabel,
  FormControlLabelProps,
  FormHelperText,
  FormLabel,
  MenuItem,
  Radio,
  RadioGroup,
  TextField,
  TextFieldProps,
} from '@mui/material';
import * as React from 'react';
import { useLocalization, useOptions } from '../../hooks';
import { Column, LookupItemProps } from '../../../column';
import { commonMenuProps } from '../../commons';

export type StringBoolean = 'true' | 'false' | 'undefined';

export const options: Array<StringBoolean> = ['true', 'false'];

export type BooleanInputProps<RowData extends object = {}> = Pick<
  Column<RowData>,
  | 'lookup'
  | 'title'
  | 'lookupInputType'
  | 'lookupInputProps'
  | 'lookupItemRender'
  | 'lookupLabelProps'
  | 'lookupRadioGroupProps'
  | 'lookupSort'
> &
  Pick<
    TextFieldProps,
    | 'margin'
    | 'className'
    | 'style'
    | 'placeholder'
    | 'disabled'
    | 'error'
    | 'helperText'
    | 'size'
    | 'inputProps'
    | 'required'
  > & {
    lookupItemDisabled?: (item: LookupItemProps) => boolean;
    onChange: (value: boolean | undefined) => void;
    value?: boolean | undefined;
  };

export function BooleanInput<RowData extends object = {}>(props: BooleanInputProps<RowData>) {
  const {
    title,
    lookup: initialLookup,
    lookupInputType = 'text-field',
    lookupInputProps,
    lookupItemDisabled,
    lookupSort,
    lookupItemRender,
    lookupLabelProps,
    lookupRadioGroupProps,
    onChange,
    className,
    style,
    margin,
    value,
    placeholder,
    disabled,
    error,
    helperText,
    inputProps,
    required,
    size,
  } = props;
  const localization = useLocalization();
  const { inputVariant } = useOptions<RowData>();
  const lookup = React.useMemo<Map<string, string>>(
    () =>
      initialLookup ??
      new Map<string, string>([
        ['true', localization.row?.boolean?.true],
        ['false', localization.row?.boolean?.false],
        ['undefined', localization.row?.boolean?.undefined],
      ]),
    [initialLookup, localization],
  );
  const items = React.useMemo(() => {
    const lookupItemProps: Array<LookupItemProps> = options.map((option) => ({
      value: String(option),
      text: String(lookup.get(option) ?? option),
    }));
    const lookupItemPropsSorted = !lookupSort ? lookupItemProps.sort(lookupSort) : lookupItemProps;
    return lookupItemPropsSorted.map((option) =>
      lookupInputType === 'radio-group' || lookupInputType === 'checkbox-group' ? (
        <FormControlLabel
          key={option.value}
          value={option.value}
          {...((lookupInputProps ?? {}) as FormControlLabelProps)}
          label={<>{!!lookupItemRender ? lookupItemRender(option) : option.text ?? option.value}</>}
          control={((lookupInputProps ?? {}) as FormControlLabelProps).control ?? <Radio />}
          disabled={
            ((lookupInputProps ?? {}) as FormControlLabelProps).disabled ??
            (!!lookupItemDisabled ? lookupItemDisabled(option) : undefined)
          }
        />
      ) : (
        <MenuItem
          key={option.value}
          value={option.value}
          disabled={!!lookupItemDisabled ? lookupItemDisabled(option) : undefined}
        >
          {!!lookupItemRender ? lookupItemRender(option) : option.text ?? option.value}
        </MenuItem>
      ),
    );
  }, [lookup, lookupInputType, lookupInputProps, lookupItemDisabled, lookupItemRender, lookupSort]);

  const handleChange = React.useCallback(
    (event: React.ChangeEvent<{ value: unknown }>) => {
      const eventValue = event.target.value as string;
      const newValue = (eventValue === 'false' ? false : undefined) ?? (eventValue === 'true' ? true : undefined);
      onChange?.(newValue);
      return newValue;
    },
    [onChange],
  );

  const resolvedInputProps = React.useMemo<TextFieldProps['inputProps']>(
    () => ({ ...(inputProps ?? {}) }),
    [inputProps],
  );

  return React.useMemo(
    () =>
      (lookupInputType === 'radio-group' && (
        <FormControl fullWidth margin={margin} error={error}>
          <FormLabel {...(lookupLabelProps ?? {})}>{(lookupLabelProps ?? {})?.children ?? title}</FormLabel>
          <RadioGroup
            {...(lookupRadioGroupProps ?? {})}
            value={(lookupRadioGroupProps ?? {})?.value ?? String(value ?? '')}
            onChange={
              !!(lookupRadioGroupProps ?? {})?.onChange ? (lookupRadioGroupProps ?? {})?.onChange : handleChange
            }
          >
            {items}
          </RadioGroup>
          <FormHelperText>{helperText}</FormHelperText>
        </FormControl>
      )) || (
        <TextField
          select
          variant={inputVariant}
          label={title}
          value={String(value ?? '')}
          onChange={handleChange}
          margin={margin ?? 'dense'}
          fullWidth
          style={style}
          className={className}
          disabled={disabled}
          placeholder={placeholder}
          error={error}
          helperText={helperText}
          size={size ?? 'small'}
          inputProps={resolvedInputProps}
          required={required}
          SelectProps={{
            MenuProps: commonMenuProps,
          }}
        >
          {items}
        </TextField>
      ),
    [
      inputVariant,
      title,
      value,
      handleChange,
      margin,
      style,
      className,
      disabled,
      placeholder,
      error,
      helperText,
      items,
      size,
      resolvedInputProps,
      required,
      lookupInputType,
      lookupRadioGroupProps,
      lookupLabelProps,
    ],
  );
}
