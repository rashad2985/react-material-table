import {
  FormControlLabel,
  IconButton,
  List,
  ListItem,
  ListItemText,
  Paper,
  Popover,
  Switch,
  Tooltip,
} from '@mui/material';
import * as React from 'react';
import * as ReactTable from 'react-table';
import { UseTableInstanceProps } from 'react-table';
import { useEvents, useIcons, useLocalization } from '../hooks';
import { Column } from '../../column';

const ID = 'trautmann-material-table-column-chooser';

export interface ColumnChooserProps<RowData extends object = {}> {
  allColumns: UseTableInstanceProps<RowData>['allColumns'];
}

export function ColumnChooser<RowData extends object = {}>({ allColumns }: ColumnChooserProps<RowData>) {
  const localization = useLocalization();
  const { onChangeColumnHidden } = useEvents<RowData>();
  const { ViewColumn: ColumnChooserIcon } = useIcons();
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

  const handleClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => setAnchorEl(event.currentTarget),
    [],
  );

  const handleClose = React.useCallback(() => setAnchorEl(null), []);

  const open = Boolean(anchorEl);
  const id = open ? ID : undefined;
  const columns = allColumns.map((column) => {
    const { onChange, ...otherToggleHiddenProps } =
      column.getToggleHiddenProps() as ReactTable.TableToggleHideAllColumnProps;
    return (
      <ListItem key={column.id}>
        <FormControlLabel
          control={
            <Switch
              {...otherToggleHiddenProps}
              color="primary"
              disabled={(column as unknown as Column<RowData>).removable === false}
              onChange={(event) => {
                onChange?.(event);
                onChangeColumnHidden?.(column as unknown as Column<RowData>, !event.target.checked);
              }}
            />
          }
          label={<>{column.render('Header')}</>}
        />
      </ListItem>
    );
  });

  return React.useMemo<React.ReactElement<ColumnChooserProps>>(
    () => (
      <React.Fragment>
        <Tooltip title={localization.toolbar.showColumnsTitle}>
          <IconButton
            aria-describedby={id}
            onClick={handleClick}
            aria-label={localization.toolbar.showColumnsAriaLabel}
            size="large"
          >
            <ColumnChooserIcon />
          </IconButton>
        </Tooltip>
        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <Paper elevation={0}>
            <List>
              <ListItem>
                <ListItemText primary={localization?.toolbar?.addRemoveColumns} />
              </ListItem>
              {columns}
            </List>
          </Paper>
        </Popover>
      </React.Fragment>
    ),
    [id, handleClick, open, anchorEl, handleClose, columns, localization, ColumnChooserIcon],
  );
}
