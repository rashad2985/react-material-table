export type Editable<RowData extends object = {}> = {
  /**
   * Will be called on confirm of the row add operation.
   * @param newData
   */
  onRowAdd?: (newData: Partial<RowData>) => Promise<any>;
  /**
   * Disabled state of the add button.
   * @default true
   */
  canAdd?: boolean;
  /**
   * Hidden state of the add button.
   * @default false
   */
  isAddHidden?: boolean;
  /**
   * Will be called on cancel of the row add operation.
   * @param rowData
   */
  onRowAddCancelled?: (rowData: Partial<RowData>) => void;
  /**
   * Will be called on confirm of the row edit operation.
   * @param newData
   * @param oldData
   */
  onRowUpdate?: (newData: Partial<RowData>, oldData?: Partial<RowData>) => Promise<any>;
  /**
   * Disabled state of the row edit button.
   * @param rowData
   */
  isEditable?: (rowData: Partial<RowData>) => boolean;
  /**
   * Overrides tooltip of the edit button.
   * @param rowData
   */
  editTooltip?: (rowData: Partial<RowData>) => string;
  /**
   * Will be called on cancel of the row edit operation.
   * @param rowData
   */
  onRowUpdateCancelled?: (rowData: Partial<RowData>) => void;
  /**
   * Hidden state of the row edit button.
   * @param rowData
   */
  isEditHidden?: (rowData: Partial<RowData>) => boolean;
  /**
   * Will be called on confirm of the row delete operation.
   * @param oldData
   */
  onRowDelete?: (oldData: Partial<RowData>) => Promise<any>;
  /**
   * Disabled state of the row delete button.
   * @param rowData
   */
  isDeletable?: (rowData: Partial<RowData>) => boolean;
  /**
   * Overrides tooltip of the delete button.
   * @param rowData
   */
  deleteTooltip?: (rowData: Partial<RowData>) => string;
  /**
   * Hidden state of the row edit button.
   * @param rowData
   */
  isDeleteHidden?: (rowData: Partial<RowData>) => boolean;

  /**
   * TODO: Object to allow update a cell data on click
   */
  /*onCellEditApproved?: (
    newValue: any,
    oldValue: any,
    rowData: Partial<RowData>,
    columnDef: Column<RowData>
  ) => Promise<void>;*/

  // onBulkUpdate?: (changes: { oldData: Row; newData: Row }[]) => Promise<any>;
};
