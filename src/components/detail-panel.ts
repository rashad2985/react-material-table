import * as React from 'react';
import { SvgIconComponent } from '@mui/icons-material';
import { IconProps } from '@mui/material';

export type DetailPanel<DataType extends object = {}> = {
  /**
   * Flag for button disabled or enabled
   * @default false
   */
  disabled?: boolean;

  /**
   * Icon of button from material icons or custom component
   */
  icon?: string | (() => React.ReactElement<any>) | SvgIconComponent;

  /**
   * Icon of button when detailPanel is open
   */
  openIcon?: string | (() => React.ReactElement<any>) | SvgIconComponent;

  /**
   * Tooltip for button
   */
  tooltip?: string;

  /**
   * Props for icon as IconProps from https://mui.com/api/icon/
   */
  iconProps?: IconProps;

  /**
   * Render result of detail panel according rowData
   * @param row
   */
  render: (row: DataType) => React.ReactNode;
};
