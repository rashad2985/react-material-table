import { InternalOptions } from '../internal';

export type Options<T extends object = {}> = {
  [Key in keyof InternalOptions<T>]?: InternalOptions<T>[Key];
};
