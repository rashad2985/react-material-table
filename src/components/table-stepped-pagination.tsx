import * as React from 'react';
import { TablePaginationActionsProps } from '@mui/material/TablePagination/TablePaginationActions';
import { useOptions } from '../internal';
import { Pagination, PaginationProps, useMediaQuery, useTheme } from '@mui/material';
import { onQueryChangeEvent } from '../events';
import { Query } from '../data';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(({ spacing }) => ({
  steppedPagination: {
    flexWrap: 'nowrap',
    justifyContent: 'center',
  },
  padding1: {
    padding: spacing(1),
  },
  flex: {
    display: 'flex',
  },
}));

export function TableSteppedPagination({ count, page, rowsPerPage, onPageChange }: TablePaginationActionsProps) {
  const styles = useStyles();
  const self = React.useRef<HTMLDivElement | null>(null);
  const { padding, responsiveBreakPoint, showFirstLastPageButtons } = useOptions();
  const theme = useTheme();
  const isResponsive = useMediaQuery(theme.breakpoints.down(responsiveBreakPoint));
  const onChange = React.useCallback(
    (event: React.ChangeEvent<unknown>, value: number) => {
      onPageChange(null, value - 1);
      self.current?.dispatchEvent?.(
        new CustomEvent<Partial<Query<any>>>(onQueryChangeEvent, { detail: { page: value } }),
      );
    },
    [onPageChange],
  );

  const pageCount = React.useMemo(() => Math.ceil(count / rowsPerPage), [count, rowsPerPage]);
  const classes = React.useMemo<PaginationProps['classes']>(
    () => ({
      root: `${styles.padding1} ${isResponsive ? styles.flex : ''}`,
      ul: styles.steppedPagination,
    }),
    [isResponsive, styles.padding1, styles.flex, styles.steppedPagination],
  );

  return React.useMemo(
    () => (
      <Pagination
        ref={self}
        shape="rounded"
        size={padding === 'dense' ? 'small' : undefined}
        showFirstButton={showFirstLastPageButtons}
        showLastButton={showFirstLastPageButtons}
        count={pageCount}
        page={page + 1}
        onChange={onChange}
        boundaryCount={1}
        siblingCount={1}
        classes={classes}
      />
    ),
    [padding, showFirstLastPageButtons, pageCount, page, onChange, classes],
  );
}
