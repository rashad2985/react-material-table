import * as React from 'react';
import { IconButton, Tooltip, useMediaQuery, useTheme } from '@mui/material';
import { useStyles } from '../styles';
import { TablePaginationActionsProps } from '@mui/material/TablePagination/TablePaginationActions';
import { useIcons, useLocalization, useOptions } from '../internal';

export function TablePaginationActions({ count, page, rowsPerPage, onPageChange }: TablePaginationActionsProps) {
  const styles = useStyles();
  const localization = useLocalization();
  const { responsiveBreakPoint, showFirstLastPageButtons } = useOptions();
  const theme = useTheme();
  const isResponsive = useMediaQuery(theme.breakpoints.down(responsiveBreakPoint));
  const {
    FirstPage: FirstPageIcon,
    LastPage: LastPageIcon,
    NextPage: NextPageIcon,
    PreviousPage: PreviousPageIcon,
  } = useIcons();

  const handleFirstPageButtonClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => onPageChange(event, 0),
    [onPageChange],
  );

  const handlePreviousPageButtonClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => onPageChange(event, page - 1),
    [onPageChange, page],
  );

  const handleNextPageButtonClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => onPageChange(event, page + 1),
    [onPageChange, page],
  );

  const handleLastPageButtonClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) =>
      onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1)),
    [onPageChange, count, rowsPerPage],
  );

  return React.useMemo(
    () => (
      <div className={isResponsive ? `${styles.flex} ${styles.tablePaginationActions}` : styles.tablePaginationActions}>
        {showFirstLastPageButtons && (
          <Tooltip title={localization.pagination.firstTooltip}>
            <span>
              <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label={localization.pagination.firstAriaLabel}
              >
                {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
              </IconButton>
            </span>
          </Tooltip>
        )}
        <Tooltip title={localization.pagination.previousTooltip}>
          <span>
            <IconButton
              onClick={handlePreviousPageButtonClick}
              disabled={page === 0}
              aria-label={localization.pagination.previousAriaLabel}
            >
              {theme.direction === 'rtl' ? <NextPageIcon /> : <PreviousPageIcon />}
            </IconButton>
          </span>
        </Tooltip>
        <Tooltip title={localization.pagination.nextTooltip}>
          <span>
            <IconButton
              onClick={handleNextPageButtonClick}
              disabled={page >= Math.ceil(count / rowsPerPage) - 1}
              aria-label={localization.pagination.nextAriaLabel}
            >
              {theme.direction === 'rtl' ? <PreviousPageIcon /> : <NextPageIcon />}
            </IconButton>
          </span>
        </Tooltip>
        {showFirstLastPageButtons && (
          <Tooltip title={localization.pagination.lastTooltip}>
            <span>
              <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label={localization.pagination.lastAriaLabel}
              >
                {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
              </IconButton>
            </span>
          </Tooltip>
        )}
      </div>
    ),
    [
      theme.direction,
      localization,
      styles,
      handleFirstPageButtonClick,
      handlePreviousPageButtonClick,
      handleNextPageButtonClick,
      handleLastPageButtonClick,
      count,
      page,
      rowsPerPage,
      showFirstLastPageButtons,
      FirstPageIcon,
      LastPageIcon,
      NextPageIcon,
      PreviousPageIcon,
      isResponsive,
    ],
  );
}
