import { Box, Checkbox, Collapse, Grid, IconButton, Typography, TypographyProps, useTheme } from '@mui/material';
import TableCell, { TableCellProps } from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import * as React from 'react';
import { useStyles } from '../styles';
import { Column } from '../column';
import {
  Action as ActionButton,
  EditableContext,
  EditRowButton,
  isRowAction,
  noop,
  useEvents,
  useLocalization,
  useOptions,
} from '../internal';
import { DetailPanel } from './detail-panel';
import { DetailPanelButton } from '../internal/components/details-panel';
import { Editable } from '../editable';
import { DeleteRowButton } from '../internal/components/editable/delete-row-button';
import { RowProps } from './row';
import { ExpandLess, ExpandMore } from '@mui/icons-material';
import { Property } from 'csstype';
import { ChangeEvent } from 'react';

type CellRowProps = Pick<TableCellProps, 'onClick' | 'style' | 'align' | 'className'> & {
  header: React.ReactNode;
  value: React.ReactNode;
  valueClassName?: TypographyProps['className'];
};

function CellRow({ header, value, onClick, style, align, className, valueClassName }: CellRowProps) {
  const theme = useTheme();
  const { responsiveRowHeaderValueProportions } = useOptions();
  const [headerProportion, valueProportion] = responsiveRowHeaderValueProportions;
  const headerStyle = React.useMemo<React.CSSProperties>(
    () => ({
      ...(theme.typography.overline as React.CSSProperties),
      fontSizeAdjust: theme?.typography?.overline?.fontSizeAdjust as Property.FontSizeAdjust,
      lineHeight: theme.typography.body2.lineHeight,
      fontSize: theme.typography.body2.fontSize,
    }),
    [theme],
  );
  return React.useMemo(
    () => (
      <Grid
        item
        xs={12}
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="center"
        onClick={onClick}
        style={style}
        className={className}
        spacing={1}
      >
        <Grid item xs={headerProportion}>
          <Box
            display="block !important"
            textOverflow="ellipsis"
            whiteSpace="nowrap"
            overflow="hidden"
            style={headerStyle}
          >
            {header}
          </Box>
        </Grid>
        <Grid item xs={valueProportion}>
          <Typography component="div" variant="body2" noWrap align={align} className={valueClassName}>
            {value}
          </Typography>
        </Grid>
      </Grid>
    ),
    [onClick, style, className, valueClassName, headerProportion, headerStyle, header, valueProportion, align, value],
  );
}

export function ResponsiveRow<RowData extends object = {}>(props: RowProps<RowData>) {
  const {
    id,
    actions = [],
    detailPanel,
    openedPanels,
    togglePanel,
    selectedFlatRows = [],
    allColumns,
    getToggleRowSelectedProps,
    original: rowData,
    getRowProps,
    isSelected,
    ...rowProps
  } = props;
  const { onChange: toggleRowSelection = noop, ...otherToggleRowSelectedProps } = getToggleRowSelectedProps?.() ?? {};
  const { checked } = otherToggleRowSelectedProps ?? {};
  const styles = useStyles();
  const localization = useLocalization();
  const { onRowClick, onSelectionChange } = useEvents<RowData>();
  const { selection, selectionProps, actionsColumnIndex, responsiveMaxInitialCells, padding, responsiveRowStyle } =
    useOptions<RowData>();
  const { onRowUpdate, onRowDelete, isEditHidden, isDeleteHidden } =
    React.useContext<Editable<RowData>>(EditableContext);

  const [expanded, setExpanded] = React.useState<boolean>(false);
  const expand = React.useCallback(() => setExpanded(true), []);
  const showLess = React.useCallback(() => setExpanded(false), []);

  const showActionsColumn = React.useMemo<boolean>(
    () =>
      actions.filter((action) => isRowAction(typeof action === 'function' ? action({} as RowData) : action)).length >
        0 ||
      !!onRowUpdate ||
      !!onRowDelete,
    [actions, onRowUpdate, onRowDelete],
  );

  const allCells = React.useMemo(
    () =>
      (rowProps?.cells ?? []).filter(
        (cell) => ((cell?.column || {}) as unknown as Column<RowData>)?.responsive !== false,
      ),
    [rowProps.cells],
  );

  const renderableCells = React.useMemo(
    () => (!!responsiveMaxInitialCells ? allCells.slice(0, responsiveMaxInitialCells) : allCells),
    [allCells, responsiveMaxInitialCells],
  );

  const expandableCells = React.useMemo(
    () => (!!responsiveMaxInitialCells ? allCells.slice(responsiveMaxInitialCells) : []),
    [allCells, responsiveMaxInitialCells],
  );

  const rowStyle = React.useMemo(
    () =>
      !!responsiveRowStyle
        ? typeof responsiveRowStyle === 'function'
          ? responsiveRowStyle(rowData)
          : responsiveRowStyle
        : undefined,
    [responsiveRowStyle, rowData],
  );

  const handleSelectionChange = React.useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      toggleRowSelection?.(event);
      onSelectionChange?.(
        !!checked
          ? selectedFlatRows.filter((row) => row.id !== id).map((row) => row.original)
          : [...selectedFlatRows.map((row) => row.original), rowData],
        rowData,
      );
    },
    [id, toggleRowSelection, onSelectionChange, checked, selectedFlatRows, rowData],
  );

  return (
    <TableRow
      {...(getRowProps?.() ?? {})}
      key={`responsive-row-${id}`}
      selected={isSelected}
      hover={!!onRowClick}
      style={rowStyle}
    >
      <TableCell variant="body" size={padding === 'dense' ? 'small' : undefined}>
        {selection && (
          <CellRow
            key={`responsive-row-selection-cell-${id}`}
            header={localization.row.selectTooltip}
            value={
              <Checkbox
                {...((typeof selectionProps === 'function' ? selectionProps(rowData) : selectionProps) ?? {})}
                {...(otherToggleRowSelectedProps || {})}
                onChange={handleSelectionChange}
                className={styles.responsiveActionsContainer}
              />
            }
          />
        )}
        {renderableCells.map((cell) => (
          <CellRow
            {...cell.getCellProps()}
            header={cell.column.render('Header')}
            value={cell.render('Cell')}
            align="left"
            style={{ ...(((cell?.column || {}) as unknown as Column<RowData>)?.cellStyle ?? {}) }}
            onClick={(event) =>
              !((cell?.column || {}) as unknown as Column<RowData>)?.disableClick &&
              onRowClick?.(event, rowData, () => togglePanel(id, 0))
            }
            className={`${
              !((cell?.column || {}) as unknown as Column<RowData>)?.disableClick && !!onRowClick
                ? styles.clickable
                : ''
            }`}
          />
        ))}
        {!!expandableCells.length && (
          <Collapse in={expanded} timeout="auto" unmountOnExit>
            {expandableCells.map((cell) => (
              <CellRow
                {...cell.getCellProps()}
                header={cell.column.render('Header')}
                value={cell.render('Cell')}
                align="left"
                style={{ ...(((cell?.column || {}) as unknown as Column<RowData>)?.cellStyle ?? {}) }}
                onClick={(event) =>
                  !((cell?.column || {}) as unknown as Column<RowData>)?.disableClick &&
                  onRowClick?.(event, rowData, () => togglePanel(id, 0))
                }
                className={`${
                  !((cell?.column || {}) as unknown as Column<RowData>)?.disableClick && !!onRowClick
                    ? styles.clickable
                    : ''
                }`}
              />
            ))}
          </Collapse>
        )}
        {!!showActionsColumn && (
          <CellRow
            header={localization.header.actions}
            value={
              <React.Fragment>
                {actions
                  .filter((action) => {
                    const {
                      position = 'row',
                      hidden = false,
                      isFreeAction = false,
                    } = typeof action === 'function' ? action(rowData) : action;
                    return position === 'row' && !hidden && !isFreeAction;
                  })
                  .map((action, actionIndex) => (
                    <ActionButton<RowData>
                      key={`responsive-row-action-${actionIndex}`}
                      row={rowData}
                      selectedRows={selectedFlatRows.map((selectedRow) => selectedRow.original)}
                      {...(typeof action === 'function' ? action(rowData) : action)}
                    />
                  ))}
                {!!onRowUpdate && !isEditHidden?.(rowData) && (
                  <EditRowButton<RowData>
                    key={`responsive-edit-${id}`}
                    columns={allColumns as unknown as Array<Column<RowData>>}
                    rowData={rowData}
                  />
                )}
                {!!onRowDelete && !isDeleteHidden?.(rowData) && (
                  <DeleteRowButton<RowData> key={`responsive-delete-${id}`} rowData={rowData} />
                )}
              </React.Fragment>
            }
            key={`actions-cell-${actionsColumnIndex}`}
            align="left"
            valueClassName={styles.responsiveActionsContainer}
          />
        )}
        {!!detailPanel && (
          <CellRow
            header={localization.row.detailPanelTooltip}
            value={(
              (Array.isArray(detailPanel) &&
                detailPanel.map<DetailPanel<RowData>>((dp) => (typeof dp === 'function' ? dp(rowData) : dp))) ||
              (typeof detailPanel === 'function' && [detailPanel(rowData)]) ||
              (!Array.isArray(detailPanel) && typeof detailPanel !== 'function' && [detailPanel]) ||
              []
            ).map((detailsPanelProps, detailsPanelIndex) => (
              <DetailPanelButton<RowData>
                {...detailsPanelProps}
                key={detailsPanelIndex}
                open={!!openedPanels?.[detailsPanelIndex]}
                onClick={() => togglePanel(id, detailsPanelIndex)}
              />
            ))}
            key={`responsive-detail-panel-buttons-${id}`}
            align="left"
            valueClassName={styles.responsiveActionsContainer}
          />
        )}
        {!!expandableCells.length && (
          <Grid item xs={12} container direction="row" justifyContent="center" alignItems="center">
            {expanded ? (
              <IconButton onClick={showLess} size={padding === 'dense' ? 'small' : undefined}>
                <ExpandLess />
              </IconButton>
            ) : (
              <IconButton onClick={expand} size={padding === 'dense' ? 'small' : undefined}>
                <ExpandMore />
              </IconButton>
            )}
          </Grid>
        )}
      </TableCell>
    </TableRow>
  );
}
