import { useTheme } from '@mui/material';
import TableCell, { TableCellProps } from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import * as React from 'react';
import { useOptions } from '../internal';

// eslint-disable-next-line
export type EmptyTableRowProps<DataType extends object = {}> = Required<Pick<TableCellProps, 'colSpan'>>;

export function EmptyTableRow<DataType extends object = {}>({ colSpan }: EmptyTableRowProps<DataType>) {
  const { padding } = useOptions<DataType>();
  const theme = useTheme();
  const backgroundColor = theme.palette.background.paper;
  const cellStyle = React.useMemo<React.CSSProperties>(
    () => ({ borderBottomColor: backgroundColor }),
    [backgroundColor],
  );
  return React.useMemo(
    () => (
      <TableRow>
        <TableCell variant="body" colSpan={colSpan} style={cellStyle} size={padding === 'dense' ? 'small' : undefined}>
          {String.fromCharCode(160)}
        </TableCell>
      </TableRow>
    ),
    [colSpan, cellStyle, padding],
  );
}
