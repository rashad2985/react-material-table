import MuiTableContainer, { TableContainerProps as MuiTableContainerProps } from '@mui/material/TableContainer';
import * as React from 'react';
import {
  defined,
  FilterChooser,
  FiltersContext,
  FiltersContextProps,
  useColumnTransformer,
  useComponents,
  useLocalization,
  useOptions,
  useTableState,
} from '../internal';
import * as ReactTable from 'react-table';
import { Table, TableInstanceProps, TableProps, TableState } from './table';
import { alpha, Backdrop, Box, Grid, Stack, useTheme } from '@mui/material';
import { Column } from '../column';
import { Query, QueryResult } from '../data';
import { onQueryChangeEvent } from '../events';
import { TableProps as MuiTableProps } from '@mui/material/Table/Table';
import { TablePaginationProps as MuiTablePaginationProps } from '@mui/material/TablePagination/TablePagination';

export interface TableContainerProps<RowData extends object = {}>
  extends Pick<TableProps<RowData>, 'actions' | 'detailPanel'> {
  /**
   * Data to be rendered
   */
  data: Array<RowData> | ((query: Query<RowData>) => Promise<QueryResult<RowData>>);
  /**
   * Column definitions
   */
  columns: Array<Column<RowData>>;
  /**
   * Table Title (only render if toolbar option is true)
   */
  title?: string;
  /**
   * Style would be applied to Container of table
   */
  style?: React.CSSProperties;
  /**
   * Could be used to pass ref over withStyles
   */
  tableRef?: unknown;

  /**
   * Defines initially selected state of rows.
   * @param row
   */
  isRowInitiallySelected?: (row: RowData) => boolean;
}

export function TableContainer<RowData extends object = {}>(tableContainerProps: TableContainerProps<RowData>) {
  const { columns, data, actions, style, tableRef, detailPanel, isRowInitiallySelected } = tableContainerProps;
  const {
    toolbar: showToolbar,
    pageSize,
    thirdSortClick,
    initialPage,
    paging,
    pageSizeAll,
    pageSizeOptions,
    sorting,
    filtering,
    loadingType,
    paginationPosition,
    searchText,
    minBodyHeight,
    maxBodyHeight,
  } = useOptions<RowData>();
  const {
    pagination: { pageSizeAll: pageSizeAllLabel },
  } = useLocalization();
  const { Container, OverlayLoading, Toolbar, Pagination } = useComponents<RowData>();
  const { isLoading } = useTableState();
  const columnTransformer = useColumnTransformer<RowData>();
  /* Responsive view */
  const theme = useTheme();
  /* region Filters */
  const { filters } = React.useContext<FiltersContextProps<RowData>>(FiltersContext);
  const filtersOrder = React.useMemo<Map<string, number>>(
    () => new Map<string, number>(filters.map((filter, index) => [String(filter.field), index])),
    [filters],
  );
  /* endregion Filters */
  /* region Sorting */
  const sortBy = React.useMemo<Array<ReactTable.SortingRule<RowData>>>(
    () =>
      [
        [
          ...columns
            .filter((column) => !!column.defaultSort)
            // Sorting is reverted!
            .map((column) => ({ id: String(column.field), desc: column.defaultSort === 'asc' })),
        ].shift(),
      ].filter(defined),
    [columns],
  );
  /* endregion Sorting */
  /* region Column transformation to provide react-table friendly column definitions */
  const reactTableCompatibleColumns = React.useMemo<Array<ReactTable.Column<RowData>>>(
    () =>
      columns
        .filter((column) => !column.hidden || !!column.hiddenByColumnsButton)
        .map((column) => columnTransformer.transform(column, filters)),
    [columns, filters, columnTransformer],
  );
  const hiddenColumns = React.useMemo<Array<keyof RowData | string>>(
    () =>
      columns
        .filter((column) => !!column.hidden && !!column.hiddenByColumnsButton)
        .map((column) => column.field)
        .filter(defined),
    [columns],
  );
  /* endregion Column transformation to provide react-table friendly column definitions */
  /* region Data */
  const self = React.useRef<HTMLDivElement | null>(null);
  const [queryProps, setQueryProps] = React.useState<Query<RowData>>({
    page: initialPage,
    pageSize,
    totalCount: 0,
  });
  const updateQuery = React.useCallback<(event: Event) => void>((event) => {
    event.stopPropagation?.();
    setQueryProps((prevQueryProps) => ({
      ...prevQueryProps,
      ...((event as CustomEvent)?.detail ?? {}),
    }));
  }, []);
  React.useEffect(() => {
    const current = self.current;
    current?.addEventListener(onQueryChangeEvent, updateQuery, true);
    return () => {
      current?.removeEventListener(onQueryChangeEvent, updateQuery, true);
    };
  }, [updateQuery]);
  const [queryResult, setQueryResult] = React.useState<QueryResult<RowData> | undefined>(undefined);
  const reloadData = React.useCallback(() => {
    if (!!data && typeof data === 'function') {
      data({ ...queryProps, totalCount: queryResult?.totalCount ?? 0 }).then((newQueryResult) => {
        setQueryResult(() => {
          if (queryProps.totalCount !== newQueryResult.totalCount) {
            self.current?.dispatchEvent?.(
              new CustomEvent<Partial<Query<RowData>>>(onQueryChangeEvent, {
                bubbles: true,
                detail: { totalCount: newQueryResult.totalCount },
              }),
            );
          }
          return { ...newQueryResult };
        });
      });
    }
  }, [data, queryProps, queryResult?.totalCount]);
  React.useEffect(reloadData, [reloadData]);
  const initiallySelectedRowIds = React.useMemo(
    () =>
      !isRowInitiallySelected
        ? {}
        : (typeof data === 'function' ? queryResult?.data ?? [] : data ?? []).reduce(
            (accumulator, row, index) => ({ ...accumulator, [String(index)]: isRowInitiallySelected(row) }),
            {} as Record<string, boolean>,
          ),
    [isRowInitiallySelected, data, queryResult?.data],
  );
  /* endregion Data */
  /* region React Table */
  const options = React.useMemo<
    ReactTable.UseTableOptions<RowData> &
      ReactTable.UseSortByOptions<RowData> &
      ReactTable.UsePaginationOptions<RowData> &
      ReactTable.UseGlobalFiltersOptions<RowData>
  >(
    () => ({
      columns: reactTableCompatibleColumns,
      data: typeof data === 'function' ? queryResult?.data ?? [] : data,
      initialState: {
        globalFilter: searchText,
        pageSize:
          pageSize ||
          (Array.isArray(data) && data.length) ||
          (typeof pageSizeOptions?.[0] === 'number' ? pageSizeOptions?.[0] : pageSizeOptions?.[0]?.value),
        pageIndex: typeof data === 'function' ? queryResult?.page : initialPage,
        sortBy,
        hiddenColumns,
        selectedRowIds: initiallySelectedRowIds,
      } as TableState<RowData>,
      autoResetGlobalFilter: false,
      disableSortRemove: !thirdSortClick,
      manualPagination: typeof data === 'function' ? true : undefined,
      pageCount: typeof data === 'function' ? Math.ceil(queryResult?.totalCount ?? 0 / pageSize) : undefined,
      disableSortBy: !sorting,
    }),
    [
      reactTableCompatibleColumns,
      data,
      queryResult,
      searchText,
      pageSize,
      initialPage,
      sortBy,
      hiddenColumns,
      sorting,
      initiallySelectedRowIds,
      thirdSortClick,
      pageSizeOptions,
    ],
  );

  const tableInstance = ReactTable.useTable<RowData>(
    options,
    ReactTable.useColumnOrder,
    ReactTable.useGlobalFilter,
    ReactTable.useFilters,
    ReactTable.useSortBy,
    ReactTable.usePagination,
    ReactTable.useRowSelect,
  ) as TableInstanceProps<RowData> & ReactTable.UseGlobalFiltersInstanceProps<RowData>;
  /* endregion React Table */
  /* region Table Height */
  const maxTableHeight = React.useMemo<Required<MuiTableProps>['style']>(
    () => (maxBodyHeight ? { maxHeight: maxBodyHeight } : {}),
    [maxBodyHeight],
  );
  const minTableHeight = React.useMemo<Required<MuiTableProps>['style']>(
    () => (minBodyHeight ? { minHeight: minBodyHeight } : {}),
    [minBodyHeight],
  );
  /* endregion Table Height */
  /* region Pagination */
  const tableInstanceRowsLength = tableInstance?.rows?.length;
  const rowsPerPageOptions = React.useMemo<Required<MuiTablePaginationProps>['rowsPerPageOptions']>(
    () =>
      [
        ...(pageSizeOptions ?? []).map((option) =>
          typeof option === 'number' ? { value: option, label: String(option) } : option,
        ),
        !!pageSizeAll && tableInstanceRowsLength
          ? { value: tableInstanceRowsLength, label: pageSizeAllLabel }
          : undefined,
      ].filter(defined),
    [pageSizeOptions, pageSizeAll, pageSizeAllLabel, tableInstanceRowsLength],
  );
  const tableInstanceSetPageSize = tableInstance?.setPageSize;
  React.useEffect(() => {
    if (
      paging &&
      pageSizeAll &&
      tableInstanceRowsLength &&
      !rowsPerPageOptions.some((option) => (typeof option === 'number' ? option : option.value) === pageSize)
    ) {
      tableInstanceSetPageSize?.(tableInstanceRowsLength);
    }
  }, [paging, pageSizeAll, rowsPerPageOptions, tableInstanceRowsLength, tableInstanceSetPageSize, pageSize]);
  /* endregion Pagination */

  return (
    <Container ref={self}>
      <Stack direction="column" justifyContent="flex-start" alignItems="stretch">
        {paging && (paginationPosition === 'both' || paginationPosition === 'top') && (
          <Grid container direction="row" justifyContent="flex-end" alignItems="center">
            <Pagination
              count={tableInstanceRowsLength}
              page={tableInstance?.state?.pageIndex}
              rowsPerPageOptions={rowsPerPageOptions}
              rowsPerPage={tableInstance?.state?.pageSize}
              gotoPage={tableInstance?.gotoPage}
              setPageSize={tableInstanceSetPageSize}
            />
          </Grid>
        )}
        {showToolbar && !!Toolbar && <Toolbar {...{ tableContainerProps, tableInstanceProps: tableInstance }} />}
        {!!filtering && (
          <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start" flexWrap="nowrap">
            <Grid item>
              <FilterChooser<RowData> initialColumns={columns} />
            </Grid>
            <Grid item xs sx={{ pt: 1 }}>
              <Grid container direction="row" justifyContent="flex-start" alignItems="center" spacing={1}>
                {tableInstance?.allColumns
                  ?.filter((column) => !!column.canFilter)
                  ?.sort(
                    (columnA, columnB) =>
                      (filtersOrder.get(columnA.id) ?? tableInstance.columns.length) -
                      (filtersOrder.get(columnB.id) ?? tableInstance.columns.length),
                  )
                  ?.map((column, index) => (
                    <Grid item>{column.render('Filter', { key: index })}</Grid>
                  ))}
              </Grid>
            </Grid>
          </Grid>
        )}
        <MuiTableContainer
          style={{ ...style, ...maxTableHeight, ...minTableHeight }}
          ref={tableRef as MuiTableContainerProps['ref']}
        >
          <Table<RowData> {...tableInstance} actions={actions} detailPanel={detailPanel} />
        </MuiTableContainer>
        {paging && (paginationPosition === 'both' || paginationPosition === 'bottom') && (
          <Grid container direction="row" justifyContent="flex-end" alignItems="center">
            <Pagination
              count={tableInstanceRowsLength}
              page={tableInstance?.state?.pageIndex}
              rowsPerPageOptions={rowsPerPageOptions}
              rowsPerPage={tableInstance?.state?.pageSize}
              gotoPage={tableInstance?.gotoPage}
              setPageSize={tableInstanceSetPageSize}
            />
          </Grid>
        )}
      </Stack>
      {isLoading && loadingType === 'overlay' && (
        <Backdrop
          open={isLoading && loadingType === 'overlay'}
          style={{ zIndex: 11, backgroundColor: alpha(theme.palette.background.default, 0.7) }}
        >
          <Box display="flex" flexDirection="row" justifyContent="center" alignItems="center">
            <OverlayLoading />
          </Box>
        </Backdrop>
      )}
    </Container>
  );
}
