import {
  Checkbox,
  LinearProgress,
  Table as MuiTable,
  TableCell as MuiTableCell,
  TableHead,
  TableRow as TableRow,
  TableSortLabel,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material';
import * as React from 'react';
import { ChangeEvent } from 'react';
import * as ReactTable from 'react-table';
import { useStyles } from '../styles';
import { Column } from '../column';
import { DragDropContext, Draggable, Droppable, DropResult } from 'react-beautiful-dnd';
import {
  EditableContext,
  insert,
  isRowAction,
  noop,
  resolveCellAlign,
  useComponents,
  useDetailPanels,
  useEvents,
  useIcons,
  useLocalization,
  useOptions,
  useTableState,
} from '../internal';
import { Action } from '../action';
import { DetailPanel } from './detail-panel';
import { Editable } from '../editable';
import { RowType } from './row';

const linearProgressStyle: React.CSSProperties = { display: 'block' };

type ColumnProps<RowData extends object = {}> = ReactTable.ColumnInstance<RowData> &
  ReactTable.UseFiltersColumnProps<RowData>;

type SortableColumn<RowData extends object = {}> = ReactTable.ColumnInstance<RowData> &
  ReactTable.UseSortByColumnProps<RowData>;

export type TableState<RowData extends object = {}> = ReactTable.TableState<RowData> &
  ReactTable.UsePaginationState<RowData> &
  ReactTable.UseSortByState<RowData> &
  ReactTable.UseGlobalFiltersState<RowData>;

export type TableInstanceProps<RowData extends object = {}> = Omit<
  ReactTable.TableInstance<RowData>,
  'allColumns' | 'rows'
> &
  ReactTable.UseColumnOrderInstanceProps<RowData> &
  ReactTable.UseFiltersInstanceProps<RowData> &
  ReactTable.UseSortByInstanceProps<RowData> &
  Omit<ReactTable.UsePaginationInstanceProps<RowData>, 'page'> &
  ReactTable.UseRowSelectInstanceProps<RowData> & {
    rows: Array<RowType<RowData>>;
    page: Array<RowType<RowData>>;
    allColumns: Array<ColumnProps<RowData>>;
    state: TableState<RowData>;
    /**
     * Action list. An icon button will be rendered for each action.
     */
    actions?: Array<Action<RowData> | ((rowData: RowData) => Action<RowData>)>;
  };

export interface TableProps<RowData extends object = {}> extends TableInstanceProps<RowData> {
  /**
   * Component(s) to be rendered on detail panel
   */
  detailPanel?:
    | (DetailPanel<RowData> | ((rowData: RowData) => DetailPanel<RowData>))
    | Array<DetailPanel<RowData> | ((rowData: RowData) => DetailPanel<RowData>)>;
}

export function Table<RowData extends object = {}>(props: TableProps<RowData>) {
  const {
    rows,
    page,
    state: { pageSize: rowsPerPage },
    prepareRow,
    visibleColumns = [],
    getTableProps,
    getTableBodyProps,
    setColumnOrder,
    getToggleAllRowsSelectedProps,
    selectedFlatRows = [],
    actions = [],
    detailPanel,
    allColumns,
  } = props;
  const styles = useStyles();
  const localization = useLocalization();
  const { onSelectionChange, onColumnDragged, onOrderChange } = useEvents<RowData>();
  const { isLoading } = useTableState();
  const { Body: TableBody, Row, Cell: TableCell, ResponsiveRow, EmptyRow, LoadingRow } = useComponents<RowData>();
  const {
    selection,
    paging,
    pageSizeOptions,
    emptyRowsWhenPaging,
    loadingType,
    detailPanelColumnAlignment,
    header,
    headerStyle,
    actionsColumnIndex,
    responsive,
    responsiveBreakPoint,
    fixedColumns,
    padding,
    showEmptyDataSourceMessage,
    showSelectAllCheckbox,
    draggable,
    maxBodyHeight,
  } = useOptions<RowData>();
  const { left: fixedLeft, right: fixedRight } = fixedColumns || {};
  /* Responsive view */
  const theme = useTheme();
  const isResponsive = useMediaQuery(theme.breakpoints.down(responsiveBreakPoint)) && responsive;
  /* DnD */
  const onReorderEnd = React.useCallback(
    (result: DropResult) => {
      if (!result.destination) {
        return;
      }
      const oldIndex = result.source.index;
      const newIndex = result.destination.index;

      const newOrder = [...visibleColumns.map((column) => column.id)];
      const [moved] = newOrder.splice(oldIndex, 1);
      newOrder.splice(newIndex, 0, moved);
      setColumnOrder(newOrder);
      onColumnDragged?.(oldIndex, newIndex);
    },
    [visibleColumns, onColumnDragged, setColumnOrder],
  );

  const { onRowUpdate, onRowDelete } = React.useContext<Editable<RowData>>(EditableContext);
  const showActionsColumn = React.useMemo<boolean>(
    () =>
      actions.filter((action) => isRowAction(typeof action === 'function' ? action({} as RowData) : action)).length >
        0 ||
      !!onRowUpdate ||
      !!onRowDelete,
    [actions, onRowUpdate, onRowDelete],
  );

  const { onChange: toggleAllRowsSelection = noop, ...otherToggleAllRowsSelectedProps } = selection
    ? getToggleAllRowsSelectedProps()
    : {};

  const colSpan = React.useMemo<number>(
    () => visibleColumns.length + (selection ? 1 : 0) + (showActionsColumn ? 1 : 0) + (!!detailPanel ? 1 : 0),
    [visibleColumns.length, selection, showActionsColumn, detailPanel],
  );

  const { SortArrow: SortArrowIcon } = useIcons();

  const { openedPanels, togglePanel } = useDetailPanels<RowData>(paging ? page : rows, detailPanel);

  const handleAllSelectionChange = React.useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      const { checked } = event?.target ?? {};
      toggleAllRowsSelection?.(event);
      onSelectionChange?.(!!checked ? [...(paging ? page ?? [] : rows ?? []).map((row) => row.original)] : []);
    },
    [toggleAllRowsSelection, onSelectionChange, paging, page, rows],
  );

  const finalHeaderStyle = React.useMemo<React.CSSProperties>(
    () => ({ ...headerStyle, ...(!!maxBodyHeight ? { left: 'unset' } : {}) }),
    [headerStyle, maxBodyHeight],
  );

  return (
    <MuiTable stickyHeader={!!maxBodyHeight} {...getTableProps()}>
      {header && !isResponsive && (
        <TableHead>
          <DragDropContext onDragEnd={onReorderEnd}>
            <Droppable droppableId="droppable" direction="horizontal">
              {(providedDroppable) => (
                <TableRow {...providedDroppable.droppableProps} ref={providedDroppable.innerRef}>
                  {!!detailPanel && detailPanelColumnAlignment === 'left' && (
                    <MuiTableCell key="details-panel-header" variant="head" align="center" style={finalHeaderStyle}>
                      &nbsp;
                    </MuiTableCell>
                  )}
                  {selection && (
                    <MuiTableCell
                      key="selection-header"
                      variant="head"
                      align="center"
                      padding="checkbox"
                      style={finalHeaderStyle}
                    >
                      {showSelectAllCheckbox && (
                        <Checkbox {...otherToggleAllRowsSelectedProps} onChange={handleAllSelectionChange} />
                      )}
                    </MuiTableCell>
                  )}
                  {insert(
                    (visibleColumns as Array<SortableColumn<RowData>>).map((column, index) => {
                      const { onClick: handleToggleSortBy, ...otherSortByToggleProps } =
                        (column.getSortByToggleProps?.() ?? {}) as any;
                      return (
                        <Draggable
                          key={column.id}
                          draggableId={String(column.id)}
                          index={index}
                          isDragDisabled={!draggable}
                        >
                          {(providedDraggable) => (
                            <MuiTableCell
                              ref={providedDraggable.innerRef}
                              {...providedDraggable.draggableProps}
                              {...providedDraggable.dragHandleProps}
                              {...(column.canSort
                                ? column.getHeaderProps(column.getSortByToggleProps())
                                : column.getHeaderProps())}
                              key={column.id}
                              variant="head"
                              className={`${styles.dnd} ${!!fixedLeft && index === 0 ? styles.stickyLeft : ''} ${
                                !!fixedRight && index === visibleColumns.length - 1 ? styles.stickyRight : ''
                              }`}
                              align={resolveCellAlign((column || {}) as unknown as Column<RowData>)}
                              style={{
                                ...finalHeaderStyle,
                                ...(((column || {}) as unknown as Column<RowData>)?.headerStyle || {}),
                                ...(!!((column || {}) as unknown as Column<RowData>)?.width
                                  ? { width: ((column || {}) as unknown as Column<RowData>)?.width }
                                  : {}),
                                ...providedDraggable.draggableProps.style,
                              }}
                            >
                              {column.canSort ? (
                                <TableSortLabel
                                  {...(otherSortByToggleProps ?? {})}
                                  onClick={(event) => {
                                    (handleToggleSortBy as ((event: unknown) => void) | undefined)?.(event);
                                    onOrderChange?.(
                                      index,
                                      typeof column.isSortedDesc === 'boolean'
                                        ? column.isSortedDesc
                                          ? undefined
                                          : 'asc'
                                        : 'desc',
                                    );
                                  }}
                                  active={!!column.isSorted}
                                  direction={!!column.isSortedDesc ? 'desc' : 'asc'}
                                  IconComponent={SortArrowIcon}
                                >
                                  <Typography component="div" variant="inherit" noWrap>
                                    {column.render('Header')}
                                  </Typography>
                                </TableSortLabel>
                              ) : (
                                <Typography component="div" variant="inherit" noWrap>
                                  {column.render('Header')}
                                </Typography>
                              )}
                            </MuiTableCell>
                          )}
                        </Draggable>
                      );
                    }),
                    actionsColumnIndex,
                    !!showActionsColumn ? (
                      <MuiTableCell key="actions-header" variant="head" align="center">
                        <Typography component="div" variant="inherit" noWrap>
                          {localization.header.actions}
                        </Typography>
                      </MuiTableCell>
                    ) : undefined,
                  )}
                  {!!detailPanel && detailPanelColumnAlignment === 'right' && (
                    <MuiTableCell key="details-panel-header" variant="head" align="center">
                      {String.fromCodePoint(160)}
                    </MuiTableCell>
                  )}
                  {providedDroppable.placeholder}
                </TableRow>
              )}
            </Droppable>
          </DragDropContext>
        </TableHead>
      )}
      <TableBody {...getTableBodyProps()}>
        {isLoading && loadingType === 'linear' && (
          <TableRow key="loading-indicator-row">
            <TableCell key="loading-indicator-cell" padding="none" colSpan={colSpan}>
              <LinearProgress style={linearProgressStyle} />
            </TableCell>
          </TableRow>
        )}
        {(paging ? page : rows).map((row, rowIndex) => {
          prepareRow(row);
          return (
            <React.Fragment key={rowIndex}>
              {isResponsive ? (
                <ResponsiveRow
                  {...row}
                  detailPanel={detailPanel}
                  actions={actions}
                  selectedFlatRows={selectedFlatRows}
                  allColumns={allColumns}
                  openedPanels={openedPanels?.[rowIndex] ?? []}
                  togglePanel={togglePanel}
                />
              ) : (
                <Row
                  {...row}
                  detailPanel={detailPanel}
                  actions={actions}
                  selectedFlatRows={selectedFlatRows}
                  allColumns={allColumns}
                  openedPanels={openedPanels?.[rowIndex] ?? []}
                  togglePanel={togglePanel}
                />
              )}
              {!!detailPanel && (openedPanels?.[rowIndex] || []).indexOf(true) >= 0 && (
                <TableRow {...row.getRowProps()} key={`detail-panel-row-${rowIndex}`}>
                  <TableCell key={`detail-panel-cell-${rowIndex}`} variant="body" padding="none" colSpan={colSpan}>
                    {((Array.isArray(detailPanel) &&
                      detailPanel.map<DetailPanel<RowData>>((dp) =>
                        typeof dp === 'function' ? dp(row.original) : dp,
                      )) ||
                      (typeof detailPanel === 'function' && [detailPanel(row.original)]) ||
                      (!Array.isArray(detailPanel) && typeof detailPanel !== 'function' && [detailPanel]) ||
                      [])?.[openedPanels[rowIndex].indexOf(true)]?.render(row.original)}
                  </TableCell>
                </TableRow>
              )}
            </React.Fragment>
          );
        })}
        {showEmptyDataSourceMessage && (paging ? page : rows).length < 1 && !isLoading && (
          <TableRow>
            <TableCell variant="body" colSpan={colSpan} align="center" size={padding === 'dense' ? 'small' : undefined}>
              {localization.body.emptyDataSourceMessage}
            </TableCell>
          </TableRow>
        )}
        {emptyRowsWhenPaging &&
          !isLoading &&
          !isResponsive &&
          (paging ? page : rows).length < rowsPerPage &&
          new Array(
            rowsPerPage -
              (paging ? page : rows).length -
              (showEmptyDataSourceMessage && (paging ? page : rows).length < 1 ? 1 : 0),
          )
            .fill(0)
            .map((_, index) => <EmptyRow key={`page-${page}-empty-row-${index}`} colSpan={colSpan} />)}
        {emptyRowsWhenPaging &&
          isLoading &&
          !isResponsive &&
          (paging ? page : rows).length <= 0 &&
          new Array(
            Math.min(
              ...(pageSizeOptions?.map((option) => (typeof option === 'number' ? option : option.value)) ?? [0]),
            ),
          )
            .fill(0)
            .map((_, index) => <LoadingRow key={`page-${page}-loading-row-${index}`} colSpan={colSpan} />)}
      </TableBody>
    </MuiTable>
  );
}
