import * as React from 'react';
import {
  Action as ActionButton,
  AddRowButton,
  ColumnChooser,
  EditableContext,
  ExportButton,
  ExportButtonProps,
  formatLocalization,
  useEvents,
  useIcons,
  useLocalization,
  useOptions,
} from '../internal';
import { useStyles } from '../styles';
import * as ReactTable from 'react-table';
import { TableInstanceProps } from './table';
import Grid from '@mui/material/Grid';
import { Box, TextField, Tooltip, Typography } from '@mui/material';
import { Column } from '../column';
import { Editable } from '../editable';
import { TableContainerProps } from './table-container';

export type ToolbarProps<RowData extends object = {}> = {
  tableContainerProps: TableContainerProps<RowData>;
  tableInstanceProps: TableInstanceProps<RowData> & ReactTable.UseGlobalFiltersInstanceProps<RowData>;
};

export function Toolbar<RowData extends object = {}>(toolbarProps: ToolbarProps<RowData>) {
  const { title = '', actions } = toolbarProps?.tableContainerProps || {};
  const {
    allColumns,
    setGlobalFilter,
    state: { globalFilter: currentSearchText = '' } = {},
    selectedFlatRows,
    visibleColumns,
    rows,
    page,
  } = toolbarProps?.tableInstanceProps ?? {};
  const styles = useStyles();
  const {
    debounceInterval,
    showTitle,
    showTextRowsSelected,
    search: showSearchInput,
    searchFieldAlignment,
    searchFieldStyle,
    searchAutoFocus,
    toolbarButtonAlignment,
    columnsButton,
    exportButton,
    exportAllData,
    inputVariant,
  } = useOptions<RowData>();
  const localization = useLocalization();
  const { onSearchChange } = useEvents();

  /* Search */
  const { Search: SearchIcon } = useIcons();
  const search = ReactTable.useAsyncDebounce((event: React.ChangeEvent<HTMLInputElement>) => {
    setGlobalFilter(event.target.value || undefined);
    onSearchChange?.(event.target.value ?? '');
  }, debounceInterval);

  /* Actions */
  const toolbarActions = React.useMemo(
    () =>
      (actions || [])
        .filter((action) => {
          const {
            position = 'row',
            hidden = false,
            isFreeAction = false,
          } = typeof action === 'function' ? action({} as RowData) : action;
          return (
            !hidden &&
            (isFreeAction ||
              (selectedFlatRows?.length > 0
                ? // Case: Rows selected
                  position === 'toolbarOnSelect'
                : // Case: No selected Rows
                  position === 'toolbar'))
          );
        })
        .map((action, index) => (
          <ActionButton<RowData>
            key={`toolbar-action-${index}`}
            row={{} as RowData}
            selectedRows={(selectedFlatRows || []).map((selectedRow) => selectedRow.original)}
            {...(typeof action === 'function' ? action({} as RowData) : action)}
          />
        )),
    [actions, selectedFlatRows],
  );

  /* Export */
  const getColumnsToExport = React.useCallback<() => ExportButtonProps<RowData>['columns']>(
    () =>
      !exportButton
        ? []
        : (visibleColumns ?? [])
            .filter((visibleColumn) => (visibleColumn as unknown as Column<RowData>).export ?? true)
            .map((visibleColumn) => ({
              title: String(visibleColumn.Header),
              field: String(visibleColumn.id) as keyof RowData,
              type: (visibleColumn as unknown as Column<RowData>).type || 'string',
              value: (visibleColumn as unknown as Column<RowData>).value,
              lookup: (visibleColumn as unknown as Column<RowData>).lookup,
              dateSetting: (visibleColumn as unknown as Column<RowData>).dateSetting,
              currencySetting: (visibleColumn as unknown as Column<RowData>).currencySetting,
              numberSettings: (visibleColumn as unknown as Column<RowData>).numberSettings,
              align: (visibleColumn as unknown as Column<RowData>).align,
              emptyValue: (visibleColumn as unknown as Column<RowData>).emptyValue,
            })),
    [exportButton, visibleColumns],
  );
  const getRowsToExport = React.useCallback<() => ExportButtonProps<RowData>['rows']>(
    () =>
      !exportButton ? [] : (!!exportAllData ? rows ?? [] : page ?? []).map((reactTableRow) => reactTableRow.original),
    [exportButton, rows, page, exportAllData],
  );

  const { onRowAdd } = React.useContext<Editable<RowData>>(EditableContext);

  const showToolbarActions = React.useMemo<boolean>(
    () => columnsButton || exportButton || toolbarActions.length > 0 || !!onRowAdd,
    [columnsButton, exportButton, toolbarActions.length, onRowAdd],
  );

  const resolvedTitle = React.useMemo(
    () =>
      showTextRowsSelected && !!selectedFlatRows?.length
        ? formatLocalization(localization.toolbar.nRowsSelected, { n: String(selectedFlatRows?.length) })
        : title,
    [selectedFlatRows, localization, title, showTextRowsSelected],
  );

  return React.useMemo(
    () => (
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="stretch"
        spacing={1}
        className={`${styles.padding2} ${
          showTextRowsSelected && !!selectedFlatRows?.length ? styles.toolbarOnSelection : ''
        }`}
      >
        {(showTitle || (showTextRowsSelected && !!selectedFlatRows?.length)) && (
          <Grid
            item={true}
            xs={12}
            md={true}
            container={true}
            direction="row"
            justifyContent="flex-start"
            alignItems="flex-start"
          >
            <Typography component="div" variant="h6" noWrap>
              {resolvedTitle}
            </Typography>
          </Grid>
        )}
        {(showSearchInput || showToolbarActions) && (
          <Grid
            item={true}
            xs={12}
            md={true}
            container={true}
            direction={toolbarButtonAlignment === 'left' && searchFieldAlignment === 'right' ? 'row-reverse' : 'row'}
            justifyContent={
              !showSearchInput &&
              ((searchFieldAlignment === 'left' && toolbarButtonAlignment === 'right') ||
                (searchFieldAlignment === 'right' && toolbarButtonAlignment === 'left'))
                ? 'space-between'
                : 'flex-end'
            }
            alignItems="center"
          >
            {showSearchInput && (
              <Box
                display="flex"
                flexDirection="row"
                justifyContent={searchFieldAlignment === 'right' ? 'flex-end' : 'flex-start'}
                alignItems="center"
                className={styles.search}
              >
                <TextField
                  key="search-input"
                  fullWidth={true}
                  margin="dense"
                  size="small"
                  label={localization.toolbar.searchLabel}
                  placeholder={localization.toolbar.searchPlaceholder}
                  type="search"
                  defaultValue={currentSearchText}
                  variant={inputVariant}
                  style={searchFieldStyle}
                  autoFocus={searchAutoFocus}
                  onChange={search}
                  InputProps={{
                    startAdornment: !!localization.toolbar.searchTooltip ? (
                      <Tooltip title={localization.toolbar.searchTooltip}>
                        <SearchIcon />
                      </Tooltip>
                    ) : (
                      <SearchIcon />
                    ),
                  }}
                />
              </Box>
            )}
            {showToolbarActions && (
              <Grid
                item
                xs={12}
                md={true}
                container
                direction="row"
                justifyContent={toolbarButtonAlignment === 'right' ? 'flex-end' : 'flex-start'}
                alignItems="center"
              >
                {columnsButton && <ColumnChooser allColumns={allColumns} />}
                {exportButton && (
                  <ExportButton tableTitle={title} columns={getColumnsToExport()} rows={getRowsToExport()} />
                )}
                {toolbarActions}
                {!!onRowAdd && <AddRowButton<RowData> columns={allColumns as unknown as Array<Column<RowData>>} />}
              </Grid>
            )}
          </Grid>
        )}
      </Grid>
    ),
    [
      SearchIcon,
      allColumns,
      columnsButton,
      currentSearchText,
      exportButton,
      getColumnsToExport,
      getRowsToExport,
      inputVariant,
      localization.toolbar.searchLabel,
      localization.toolbar.searchPlaceholder,
      localization.toolbar.searchTooltip,
      onRowAdd,
      resolvedTitle,
      search,
      searchAutoFocus,
      searchFieldAlignment,
      searchFieldStyle,
      selectedFlatRows?.length,
      showSearchInput,
      showTextRowsSelected,
      showTitle,
      showToolbarActions,
      styles.padding2,
      styles.search,
      styles.toolbarOnSelection,
      title,
      toolbarActions,
      toolbarButtonAlignment,
    ],
  );
}
