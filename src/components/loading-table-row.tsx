import { useTheme } from '@mui/material';
import TableCell, { TableCellProps } from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import * as React from 'react';
import { useOptions } from '../internal';
import { Skeleton } from '@mui/material';

// eslint-disable-next-line
export type LoadingTableRowProps<DataType extends object = {}> = Required<Pick<TableCellProps, 'colSpan'>>;

export function LoadingTableRow<DataType extends object = {}>({ colSpan }: LoadingTableRowProps<DataType>) {
  const { loadingType, padding } = useOptions<DataType>();
  const theme = useTheme();
  const backgroundColor = theme.palette.background.paper;
  const cellStyle = React.useMemo<React.CSSProperties>(
    () => ({ borderBottomColor: backgroundColor }),
    [backgroundColor],
  );
  return React.useMemo(
    () => (
      <TableRow>
        {new Array(colSpan).fill(0).map((_, cellIndex) => (
          <TableCell
            key={`loading-cell-${cellIndex}`}
            variant="body"
            style={cellStyle}
            size={padding === 'dense' ? 'small' : undefined}
          >
            {loadingType === 'skeleton' ? <Skeleton /> : String.fromCharCode(160)}
          </TableCell>
        ))}
      </TableRow>
    ),
    [colSpan, cellStyle, loadingType, padding],
  );
}
