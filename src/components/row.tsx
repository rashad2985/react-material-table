import { Checkbox, TableRow, Typography } from '@mui/material';
import * as React from 'react';
import { ChangeEvent } from 'react';
import * as ReactTable from 'react-table';
import { useStyles } from '../styles';
import { Column } from '../column';
import {
  Action as ActionButton,
  EditableContext,
  EditRowButton,
  insert,
  isRowAction,
  noop,
  resolveCellAlign,
  useComponents,
  useEvents,
  useOptions,
} from '../internal';
import { DetailPanel } from './detail-panel';
import { DetailPanelButton } from '../internal/components/details-panel';
import { Editable } from '../editable';
import { DeleteRowButton } from '../internal/components/editable/delete-row-button';
import { TableProps } from './table';

export type RowType<RowData extends object = {}> = ReactTable.Row<RowData> & ReactTable.UseRowSelectRowProps<RowData>;

export type RowProps<RowData extends object = {}> = RowType<RowData> &
  Pick<Required<TableProps<RowData>>, 'actions' | 'selectedFlatRows' | 'allColumns'> &
  Pick<TableProps<RowData>, 'detailPanel'> & {
    openedPanels: Array<boolean>;
    togglePanel: (rowId: string, detailsPanelIndex: number) => void;
  };

export function Row<RowData extends object = {}>(props: RowProps<RowData>) {
  const {
    id,
    actions = [],
    detailPanel,
    openedPanels,
    togglePanel,
    selectedFlatRows = [],
    allColumns,
    getToggleRowSelectedProps,
    original: rowData,
    getRowProps,
    isSelected,
    ...rowProps
  } = props;
  const { onChange: toggleRowSelection = noop, ...otherToggleRowSelectedProps } = getToggleRowSelectedProps?.() ?? {};
  const { checked } = otherToggleRowSelectedProps ?? {};
  const styles = useStyles();
  const { onRowClick, onSelectionChange } = useEvents<RowData>();
  const {
    selection,
    selectionProps,
    detailPanelColumnAlignment,
    actionsColumnIndex,
    padding,
    fixedColumns,
    rowStyle: passedRowStyle,
  } = useOptions<RowData>();
  const { left: fixedLeft, right: fixedRight } = fixedColumns || {};
  const { Cell: TableCell } = useComponents<RowData>();
  const { onRowUpdate, onRowDelete, isEditHidden, isDeleteHidden } =
    React.useContext<Editable<RowData>>(EditableContext);
  const showActionsColumn = React.useMemo<boolean>(
    () =>
      actions.filter((action) => isRowAction(typeof action === 'function' ? action({} as RowData) : action)).length >
        0 ||
      !!onRowUpdate ||
      !!onRowDelete,
    [actions, onRowUpdate, onRowDelete],
  );

  const rowStyle = React.useMemo(
    () =>
      !!passedRowStyle ? (typeof passedRowStyle === 'function' ? passedRowStyle(rowData) : passedRowStyle) : undefined,
    [passedRowStyle, rowData],
  );

  const handleSelectionChange = React.useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      toggleRowSelection?.(event);
      onSelectionChange?.(
        !!checked
          ? selectedFlatRows.filter((row) => row.id !== id).map((row) => row.original)
          : [...selectedFlatRows.map((row) => row.original), rowData],
        rowData,
      );
    },
    [id, toggleRowSelection, onSelectionChange, checked, selectedFlatRows, rowData],
  );

  return (
    <TableRow
      {...(getRowProps?.() ?? {})}
      key={`row-${id}`}
      selected={isSelected}
      hover={!!onRowClick}
      style={rowStyle}
    >
      {/* region Details Panel at left */}
      {detailPanel && detailPanelColumnAlignment === 'left' && (
        <TableCell
          padding="none"
          variant="body"
          align="center"
          className={styles.noWrap}
          size={padding === 'dense' ? 'small' : undefined}
        >
          {(
            (Array.isArray(detailPanel) &&
              detailPanel.map<DetailPanel<RowData>>((dp) => (typeof dp === 'function' ? dp(rowData) : dp))) ||
            (typeof detailPanel === 'function' && [detailPanel(rowData)]) ||
            (!Array.isArray(detailPanel) && typeof detailPanel !== 'function' && [detailPanel]) ||
            []
          ).map((detailPanelProps, detailsPanelIndex) => (
            <DetailPanelButton<RowData>
              {...detailPanelProps}
              key={detailsPanelIndex}
              open={!!openedPanels?.[detailsPanelIndex]}
              onClick={() => togglePanel(id, detailsPanelIndex)}
            />
          ))}
        </TableCell>
      )}
      {/* endregion Details Panel at left */}
      {/* region Selection column */}
      {selection && (
        <TableCell key={`row-selection-cell-${id}`} variant="body" align="center" padding="checkbox">
          <Checkbox
            {...((typeof selectionProps === 'function' ? selectionProps(rowData) : selectionProps) ?? {})}
            {...(otherToggleRowSelectedProps || {})}
            onChange={handleSelectionChange}
          />
        </TableCell>
      )}
      {/* endregion Selection column */}
      {/* region Columns */}
      {insert(
        rowProps.cells.map((cell, cellIndex) => (
          <TableCell
            {...cell.getCellProps()}
            variant="body"
            align={resolveCellAlign((cell?.column || {}) as unknown as Column<RowData>)}
            style={
              !!((cell?.column || {}) as unknown as Column<RowData>)?.cellStyle ||
              !!((cell?.column || {}) as unknown as Column<RowData>)?.width
                ? {
                    ...(((cell?.column || {}) as unknown as Column<RowData>)?.cellStyle ?? {}),
                    ...(!!((cell?.column || {}) as unknown as Column<RowData>)?.width
                      ? { width: ((cell?.column || {}) as unknown as Column<RowData>)?.width }
                      : {}),
                  }
                : undefined
            }
            onClick={(event: React.MouseEvent<HTMLTableHeaderCellElement, MouseEvent>) =>
              !((cell?.column || {}) as unknown as Column<RowData>)?.disableClick &&
              onRowClick?.(event, rowData, () => togglePanel(id, 0))
            }
            className={`${!!fixedLeft && cellIndex === 0 ? styles.stickyLeft : ''} ${
              !!fixedRight && cellIndex === rowProps.cells.length - 1 ? styles.stickyRight : ''
            } ${
              !((cell?.column || {}) as unknown as Column<RowData>)?.disableClick && !!onRowClick
                ? styles.clickable
                : ''
            }`}
            size={padding === 'dense' ? 'small' : undefined}
          >
            <Typography component="div" variant="inherit" noWrap>
              {cell.render('Cell')}
            </Typography>
          </TableCell>
        )),
        actionsColumnIndex,
        !!showActionsColumn ? (
          <TableCell
            key={`actions-cell-${actionsColumnIndex}`}
            padding="none"
            variant="body"
            align="center"
            className={styles.noWrap}
          >
            {actions
              .filter((action) => {
                const {
                  position = 'row',
                  hidden = false,
                  isFreeAction = false,
                } = typeof action === 'function' ? action(rowData) : action;
                return position === 'row' && !hidden && !isFreeAction;
              })
              .map((action, actionIndex) => (
                <ActionButton<RowData>
                  key={`row-action-${actionIndex}`}
                  row={rowData}
                  selectedRows={selectedFlatRows.map((selectedRow) => selectedRow.original)}
                  {...(typeof action === 'function' ? action(rowData) : action)}
                />
              ))}
            {!!onRowUpdate && !isEditHidden?.(rowData) && (
              <EditRowButton<RowData>
                key={`edit-${id}`}
                columns={allColumns as unknown as Array<Column<RowData>>}
                rowData={rowData}
              />
            )}
            {!!onRowDelete && !isDeleteHidden?.(rowData) && (
              <DeleteRowButton<RowData> key={`delete-${id}`} rowData={rowData} />
            )}
          </TableCell>
        ) : undefined,
      )}
      {/* endregion Columns */}
      {/* region Details Panel at right */}
      {!!detailPanel && detailPanelColumnAlignment === 'right' && (
        <TableCell
          key={`detail-panel-buttons-right-${id}`}
          padding="none"
          variant="body"
          align="center"
          className={styles.noWrap}
        >
          {(
            (Array.isArray(detailPanel) &&
              detailPanel.map<DetailPanel<RowData>>((dp) => (typeof dp === 'function' ? dp(rowData) : dp))) ||
            (typeof detailPanel === 'function' && [detailPanel(rowData)]) ||
            (!Array.isArray(detailPanel) && typeof detailPanel !== 'function' && [detailPanel]) ||
            []
          ).map((detailsPanelProps, detailsPanelIndex) => (
            <DetailPanelButton<RowData>
              {...detailsPanelProps}
              key={detailsPanelIndex}
              open={!!openedPanels?.[detailsPanelIndex]}
              onClick={() => togglePanel(id, detailsPanelIndex)}
            />
          ))}
        </TableCell>
      )}
      {/* endregion Details Panel at right */}
    </TableRow>
  );
}
