import {
  LabelDisplayedRowsArgs,
  TablePagination as MuiTablePagination,
  TablePaginationProps as MuiTablePaginationProps,
  useMediaQuery,
  useTheme,
} from '@mui/material';
import * as React from 'react';
import { formatLocalization, useEvents, useLocalization, useOptions } from '../internal';
import { TablePaginationActions } from './table-pagination-actions';
import { TableProps } from './table';
import { EmptyTableRowProps } from './empty-table-row';
import { TableSteppedPagination } from './table-stepped-pagination';
import { useStyles } from '../styles';
import { Query } from '../data';
import { onQueryChangeEvent } from '../events';

type PaginationInfo = Pick<LabelDisplayedRowsArgs, 'from' | 'to' | 'count' | 'page'>;

export type TablePaginationProps<RowData extends object = {}> = Omit<
  MuiTablePaginationProps<'div'>,
  'onPageChange' | 'onRowsPerPageChange' | 'colSpan'
> &
  Pick<TableProps, 'gotoPage' | 'setPageSize'> &
  Omit<EmptyTableRowProps<RowData>, 'colSpan' | 'index' | 'page'>;

export function TablePagination<RowData extends object = {}>(props: TablePaginationProps<RowData>) {
  const { count, page = 0, rowsPerPage, gotoPage, setPageSize, ...remainingTablePaginationProps } = props;
  const styles = useStyles();
  const self = React.useRef<HTMLDivElement | null>(null);
  const {
    pagination: { labelDisplayedRows, labelRowsPerPage },
  } = useLocalization();
  const { paginationType, responsiveBreakPoint } = useOptions<RowData>();
  const { onPageChange, onRowsPerPageChange } = useEvents<RowData>();

  const formatPaginationInformation = React.useCallback(
    (paginationInfo: PaginationInfo) => {
      const { from, to, count: pageCount, page: currentPage } = paginationInfo;
      const replacements: Record<string, string> = {
        from: String(from),
        to: String(to),
        count: String(pageCount),
        page: String(currentPage + 1),
      };
      return formatLocalization(labelDisplayedRows, replacements);
    },
    [labelDisplayedRows],
  );

  const handleChangePage = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
      gotoPage(newPage);
      onPageChange?.(newPage, rowsPerPage);
      self.current?.dispatchEvent?.(
        new CustomEvent<Partial<Query<RowData>>>(onQueryChangeEvent, { bubbles: true, detail: { page: newPage } }),
      );
    },
    [gotoPage, onPageChange, rowsPerPage],
  );
  const handleChangeRowsPerPage = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const pageSize = parseInt(event.target.value, 10);
      setPageSize(pageSize);
      onRowsPerPageChange?.(pageSize);
      self.current?.dispatchEvent?.(
        new CustomEvent<Partial<Query<RowData>>>(onQueryChangeEvent, { bubbles: true, detail: { pageSize } }),
      );
    },
    [setPageSize, onRowsPerPageChange],
  );

  const theme = useTheme();
  const isResponsive = useMediaQuery(theme.breakpoints.down(responsiveBreakPoint));
  const classes = React.useMemo<MuiTablePaginationProps['classes']>(
    () =>
      isResponsive
        ? {
            root: styles.tablePaginationRoot,
            toolbar: styles.tablePaginationToolbar,
            spacer: styles.flex,
            caption: styles.flex,
          }
        : {},
    [isResponsive, styles.tablePaginationRoot, styles.tablePaginationToolbar, styles.flex],
  );

  return (
    <MuiTablePagination
      {...remainingTablePaginationProps}
      ref={self}
      component="div"
      count={count}
      page={page}
      onPageChange={handleChangePage}
      rowsPerPage={rowsPerPage}
      onRowsPerPageChange={handleChangeRowsPerPage}
      labelRowsPerPage={labelRowsPerPage}
      labelDisplayedRows={formatPaginationInformation}
      ActionsComponent={paginationType === 'stepped' ? TableSteppedPagination : TablePaginationActions}
      classes={classes}
    />
  );
}
