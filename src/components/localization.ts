import { InternalLocalization } from '../internal';

type Nullable<T> = T extends object ? { [Key in keyof T]?: Nullable<T[Key]> } : { [Key in keyof T]?: T[Key] };

export type Localization = {
  [Key in keyof InternalLocalization]?: Nullable<InternalLocalization[Key]>;
};
