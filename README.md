<h1 align="center">React Material Table</h1>

<p align="center">
  <a aria-label="Build" href="#">
    <img alt="Gitlab pipeline status" src="https://img.shields.io/gitlab/pipeline-status/rashad2985/react-material-table?branch=master&style=for-the-badge">
  </a>
  <a aria-label="Size" href="https://www.npmjs.com/package/@trautmann/react-material-table">
    <img alt="npm bundle size (scoped version)" src="https://img.shields.io/bundlephobia/min/@trautmann/react-material-table/latest?style=for-the-badge">
  </a>
  <a aria-label="Size" href="https://www.npmjs.com/package/@trautmann/react-material-table">
    <img alt="npm" src="https://img.shields.io/npm/dm/@trautmann/react-material-table?style=for-the-badge">
  </a>
  <a aria-label="License" href="https://gitlab.com/rashad2985/react-material-table/-/raw/master/LICENSE">
    <img alt="NPM" src="https://img.shields.io/npm/l/@trautmann/react-material-table?style=for-the-badge">
  </a>
  <a aria-label="License" href="https://opencollective.com/react-material-table">
    <img alt="Open Collective backers and sponsors" src="https://img.shields.io/opencollective/all/react-material-table?style=for-the-badge">
  </a>
</p>

## Introduction
`@trautmann/react-material-table` is a simple and powerful Datatable for React based on Material-UI Table inspired from `material-table` with some additional features.
* Filtering that follows Google Material Design guidelines and also offer a variety of built-in filters.
  ![](https://gitlab.com/rashad2985/react-material-table/-/raw/master/src/stories/images/example-filter.png)
* Responsive Forms for add/edit/delete operations.
  ![](https://gitlab.com/rashad2985/react-material-table/-/raw/master/src/stories/images/example-editing.png)
* Well, not Google Material Design guideline, but a long waited feature - responsive rows that transform table rows into boxes containing header/cell value pair for each column as rows in a box. Explore it in "Responsive Design Row Transformation" part of the "Features" documentation.
  - Desktop\
    ![](https://gitlab.com/rashad2985/react-material-table/-/raw/master/src/stories/images/example-responsive-1.png)
  - Mobile\
    ![](https://gitlab.com/rashad2985/react-material-table/-/raw/master/src/stories/images/example-responsive-2.png)
* and more cool features.

## Documentation and Demo
Storybook: https://react-material-table.trautmann.software

## Support
You can support `@trautmann/react-material-table` via:

| [<img src="https://cdn.buymeacoffee.com/buttons/v2/default-blue.png" alt="Buy Me A Coffee" style="objectFit: contain; width: 217px;"/>](https://www.buymeacoffee.com/rashad2985) | [<img src="https://gitlab.com/rashad2985/react-material-table/-/raw/master/src/stories/images/bmc_qr.png" alt="Buy Me A Coffee" style="objectFit: contain; width: 217px;"/>](https://www.buymeacoffee.com/rashad2985) |
|-----|-----|
| [Stripe](https://donate.stripe.com/bIY29C7So1ve5nqfYY) | [<img src="https://gitlab.com/rashad2985/react-material-table/-/raw/master/src/stories/images/stripe_qr.png" alt="Buy Me A Coffee" style="objectFit: contain; width: 217px;"/>](https://donate.stripe.com/bIY29C7So1ve5nqfYY) |

## Installation
Before starting with coding, please consider that this package has peer dependencies depending on what you are going to use.
### You have to install following dependencies:
1. `"@mui/material": "^5.11.2"`
2. `"@mui/styles": "^5.11.2"`
3. `"@mui/icons-material": "^5.11.0"`
4. `"@mui/x-date-pickers": "^5.0.12"`
5. `"react": "^17.0.2"`
6. `"react-beautiful-dnd": "^13.1.1"`
7. `"react-dom": "^17.0.2"`
8. `"react-table": "^7.8.0"`

### If you are going to use export feature to generate PDF/CSV files from tables:
1. `"filefy": "^0.1.11"`
2. `"jspdf": "^2.5.1"`
3. `"jspdf-autotable": "^3.5.28"`

### Complete installation:
#### npm
```
npm i @trautmann/react-material-table @mui/material @mui/styles @mui/icons-material @mui/x-date-pickers react react-beautiful-dnd react-dom react-table filefy jspdf jspdf-autotable
```
#### yarn
```
yarn add @trautmann/react-material-table @mui/material @mui/styles @mui/icons-material @mui/x-date-pickers react react-beautiful-dnd react-dom react-table filefy jspdf jspdf-autotable
```

## Usage
Please have a look into the storybook for more detailed usage examples and documentation.
```
import ReactDOM from "react-dom";
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { ReactMaterialTable } from "@trautmann/react-material-table";

const rootElement = document.getElementById("root");
ReactDOM.render(
  // We have date/time/date-time type columns, so MUI Picker Utils Provider is needed.
  <LocalizationProvider dateAdapter={AdapterDateFns}>
    <ReactMaterialTable
      columns={[
        { title: "Dessert (100g serving)", field: "name", type: "string" },
        { title: "Calories", field: "calories", type: "numeric", numberSettings: { locale: "en-US" } },
        { title: "Fat (g)", field: "fat", type: "numeric", numberSettings: { locale: "en-US" } },
        { title: "Carbs (g)", field: "carbs", type: "numeric", numberSettings: { locale: "en-US" } },
        { title: "Protein (g)", field: "protein", type: "numeric", numberSettings: { locale: "en-US" } },
        { title: "Date", field: "date", type: "date", dateSetting: { locale: "en-US" } },
        { title: "Time", field: "time", type: "time", dateSetting: { locale: "en-US" } },
        { title: "Date & Time", field: "datetime", type: "datetime", dateSetting: { locale: "en-US" } },
        { title: "Recommended", field: "recommended", type: "boolean" },
        { title: "Price", field: "price", type: "currency", currencySetting: { locale: "en-US", formatOptions: { currency: "USD" } } }
      ]}
      data={[
        {
          name: "Frozen yoghurt",
          calories: 159,
          fat: 6.0,
          carbs: 24,
          protein: 4.0,
          date: new Date(2021, 3, 5, 15, 15, 5, 200),
          time: new Date(2021, 3, 5, 15, 15, 5, 200),
          datetime: new Date(2021, 3, 5, 15, 15, 5, 200),
          recommended: true,
          price: 2.99
        },
        {
          name: "Ice cream sandwich",
          calories: 237,
          fat: 9.0,
          carbs: 37,
          protein: 4.3,
          date: new Date(2021, 3, 1, 0, 15, 15, 100),
          time: new Date(2021, 3, 1, 0, 15, 15, 100),
          datetime: new Date(2021, 3, 1, 0, 15, 15, 100),
          recommended: true,
          price: 5.99
        }
      ]}
      options={{}}
    />
  </LocalizationProvider>,
  rootElement
);
```
