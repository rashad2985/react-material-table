import { createTheme, ThemeProvider } from '@mui/material/styles';
import { ThemeProvider as Emotion10ThemeProvider } from 'emotion-theming';
import { CssBaseline } from "@mui/material";
import * as React from "react";

let defaultMode = 'light';

const withThemeProvider = (Story, context) => {
  const defaultTheme = createTheme({ palette: { mode: 'light' } }); // or your custom theme
  return (
    <Emotion10ThemeProvider theme={defaultTheme}>
      <ThemeProvider theme={defaultTheme}>
        <CssBaseline />
        <Story {...context} />
      </ThemeProvider>
    </Emotion10ThemeProvider>
  );
};

export const decorators = [withThemeProvider];

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: { expanded: true },
  backgrounds: {
    default: defaultMode,
    values: [
      {
        name: 'light',
        value: '#ffffff',
      },
      {
        name: 'dark',
        value: '#303030',
      },
    ],
  },
};
